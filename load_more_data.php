<?php
/**
 * custom search template
 */
require dirname(__FILE__) . '/wp-load.php';
global $wpdb;
global $load_more_data;
$start = $_POST['start'];
$limit = $_POST['limit'];
/*print_r($_POST);
exit;*/
//LIMIT POSTS
// set_time_limit(0);
// ini_set('xdebug.var_display_max_depth', -1);
// ini_set('xdebug.var_display_max_children', -1);
// ini_set('xdebug.var_display_max_data', -1);
// ini_set('memory_limit', '512M');

$search_string='';
$sort_term='weight';
$post_types=array();
$scholar_sub_types=array();
$wor_sub_types=array();
$grants_sub_types=array();
$syllabi_sub_types=array();

$load_more_data=true;

process_single_results_request();

function process_single_results_request(){
    global $search_string, $scholar_sub_types, $wor_sub_types, $post_types, $sort_term, $grants_sub_types, $syllabi_sub_types;
    if(!empty($_POST['st']))
    {
        $search_string = $_POST['st'];
    }
    else {
        $uri = $_SERVER['REQUEST_URI'];
        list($path, $qs) = explode("?", $_SERVER["REQUEST_URI"], 2);
        parse_str($qs, $urlvars);
        $search_string = $urlvars['st'];
    }
    // get post types active for search
    if(!empty($_GET['pt'])){
        $post_types[$_GET['pt']] = $_GET['pt'];
    }
    else
    {
        $post_types[$_POST['pt']] = $_POST['pt'];
    }

    //SCHOLARSHIP SUBTYPES
    if ((!empty($_GET['st-web']) && $_GET['st-web'] == 'on' ) || ( !empty($_POST['st-web']) && $_POST['st-web'] == 'on')) {
        $scholar_sub_types['st-web'] = 'Web';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-journal']) && $_GET['st-journal'] == 'on' ) || (!empty($_POST['st-journal']) && $_POST['st-journal'] == 'on')) {
        $scholar_sub_types['st-journal'] = 'Journal Issue';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-book']) && $_GET['st-book'] == 'on')  || (!empty($_POST['st-book']) && $_POST['st-book'] == 'on')) {
        $scholar_sub_types['st-book'] = 'Book';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-article']) && $_GET['st-article'] == 'on' ) || (!empty($_POST['st-article']) &&  $_POST['st-article'] == 'on')) {
        $scholar_sub_types['st-article'] = 'Article';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-ttr']) && $_GET['st-ttr'] == 'on') || (!empty($_POST['st-ttr']) && $_POST['st-ttr'] == 'on')) {
        $scholar_sub_types['st-ttr'] = 'TTR';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-tactic']) && $_GET['st-tactic'] == 'on') || (!empty($_POST['st-tactic']) && $_POST['st-tactic'] == 'on')){
        $scholar_sub_types['st-tactic'] = 'Tactic';
        $post_types['scholarship'] = true;
    }
    //WEBSITE SUBTYPES
    if ((!empty($_GET['wt-e-text']) && $_GET['wt-e-text'] == 'on' ) || (!empty($_POST['wt-e-text']) && $_POST['wt-e-text'] == 'on')) {
        $wor_sub_types['wt-e-text'] = 'e-text';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-e-journal']) && $_GET['wt-e-journal'] == 'on') || (!empty($_POST['wt-e-journal']) && $_POST['wt-e-journal'] == 'on')) {
        $wor_sub_types['wt-e-journal'] = 'e-journal';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-image']) && $_GET['wt-image'] == 'on') || (!empty($_POST['wt-image']) && $_POST['wt-image'] == 'on' )) {
        $wor_sub_types['wt-image'] = 'image';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-biblio']) && $_GET['wt-biblio'] == 'on') || (!empty($_POST['wt-biblio']) && $_POST['wt-biblio'] == 'on')) {
        $wor_sub_types['wt-biblio'] = 'biblio';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-web']) && !empty($_GET['wt-web'] == 'on')) || (!empty($_POST['wt-web']) && !empty($_POST['wt-web'] == 'on'))) {
        $wor_sub_types['wt-web'] = 'web';
        $post_types['website_on_religion'] = true;
    }
	// GRANTS SUBTYPES
	if ((!empty($_GET['gs-college-uni']) && $_GET['gs-college-uni'] == 'on' ) || (!empty($_POST['gs-college-uni']) && $_POST['gs-college-uni'] == 'on')) {
		$grants_sub_types['gs-college-uni'] = 'Colleges/Universities';
	}
	if ((!empty($_GET['gs-theo-school']) && $_GET['gs-theo-school'] == 'on' ) || (!empty($_POST['gs-theo-school']) && $_POST['gs-theo-school'] == 'on')) {
		$grants_sub_types['gs-theo-school'] = 'Theological Schools';
	}
	if ((!empty($_GET['gs-agencies']) && $_GET['gs-agencies'] == 'on' ) || (!empty($_POST['gs-agencies']) && $_POST['gs-agencies'] == 'on')) {
		$grants_sub_types['gs-agencies'] = 'Agencies';
	}
	// SYLLABI SUBTYPES
	if ((!empty($_GET['sy-undergrad']) && $_GET['sy-undergrad'] == 'on' ) || (!empty($_POST['sy-undergrad']) && $_POST['sy-undergrad'] == 'on')) {
		$syllabi_sub_types['sy-undergrad'] = '%Undergraduate%';
	}
	if ((!empty($_GET['sy-grad']) && $_GET['sy-grad'] == 'on' ) || (!empty($_POST['sy-grad']) && $_POST['sy-grad'] == 'on')) {
		$syllabi_sub_types['sy-grad'] = '%Graduate%';
	}
	if ((!empty($_GET['sy-intro-course']) && $_GET['sy-intro-course'] == 'on' ) || (!empty($_POST['sy-intro-course']) && $_POST['sy-intro-course'] == 'on')) {
		$syllabi_sub_types['sy-intro-course'] = '%Introductory%';
	}
	if ((!empty($_GET['sy-online']) && $_GET['sy-online'] == 'on' ) || (!empty($_POST['sy-online']) && $_POST['sy-online'] == 'on')) {
		$syllabi_sub_types['sy-online'] = '%Online Only%';
	}
	if ((!empty($_GET['sy-online-f2f']) && $_GET['sy-online-f2f'] == 'on' ) || (!empty($_POST['sy-online-f2f']) && $_POST['sy-online-f2f'] == 'on')) {
		$syllabi_sub_types['sy-online-f2f'] = '%Online and F2F%';
	}

    // get sort term
    if (!empty($_POST['sort_term'])) {
        $sort_term = $_POST['sort_term'];
    }
    if (!empty($_GET['sort_term'])) {
        $sort_term = $_GET['sort_term'];
    }


    if(empty($post_types)){
        $post_types['page'] = 'Wabash Programs';
        $post_types['ttr'] = 'Teaching Theology and Religion (Wabash Journal)';
        $post_types['grants'] = 'Awarded Grants';
        $post_types['post'] = 'Blog';
        $post_types['scholarship'] = 'Scholarchip on Teaching';
        $post_types['website_on_religion'] = 'Website on Religion';
        $post_types['syllabi'] = 'Syllabi';
        $post_types['book_reviews'] = 'Book Reviews';
        $post_types['video'] = 'Video';
    }

    // var_dump($search_string);
    // var_dump($post_types);
    // var_dump($scholar_sub_types);
    // var_dump($wor_sub_types);
    // var_dump($sort_term);

}
$data = array();
    $html='';
    switch(key($post_types)){
        case 'page':

            $programs=search_programs($search_string,$limit,$start);

            $data['count'] = COUNT($programs);
            foreach($programs as $program){
                $html.= display_tile_program_object($program);
            }
            break;
        case 'grants':
            $grants=search_grant($search_string,$grants_sub_types,$sort_term,$limit,$start);

            $data['count'] = COUNT($grants);
            foreach($grants as $grant){
                $html.= display_tile_grant_object($grant);
            }
            break;
        case 'post':

            $blogs=search_blogs($search_string,'weight',$limit,$start);

            $data['count'] = COUNT($blogs);
            foreach($blogs as $blog){
                $html.= display_tile_blog_object($blog);
            }
            break;
        case 'scholarship':

            $scholarships=search_scholarship($search_string, $scholar_sub_types, 'scholarship', $sort_term,$limit,$start);

            $data['count'] = COUNT($scholarships);
            foreach($scholarships as $scholar){
                $html.= display_tile_scholarship_object($scholar);
            }
            break;
        case 'ttr':

            if (empty($scholar_sub_types)) {
                $scholar_sub_types = array( 'TTR', 'Tactic');
            }else{
                unset($scholar_sub_types['Article']);
                unset($scholar_sub_types['Book']);
                unset($scholar_sub_types['Journal Issue']);
                unset($scholar_sub_types['Web']);
            }
            $ttrs=search_scholarship($search_string, $scholar_sub_types, 'ttr', $sort_term,$limit,$start);
            $data['count'] = COUNT($ttrs);

            foreach($ttrs as $scholar){
                $html.= display_tile_scholarship_object($scholar);
            }
            break;
        case 'website_on_religion':

            $wors=search_website_on_religion($search_string, $wor_sub_types, $sort_term,$limit,$start);

            $data['count'] = COUNT($wors);
            foreach($wors as $wor){
                $html.= display_tile_wor_object($wor);
            }
            break;
        case 'syllabi':

            $syllabi=search_syllabi($search_string, $syllabi_sub_types, $sort_term,$limit,$start);

            $data['count'] = COUNT($syllabi);
            foreach($syllabi as $syllabus){
                $html.= display_tile_syllabi_object($syllabus);
            }
            break;
        case 'book_reviews':
            $book_filter = '';
            $book_reviewed_filter='';
            if (isset($_POST['reviewed'])) {
                $book_reviewed_filter = 1;
            }
            /*If filter by book only*/
            if (isset($_POST['book'])) {
                $book_filter = 1;
            }
            $book_reviews=search_book_reviews($search_string, $sort_term,$limit,$start, $book_reviewed_filter, $book_filter);

            $data['count'] = COUNT($book_reviews);
            foreach($book_reviews as $book){
                $html.= display_tile_book_reviews_object($book);
            }

            break;
        case 'video':

            $videos=search_videos($search_string,$limit,$start);

            foreach($videos as $video){
                $html.= display_tile_video_object($video);
            }
    }
if($html!='')
{
    $data['html'] = $html;
}
else
{
    $data['count']=0;
}
echo json_encode($data);
