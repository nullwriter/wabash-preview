<html>

<head>
<title>AAR Syllabi Project: The Sermon on the Mount (Carlson)</title>
<meta name="GENERATOR" content="Microsoft FrontPage 3.0">
</head>

<body>
<font SIZE="7">

<p ALIGN="CENTER">The Sermon on the Mount</font></p>

<p ALIGN="CENTER"><strong>Focal Point Seminar, IDS 101, Section 208</strong></p>

<p ALIGN="CENTER"><strong>Winter, 1998</strong></p>

<p ALIGN="CENTER"><strong>DePaul University</strong></p>

<p>Instructor: Dr. Jeffrey Carlson, Associate Professor and Chairperson, Department of
Religious Studies</p>

<p>Office: SAC 430 (773-325-7386)</p>

<p>Hours: Tuesdays and Thursdays, 1:00-2:00, and by appointment</p>

<p>E-mail: <a HREF="mailto:jcarlson@condor.depaul.edu">jcarlson@condor.depaul.edu</a></p>

<p>&nbsp;</p>
<b>

<p>A. INTRODUCTION AND OBJECTIVES</b></p>

<p>Welcome to one of the many &quot;Focal Point Seminars&quot; offered in DePaul&#146;s
Liberal Studies (general education) program. These seminars concentrate on a particular
person, place, text, object or event significant to our world because of its impact on how
we live, think, or determine our values. A Focal Point Seminar approaches its topic with
the idea that understanding is deepened through the discovery, engagement and synthesis of
multiple perspectives. The course introduces students to the nature of university-level
inquiry by &quot;problematizing&quot; initial interpretations of the topic, so that our
present assumptions might be developed, extended and, in some ways, challenged or perhaps
reinforced by our careful consideration of diverse possibilities. Thus understood, the
Focal Point Seminars are truly &quot;practice sessions for the life of the mind.&quot; As
educated persons we always strive to see other persons, places, texts, objects and events
in increasingly deeper and increasingly less superficial ways. Through reading, writing
and discussion, we will try to do that in this course.</p>

<p>Our course explores the Sermon on the Mount. We will investigate this classic text in
terms of its roots in Judaism and the Greco-Roman world, its interpretations in the
Christian tradition, in other religions, and in philosophy, the arts and literature. This
text, like other &quot;classics,&quot; has had a profound and kaleidoscopically diverse
history of reception and influence. The text does not &quot;belong&quot; to Christianity.
The Sermon on the Mount (SM) illustrates the phenomenon of a &quot;deep symbol,&quot; a
&quot;classic&quot; (vs. a mere period piece) with a surplus and excess of meaning,
evoking an inexhaustible multiplicity of readings, responses and effects.</p>

<p>Our specific learning goals are as follows:

<ol>
  <li>to gain <b>accurate knowledge</b> about the important themes, figures, texts and other
    materials we consider &#150; including interpretations of the SM from modern biblical
    scholarship, from the Christian theological tradition, from philosophical and literary
    perspectives, from non-Christian religions, and with a focus on such issues as war and
    peace, and interpretations of the Lord&#146;s Prayer &#150; so that you are able to
    present and support significant facts correctly, clearly and thoroughly;</li>
  <li>to develop <b>accurate</b> <b>analyses </b>of the positions considered, explaining how
    complex arguments and interpretations are constructed;</li>
  <li>to propose <b>plausible comparisons </b>between the different perspectives considered,
    suggesting viable connections, applications and patterns;</li>
  <li>to construct your own <b>articulate and respectful assessment </b>of the substance of
    this course, and of the viewpoints of others, developing and supporting your own reasoned
    evaluations and creative<b> </b>responses, based on clearly formulated criteria;</li>
  <li>to develop your capacity for clear and effective <b>writing</b>;</li>
  <li>to develop your capacity for clear and effective <b>verbal</b> communication.</li>
</ol>

<p>&nbsp;</p>
<b>

<p>B. STUDENT REQUIREMENTS</b>

<ol>
  <b>
  <li>Assignment preparation</b>. Come to each class having done the readings, taken careful
    notes, and having tried your best to find out the meanings of terms and concepts in the
    readings that are unfamiliar to you. At the start of every session, I will ask for a show
    of hands from those who have completed the readings.</li>
  <li><b>Class discussions</b>. Asking questions, raising concerns, offering your own ideas
    during class discussions is a crucial component of the learning process. You will be
    expected to be an active participant in classroom conversations. Furthermore, in addition
    to discussions during scheduled classroom times, we will be setting up an electronic
    discussion group for the members of this course in order to facilitate further
    conversation outside of class. While your participation in this electronic forum will be
    encouraged, it is not required and if you choose not to participate it will not affect
    your grade adversely. </li>
  <b>
  <li>Logbook</b>. You will be required to keep a &quot;logbook&quot; throughout the autumn
    quarter. According to Webster&#146;s Dictionary, the standard definition of a
    &quot;logbook&quot; is &quot;a daily record of a ship&#146;s speed or progress or the full
    record of a ship&#146;s voyage including notes on the ship&#146;s position at various
    times and including notes on the weather and on any important incidents occurring during
    the voyage.&quot; Think of the ship as you, of this course as the voyage, then set sail
    with the metaphor. Your logbook is the place for you to think critically and creatively,
    ask hard questions, and explore in writing your understanding of and response to the major
    themes and issues considered in our course. </li>
</ol>

<blockquote>
  <p>While the <b>papers</b> (see item 4 below) will be the primary public <i>products</i>
  of your work, they will be direct outcomes of an ongoing writing <i>process</i> that
  begins and evolves in your logbook. Discussions, films and readings will influence what
  you write there. And the relationship works both ways, as your participation in
  discussions will be shaped by previous logbook entries. Often I will guide you in the
  writing, asking you to make particular sorts of entries during class time as well as
  outside of class.</p>
  <p>Your logbook should be clearly divided into <i>two separate sections</i>, designated as
  <b>RAW NOTES </b>and <b>REFLECTIVE ENTRIES</b>. In the Raw Notes section, you will record
  your <i>initial</i> summaries of the contents of the readings and class sessions, listing
  and defining key terms and concepts. In this section, you will also jot down your <i>initial</i>
  questions, observations, criticisms, applications, and other forms of general and specific
  reaction to the reading or class session. This section of the logbook is the place to
  &quot;get it all down on paper&quot; so you won&#146;t forget important facts or insights
  you might need later.</p>
  <p>The Reflective Entries section is where you pause, and step back from the immediate and
  initial experience of taking notes on a reading or ongoing classroom session. Here, you
  gather your thoughts, sort through the facts and initial impressions recorded earlier, and
  engage in more focused and sustained thinking &#150; in writing. This section will consist
  of individual, dated entries, in the form of full sentences and paragraphs. </p>
  <p>I will ask you to make many short Reflective Entries during class time. Entries made in
  class will be of varying lengths. In addition, you will be required to make at least <b>two
  Reflective Entries outside of class per week. Each entry made outside of class must be at
  least 1� pages in length.</b> All Reflective Entries must be dated and written on
  standard 8� x 11 college ruled paper (or typed if you prefer). Be sure to indicate
  clearly in your logbook whether Reflective Entries are made &quot;outside of class&quot;
  or &quot;in class.&quot; <b>Bring the complete logbook (both sections) with you to every
  class session. I reserve the right to collect logbooks at any time during the quarter.</p>
  </b><p>4. <b>Papers</b>. You will be asked to write three papers, each approximately<b> 6
  pages, typed and double-spaced</b>. These papers will allow you to demonstrate competence
  in the first 5 of the 6 learning goals listed in part A of this syllabus and, as already
  noted, the papers will allow you to deepen and extend work begun in your logbook. Specific
  assignments and guidelines will be given in separate handouts.</p>
  <blockquote>
    <b><p>Paper 1 assigned: January 20 Due in class: January 27</p>
    <p>Paper 2 assigned: February 12 Due in class: February 19</p>
    <p>Paper 3 assigned: March 12 Due in my office: By noon, March 20</p>
    </b>
  </blockquote>
</blockquote>

<p>&nbsp;<b></p>

<p>C. FACULTY/STUDENT COMMITMENTS</b></p>

<p>I promise to use whatever talent, energy and knowledge I have to help awaken your
intellectual curiosity; to encourage and respect you; and to help you to extend your
capacities for careful and insightful thinking, reading, writing and speaking. I promise
to meet with you whenever necessary, and to respond to your work as honestly and promptly
as possible.</p>

<p>If you remain registered for this course I will take that as your promise to strive to
be open to new ways of seeing the world; to respect the learning process by your timely
attendance and careful preparation of readings, using your logbook as a principal and
continuous means of discovering for yourself ways in which you can vigorously participate
in and stimulate discussion; and to respect the experiences, beliefs and informed opinions
of others in the class even as you challenge us to think more carefully and insightfully.</p>
<b>

<p>&nbsp;</b></p>

<p><b>D. GRADING</b>

<ul>
  <li>20% of your final grade will be based on an assessment of your overall <b>course
    preparedness and involvement</b>, including regular and prompt attendance and informed
    participation in class discussions. <i>Students arriving late for class, missing class or
    not indicating that they have completed the assigned readings will receive a lower grade
    in this area.</i> </li>
</ul>

<ul>
  <li>20% of your grade will be based on an overall assessment of your <b>logbook</b>, taking
    into account the number of entries made<i> </i>and the degree of intellectual engagement
    shown.</li>
</ul>

<ul>
  <li>Each of the three <b>papers</b> will be worth 20% of the final grade.</li>
</ul>

<p>&nbsp;</p>
<b>

<p>E. ACADEMIC INTEGRITY</b>

<ol START="5" TYPE="A">
  <p>Plagiarism, like other forms of academic dishonesty, is always a serious matter. This
  course adheres to the university&#146;s policies on plagiarism as stated in the current <i>Student
  Handbook</i>. Consult the manual, &quot;Writing in the Liberal Studies,&quot; for
  instructions about proper citation or acknowledgment of the work of others in class
  assignments.</p>
  <p>&nbsp;</p>
</ol>
<b>

<p>F. REQUIRED TEXTS</b>

<ol>
  <li>Warren Carter, <i>What are they Saying about Matthew&#146;s Sermon on the Mount?</i> New
    York: Paulist Press, 1994. This text is available from the LPC Bookstore and is also on
    reserve in the Richardson Library.</li>
  <i>
  <li>The Sermon on the Mount Course Reader</i>. This bound set of photocopied readings can be
    purchased for $13.50 from the Department of Religious Studies Secretary in SAC 432, Monday
    through Thursday from 8:00 AM until 6:00 PM, or Friday from 8:00 AM until 4:00 PM. One
    copy of the <i>Reader</i> is also on reserve in the Richardson Library. However, it is
    strongly recommended that you purchase both Carter&#146;s text and the <i>Reader</i>, and
    bring them with you to class on the days they are being discussed.</li>
</ol>
<i>

<p>&nbsp;</p>
<b>

<p></b></i><strong>G. SCHEDULE OF TOPICS, READINGS AND DUE DATES</strong><i></p>
</i><b>

<p ALIGN="CENTER">1. Interpretation Theory and the Notion of a Focal Point Seminar</p>

<p>Tuesday, January 6</b></p>

<p>Syllabus and excerpts from films depicting the SM.</p>
<b>

<p ALIGN="CENTER">&nbsp;</p>

<p ALIGN="CENTER">2. Interpretations from Modern Biblical Scholarship</p>

<p>Thursday, January 8</b></p>

<p>READINGS: <u><i>Reader</i> selection #1</u>,&quot;The Sermon on the Mount,&quot; in <i>Gospel
Parallels: A Comparison of the Synoptic Gospels With Alternative Readings from the
Manuscripts and Noncanonical Parallels, </i>pp. 24-38; &amp; <u>Carter, pp. 1-34</u>,
&quot;Introduction,&quot; &amp; &quot;The Sources of the SM.&quot;</p>

<p>&nbsp;<b></p>

<p>Tuesday, January 13</b></p>

<p>READING: <u>Carter, pp. 35-77</u>, &quot;The Structure of the SM&quot; &amp; &quot;The
Function and Socio-Historical Setting of Matthew&#146;s SM.&quot;</p>

<p>&nbsp;<b></p>

<p>Thursday, January 15</b></p>

<p>READING: <u>Carter, pp. 78-129</u>, &quot;The Content of the SM: Part I&quot; &amp;
&quot;The Content of the SM: Part II.&quot;</p>

<p>&nbsp;<b></p>

<p ALIGN="CENTER">3. Interpretations from the Christian Theological Tradition</b></p>

<p>&nbsp;<b></p>

<p>Tuesday, January 20</b></p>

<p>READING: <u><i>Reader</i> selection #2</u>, St. Augustine, <i>Commentary on the
Lord&#146;s Sermon on the Mount, with 17 Related Sermons</i>, pp. 80-108. <b><u>PAPER 1
ASSIGNED</u>.</p>

<p>Thursday, January 22</b></p>

<p>READING: <u><i>Reader</i> selection #3</u>, Martin Luther, <i>The Sermon on the Mount
(Sermons) and The Magnificat</i>, pp. 105-129.</p>

<p>&nbsp;<b></p>

<p>Tuesday, January 27</p>

<p>&nbsp;<u></p>

<p>PAPER 1 DUE</u>. </b>Film: <i>Mother Teresa</i>.</p>

<p>&nbsp;<b></p>

<p ALIGN="CENTER">4. Philosophical Interpretations</p>

<p>Thursday, January 29</b></p>

<p>READING: <u><i>Reader</i> selection #4</u>, Karl Marx, <i>On Religion</i>, pp. 41-42,
69-89.</p>

<p>&nbsp;<b></p>

<p>Tuesday, February 3</b></p>

<p>READING: <u><i>Reader</i> selection #5</u>, Friedrich Nietzsche, <i>On the Genealogy of
Morality</i>, pp. 11-37.</p>

<p>&nbsp;<b></p>

<p ALIGN="CENTER">5. A Literary Interpretation</p>

<p>Thursday, February 5</b></p>

<p>READING: <u><i>Reader</i> selection #6</u>, Annie Dillard, &quot;An Expedition to the
Pole,&quot; in <i>The Annie Dillard Reader</i>, pp. 21-47.</p>
<b>

<p ALIGN="CENTER">&nbsp;</p>

<p ALIGN="CENTER">6. Contemporary Interpretations Related to War and Peace</p>

<p>Tuesday, February 10</b></p>

<p>READING: <u><i>Reader</i> selection #7</u>, Stanley Hauerwas, &quot;A Sermon on the
Sermon on the Mount&quot; &amp; &quot;Lust for Peace,&quot; in <i>Unleashing the
Scripture: Freeing the Bible from Captivity to America</i>, pp. 63-72, 126-133.</p>

<p>&nbsp;<b></p>

<p>Thursday, February 12</b></p>

<p>READING: <u><i>Reader</i> selection #8</u>, Daniel Berrigan, <i>The Trial of the
Catonsville Nine</i>, pp. vii-xi, 81-123. <b><u>PAPER 2 ASSIGNED</u>.</p>

<p>Tuesday, February 17</b></p>

<p>READING: <u><i>Reader</i> selection #9</u>, <i>The Challenge of Peace: God&#146;s
Promise and Our Response</i>. <i>A Pastoral Letter on War and Peace</i>, pp. 9-37.</p>
<b>

<p align="center">7. Interpretations of the Lord&#146;s Prayer</p>
</b>

<ol START="7">
  <b><p>Thursday, February 19</p>
  <u><p>PAPER 2 DUE</u>. </b>The Lord&#146;s Prayer in music and liturgy.</p>
  <b><p>Tuesday, February 24</p>
  </b><p>READING: <u><i>Reader</i>, selection #10</u>, Bishop Dmitri, <i>The Kingdom of God:
  The Sermon on the Mount</i>, pp. 99-128.</p>
  <b><p>Thursday, February 26</p>
  </b><p>READING: <u><i>Reader</i>, selection #11</u>, Leonardo Boff, &quot;And Do Not Bring
  Us to the Test,&quot; &quot;Save us from the Evil One&quot; &amp; &quot;Amen,&quot; in <i>The
  Lord&#146;s Prayer: The Prayer of Integral Liberation</i>, pp. 97-122.</p>
</ol>
<b>

<p align="center">8. Non-Christian Religious Interpretations and Christian Responses</b></p>

<p>&nbsp;<b></p>

<p>Tuesday, March 3</b></p>

<p>READING: <u><i>Reader</i>, selection #12</u>, Mahatma Gandhi, &quot;Encounters with
Christianity&quot; &amp; &quot;The Message of Jesus&quot; in <i>Gandhi on Christianity</i>,
pp. 3-30.</p>

<p>&nbsp;<b></p>

<p>Thursday, March 5</b></p>

<p>READING: <u><i>Reader</i>, selections #13 &amp; #14</u>, Thich Nhat Hanh, &quot;A
Peaceful Heart&quot; in <i>Living Buddha, Living Christ</i>, pp. 74-86; &amp; Pinchas
Lapide, &quot;Abrogation of the Law?&quot;, &quot;The Higher Law&quot; and &quot;Hate Your
Enemy?&quot; in <i>The Sermon on the Mount: Utopia or Program for Action?</i> pp. 41-48,
76-95.</p>

<p>&nbsp;<b></p>

<p>Tuesday, March 10</b></p>

<p>READING: <u><i>Reader</i>, selections #15 &amp; #16</u>, Martin Luther King, Jr.,
&quot;Pilgrimage to Nonviolence&quot; in <i>Stride Toward Freedom: The Montgomery Story</i>,
pp. 90-107; &amp; &quot;Loving Your Enemies&quot; in <i>Strength to Love</i>, pp. 49-57.</p>

<p>&nbsp;<b></p>

<p>Thursday, March 12</b></p>

<p>READING: <u><i>Reader</i>, selection #17</u>, Diana L. Eck, &quot;The Imagined
Community: Spiritual Interdependence and a Wider Sense of &#145;We&#146;&quot; in <i>Encountering
God: A Spiritual Journey from Bozeman to Banaras</i>, pp. 200-231. <b><u>PAPER 3 ASSIGNED</u>.</b></p>

<p ALIGN="CENTER">&nbsp;</p>
<b><u>

<p>PAPER 3 IS DUE BY NOON ON FRIDAY, MARCH 20 IN MY OFFICE, SAC 430</u>.</b></p>
</body>
</html>


