<!doctype html public "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
  <META NAME="GENERATOR" CONTENT="Internet Assistant for Word 1.0Z">
  <META NAME="AUTHOR" CONTENT="S. Marc Cohen">
  <META NAME="OPERATOR" CONTENT="S. Marc Cohen">
  <TITLE>Lecture on 4 causes</TITLE>
</HEAD>
<BODY BGCOLOR="#f3e2e0">
<H1 ALIGN=Center>
  The Four Causes
</H1>
<OL>
  <LI>
    Aristotle's doctrine of the four causes is crucial, but easily misunderstood.
    It is natural for us (post-Humeans) to think of (what Aristotle calls) "causes"
    in terms of our latter-day notion of cause-and-effect. This is misleading
    in several ways:<BR>
    <OL type=a>
      <LI>
	Only one of Aristotle's causes (the "efficient" cause) sounds even remotely
	like a Humean cause.
      <LI>
	Humean causes are <B>events</B>, and so are their effects, but Aristotle
	doesn't limit his causes in that way. Typically, it is <B>substances</B>
	that have causes. And that sounds odd.<BR>
    </OL>
  <LI>
    But to charge Aristotle with having only a dim understanding of causality
    is to accuse him of missing a target he wasn't even aiming at. We must keep
    this in mind whenever we use the word "cause" in connection with Aristotle's
    doctrine.<BR>
  <LI>
    What is it Aristotle says there are four of? The Greek word is <I>aition</I>
    (plural <I>aitia</I>); sometimes it takes a feminine form, <I>aitia</I> (plural
    <I>aitiai</I>). And what is an <I>aition</I>? Part of Aristotle's point is
    that there is no one answer to this question.<BR>
  <LI>
    An <I>aition</I> is something one can cite in answer to a "why?" question.
    And what we give in answering a "why?" question is an explanation. So an
    <I>aition</I> is best thought of as an <B>explanation</B> or an <B>explanatory
    factor</B>.<BR>
  <LI>
    This is a good start, but it's still not totally clear what an <I>aition</I>
    is. For Aristotle thinks that you can ask what the <I>aitia</I> of this table
    are, and it's not clear what sense, if any, it makes to ask for an explanation
    of the table.<BR>
  <LI>
    <B>TEXTS</B>:
    <OL type=a>
      <LI>
	<I> </I>In RAGP: <I>Phys</I>. II.3; and (extensively) in <I>Metaph</I>. A.3
	ff. See also <I>Part. An</I>. 639b12ff
      <LI>
	Additionally (not in RAGP): <I>APo</I>. II.11; <I>Metaph</I>. D.2; <I>GC</I>
	335a28-336a12.<BR>
    </OL>
  <LI>
    The traditional picture and terminology (not all Aristotle's terminology):<BR>
    <OL type=a>
      <LI>
	<B>Material</B> cause: "that out of which a thing comes to be, and which
	persists," e.g., bronze, silver, and the genus of these (= metal?).
      <LI>
	<B>Formal</B> cause: "the statement of essence" "the account of what-it-is-
	to-be, and the parts of the account."
      <LI>
	<B>Efficient</B> cause: "the primary source of change," e.g., the man who
	gives advice, the father (of the child).
      <LI>
	<B>Final</B> cause: "the end (<I>telos</I>), that for the sake of which a
	thing is done," e.g., health (is the cause of exercise).<BR>
    </OL>
  <LI>
    Aristotle's doctrine is that <I>aition</I> is <B>ambiguous</B>. As he puts
    it, "<I>aition</I> is said in many ways." That is, when one says that
    <I>x</I> is the <I>aition</I> of <I>y</I>, it isn't clear what is meant until
    one specifies what <B>sense</B> of <I>aition</I> is intended:<BR>
    <OL type=a>
      <LI>
	<I>x</I> is what <I>y</I> is [made] <B>out of</B>.
      <LI>
	<I>x</I> is what it is to be <I>y</I>.
      <LI>
	<I>x</I> is what <B>produces</B> <I>y</I>.
      <LI>
	<I>x</I> is what <I>y</I> is <B>for</B>.<BR>
    </OL>
  <LI>
    This makes it hard for us to get clear on what Aristotle was up to, since
    neither "cause" nor "explanation" is ambiguous in the way Aristotle claims
    <I>aition</I> is. There is no English <B>translation</B> of <I>aition</I>
    that is ambiguous in the way (Aristotle claims) <I>aition</I> is. But if
    we shift from the noun "cause" to the verb "makes" we may get somewhere.<BR>
  <LI>
    Aristotle's point may be put this way: if we ask "what makes something
    so-and-so?" we can give four very different sorts of answer - each appropriate
    to a different sense of "makes." Consider the following sentences:<BR>
    <OL>
      <LI>
	The table is <B>made of</B> wood.
      <LI>
	Having four legs and a flat top <B>makes</B> this a table.
      <LI>
	A carpenter <B>makes</B> a table.
      <LI>
	Having a surface suitable for eating or writing on <B>makes</B> this a table.
    </OL>
    <BLOCKQUOTE>
      Aristotelian versions of (1) - (4): <BR>
      <BR>
      1a) &nbsp;Wood is an <I>aition</I> of a table. <BR>
      2a) &nbsp;Having four legs and a flat top is an <I>aition</I> of a table.
      <BR>
      3a) &nbsp;A carpenter is an <I>aition</I> of a table. <BR>
      4a) &nbsp;Having a surface suitable for eating or writing on is an
      <I>aition</I> of a table.<BR>
      <BR>
      These sentences can be disambiguated by specifying the relevant sense of
      <I>aition</I> in each case:<BR>
      <BR>
      1b) &nbsp;Wood is what the table is <B>made out of</B>. <BR>
      2b) &nbsp;Having four legs and a flat top is <B>what it is to be </B>a table.
      <BR>
      3b) &nbsp;A carpenter is what <B>produces</B> a table. <BR>
      4b) &nbsp;Eating and writing on is what a table is <B>for</B>.<BR>
    </BLOCKQUOTE>
  <LI>
    Matter and form are two of the four causes, or explanatory factors. They
    are used to analyze the world <B>statically</B> - they tell us how it is.
    But they do not tell us how it came to be that way.<BR>
    <BR>
    For that we need to look at things <B>dynamically</B> - we need to look at
    causes that explain why matter is formed in the way that it is.<BR>
    <BR>
    Change consists in matter taking on (or losing) form. Efficient and final
    causes are used to explain why change occurs.<BR>
  <LI>
    This is easiest to see in the case of an artifact, like a statue or a table.
    The table has come into existence because the carpenter put the form of the
    table (which he had in his mind) into the wood of which the table is
    composed.<BR>
    <BR>
    The carpenter has done this for the purpose of creating something he can
    write on or eat on. (Or, more likely, that he can sell to someone who wants
    it for that purpose.) This is a <B>teleological</B> explanation of there
    being a table.<BR>
  <LI>
    But what about <B>natural</B> objects? Aristotle (notoriously) held that
    the four causes could be found in nature, as well. That is, that there is
    a final cause of a tree, just as there is a final cause of a table.<BR>
    <BR>
    Here he is commonly thought to have made a huge mistake. How can there be
    <B>final</B> causes in nature, when final causes are <B>purposes</B>, what
    a thing is <B>for</B>? In the case of an artifact, the final cause is the
    end or goal that the artisan had in <B>mind</B> in making the thing.<BR>
    <BR>
    But what is the final cause of a dog, or a horse, or an oak tree?<BR>
    <OL type=a>
      <LI>
	What they are used for? E.g., pets, pulling plows, serving as building materials,
	etc. To suppose so would be to suppose Aristotle guilty of reading
	<B>human</B> purposes and plans into nature. But this is not what he has
	in mind.
      <LI>
	Perhaps he thinks of nature as being like art, except that the artisan is
	God? God is the efficient cause of natural objects, and God's purposes are
	the final causes of the natural objects that he creates.
    </OL>
    <P>
    No. In both (a) and (b), the final cause is <B>external</B> to the object.
    (Both the artisan and God are external to their artifacts; they impose form
    on matter from the outside.) But the final causes of natural objects are
    <B>internal</B> to those objects.<BR>
  <LI>
    <B>Final causes in nature: some of the details of Aristotle's
    account.</B><BR>
    <OL type=a>
      <LI>
	The final cause of a <B>natural object</B> - a plant or an animal - is
	<B>not</B> a purpose, plan, or "intention". Rather, it is whatever lies at
	the <B>end</B> of the regular series of developmental changes that typical
	specimens of a given species undergo.<BR>
	<BR>
	<B>The final cause need not be a purpose that someone has in mind.</B><BR>
	<BR>
	I.e., where <I>F</I> is a biological kind: the <I>telos</I> of an <I>F</I>
	is what embryonic, immature, or developing <I>F</I>s are all tending to grow
	into. The <I>telos</I> of a developing tiger is <B>to be a tiger</B>.<BR>
      <LI>
	Aristotle opposes final causes in nature to <B>chance</B> or
	<B>randomness</B>. So the fact that there is regularity in nature - as Aristotle
	says, things in nature happen "always or for the most part" - suggests to
	him that biological individuals run <B>true to form</B>. So this end, which
	developing individuals regularly achieve, is what they are "aiming at".<BR>
	<BR>
	Thus, for a natural object, the <B>final</B> cause is typically identified
	with the <B>formal</B> cause. The final cause of a developing plant or animal
	is the <B>form</B> it will ultimately achieve, the form into which it grows
	and develops.<BR>
	<BR>
	References: <I>Phys</I>. 198a25, 199a31, <I>DA</I> 415b10, <I>GA</I>
	715a4ff.<BR>
      <LI>
	This helps to explain why "form, mover, and <I>telos</I> often coincide"
	(198a25). I.e., formal, efficient, and final causes "coincide", Aristotle
	says.<BR>
	<BR>
	The <I>telos</I> of a (developing) tiger is just (to be) a tiger (i.e. to
	be an animal with the characteristics specified in the definition of a tiger).
	Thus, Aristotle says (198b3) that a source of natural change is "a thing's
	form, or what it is, for that is its end and what it is for."<BR>
	<BR>
	Claims like "a tiger is for the sake of a tiger" or "an apple tree is for
	the sake of an apple tree" sound vacuous. But the identification of formal
	with final causes is not vacuous. It is to say, about a developing entity,
	that <B>there is something internal to it which will have the result that
	the outcome of the sequence of changes it is undergoing </B>- <B>if it runs
	true to form </B>- <B>will be another entity of the same kind</B> - a tiger,
	or an apple tree.<BR>
      <LI>
	So form and <I>telos</I> coincide. What about the <B>efficient</B> cause?
	The internal factor which accounts for this cub's growing up to be a tiger
	(a) has causal efficacy, and (b) was itself contributed by a tiger (i.e.
	the cub's father).<BR>
	<BR>
	This can be more easily grasped if we realize that for Aristotle questions
	about causes in nature are raised about <B>universals</B>. Hence, the answers
	to these questions will also be given in terms of universals. The questions
	that ask for formal, final, and efficient causes, respectively, are:<BR>
	<OL>
	  <LI>
	    What <B>kind</B> of thing do these flesh-and-bones constitute?
	  <LI>
	    What has this (seed, embryo, cub) all along been developing <B>into</B>?
	  <LI>
	    What <B>produces</B> a tiger?
	</OL>
	<P>
	The answer to all three questions is the same: "a tiger". It is in this sense
	that these three causes coincide.<BR>
      <LI>
	Aristotle's account of animal reproduction makes use of just these points
	(cf. <I>GA</I> I.21, II.9 and <I>Metaph</I>. Z.7-9):<BR>
	<OL type=i>
	  <LI>
	    The basic idea (as in all change) is that matter takes on form. The form
	    is contributed by the male parent (which actually does have the form), the
	    matter by the female parent. This matter has the potentiality to be informed
	    by precisely that form.
	  <LI>
	    The embryonic substance has the form potentially, and can be "called by the
	    same name" as what produces it. (E.g., the embryonic tiger can be called
	    a <I>tiger</I>, for that is what it is, potentially at least.) [But there
	    are exceptions: the embryonic mule cannot be called by the name of its male
	    parent, for that is a <I>horse</I> (1034b3).]
	  <LI>
	    The form does not come into existence. Rather, it must exist beforehand,
	    and get imposed on appropriate matter. In the case of the production of
	    artifacts, the pre-existing form may exist merely potentially. (E.g., the
	    artist has <B>in mind</B> the form he will impose on the clay. Nothing has
	    to have the form in actuality.)
	  <LI>
	    But in the case of natural generation, the pre-existing form must exist <B>in
	    actuality</B>: "there must exist beforehand another <B>actual</B> substance
	    which produces it, e.g. an animal must exist beforehand if an animal is produced"
	    (1034b17).<BR>
	</OL>
      <LI>
	So the final cause of a natural substance is its form. But what is the form
	of such a substance <B>like</B>? Is form merely <B>shape</B>, as the word
	suggests? No. For natural objects - living things - form is more complex.
	It has to do with <B>function</B>.<BR>
	<BR>
	We can approach this point by beginning with the case of bodily organs. For
	example, the final cause of an eye is its function, namely, <B>sight.</B>
	That is what an eye is <B>for</B>.<BR>
	<BR>
	And this function, according to Aristotle, is part of the <B>formal</B> cause
	of the thing, as well. Its function tells us what it <B>is</B>. What it is
	to be an eye is to be an organ of <B>sight</B>.<BR>
	<BR>
	To say what a bodily organ <B>is</B> is to say what it <B>does</B> - what
	function it performs. And the function will be one which serves the purpose
	of preserving the organism or enabling it to survive and flourish in its
	environment.<BR>
	<BR>
	Since typical, non-defective, specimens of a biological species do survive
	and flourish, Aristotle takes it that the function of a kind of animal is
	to do <B>what animals of that kind typically do</B>, and as a result of doing
	which they survive, flourish, and reproduce. &nbsp;Cf. Charlton (<I>Aristotle's
	Physics</I>, p. 102):
	<BLOCKQUOTE>
	  ". . . the widest or most general kind of thing which all non-defective members
	  of a class can do, which differentiates them from other members of the next
	  higher genus, is their function."<BR>
	</BLOCKQUOTE>
      <LI>
	To say that there are ends (<I>tel&ecirc;</I>) in nature is <B>not</B> to
	say that nature has a purpose. Aristotle is not seeking some one answer to
	a question like "What is the purpose of nature?" Rather, he is seeking a
	single kind of explanation of the characteristics and behavior of natural
	objects. That is, plants and animals develop and reproduce in regular ways,
	the processes involved (even where not consciously aimed at or deliberated
	about) are all <B>toward</B> certain ends.<BR>
    </OL>
  <LI>
    There is much that can be said in opposition to such a view. But at least
    it is not ridiculous, as is sometimes supposed. In so far as functional
    explanation still figures in biology, there is a residue of Aristotelian
    teleology in biology. And it has yet to be shown that biology can get along
    without teleological notions. The notions of function, and what something
    is for, are still employed in describing at least some of nature.
</OL>
<P>
<BR>
  <HR>
<P>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif" WIDTH="20" HEIGHT="20">Go to next lecture
on &nbsp;<A HREF="zeta17.htm">Matter, Form, and Substance</A>.<BR>
<BR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif" WIDTH="22" HEIGHT="22">Go to previous
lecture on &nbsp;<A HREF="archange.htm">Aristotle on Change</A>
<P>
<P>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.<BR>
</UL>
</BODY></HTML>


