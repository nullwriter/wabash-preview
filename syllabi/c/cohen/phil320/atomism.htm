<!doctype html public "-//w3c//dtd html 4.0 transitional//en">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <META name="GENERATOR" content="Microsoft FrontPage 4.0">
  <META name="AUTHOR" content="S. Marc Cohen">
  <META name="OPERATOR" content="S. Marc Cohen">
  <TITLE>Atomism</TITLE>
</HEAD>
<BODY bgcolor="#D3E4D8">
<CENTER>
  <H1>
    ATOMISM
  </H1>
</CENTER>
<OL>
  <LI>
    Atomism was devised by Leucippus and his student Democritus. Democritus was
    born about 460 B.C., which makes him about 40 years younger than Anaxagoras,
    and about 10 years <B>younger</B> than Socrates.
    <P>
    "<I>Atomism is the final, and most successful, attempt to rescue the reality
    of the physical world from the fatal effects of Eleatic logic by means of
    a pluralistic theory</I>." (<B>Guthrie</B>, vol. <B>2</B>, p. <B>389</B>)
    <P>
  <LI>
    Overview of atomism:
    <P>
    <OL type=a>
      <LI>
	Imagine each atom, taken by itself, as a Parmenidean <B>unit</B>. Each is
	<B>indivisible</B>. There is no differentiation between one part of an atom
	and another part. There is no empty space within an atom <TT>-</TT> it's
	a <I>plenum</I>.
	<P>
      <LI>
	Thus, the atomists tried to agree with Parmenides about everything except
	the <B>number</B> of real beings.
	<P>
      <LI>
	Comparison with Anaxagoras and Empedocles: theirs was a <B>qualitative</B>
	pluralism. The atomists offered a <B>quantitative</B> pluralism.
	<P>
    </OL>
  <LI>
    Properties of atoms: each atom is uniform, homogeneous, colorless, tasteless,
    and indivisible. (We will inquire in a moment into precisely the sense in
    which atoms are indivisible.)
    <P>
    Atoms have <B>size</B>, <B>shape</B>, and (perhaps) <B>weight</B>. And they
    can <B>move</B>.&nbsp; That is, atoms have (what have come to be called)
    <B>primary</B> qualities. As for such secondary qualities as color, taste,
    etc., atoms do not have them - an atom cannot be yellow, or salty.
    <P>
  <LI>
    Democritus' atomistic universe: atoms move about in the void (empty space),
    collide, attach to others to form compounds. These compounds can have secondary
    qualities, but such qualities can be reduced to the primary qualities of
    their component atoms. Cf. <B>20</B>=A129:
    <P>
    <I>He makes sweet that which is round and good-sized; astringent that which
    is large, rough, polygonal, and not rounded; sharp tasting, as its name
    indicates, that which is sharp in body, and angular, bent and not rounded;
    pungent that which is round and small and angular and bent; salty that which
    is angular and good-sized and crooked and equal sided; bitter that which
    is round and smooth, crooked and small sized; oily that which is fine and
    round and small.</I>
    <P>
    That is, <B>the (secondary) qualities of a compound are completely determined
    by and reducible to the (primary) qualities of its component atoms</B>.
    <P>
  <LI>
    The coming together of atoms to form compounds <B>appears</B> to be generation
    (new things appear to come into existence) but it is explained away, as in
    Empedocles. For <B>arrangements</B> and <B>clusters</B> of atoms <B>are not
    real</B>, according to Democritus.
    <P>
    <B>Nor are the apparent properties of those compounds of atoms real</B>.
    Such a compound may appear to be white, or green, but this is not so. There
    is nothing that is <B>really</B> white or green.
    <P>
    The only things that are real are the atoms, and the empty space they move
    about in. Cf. <B>26</B>=B 9:
    <P>
    <I>By convention, sweet; by convention, bitter; by convention, hot; by
    convention, cold; by convention, color; but in reality, atoms and
    void<B>.</B></I>
    <P>
  <LI>
    The picture is entirely <B>mechanistic</B>. The movement of atoms is explained
    without recourse to reasons, motives, Mind, the Good, Love, Strife, as was
    common among other Presocratics. Our only fragment from Leucippus attests
    to this (<B>1</B>=B2):
    <P>
    <I>No thing happens at random</I> <I>but all things as a result of a reason
    and by necessity</I>.
    <P>
    This is causal <B>determinism</B>.
    <P>
    <OL type=a>
      <LI>
	An individual atom has no choice concerning its movements. If pushed, it
	moves. Its "motivational forces" are all external.
	<P>
      <LI>
	The compounds of atoms don't have any choice, either. For their movements
	are all a function of the movements of their component atoms. <B>The movement
	of an entire system of atoms is just the sum of the movements of all of its
	individual component atoms.</B>
	<P>
      <LI>
	Explanations are <B>bottom up</B>, not <B>top down</B>. That is, the movements
	and behavior of a compound of atoms (e.g., a tree, an animal) are to be
	understood as the sum of the individual movements of all the atoms composing
	that compound. Thus, one explains why the tree or animal moves in such-and-such
	a way by explaining why each of its component atoms moves as it does. (It
	is this kind of explanation that particularly exercised Plato, who thought
	this idea was a colossal mistake. Cf. <I>Phaedo</I> 98c)
	<P>
    </OL>
  <LI>
    This very compelling world-view has given rise to a mechanistic, deterministic,
    point of view that has been even more popular in modern times than it was
    in ancient times. (Contemporary problems about deterministic physics arising
    from quantum mechanics have considerably weakened the support for this point
    of view. The classical Newtonian view that quantum theory has replaced is
    basically Democritean.)
    <P>
    The ancient atomists may appear to have provided a brilliant anticipation
    of a much later scientific theory. But is this picture accurate? Our enthusiasm
    for the achievements of the ancient atomists must be tempered by a closer
    look at the basis of their view.
    <P>
  <LI>
    Their impetus did not come from <B>physical</B> inquiries, but from the
    <B>logical</B> and <B>metaphysical</B> positions of Parmenides and Zeno.
    As Barnes says (<I>Presocratics</I>, p. 346: "the first atoms came from Elea."
    Atoms were postulated in response to the Eleatic view that a truly real entity
    must be <B>one</B> and <B>indivisible</B>. So we must ask:
    <B><FONT color="#0000FF">In what sense were Democritus' atoms
    indivisible</FONT>?</B> Democritus might have meant either of the following:
    <P>
    <OL type=a>
      <LI>
	It is <B>physically</B> impossible to divide an atom.
	<P>
      <LI>
	It is <B>logically</B> or <B>conceptually</B> impossible to divide an atom.
	<P>
	If (a) is the Democritean position, then it would make <B>sense</B> to talk
	about the <B>parts</B> of an atom <TT>-</TT> there might even <B>be</B> such
	parts <TT>-</TT> although it would not be physically possible to separate
	the parts.
	<P>
	If (b) is what Democritus maintained, then this sort of talk makes no sense.
	The very idea of "splitting an atom" would represent not just a technological
	difficulty (or even a technological impossibility) but a conceptual absurdity.
	<P>
    </OL>
  <LI>
    Opinion is divided on this issue.
    <P>
    <OL type=a>
      <LI>
	In favor of (a) are
	<P>
	<OL type=i>
	  <LI>
	    <B>Burnet</B> (<I>EGP</I>, p. <B>336</B>): "<I>We must observe that the atom
	    is not mathematically indivisible, for it has magnitude; it is however physically
	    indivisible, because, like the One of Parmenides, it contains no empty
	    space</I>."
	    <P>
	  <LI>
	    <I>KRS</I>, p. <B>415</B>: "<I>[An atom] is presumably only physically, not
	    notionally, indivisible, since for example atoms differ in size.</I>"
	    <P>
	</OL>
      <LI>
	In favor of (b) are
	<P>
	<OL type=i>
	  <LI>
	    <B>Guthrie</B> (vol. <B>2, </B>p. <B>396</B>): "<I>Democritus held that his
	    atoms, being not only very small but the smallest possible particles of matter,
	    were not only too small to be divided physically but also logically
	    indivisible.</I>"
	    <P>
	  <LI>
	    <B>Furley</B>: I will give a quick sketch of the case he makes for (b).
	    <P>
	</OL>
    </OL>
  <LI>
    Furley's argument for theoretically indivisible Democritean atoms.
    <P>
    <OL type=a>
      <LI>
	Aristotle says that atoms were postulated to meet (what he called) Zeno's
	"Dichotomy Argument." This would be either the paradox of the race course,
	or the paradox of plurality.
	<P>
      <LI>
	But, as we have seen, both of these arguments of Zeno's are meant to show
	that infinite divisibility (whether physical or theoretical) leads to absurd
	results. Hence,
	<P>
      <LI>
	The atomists would not be meeting Zeno's argument unless they conceived of
	atoms as <B>both</B> physically and theoretically indivisible. <B>Furley</B>
	(p. <B>510</B>):
	<P>
	"<I>A theoretically divisible atom would not answer either of Zeno's arguments.
	[The plurality paradox] would show that an atom theoretically divisible to
	infinity must be infinite in magnitude; and [the race course] would show
	that such an atom could never be traversed <TT>-</TT> that is, if one starts
	imagining it, one can never imagine the whole of it.</I>"
	<P>
      <LI>
	This is supported by further evidence from Aristotle. According to him, atomism
	conflicts with mathematics (<I>De Caelo</I> 303a20):
	<P>
	&nbsp; &nbsp; &nbsp; &nbsp; "<I>They must be in conflict with mathematics
	when they say there are indivisible bodies</I> "
	<P>
	But an atom that is (merely) physically unsplittable would not conflict with
	mathematics.
	<P>
	If this interpretation is correct, and atoms are theoretically indivisible,
	then the differences between the Democritean view and modern scientific atomism
	are greater than the similarities.
	<P>
    </OL>
  <LI>
    Objections to theoretically indivisible Democritean atoms.
    <P>
    <OL type=a>
      <LI>
	According to Simplicius (<I>De Caelo</I>), Democritus thought that atoms
	had <B>size </B>and <B>shape</B>:
	<P>
	" <I>for some of them are angular, some hooked, some concave, some convex,
	and indeed with countless other differences these atoms move in the infinite
	void, separate one from the other and differing in <B>shapes</B>,
	<B>sizes</B>, position and arrangement.</I>"
	<P>
      <LI>
	But it is hard to see how someone could conceive of atoms as having size
	and shape, and still being theoretically indivisible. For it would seem that,
	for any size <I>x</I>, we can always think of something that is only half
	that size: we can always divide <I>x</I> by 2.
	<P>
    </OL>
  <LI>
    <B>Atomism and Mathematics</B>
    <P>
    It's possible that Democritus thought not just of matter, but also of
    <B>space</B> in an atomistic way. That is, the size of an atom would be an
    <B>atomic space</B>. In such a system, the <B>ultimate unit of measurement</B>
    would be the size of an atom. Within that framework, the very notion of
    <B>half</B> of an atomic space would be unintelligible. So, Democritus would
    be able to say, coherently, that an atom has size even though it is theoretically
    indivisible.
    <P>
    <OL type=a>
      <LI>
	<B>The "Weyl Tile" Argument</B>:
	<P>
	But if Democritus "atomized" space in this way (as he very likely did), he
	runs into another problem. For Euclidean geometry (in particular, the Pythagorean
	theorem) requires that <B>space</B> be continuously divisible. Hence, if
	atomism denies the continuity of space, it will fail to get mathematics right.
	<P>
	Why is this?&nbsp; There is a famous argument by the mathematician Hermann
	Weyl (the "Weyl tile" argument) that clearly shows what is problematical
	about atomistic geometry:
	<P>
	Consider any geometrical figure (e.g., squares, triangles, etc.) with straight
	lines as sides. The <B>length</B> of each side will be measured in (space)
	atoms, and each side will be assigned an <B>integer</B> as its measure. (Each
	side will be <I>n</I> atomic units long, where <I>n</I> is a positive integer.)
	<P>
	Now consider a square whose sides are some whole number of atoms long. How
	long is the <B>diagonal</B> of the square?
	<P>
	Since the diagonal of a square divides it into two right triangles, we get
	our answer from the Pythagorean theorem:&nbsp; the square of the hypotenuse
	of a right triangle = the sum of the squares of the other two sides:
	<P>
	<B><I>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; c</I><SUP>2</SUP>=
	<I>a</I><SUP>2</SUP> + <I>b</I><SUP>2</SUP></B>.
	<P>
	So if a square is, e.g., 4 atoms long per side, then <I>a</I> = 4 and
	<I>b</I> = 4. <BR>
	So: <I>c</I><SUP>2</SUP> = 32 = 16 x 2. <BR>
	So <I>c</I> = 4 x the square root of 2.
	<P>
	<B>But this is an irrational number</B>. So no square whose sides are 4 atoms
	long can have a diagonal that is any whole number of atoms long!
	<P>
	Note that the situation does not change if we make the square <B>larger</B>.
	Even if the sides are <B>billions</B> of atoms long, the length of the hypotenuse
	will still be irrational. Let <I>a</I> be as large as you like; <I>c</I>
	will still be an irrational number.
	<P>
	For <I>c</I> = <I>a</I> x the square root of 2.
	<P>
	So "atomistic geometry" seems inherently flawed. If we measure the sides
	of a square as a whole number (of atomic units), and we try to measure the
	diagonal as a whole number (of atomic units), we will never get the
	<B>correct</B> answer to the question: "How long is the diagonal of a square?"
	<P>
      <LI>
	<A NAME="Arrow"><!-- --></A><B>Zeno's <A HREF="ZenoArrow.htm">Paradox of
	the Arrow</A></B>:
	<P>
	Zeno's argument that an (apparently) moving arrow is really at rest throughout
	its flight seems easy to evade if one insists that space is continuous (and
	hence infinitely divisible). &nbsp;But an atomist who insists on theoretically
	indivisible atoms seems bound to deny that space is infinitely divisible.
	&nbsp;And Zeno's Arrow Paradox poses an especially troubling problem for
	such an atomist.
	<P>
	For how will the arrow (or any object, in fact) move through an atomic space?
	&nbsp;Since the space cannot be divided, the tip of the arrow must advance
	from one end of the space to the other without ever having occupied any of
	the intervening space. &nbsp;At one moment, <I>t</I><SUB>1</SUB>, it's in
	one place, <I>p</I><SUB>1</SUB>; at some later moment,
	<I>t</I><SUB>2</SUB>, it's in another place, <I>p</I><SUB>2</SUB>. But if
	you pick any time <I>t<SUB>i</SUB></I> that falls between
	<I>t</I><SUB>1</SUB>&nbsp;and <I>t</I><SUB>2</SUB>, the arrow is either still
	at <I>p</I><SUB>1</SUB> or already at <I>p</I><SUB>2</SUB>. It <B>never moves
	</B>from <I>p</I><SUB>1</SUB> to <I>p</I><SUB>2</SUB>, because the space
	from <I>p</I><SUB>1</SUB> to <I>p</I><SUB>2</SUB> is atomic and therefore
	cannot be divided.
	<P>
	Although we cannot, of course, be certain that Zeno intended his Arrow Paradox
	specifically against the atomists, it constitutes a formidable objection
	to an "atomic" conception of space.<BR>
	<BR>
	(Nevertheless, physicists are still enamored of the idea that space and time
	come in discrete "quanta" which cannot meaningfully be further sudivided,
	even conceptually. If you want proof, check out this
	New York Times article of December 7, 1999.)
	<P>
    </OL>
  <LI>
    Finally, let us consider Democritus' idea that atoms have <B>shape</B>:
    <P>
    <OL type=a>
      <LI>
	Democritus did not just think that atoms had magnitude. He thought that they
	had <B>different</B> sizes, and <B>shapes</B>. And this seems to conflict
	with the idea that there are atomic sizes. For how could one atom be larger
	than another unless one of them were either larger than (or smaller than)
	the atomic size.
	<P>
      <LI>
	Perhaps Democritus thought that there was a smallest size atom, and the size
	of <B>that</B> atom was the atomic unit of measurement. But if that atom
	has a <B>shape</B>, the view seems to unravel. Cf. <B>Furley</B> (p.
	<B>521</B>):
	<P>
	"<I>Democritus' atoms had many variations in shape and size. There seems
	to be an inescapable contradiction here. If we take together a smaller atom
	and a larger one, we can always distinguish in the larger one that part which
	is covered by the smaller and that which is not. Even within the limits of
	a single atom, supposing it to be of a complex shape (say hook-shaped), we
	can always distinguish one part of the shape from another (say the hook from
	the shaft).</I>"
	<P>
      <LI>
	Furley concludes that Democritus did, indeed, think of his atoms as being
	<B>both</B> theoretically indivisible <B>and</B> differing in shape, and
	that his view was therefore internally inconsistent.
	<P>
      <LI>
	For more on this interpretation, see <B>Guthrie</B>, vol. <B>2</B>, Appendix,
	pp. <B>503-7</B>.
	<P>
	For an opposing view, cf. <B>Barnes</B>, <I>Presocratics</I>, <B>352-360</B>.
	Barnes considers the idea that Democritean atoms are theoretically indivisible,
	in three different senses: conceptually, geometrically, and logically
	indivisible. He argues that the available texts do not adequately support
	the idea that atoms are theoretically indivisible, and concludes that the
	case has not been proven either way.
    </OL>
</OL>
<P>
  <HR>
<IMG HEIGHT=20 WIDTH=20 ALIGN=CENTER SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif">Go to next lecture
on&nbsp; <A HREF="socdef.htm">Socratic Definitions</A>
<P>
<IMG HEIGHT=22 WIDTH=22 ALIGN=CENTER SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif">Go to previous lecture
on <A HREF="anaxag.htm">Anaxagoras</A>
<P>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.
</UL>
</BODY></HTML>


