<!doctype html public "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
  <META NAME="GENERATOR" CONTENT="Internet Assistant for Word 1.0Z">
  <META NAME="AUTHOR" CONTENT="S. Marc Cohen">
  <TITLE>Predication and Ontology: Categories</TITLE>
</HEAD>
<BODY BGCOLOR="#F7F7FF">
<H1 ALIGN=Center>
  Predication and Ontology:<I> The Categories</I><BR>
</H1>
<OL>
  <LI>
    This is a book about ontology ("what there is") and predication (what it
    is to <B>say something about</B> something that there is).<BR>
    <OL type=a>
      <LI>
	<B>Ontology</B>: The Ten Categories
	<P>
	In the <I>Categories</I>, we get this list (1b25):<BR>
	<OL>
	  <LI>
	    <B>Substance</B>
	  <LI>
	    Quality
	  <LI>
	    Quantity
	  <LI>
	    Relation
	  <LI>
	    Where
	  <LI>
	    When
	  <LI>
	    Position
	  <LI>
	    Having
	  <LI>
	    Action
	  <LI>
	    Passion
	</OL>
	<P>
	This is presumably a list of the ten fundamentally different kinds of things
	that there are. The first category - substance - is the most important in
	Aristotle's ontology. Substances are, for Aristotle, the fundamental entities.
	To see why this is so, we will have to understand what Aristotle says about
	predication.
	<P>
      <LI>
	<B>Predication</B>
	<P>
	<UL>
	  <LI>
	    A <B>subject</B> (<I>hupokeimenon</I>) is what a statement is about.
	  <LI>
	    A <B>predicate</B> (<I>kat&ecirc;goroumenon</I>) is what a statement says
	    about its subject.
	</UL>
	<P>
	Examples:
	<P>
	<UL>
	  <LI>
	    This (particular animal) is a man.
	  <LI>
	    Man is an animal.
	  <LI>
	    This (particular color) is white.
	  <LI>
	    White is a color.
	</UL>
	<P>
	The same thing may be both a subject and a predicate, e.g., <I>man</I> and
	<I>white</I> above. Some things are subjects but are never predicates, e.g.,
	this (particular) animal, or this (particular) color.
	<P>
    </OL>
  <LI>
    <B>Two kinds of predication</B>
    <P>
    Consider the following pair of simple (atomic) sentences:<BR>
    <UL>
      <LI>
	"Socrates is a human being"
      <LI>
	"Socrates is wise"
    </UL>
    <P>
    Do both of these atomic sentences have the same kind of <B>ontological
    underpinning</B>? I.e., is the structure of the fact that Socrates is a man
    the same as the structure of the fact that Socrates is wise? Plato's account
    suggests that it is.<BR>
    <UL>
      <LI>
	For Plato:"<I>x</I> is <I>F</I>" means that <I>x</I> partakes of the Form,
	<I>F</I>-ness.
    </UL>
    <P>
    For Plato, predication, in general, is explicated in terms of the notion
    of <B>participating in a Form</B>. In response, Aristotle thinks this
    oversimplifies. For Aristotle:<BR>
    <UL>
      <LI>
	"Socrates is a human being" tells us something fundamental about what kind
	of a thing Socrates is: it is an <B>essential</B> predication.
      <LI>
	"Socrates is wise" tells us something less fundamental, something that merely
	happens to be the case: it is an <B>accidental</B> predication.
    </UL>
    <P>
    This idea emerges in the <I>Categories</I> distinction between what is <B>said
    of</B> a subject and what is <B>in</B><I> </I>a subject, introduced as part
    of the four-fold distinction drawn at 1a20.
    <P>
  <LI>
    <B>Two fundamental relations: SAID OF and IN a subject</B>.
    <P>
    There are two basic ontological relations that cut across all ten categories.
    These correspond to the notions (that Aristotle later develops) of
    <I>essential</I> and <I>accidental</I> predication.
    <P>
    <OL type=a>
      <LI>
	<B>SAID OF a subject </B><BR>
	<UL>
	  <LI>
	    This is a relation of fundamental ontological <B>classification</B>. It is
	    the relation between a <B>kind</B> and a thing that falls under it.
	  <LI>
	    It is a <B>transitive</B> relation (i.e., if <I>x</I> is SAID OF <I>y</I>
	    and <I>y</I> is SAID OF <I>z</I>, it follows that <I>x</I> is SAID OF
	    <I>z</I>).
	  <LI>
	    Its relata belong to the <B>same category</B>. A universal in a given category
	    is SAID OF the lower-level universals and individuals that fall under it.
	</UL>
	<P>
	Examples:<BR>
	<OL>
	  <LI>
	    Man is SAID OF Socrates.
	  <LI>
	    Animal is SAID OF man.
	  <LI>
	    (Hence) animal is SAID OF Socrates.
	  <LI>
	    White is SAID OF this (particular) color.
	  <LI>
	    Color is SAID OF white.
	    <P>
	</OL>
      <LI>
	<B>IN a subject </B><BR>
	<UL>
	  <LI>
	    This is a relation of fundamental ontological <B>dependence</B>. What is
	    IN a subject, Aristotle says, belongs to it "not as a part, and cannot exist
	    separately from what it is in" (1a24).
	  <LI>
	    This is a <B>cross-categorial</B> relation; things IN a subject are
	    non-substances; the things they are IN are substances: <B>non-substances
	    are </B>IN<B> substances</B>.
	</UL>
	<P>
	Examples:<BR>
	<OL>
	  <LI>
	    This grammatical knowledge is IN a soul.
	  <LI>
	    This white is IN a body.
	  <LI>
	    Color is IN body.
	    <P>
	</OL>
    </OL>
  <LI>
    <B>Universals and Particulars</B>
    <P>
    Although Aristotle does not use these <B>terms</B> in the <I>Categories</I>,
    it is clear that he intends to capture the notions of universal and particular
    with his SAID OF locution:<BR>
    <UL>
      <LI>
	A <B>universal</B> is what is SAID OF some subject
      <LI>
	A <B>particular</B> is what is not SAID OF any subject.
	<P>
	Note that there are universals and particulars in all the categories:<BR>
	<UL>
	  <LI>
	    Man and animal are universal substances.
	  <LI>
	    Callias and "this horse" are particular substances.
	  <LI>
	    White and color are universal qualities.
	  <LI>
	    "This white" is a particular quality.
	</UL>
	<P>
    </UL>
  <LI>
    <B>Category Trees</B>:
    <P>
    <OL type=a>
      <LI>
	Each category can be thought of as having a <B>tree structure</B>. The category
	itself can be divided into its fundamental kinds (e.g., <I>substance</I>
	can be divided into plants and animals). Each of these kinds can in turn
	be divided (e.g., <I>animal</I> can be divided into the various broad genera
	of animals). Each of these can in turn be divided into the fundamental species
	of the category in questions (e.g., into such basic kinds as <I>tiger</I>,
	and <I>horse</I>, and <I>human being)</I>. Finally, we can divide these
	lowest-level kinds into the basic individuals in the category (e.g., <I>human
	being</I> can be divided into Socrates, Callias, Coriscus, etc.).<BR>
      <LI>
	Similarly, the category of <I>quality</I> can be divided into subcategories
	such as <I>color</I>, which can in turn be divided into <I>red</I>,
	<I>green</I>, etc. Aristotle thinks that these specific qualities can be
	further divided into individuals (analogous to individual substances) such
	as <I>this individual bit of white</I>.<BR>
      <LI>
	Thus, each category is ultimately divisible into the <B>individual members</B>
	of that category.
	<P>
    </OL>
  <LI>
    The <B>fourfold division</B> (<I>Categories</I>, Ch. 2)
    <P>
    The SAID OF relation divides entities into universals and particulars; the
    IN relation divides them into substances and non-substances. Hence, the fourfold
    division at 1a20ff produces (in Aristotle's order of presentation):
    <P>
    <OL type=a>
      <LI>
	Universal substances
      <LI>
	Particular non-substances
      <LI>
	Universal non-substances
      <LI>
	Particular substances
    </OL>
    <P>
    The chart below summarizes the fourfold division of Aristotle's ontology:<BR>
    <TABLE BORDER CELLSPACING="6" CELLPADDING="6">
      <CAPTION ALIGN="Top">
	<BR>
	<FONT COLOR="Red"><B>The Ontology of Aristotle's
	<I>Categories</I></B></FONT>
      </CAPTION>
      <TR>
	<TD>(a) <B>SAID OF</B> a subject<BR>
	  &nbsp; &nbsp; &nbsp;<B>not IN</B> a subject<BR>
	  <BR>
	  &nbsp; &nbsp; man, horse, animal<BR>
	  <BR>
	  &nbsp; &nbsp; <I>Universal Substances</I></TD>
	<TD><BR>
	  (c) <B>SAID OF</B> a subject<BR>
	  &nbsp; &nbsp; &nbsp;<B>IN</B> a subject<BR>
	  <BR>
	  &nbsp; &nbsp; &nbsp;knowledge, white<BR>
	  <BR>
	  &nbsp; &nbsp; &nbsp;<I>Universal non-Substances</I><BR>
	</TD>
      </TR>
      <TR>
	<TD>(d) <B>not SAID OF</B> a subject<BR>
	  &nbsp; &nbsp; &nbsp;<B>not IN</B> a subject<BR>
	  <BR>
	  &nbsp; &nbsp; this man, <BR>
	  &nbsp; &nbsp; this horse<BR>
	  <BR>
	  &nbsp; &nbsp; <I>Individual Substances</I><BR>
	</TD>
	<TD>(b) <B>not SAID OF</B> a subject<BR>
	  &nbsp; &nbsp; &nbsp;<B>IN</B> a subject<BR>
	  <BR>
	  &nbsp; &nbsp; &nbsp;this knowledge of grammar, <BR>
	  &nbsp; &nbsp; &nbsp;this white<BR>
	  <BR>
	  &nbsp; &nbsp; &nbsp;<I>Individual non-Substances</I><BR>
	</TD>
      </TR>
    </TABLE>
    <P>
  <LI>
    <B>Cross-categorial predication</B>
    <P>
    <OL type=a>
      <LI>
	Predication within a category ("Socrates is a human," "a tiger is an animal,"
	"red is a color") involves <B>classifying</B> something (whether a particular
	or a universal) under some <B>higher</B> universal within the same category
	tree. Predication is a matter of classification.
      <LI>
	Cross-categorial predication ("Socrates is wise," "This horse is white")
	is more complicated. Here we are predicating an <B>accident</B> (something
	IN a subject) of a <B>substance</B> in which it inheres.
      <LI>
	Are such (accidental) predications still a matter of classification? Yes.
	But we are classifying something IN a substance, rather than the substance
	itself.
      <LI>
	Example: "This horse is white" classifies a particular bit of color, inhering
	in this horse, under the color-universal <I>white</I>.
      <LI>
	That is: <I>White</I> is SAID OF an individual bit of color that is IN this
	horse.
	<P>
    </OL>
  <LI>
    <B>Primary Substances: the basic individuals</B>
    <P>
    Things that are neither SAID OF nor IN any subject Aristotle calls "primary
    substances" (<I>protai ousiai</I>). <BR>
    Primary substances are fundamental in that "if they did not exist it would
    be impossible for any of the other things to exist" (2b5).
    <P>
  <LI>
    Aristotle's <B>argument for the ontological priority</B> of primary substances
    (2a34-2b7)
    <P>
    <OL type=a>
      <LI>
	Every secondary (universal) substance is predicated of (i.e., SAID OF) some
	primary substance or other.
      <LI>
	Every non-substance (whether universal or particular) is IN some primary
	substance or other.
      <LI>
	That is, everything other than primary substance is either SAID OF or IN
	primary substances.
      <LI>
	Therefore, if primary substances did not exist, neither would anything else.
	<P>
    </OL>
  <LI>
    <B>Comparison with Plato</B>
    <P>
    <OL type=a>
      <LI>
	The ontological priority is reversed. For Plato, particulars (participants
	in Forms) are the dependent entities; the Forms in which they participate
	are the independent entities. For Aristotle, it is individuals that are
	ontologically primary or basic.
      <LI>
	Aristotle has two kinds of predication relation; Plato's theory, although
	less clearly articulated, seems to have only one.
      <LI>
	The difference can be seen most clearly if we read Aristotle's
	<I>Categories</I> as a response to the <B>dilemma of participation</B> that
	Plato brings up in the <I>Parmenides</I>.
	<P>
    </OL>
  <LI>
    <A NAME="par11"><!-- --></A><B>The</B> <B>dilemma of participation</B>
    <P>
    <OL type=a>
      <LI>
	Here's Plato's presentation of the problem (<I>Parm</I>. 131a-c):
	<BLOCKQUOTE>
	  <I>Do you think, then, that the form as a whole - one thing - is in each
	  of the many ... so, being one and the same, it will be at the same time,
	  as a whole, in things that are many and separate, and thus it would be separate
	  from itself?</I>
	</BLOCKQUOTE>
	<P>
	Socrates replies with the suggestion that a Form may be "in" many things
	in the way that many people may all be covered with one sail. To which Parmenides
	replies:
	<BLOCKQUOTE>
	  <I>In that case, would the sail be, as a whole, over each person, or would
	  a part of it be over one person and another part over another?</I> ["A part,"
	  Socrates replies.] <I>So the forms themselves are divisible, Socrates, and
	  the things that partake of them would partake of a part; no longer would
	  a whole form, but only a part of it be in each thing.</I><BR>
	</BLOCKQUOTE>
      <LI>
	As Plato has Parmenides present the dilemma, both horns are unattractive:
	<P>
	<OL type=i>
	  <LI>
	    If the whole Form is in each participant, then the Form will be "separate
	    from itself," which seems impossible.
	  <LI>
	    If only a part of each Form is in each participant, then the Form will be
	    many, and not one, which also seems impossible. (An even more important problem,
	    that Plato does not mention: if two different participants have <B>two
	    different</B> things in them, what makes them have <B>one and the same
	    thing</B> in common? The theory will not explain what it is supposed to.)
	    <P>
	</OL>
      <LI>
	Aristotle's solution goes between the horns of this dilemma: it is not precisely
	correct to say that the whole universal is in each particular of which it
	is predicated, nor is it precisely correct to say that it is "only" a part
	of the universal that is in a given particular.<BR>
      <LI>
	On Aristotle's account: <I>white</I> (the genus or universal) is SAID OF
	the particular color that is IN this horse. So, for one and the same thing,
	whiteness, to be in both this horse and that horse (Plato's problem case)
	is just for the color of this horse and the color of that horse both to be
	classified as white. (<I>White</I> is SAID OF both of these two individual
	instances of color.) Whiteness is therefore in both horses without being
	"separate from itself" for it is just the common classification of the particular
	bits of color in them both.<BR>
      <LI>
	According to Aristotle, what is IN individual substances is, ultimately,
	individual. But just as individual substances can be classified under universals
	(like <I>horse</I> and <I>animal</I>), so too can the qualities, etc., of
	substances be classified under universals (like <I>white</I> and
	<I>color</I>).<BR>
      <LI>
	For more detail on this interpretation of Aristotle's <I>Categories</I> as
	a response to Plato's Dilemma of Participation, see Matthews and&nbsp;Cohen,
	"The One and the Many" p. 644, on reserve.
	<P>
    </OL>
  <LI>
    <B>Substances and Change</B>
    <P>
    In <I>Cat</I>. 5, Aristotle points out the hallmarks of substance, one of
    which is that substances are the <B>subjects that undergo change</B> ["Most
    characteristic of substance seems to be the fact that something one and the
    same in number can receive contraries" (4a10).]
    <P>
    Thus in the ontology of the <I>Categories</I>, substances are the
    <B>continuants</B> - the individuals that persist through change remaining
    one and the same in number. But, as we will see, Aristotle's investigation
    of the topic of change begins to exert pressure on this ontology. A claimant
    (viz., <B>matter</B>) will emerge to challenge the place of individual plants
    and animals as the basic subjects of predication and change.
</OL>
<P>
  <HR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif" WIDTH="20" HEIGHT="20">Go to next lecture
on <A HREF="archange.htm">Aristotle on Change</A>.<BR>
<BR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif" WIDTH="22" HEIGHT="22">Go to previous
lecture on <A HREF="tmalect.htm">Criticism of Forms</A><BR>
<BR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/reddot.gif" WIDTH="22" HEIGHT="22">
<A HREF="http://www.uh.edu/~cfreelan/courses/categories.html">Aristotle's
CATegories (with photo!), courtesy of Cynthia Freeland</A><BR>
<BR>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.<BR>
</UL>
<P>
<P>
</BODY></HTML>


