<!doctype html public "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
  <META NAME="GENERATOR" CONTENT="Internet Assistant for Word 1.0Z">
  <META NAME="AUTHOR" CONTENT="S. Marc Cohen">
  <META NAME="OPERATOR" CONTENT="S. Marc Cohen">
  <TITLE>Heraclitus lecture</TITLE>
</HEAD>
<BODY BGCOLOR="#fff2ee">
<H1 ALIGN=Center>
  <FONT SIZE=+1>Heraclitus</FONT>
</H1>
<P>
<B><FONT SIZE=+1>Introduction</FONT></B><BR>
<OL>
  <LI>
    Fl. 500 B.C. in Ephesus, north of Miletus in Asia Minor. He was known in
    antiquity as "the obscure." And even today, it is very difficult to be certain
    what Heraclitus was talking about. &nbsp;As Barnes says (<I>Presocratics</I>,
    p. 57): <BR>
    <BR>
    "Heraclitus attracts exegetes as an empty jampot wasps; and each new wasp
    discerns traces of his own favourite flavour."<BR>
    <BR>
    The reason for this: his dark and aphoristic style. He loved to appear to
    contradict himself. Even some of his doctrines sound incoherent and
    self-contradictory.<BR>
  <LI>
    One thing seems certain: Heraclitus had an extremely negative reaction to
    Milesian thought. For the Milesians, what is real is fixed and permanent;
    <B>change</B> somehow had to be explained away. They understood changes as
    alterations of some basic, underlying, material stuff which is, in its own
    nature, unchanging. Heraclitus reversed this: change is what is real. Permanence
    is only apparent.<BR>
  <LI>
    Heraclitus had a very strong influence on Plato. Plato interpreted Heraclitus
    to have believed that the material world undergoes constant change. He also
    thought Heraclitus was approximately correct in so describing the material
    world. Plato believed that such a world would be unknowable, and was thus
    driven to the conclusion that the material world was, in some sense, unreal,
    and that the real, knowable, world was immaterial.
</OL>
<P>
<B><FONT SIZE=+1>The unity of opposites</FONT></B><BR>
<OL>
  <LI>
    A number of fragments suggest that Heraclitus thought that opposites are
    really <B>one</B>.<BR>
    <BR>
    Main fragments: <I>RAGP</I> numbers <B>50</B>,<B> 60</B>,<B> 67</B>,<B> 83</B>,
    <B>86 </B>(= B61, B60, B88, B67, B62)<BR>
    <BR>
    See also: <B>70 </B>(=B111),<B> 75</B> (=B84a).<BR>
  <LI>
    What does this mean? Does Heraclitus think that hot = cold, that mortality
    = immortality, etc.? Does he think, in general, that each property that has
    an opposite is <B>identical to </B>its opposite?<BR>
    <BR>
    This is not likely. The fragments suggest, rather, that he thinks that
    <B>opposites may be compresent </B>or <B>coinstantiated.</B> That is, that
    one and the same thing may be both hot and cold, pure and polluted, etc.<BR>
  <LI>
    Barnes suggests that his view can be represented as a conjunction of the
    following two claims:<BR>
    <OL type = a>
      <LI>
	Every object instantiates at least one pair of contrary properties.
      <LI>
	Every pair of contrary properties is coinstantiated in at least one object.<BR>
    </OL>
  <LI>
    But Heraclitus's argument for this seems to be weak. He argues that sea-water
    can be both pure and polluted since it brings life to fish and death to humans.
    But&nbsp;if he thinks that sea-water is therefore both pure and polluted,
    full-stop,&nbsp;he has committed (what Barnes calls) the "fallacy of the
    dropped qualification." Sea-water is good <I>for fish</I> and bad <I>for
    humans</I>, but from this it does not follow that it is both good (simpliciter)
    and bad (simpliciter).<BR>
    <BR>
    Perhaps, however, Heraclitus was not committing this fallacy at all. He may
    have meant something more subtle, namely, that no object has any of its
    properties simpliciter, and that there is always (in some way, relative to
    something or other, with suitable qualifications) an element in it of the
    opposite property. &nbsp;It's hard to know for sure what he meant here.<BR>
  <LI>
    Of course it is true that opposites have to be understood in relation to
    one another, and that in this limited way a pair of opposites have a kind
    of unity. But that is less than Heraclitus claims.<BR>
  <LI>
    The unity of opposites that Heraclitus is talking about is perhaps better
    illustrated by his example of a bow or a lyre. Cf. fr. <B>46</B>=B51:<BR>
    <BR>
    <I>They do not understand how, though at variance with itself, it agrees
    with itself. It is a backwards-turning attunement like that of the bow and
    lyre.</I><BR>
    <BR>
    The point comes out more clearly in Freeman's (slightly less literal)
    translation:<BR>
    <BR>
    <I>They do not understand how that which differs with itself in is agreement:
    harmony consists of opposing tension, like that of the bow and the
    lyre.</I><BR>
    <BR>
    Here the tension between opposed forces - the string being pulled one way
    by one end of the bow and the other way by the other - enables the bow to
    perform its function, to be the kind of thing that it is. It <B>seems</B>
    static, but it is in fact dynamic. Beneath its apparently motionless exterior
    is a tension between opposed forces. Cf. KRS, 193:<BR>
    <BR>
    ... the tension in the string of a bow or lyre, being exactly balanced by
    the outward tension exerted by the arms of the instrument, produces a coherent,
    unified, stable and efficient complex. We may infer that if the balance between
    opposites were <I>not</I> maintained, for example if 'the hot' began seriously
    to outweigh the cold, or night day, then the unity and coherence of the world
    would cease, just as, if the tension in the bow-string exceeds the tension
    in the arms, the whole complex is destroyed.<BR>
    <BR>
    We should not be surprised to find this, for, as Heraclitus tells us, 'nature
    loves to hide' (<B>39</B>=B123) and 'An unapparent connection
    (<I>harmonia</I>) is stronger than an apparent one' (<B>47</B>=B54).<BR>
    <BR>
    Heraclitus ties these themes - the tension of the bow and the opposites -
    together beautifully, if somewhat metaphorically, in fragment <B>65</B>
    (=B48):<BR>
    <BR>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; The name of the bow (<I>bios</I>) is life
    (<I>bios</I>), but its work is death.<BR>
    <BR>
    [The accent is on different syllables in the two Greek words, but they are
    spelled the same.] The bow in tension represents the tension between opposites
    in conflict; the opposition is expressed metaphorically in the name of the
    bow, which means just the opposite of what the bow's work is.
</OL>
<P>
<B><FONT SIZE=+1>The <I>Logos</I></FONT></B><BR>
<OL>
  <LI>
    Heraclitus stresses the importance of (what he calls) "the <I>logos</I>".
    This term can have a variety of meanings: word, statement, reason, law, ratio,
    proportion, among others. (Barnes translates it as <I>account</I>.) It is
    related to the verb "to say" - a <I>logos</I> is something that is said.<BR>
  <LI>
    Fragments <B>1</B>, <B>2</B>, <B>44 </B>(=B1, B2, B50)<BR>
  <LI>
    If there is any special importance to be attached to Heraclitus's use of
    this term (Barnes thinks not - see <I>Presocratics</I> p. 59) it is this:<BR>
    <OL type=a>
      <LI>
	there is an orderly, law-governed process of change in the universe.
      <LI>
	The unity of diverse phenomena is to be found not in their matter, but in
	their <I>logos</I>. Indeed the very identity of an object depends not on
	the matter that composes it, but on the regularity and predictability of
	the changes it undergoes.
      <LI>
	The lyre (cf. above) is a good example of a <I>logos</I> in action. The orderly
	balance of opposed forces is what keeps the lyre functioning. The harmony
	of the lyre is an instance of the <I>logos</I>.
      <LI>
	Another good example in which the nature of a thing is given by its
	<I>logos</I>, and by the changes it undergoes, rather than by a list of its
	ingredients, is found in his discussion of the mixed drink that the Greeks
	called <I>kykeon</I> (here translated "posset") - a mixture of wine, barley
	and grated cheese (<B>76</B>=B125):<BR>
	<BR>
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;even the posset
	(<I>kykeon</I>) separates if it is not being stirred.<BR>
	<BR>
	His point is that the continued existence of a certain kind of thing depends
	on its undergoing continual change and movement. What makes something a posset
	is not just what it's made of (not just any collection of wine, barley, and
	cheese is a posset), but how it behaves, what kind of process it undergoes.<BR>
	<BR>
	In a way, then, the <I>logos</I> for something is rather like a recipe. That
	is, it is more than a list of ingredients. It includes an account of how
	they are put together, and how they interact.
    </OL>
</OL>
<P>
<B><FONT SIZE=+1>Puzzles about Identity and Persistence</FONT></B>
<P>
1. The puzzling doctrine for which Heraclitus is best known is reported by
Plato:
<BLOCKQUOTE>
  Heraclitus, you know, says that everything moves on and that nothing is at
  rest; and, comparing existing things to the flow of a river, he says that
  <B>you could not step into the same river twice</B> (<I>Cratylus</I> 402A).
</BLOCKQUOTE>
<P>
Plutarch, no doubt following Plato, also ascribes this idea to Heraclitus
(<B>62</B>=B91).
<P>
The idea is this: since the composition of the river changes from one moment
to the next, it is not the same (numerically the same) river for any length
of time at all.
<P>
Note that Plato thinks that Heraclitus uses the river as an <B>example</B>
of what he takes to be a general condition: <B>everything</B> is like a river
in this respect. That is, <B>nothing</B> retains its identity for any time
at all.
<P>
That is: <B>there are no persisting objects</B>.
<P>
Indeed, according to Plato and Aristotle, there was a follower of Heraclitus
who carried it even further:
<BLOCKQUOTE>
  Seeing that the whole of nature is in motion, and that nothing is true of
  what is changing, they supposed that it is not possible to speak truly of
  what is changing in absolutely all respects. For from this belief flowered
  the most extreme opinion of those I have mentioned - that of those who say
  they 'Heraclitize', and such was held by Cratylus, who in the end thought
  one should say nothing and only moved his finger, and reproached Heraclitus
  for saying that you cannot step in the same river twice - for he himself
  thought you could not do so even <B>once</B> (<I>Metaph.</I> 1010a7-15).]
</BLOCKQUOTE>
<P>
2. Did Heraclitus mean to say that there are no persisting objects? Not likely.
What Heraclitus actually said was more likely to have been this:
<BLOCKQUOTE>
  <I>Upon those who step into the same rivers, different and again different
  waters flow.</I> (<B>61</B>=B12)
</BLOCKQUOTE>
<P>
This sounds more like a genuine quotation from Heraclitus. It fits the pattern
of the "unity of opposites" fragments: suppose you step in the water of a
river. What you step in is both the <B>same</B> and <B>different</B>. So
the pair of contraries - same and different - are coinstantiated in the same
object.
<P>
And, once again, it exhibits Heraclitus' familiar tendency to appeal to different
qualifications when applying a pair of opposed concepts: what you step in
is different&nbsp;<B>water</B> but the same <B>river</B>.
<P>
Moreover, Plato's idea seems to get Heraclitus backwards. If Heraclitus thought,
as Plato suggests, that a compound object does not persist if its component
parts get replaced, then he would be making the <B>matter</B>, rather than
the orderly process of change, the <I>logos</I> of that object.
<P>
[For more on puzzles about identity and persistence, read about the famous
case of the <A HREF="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/theseus.html">Ship of Theseus</A>.]
<P>
<A NAME="Flux"><!-- --></A><B><FONT SIZE=+1>The Flux
Doctrine</FONT></B><BR>
<OL>
  <LI>
    This is the view that everything is constantly altering; no object retains
    all of its component parts, or all of its qualities or characteristics, from
    one moment to the next.
  <LI>
    Plato attributes the Doctrine of Flux to Heraclitus. And it is because he
    thought Heraclitus was a Fluxist that he thought Heraclitus denied that there
    were any persisting objects.
  <LI>
    But even if Heraclitus was a Fluxist (which is far from clear) it does not
    follow that he had to deny that there are persisting objects. If an object
    is more like a <B>process</B> than like a <B>static</B> thing, then one and
    the same object can endure even though it is undergoing constant change.<BR>
    <BR>
    Further, there are different degrees of Fluxism:<BR>
    <OL type=a>
      <LI>
	<B>Extreme fluxism: </B>The most extreme is: <B>at every moment, every object
	is changing in every respect</B>. Perhaps an extreme Fluxist is committed
	to the denial of persisting objects.
      <LI>
	<B>Moderate fluxism: </B>A less extreme version of Fluxism: <B>at every moment,
	every object is changing in some respect or other</B>. A proponent of this
	less extreme Flux doctrine could well allow for the persistence of objects
	through time.<BR>
    </OL>
  <LI>
    Heraclitean Fluxism<BR>
    <OL type=a>
      <LI>
	It is unlikely that Heraclitus was an extreme fluxist. His discussions of
	change in general, and the river fragments in particular, suggest that he
	thought that change and permanence could co-exist, that is, that an object
	could persist in spite of continually undergoing change <B>in some respect
	or other</B>.
      <LI>
	If you step in the same river, you step in different waters: the river is
	still (numerically) the same river even though it has changed (compositionally),
	in that <B>it </B>(the same river) is now composed of different waters.
      <LI>
	So it is unlikely that Heraclitus denied that there are persisting objects.
    </OL>
</OL>
<P>
  <HR>
<P>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif" WIDTH="20" HEIGHT="20">Go to next lecture
on <A HREF="parm1.htm">Parmenides, Stage I</A><BR>
<BR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif" WIDTH="22" HEIGHT="22">Go to previous
lecture on <A HREF="anaximen.htm">Anaximenes</A>
<P>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif" WIDTH="22" HEIGHT="22">For more on
Heraclitus' epistemology, see James Lesher's <A HREF="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/Lesher.html">Presocratic
Contributions to the Theory of Knowledge</A>.
<P>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.<BR>
</UL>
</BODY></HTML>


