<!doctype html public "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
  <META NAME="GENERATOR" CONTENT="Internet Assistant for Word 1.0Z">
  <META NAME="AUTHOR" CONTENT="S. Marc Cohen">
  <META NAME="OPERATOR" CONTENT="S. Marc Cohen">
  <TITLE>Socratic Definitions</TITLE>
</HEAD>
<BODY BGCOLOR="#baccd3">
<H1 ALIGN=Center>
  Socratic Definitions
</H1>
<H2>
  I. The "What is <I>X</I>?" Question
</H2>
<OL>
  <LI>
    Socrates asked a simple kind of question that revolutionized philosophy:
    "<B>What is it</B>?"
  <LI>
    Usually raised about significant moral or aesthetic qualities (e.g., justice,
    courage, wisdom, temperance, beauty).
  <LI>
    Such questions are the central concern of the "Socratic" (early) dialogues
    of Plato.
  <LI>
    A so-called "Socratic definition" is an <B>answer</B> to a "What is
    <I>X</I>?" question.
  <LI>
    Socratic definitions are not of words, but of <B>things</B>. Socrates does
    not want to know what the word 'justice' means, but what the <B>nature</B>
    of justice itself is.
  <LI>
    A correct Socratic definition is thus a true description of the essence of
    the thing to be defined. I.e., definitions can be true or false.
</OL>
<H2>
  II. The Importance of Socratic Definitions
</H2>
<H3>
  A. They are objective.
</H3>
<OL>
  <LI>
    Socrates was opposed to the moral relativism of the Sophists.
  <LI>
    He believed that there were objective moral standards; that they could be
    <B>discovered</B>; that there were right and wrong answers to moral questions
    that went beyond mere opinion and popular sentiment.
</OL>
<H3>
  B. They are fundamental for knowledge.
</H3>
<OL>
  <LI>
    Socrates claims that until you know what a thing is, you can't answer any
    other questions about it.
  <LI>
    So any inquiry into any moral question presupposes an answer to the relevant
    "What is <I>X</I>?" question. Not just that there <B>is</B> such an answer,
    but that the inquirer is in possession of it.
  <LI>
    E.g., in the <I>Meno</I>, Socrates claims that you cannot answer a question
    <B>about</B> virtue ("Can it be taught?") until you have answered a more
    fundamental question: "What <B>is</B> it?"
  <LI>
    In general, he thought that a person's having knowledge involving a concept,
    <I>X</I>, depends upon his knowing the correct answer to the "What is
    <I>X</I>?" question.
</OL>
<H3>
  C. They are fundamental for morality.
</H3>
<OL>
  <LI>
    He thought that the possibility of morality (moral character, moral behavior)
    depended on knowledge of definitions.
  <LI>
    Virtue is knowledge: if you know what is right, you will do what is right.
    Knowing a Socratic definition is thus (apparently) necessary and sufficient
    for moral behavior.
</OL>
<H2>
  III. The Objectivity of Definitions
</H2>
<P>
The definition of a moral quality is not a matter of what people
<B>think</B>. You cannot determine what goodness, or justice, or piety, is
by conducting a poll. Consequently, whether something or someone <B>has</B>
a given moral quality is also not a matter of mere opinion. Whether an act
or a person is good, or just, or pious, for example, is not to be settled
by a vote.
<P>
The <I>Euthyphro</I> gives us a good example of Socrates' belief that moral
qualities are <B>real</B>, not conventional.
<P>
Euthyphro suggests that piety can be defined as <I>what the gods all love
</I>(9e). &nbsp;Socrates objects. Even if all the gods agree about which
things are pious, that doesn't tell us what piety is. (Even a poll of the
gods is just a lot of opinions.) &nbsp;He gets Euthyphro to admit that it
is not because they are loved by the gods that things are pious. Rather,
they are loved by the gods <B>because they are pious</B>.
<P>
So piety cannot be <B>defined</B> as <I>being god-loved</I>. For if it were
to be so defined, since Euthyphro admits that:
<BLOCKQUOTE>
  the gods love pious things because they are pious
</BLOCKQUOTE>
<P>
he would also have to accept (substituting 'god-loved' for 'pious') that
<BLOCKQUOTE>
  the gods love pious things because they are god-loved.
</BLOCKQUOTE>
<P>
But this Euthyphro rightly denies. For it would lead to circularity. The
gods cannot love things because they love them. That would make their love
whimsical and without foundation. If the gods love something because it is
pious, then its being pious must be something independent of their loving
it - something independent of opinion - something objective.
<P>
Another way of putting the point: moral qualities are not like such qualities
as <I>fame</I> or <I>popularity</I>. A thing is popular just because people
like it. If you ask them why they like it, they may have their reasons: because
it's bright, or flashy, or durable, or economical, or beautiful, etc. But
someone who answers "I like it because it's popular" is making some kind
of mistake. For he seems to have no reason for liking it other than the fact
that most other people like it. But what reason do <B>they</B> have?
<P>
If their reason is the same as his, they may all be making a huge mistake.
They all agree with one another in admiring it, but there's nothing
<B>about</B> it they admire. &nbsp;If they have some other reason, then his
reason seems to depend on theirs. His liking it because they like it is
rationally justifiable only to the extent that their reason for liking it
is a good one.
<P>
Euthyphro's proposed definition leads to something like (what I'll call)
the <B>Zsa Zsa Gabor paradox</B>:
<BLOCKQUOTE>
  Zsa Zsa is famous. She appears on talk shows, and everyone knows who she
  is. But what does she <B>do</B>? What is she famous <B>for</B>? As the joke
  goes, she's famous for being famous.
  <P>
  But that's just to say that there really isn't any reason for Zsa Zsa to
  be famous. We're all making some kind of mistake to pay any attention to
  her. Likewise, if piety were being god-loved, the gods would all be making
  a mistake in admiring an act for its piety. For they would be admiring nothing
  other than their own admiration.
</BLOCKQUOTE>
<H2>
  IV. Faulty Definitions
</H2>
<P>
There are two ways definitions can go wrong.
<H3>
  A. Problems with form
</H3>
<UL>
  <LI>
    Merely citing an instance, or a list of instances, rather than giving a general
    formula, or description.
  <LI>
    Example at <I>Meno</I> 74c-d: objection to defining shape as
    <I>roundness</I>, or color as <I>white</I> or as <I>white +</I> a list of
    other colors.
</UL>
<H3>
  B. Problems with content
</H3>
<P>
Giving a description that does not capture the right class of instances.
The description may be
<H4>
  1. <B>Too broad</B>
</H4>
<UL>
  <LI>
    I.e., it gives a <B>necessary</B> but not a sufficient condition. E.g., defining
    "brother" as "sibling".
  <LI>
    Example at <I>Meno</I> 73d: defining virtue as <I>the ability to rule</I>.
    (Includes tyrants who rule unjustly.)
</UL>
<H4>
  2. <B>Too narrow</B>
</H4>
<UL>
  <LI>
    I.e., it gives a <B>sufficient</B> but not a necessary condition. E.g., defining
    "brother" as "unmarried male sibling".
  <LI>
    Example at <I>Meno</I> 71e: defining virtue as <I>managing public affairs</I>.
    (Leaves out virtuous children.)
</UL>
<P>
Note that a definition may be both too broad and too narrow, i.e., it may
admit instances that it should exclude, and exclude instances that it should
admit. E.g., defining "brother" as "unmarried sibling". This condition is
neither sufficient for being a brother (it includes some sisters, who should
be excluded) nor necessary for being a brother (it excludes married brothers).
<H2>
  V. Conditions for a Correct Definition
</H2>
<P>
Some jargon. We'll call the term to be defined the <I>definiendum</I>, and
the term that is offered to define it the <I>definiens</I>. We can then reserve
the term <I>definition</I> for the whole formula defining the
<I>definiendum</I> in terms of the <I>definiens</I>. Thus, in the definition
'A brother is a male sibling' (or, 'brother =<SUB>df</SUB> male sibling'),
'brother' is the <I>definiendum</I> and 'male sibling' is the
<I>definiens</I>.
<H3>
  A. The "application" requirement
</H3>
<P>
This requirement is simply that the <I>definiens</I> neither be too broad
nor too narrow. &nbsp;The <I>definiens</I> must provide (materially) necessary
and sufficient conditions. I.e., the proposed <I>definiens</I> should apply
to the right things, viz. exactly the things that the <I>definiendum</I>
applies to. To use some logical jargon: the <I>definiens</I> should
be&nbsp;<B>extensionally equivalent</B> to the <I>definiendum</I>:
<BLOCKQUOTE>
  <I>X</I> =<SUB>df<I> </I></SUB><I>ABC</I> only if all instances of <I>X</I>
  are instances of <I>ABC</I>, and all instances of <I>ABC</I> are instances
  of <I>X</I>.
</BLOCKQUOTE>
<P>
Most definitions found faulty in the dialogues fail the application requirement
- the proposed <I>definiens</I> turns out not to be extensionally equivalent
to the <I>definiendum</I>. But extensional equivalence by itself is not enough.
<H3>
  B. The "explanatory" requirement
</H3>
<P>
At <I>Euthyphro</I> 11a-b Socrates agrees that piety is loved by all the
gods, and that what all the gods love is pious, but still objects to defining
piety as <I>what all the gods love</I>. His objection is that it is not
<B>because</B> it is loved by the gods that a pious thing is pious.
<P>
This suggests an additional requirement, that the <I>definiens</I> should
in some way <B>explain</B> the <I>definiendum</I>:
<BLOCKQUOTE>
  <I>X</I> =<SUB>df<I> </I></SUB><I>ABC</I> only if all instances of <I>X</I>
  are so <B>because</B> they have characteristics <I>ABC</I>.
</BLOCKQUOTE>
<P>
These two necessary conditions are probably jointly sufficient:
<BLOCKQUOTE>
  <FONT COLOR="Red"><I>X</I> =<SUB>df</SUB> <I>ABC</I> iff (1) all instances
  of <I>X</I> are instances of <I>ABC</I>, and all instances of <I>ABC</I>
  are instances of <I>X</I>, and (2) all instances of <I>X</I> are instances
  of <I>X</I> <B>because</B> they have characteristics <I>ABC</I>.</FONT>
</BLOCKQUOTE>
<H2>
  VI. The Relations between Definitions and Instances
</H2>
<P>
How does one arrive at a definition? Socrates' method is to examine particular
cases, reworking his definition as he goes, until (if ever) he gets it right.
<P>
But how can he tell, in a particular case, whether he actually <B>has</B>
an instance to which the definition applies? For he maintains that you cannot
know anything about, e.g., virtue, until one knows what virtue <B>is</B>.
But if I know that reading <I>War and Peace</I> is virtuous, don't I know
something about virtue, viz., that reading <I>War and Peace</I> is an instance
of it?
<P>
This is a problem for Socrates: how can one <B>recognize</B> an instance
of <I>X</I> as such when one doesn't yet know what <I>X</I> is? Call this
"The problem of recognizing instances." (It's the first half of the general
recognition problem, of which more below.)
<H2>
  VII. Why Can't Enumerations be Definitions?
</H2>
<H3>
  A. Practical objections
</H3>
<P>
To list all the instances of the <I>definiendum</I> might produce something
practically unworkable.
<P>
E.g., suppose you were presented with a list of all the brothers in the world.
How could you tell what one thing they are all instances of? You might not
even have time to go through the entire list.
<H3>
  B. Theoretical objections
</H3>
<H4>
  1. A complete list of instances is not always possible.
</H4>
<P>
E.g., try to define "even number" by enumeration. You can't give a complete
list.
<BLOCKQUOTE>
  What about a partial list, with dots . . . . ? E.g.: 2, 4, 6, 8, . . . .
</BLOCKQUOTE>
<P>
But what tells you the right way of continuing the enumeration? We all "know"
that the next number is 10. But that's because we infer that the principle
behind the enumeration is to list all the even numbers. Still, why can't
the next number be 2? A fuller enumeration might be:
<BLOCKQUOTE>
  2, 4, 6, 8, 2, 4, 6, 8, . . . .
</BLOCKQUOTE>
<P>
(Cf. Wittgenstein on "knowing how to go on", <I>Philosophical
Investigations</I>, &#167;&#167;151-155.)
<H4>
  2. A complete list of instances is not necessary.
</H4>
<P>
It is possible to know the definition without being able to produce a complete
enumeration of its instances. (E.g., you know what it is to be a brother
even though you don't know who all the brothers are.)
<H4>
  3. A complete list of instances is not sufficient.
</H4>
<P>
Even a complete enumeration of instances may not determine a single definition.
That is, there may be two or more logically distinct definitions, incompatible
with one another, but each of which is compatible will all of the instances.
<H2>
  VIII. The Search for Definitions: the Recognition Problem
</H2>
<P>
This is an epistemological problem, similar to the problem of recognizing
instances. This time, the problem is:
<BLOCKQUOTE>
  how can you recognize when a proposed definition is the correct one?
</BLOCKQUOTE>
<P>
This is the problem of recognizing the correct <I>definiens</I>. The correct
<I>definiens</I> is the one that applies to all and only the instances of
the <I>definiendum</I>, and for the right reason. So to recognize the correct
<I>definiens</I>, we have to be able to recognize the instances. But we can't
do this, according to Socrates, until we <B>know</B> what the definition
is.
<P>
Plato is aware of this problem. It arises in the <I>Meno</I> at 80d-e, in
the form of "Meno's Paradox", or "The Paradox of Inquiry".
<P>
The argument can be shown to be sophistical, but Plato took it very seriously.
This is obvious, since his response to it is to grant its central claim:
that you can't come to know something that you didn't already know. That
is, that inquiry never produces new knowledge, but only recapitulates things
already known. &nbsp;This leads to the famous Doctrine of Recollection, to
which we now turn.
<P>
  <HR>
<P>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif" WIDTH="20" HEIGHT="20">Go to next lecture
on <A HREF="menopar.htm">Meno's Paradox</A><BR>
<BR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif" WIDTH="22" HEIGHT="22">Go to previous
lecture on <A HREF="atomism.htm">Atomism</A>
<P>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.<BR>
</UL>
</BODY></HTML>


