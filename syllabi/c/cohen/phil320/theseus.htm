<HTML>
<HEAD>
  <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=windows-1252">
  <META NAME="Generator" CONTENT="Microsoft Word 97">
  <TITLE>Identity, Persistence, and the Ship of Theseus</TITLE>
</HEAD>
<BODY BGCOLOR="#ade1eb">
<H2 ALIGN=Center>
  Identity, Persistence, and the Ship of Theseus
</H2>
<P>
1. Heraclitus's "river fragments" raise puzzles about identity and persistence:
under what conditions does an object <B>persist through time</B> as one and
the same object?
<P>
2. If the world contains things which endure, and retain their identity in
spite of undergoing alteration, then somehow those things must <B>persist</B>
through <B>changes</B>.
<P>
3. Heraclitus wonders whether one can step into the same river twice precisely
because it continually undergoes changes. In particular, it changes
<B>compositionally</B>. At any given time, it is made up of different component
parts from the ones it was previously made up of. So, according to one
interpretation, Heraclitus concludes that we do not have (numerically) the
same river persisting from one moment to the next.
<P>
4. Plato is probably the source of this "paradoxical" interpretation of
Herclitus. According to Plato, Heraclitus maintains that nothing retains
its identity for any time at all:
<BLOCKQUOTE>
  "Heraclitus, you know, says that everything moves on and that nothing is
  at rest; and, comparing existing things to the flow of a river, he says that
  you could not step into the same river twice" (<I>Cratylus</I> 402A).
</BLOCKQUOTE>
<P>
But what Heraclitus actually said was more likely to have been this:
<BLOCKQUOTE>
  "On those who enter the same rivers, ever different waters flow." (fr. 12)
</BLOCKQUOTE>
<P>
On Plato's interpretation, it's not the same river, since the waters are
different. &nbsp;On a less paradoxical interpretation, it <B>is</B> the same
river, in spite of the fact that the waters are different.
<P>
5. On both interpretations of Heraclitus, he holds the <B>Flux Doctrine</B>:
Everything is constantly altering; no object retains all of its component
parts from one moment to the next.
<P>
The issue is: what does Flux entail about identity and persistence? Plato&#146;s
interpretation requires that Heraclitus held what might be called the
<B>Mereological Theory of Identity</B> (<B>MTI</B>), i.e., the view that
the identity of an object depends on the identity of its component parts.
This view can be formulated more precisely as follows:
<BLOCKQUOTE>
  For any compound objects, <I>x</I> and <I>y</I>, <I>x</I> = <I>y</I> only
  if every part of <I>x</I> is a part of <I>y</I>, and every part of <I>y</I>
  is a part of <I>x</I>.
</BLOCKQUOTE>
<BLOCKQUOTE>
  I.e., an object <I>continues</I> to exist (from time <I>t</I><SUB>1</SUB>
  to time <I>t</I><SUB>2</SUB>) only if it is composed of all the same components
  at <I>t</I><SUB>2</SUB> as it was composed of at <I>t</I><SUB>1</SUB>. Sameness
  of parts is a <B>necessary condition </B>of identity.
</BLOCKQUOTE>
<P>
6. It now seems that if we want to allow that an object can persist through
time in spite of a change in some of its components, we must deny MTI. An
object <I>x</I>, existing at time <I>t</I><SUB>1</SUB>, can be numerically
identical to an object <I>y</I>, existing at time <I>t</I><SUB>2</SUB>, even
though <I>x</I> and <I>y</I> are not composed of exactly the same parts.
<P>
But once you deny MTI, where do you draw the line? Denying MTI leaves us
vulnerable to puzzle cases.
<P>
7. <B>The Ship of Theseus</B>:
<P>
This is a puzzle that has been around since antiquity, probably later than
Heraclitus, but not much later. It first surfaces in print in Plutarch (<I>Vita
Thesei</I>, 22-23):
<BLOCKQUOTE>
  "The ship wherein Theseus and the youth of Athens returned had thirty oars,
  and was preserved by the Athenians down even to the time of Demetrius Phalereus,
  for they took away the old planks as they decayed, putting in new and stronger
  timber in their place, insomuch that this ship became a standing example
  among the philosophers, for the logical question of things that grow; one
  side holding that the ship remained the same, and the other contending that
  it was not the same."
</BLOCKQUOTE>
<P>
Plutarch tells us that the ship was exhibited during the time [i.e., lifetime]
of Demetrius Phalereus, which means ca. 350-280 BCE. (Demetrius was a well-known
Athenian and a member of the Peripatetic school, i.e., a student of Aristotle.
He wrote some 45 books, and was also a politician).
<P>
The original puzzle is this: over the years, the Athenians replaced each
plank in the original ship of Theseus as it decayed, thereby keeping it in
good repair. Eventually, there was not a single plank left of the original
ship. So, did the Athenians still have one and the same ship that used to
belong to Theseus?
<P>
But we can liven it up a bit by considering two different, somewhat modernized,
versions. On both versions, the replacing of the planks takes place while
the ship is at sea. We are to imagine that Theseus sails away, and then
systematically replaces each plank on board with a new one. (He carries a
complete supply of new parts on board as his cargo.) Now we can consider
these two versions of the story:
<BLOCKQUOTE>
  a. Version One: Theseus completely rebuilds his ship, replaces all the parts,
  throws the old ones overboard. Does he arrive on the same ship as the one
  he left on? Of course it has <I>changed</I>. But is it <I>it</I>?
  <BLOCKQUOTE>
    Let A = the ship Theseus started his voyage on.<BR>
    Let B = the ship Theseus finished his voyage on.
  </BLOCKQUOTE>
  <P>
  Our question then is: Does A = B? If not, why not? Suppose he had left one
  original part in. Is that enough to make A identical to B? If not, suppose
  he had left two, etc. Where do you draw the line?
</BLOCKQUOTE>
<BLOCKQUOTE>
  b. Version Two: Like the first version, except that following Theseus in
  another boat is the Scavenger, who picks up the pieces Theseus throws overboard,
  and uses them to rebuild his boat. The Scavenger arrives in port in a ship
  composed of precisely the parts that composed the ship Theseus started out
  in. He parks his ship right next to one that Theseus parked.
  <P>
  Now we have:
  <P>
  C = the ship the Scavenger finished his voyage on.
</BLOCKQUOTE>
<BLOCKQUOTE>
  Our problem is to sort out the identity (and non-identity) relations among
  A, B, and C. The only "obvious" fact is that B
  <FONT FACE="Symbol" SIZE=4>&#185;</FONT> C (after all, they are berthed side
  by side in the harbor, so they can hardly be one and the same ship!). Beyond
  that, there are two alternatives:
  <BLOCKQUOTE>
    a. MTI tells us that A = C. The ship on which Theseus started his voyage,
    namely A, is identical to the ship on which the Scavenger finished his voyage,
    namely C. So we have two ships: one (A) that was sailed out by Theseus and
    (C) sailed in by the Scavenger, and another one (B) that was created (out
    of new parts) during the voyage and was sailed into port by Theseus.
  </BLOCKQUOTE>
  <BLOCKQUOTE>
    b. The alternative is to abandon MTI and hold that A = B. On this account,
    we still have two ships, but their identity and non-identity relations are
    different: one ship (A) was sailed out by Theseus and (B) sailed in by Theseus,
    and another one (C) was created (out of used parts) during the voyage and
    was sailed into port by the Scavenger.
  </BLOCKQUOTE>
  <P>
  Unfortunately, both alternatives lead to unintuitive consequences.
</BLOCKQUOTE>
<BLOCKQUOTE>
  a. The problem with alternative (a) is that it requires Theseus to have changed
  ships during the voyage. For he ends up on B, which is clearly not identical
  to C. But Theseus never once got off his ship during its entire voyage: Theseus
  got on board a ship (A), sailed a voyage during which he never got off the
  ship, and arrived at his destination in a ship (B). He was on just one ship
  during the whole process, but alternative (a) seems to require that he was
  on (at least!) two different ships.
</BLOCKQUOTE>
<BLOCKQUOTE>
  b. The problem with alternative (b) is that in holding that A = B and admitting
  (as it must) that B <FONT FACE="Symbol" SIZE=4>&#185;</FONT> C , it must
  also hold that A <FONT FACE="Symbol" SIZE=4>&#185;</FONT> C . Yet every part
  of A is a part of C, and every part of C is a part of A! So A and C are two
  different ships even though their parts are the same; and what of A and B?
  They have no parts in common, and yet A and B are <I>the same ship</I>.
  <P>
  These results seem as paradoxical as the view that there are no persisting
  objects.
</BLOCKQUOTE>
<P>
8. <B>Conclusion</B>: MTI seems too strong. It denies identity to objects
that we think of as persisting through time. But that leaves us with some
problems:
<BLOCKQUOTE>
  a. What do we replace it with? <I>Spatio-temporal continuity</I> (the intuition
  behind our alternative (b), above) is the most promising (and common) suggestion.
  A persisting object must trace a continuous path through space-time. And
  tracing a continuous path is compatible with a change of parts, so long as
  the change is gradual and the form or shape of the object is preserved through
  the changes of its component materials. So it appears that we can replace
  MTI with the theory of <I>spatio-temporal continuity</I> (STC).
  <P>
  b. But STC is also problematic. For it is easy to imagine cases in which
  our intuitions tell us that we have numerical identity without spatio-temporal
  continuity. Consider that an object can be disassembled and then reassembled.
  (Think of a bicycle that is taken apart. The parts are then placed in a number
  of separate boxes, which are then shipped, separately, across country. The
  boxes are then unpacked and the bicycle is reassembled.) How do we account
  for its identity? STC breaks down in this case, for there is no continuously
  existing bicycle-shaped object tracing a smooth path through space-time.
  But MTI gives us the right result: the reassembled bicycle is made of exactly
  the same parts as the one that was taken apart, and so is numerically the
  same bicycle.
</BLOCKQUOTE>
<P>
We are still struggling with Heraclitus's puzzle.
<P>
  <HR>
<P>
<IMG ALIGN="Middle" WIDTH="20" HEIGHT="20" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif">Go to next lecture
on <A HREF="parm1.htm">Parmenides, Stage I</A><BR>
<BR>
<IMG ALIGN="Middle" WIDTH="22" HEIGHT="22" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif">Go back to
lecture on <A HREF="heracli.htm#Flux">Heraclitus</A>
<P>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.
</UL>
</BODY></HTML>


