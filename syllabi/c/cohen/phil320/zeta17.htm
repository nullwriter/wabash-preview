<!doctype html public "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
  <META NAME="GENERATOR" CONTENT="Internet Assistant for Word 1.0Z">
  <META NAME="AUTHOR" CONTENT="S. Marc Cohen">
  <TITLE>Substance, Matter, and Form</TITLE>
</HEAD>
<BODY BACKGROUND="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/blutext.gif">
<H1 ALIGN=Center>
  Aristotle on Substance, Matter, and Form<BR>
</H1>
<OL>
  <LI>
    Matter underlies and persists through substantial changes. A substance is
    generated (destroyed) by having matter take on (lose) form.
    <P>
    <OL type=a>
      <LI>
	A house is created when bricks, boards, etc., are put together according
	to a certain plan and arranged in a certain form. It is destroyed when the
	bricks, boards, etc., lose that form.
	<P>
      <LI>
	An animal is generated when matter (contributed by the mother) combines with
	form (contributed by the father).
	<P>
    </OL>
  <LI>
    This suggests that the primary substances of the <I>Categories</I>, the
    individual plants and animals, are, when analyzed, actually compounds of
    form and matter. And in the <I>Metaphysics</I>, Aristotle suggests that a
    compound cannot be a substance (Z3, 1029a30).
    <P>
  <LI>
    This may seem a strange move for Aristotle to be making. But the idea may
    be this: a compound cannot be a basic ontological ingredient. Cf. these
    compounds:
    <P>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;a brown horse<BR>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;a scholar<BR>
    <P>
    Each of these is a compound of substance + attribute:<BR>
    <BR>
    &nbsp; &nbsp; &nbsp; &nbsp; a brown horse = a horse + brownness<BR>
    &nbsp; &nbsp; &nbsp; &nbsp; a scholar = a human + education<BR>
    <P>
    In these cases, the compound is a compound of entities that are more basic.
    ("A scholar is not an ontologically basic item in the world -- a scholar
    is just a human with a liberal education.")
    <P>
  <LI>
    If then primary substance (in the <I>Metaphysics</I> conception of primary
    substance) cannot be a form-matter compound, what is primary substance? The
    possibilities seem to be: <B>matter</B> and <B>form</B>. (Aristotle actually
    discusses more possibilities -- this is a simplification.)
    <P>
  <LI>
    In Z3, Aristotle considers the claim of matter to be substance, and rejects
    it. Substance must be <B>separable</B> and a <B>this something</B> (usually
    translated, perhaps misleadingly, as "an individual").
    <P>
    <OL type=a>
      <LI>
	<B>Separable</B>: to be separable is to be nonparasitic. Qualities, and other
	non-substances of the <I>Categories</I>, are not separable. They only exist
	<B>in</B> substances. Separability, then, amounts to <B>independent
	existence</B>.
	<P>
      <LI>
	<B>This something</B>: [there is much dispute over what Aristotle means by
	this odd locution] "Individual" comes close, except for the suggestion that
	only a primary substance of the <I>Categories</I> could count as a "this
	something". Perhaps an individual plant or animal counts as a this something,
	but perhaps other things do, too. For Aristotle seems to count form as, in
	some way, a this something (e.g., H1, 1042a28). &nbsp;But, as a rough gloss,
	<B>individuality</B> seems to be what is at issue.
	<P>
      <LI>
	Now it may seem puzzling that matter should be thought to fail the
	"separability/individuality" test. For:
	<P>
	<OL type=i>
	  <LI>
	    Separability: It seems that the matter of a compound is capable of existing
	    separately from it. (The wood of which a tree is composed can continue to
	    exist after the tree has ceased to exist.)
	    <P>
	  <LI>
	    Individuality: We can certainly pick out a definite, particular, batch of
	    matter as a singular object of reference: "the quantity of wood of which
	    this tree is composed at this time."
	    <P>
	</OL>
      <LI>
	But perhaps Aristotle's point is not that matter is neither separable nor
	individual; all he is committed to saying is that matter fails to be
	<B>both</B> separable and individual.
	<P>
	<OL type=i>
	  <LI>
	    Separability: Separate from a substance, matter fails to be a this. It owes
	    what individuality it has to the substance it is the matter of. (What makes
	    this quantity of wood one thing is that it is the wood composing this one
	    tree.)
	    <P>
	  <LI>
	    Individuality: Considered as an individual (a "this something"), matter fails
	    to be separate from substance. (This batch of wood no longer has any unity
	    once it no longer composes the tree it used to be the matter of - unless
	    it now happens to be the matter of some other substance that gives it its
	    unity.)
	    <P>
	</OL>
    </OL>
  <LI>
    So matter cannot <B>simultaneously</B> be both separable and individual,
    and therefore matter cannot be substance. The only remaining candidate for
    primary substance seems to be <B>form</B> (which Aristotle now begins to
    call <B>essence</B>). &nbsp;It is clear that Aristotle is now focusing on
    the concept of the substance of something - i.e., what it is about an individual
    plant or animal (what the Categories called a "primary substance") that makes
    it a self-subsistent, independent, thing. Some evidence:
    <P>
    <OL type=a>
      <LI>
	Z.3, 1029a30: "the substance composed of both - I mean composed of the matter
	and the form - should be set aside &#133; we must, then, consider the third
	type of substance [the form], since it is the most puzzling."
	<P>
      <LI>
	Z.6, 1031a16: "a given thing seems to be nothing other than its own substance,
	and something's substance is said to be its essence."
	<P>
      <LI>
	Z.11, 1037a6: "it is also clear that the soul is the primary substance, the
	body is matter, and man or animal is composed of the two as universal. As
	for Socrates or Coriscus, if &lt;Socrates'&gt; soul is also Socrates, he
	is spoken of in two ways; for some speak of him as soul, some as the compound."
	<P>
      <LI>
	Z.17, 1041a9: "substance is some sort of principle and cause &#133;"
	<P>
    </OL>
  <LI>
    Does Aristotle's view that substance is <B>form</B> or <B>essence</B> make
    him a Platonist? Most commentators think not, but for different reasons.
    <P>
    <OL type=a>
      <LI>
	Some think that the kind of essence or form that Aristotle counts as primary
	substance is one that is not in any way universal; a form that is as individual
	as the compound whose form it is. (Thus, Socrates and Callias would each
	have his own distinct individual form - there would be as many individual
	human forms as there are humans.)
	<P>
      <LI>
	<A NAME="text1"><!-- --></A>Others think that the "individual forms" solution
	is not to be found in Aristotle, and is anyway (for other
	reasons<SUP><A HREF="zeta17.htm#Note1">1</A></SUP>) unavailable to him. On their view,
	the primary substance of the <I>Metaphysics</I> is <B>species form</B> -
	something that is common to different members of the same species, but is
	still, in some plausible sense, an individual ("this something").
	<P>
    </OL>
  <LI>
    Z17 seems to chart a course about substance that is anti-Platonic but does
    not (so far as I can tell) decide between the individual-form and species-form
    interpretations of Aristotle's doctrine. The main ideas:
    <P>
    <OL type=a>
      <LI>
	The individual substances of the <I>Categories</I> are, indeed, compounds
	of matter and form, <B>but</B>
	<P>
      <LI>
	They are not just heaps, or piles, of components.
	<P>
      <LI>
	Rather, they're like syllables.
	<P>
	That is, they're not just unstructured collections of elements, but have
	a structure that is essential to their being what they are. The syllables
	<B>BA</B> and <B>AB</B> are different, but they are the same collection of
	components - they have the same "matter".
	<P>
      <LI>
	Structure or form is not just an ingredient (or what Aristotle here calls
	an "element") in the compound.
	<P>
	[Aristotle offers an infinite regress argument for this: if the structure
	of a compound (e.g., a syllable) were just another component (along with
	the letters) then the whole compound would just be a heap. (E.g., the syllable
	<B>BA</B> would be a collection consisting of two letters and one structure.
	But a structure considered by itself, as an element, is not the structure
	<B>of</B> the syllable. The syllable <B>BA</B> consists of two elements
	structured in a certain way; it isn't an unstructured collection of three
	things, one of which is a thing called a <I>structure</I>.]
	<P>
      <LI>
	So substance is the <B>structure</B> or <B>form</B> of a compound of matter
	and form (i.e., of a plant or an animal). At the end of Z.17, Aristotle describes
	substance, in this sense, in three ways:
	<P>
	<OL>
	  <LI>
	    Primary <B>cause</B> of being.
	  <LI>
	    The <B>nature</B> (of a plant or animal).
	  <LI>
	    Not an element, but a <B>principle</B>.
	    <P>
	</OL>
    </OL>
  <LI>
    The resulting view is not Platonism:
    <P>
    <OL type=a>
      <LI>
	The form that Aristotle says is primary substance is not, like Plato's, separable
	from all matter (except, perhaps, in thought). And it cannot exist if it
	is not the form of something. (E.g., the species-form does not exist if there
	are no specimens of that species.) But it is still separable, in Aristotle's
	sense, since it is non-parasitic: it does not depend for its existence on
	the particular batch of matter it's in, nor on the accidental characteristics
	of the compound it's the form of.
	<P>
      <LI>
	The form is not a "thing", in the manner of a Platonic form. It's the
	<B>way</B> something is, the way the matter composing an individual compound
	is organized into a functioning whole.
	<P>
    </OL>
  <LI>
    Why doesn't this view collapse into materialism? That is, why isn't the form
    that can only exist in matter just a <B>mode</B> or <B>modification</B> of
    the matter that it in-forms? Why isn't matter more basic than form in the
    way that the primary substances of the <I>Categories</I> are more basic than
    their accidents?
    <P>
    The substantial form (i.e., what makes Socrates <I>human</I>, or, for the
    proponent of individual forms, what makes Socrates <I>Socrates</I>) is really
    the basic entity that persists through change.
    <P>
    This may seem wrong, since when Socrates dies, his matter persists, although
    he no longer exists.
    <P>
    But: when we are tracing the history of Socrates through time, we do not
    follow the course of the matter that happens to compose his body at any given
    moment, but that of the form that the matter has. (Animals and plants metabolize;
    the matter that they are composed of differs from time to time.)
    <P>
    So what makes Socrates the kind of thing he is, and what makes him remain,
    over time, <B>the same thing</B> of that kind, is the form that he continues
    to have.
    <P>
    For Aristotle, the form of a compound substance is <B>essential</B> to it;
    its matter is <B>accidental</B>. (Socrates could have been composed of different
    matter from that of which he is actually composed.)
    <BLOCKQUOTE>
      <FONT COLOR="#de141f">Form may be accidental to the <B>matter</B> that it
      informs, but it is essential to the <B>compound substance</B> (i.e., the
      compound of matter and form) that it is the form of. Form is what makes the
      individual plants and animals what they are. Therefore, it is the
      <B>substance</B> of those individuals.</FONT>
    </BLOCKQUOTE>
</OL>
<P>
  <HR>
<P ALIGN=Center>
<B>Notes</B>
<P>
<A NAME="Note1"><!-- --></A>1. &nbsp;Substances are supposed to be objects
of knowledge, and objects of knowledge are universals, Aristotle says (417b21,
1140b31). Similarly, substances are supposed to be, <I>par excellence</I>,
definable, and it is universals, rather than individuals, that are definable,
according to Aristotle (90b4, 97b25, 1036a28, 1039b20, 1040a5). &nbsp;These
seem to be serious obstacles to the "individual form" interpretation.
&nbsp;&nbsp;<A HREF="zeta17.htm#text1">Back to text</A>.
<P>
  <HR>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/bluedot.gif" WIDTH="20" HEIGHT="20">Go to next lecture
on Aristotle's <I>De Anima</I>: <A HREF="psyche.htm">On the Soul</A>
<P>
<IMG ALIGN="Middle" SRC="http://www.wabashcenter.wabash.edu/syllabi/c/cohen/phil320/yellodot.gif" WIDTH="22" HEIGHT="22">Go to previous
lecture on the <A HREF="4causes.htm">Four Causes</A>.
<P>
<A HREF="index.html"><IMG SRC="goback.gif" WIDTH="40" HEIGHT="40"></A>&nbsp;
Return to the PHIL 320 <A HREF="index.html">Home Page</A><BR>
<BR>
  <HR>
Copyright &copy; 2000, S. Marc Cohen
<P>
  <HR>
<UL>
  <LI>
    This page was last updated on 11/12/00.<BR>
</UL>
</BODY></HTML>


