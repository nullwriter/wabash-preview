<html>

<head>
<title>Home Page</title>
</head>

<body>
<font FACE="Times New Roman">

<p align="center"><big><strong><big>Is Contemporary Physics Mystical?</big></strong></big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>There is widespread misuse of the word &quot;mystical&quot; and even religious
scholars do not use the word precisely. In common parlance its meaning is so loose that
the word has lost its power to communicate anything intelligible. The popular books <i>The
Tao of Physics</i> and <i>The Dancing Wu Li Masters</i> have done much to educate the
general public about the wonders of contemporary physics, but their authors are incorrect
in describing it as &quot;mystical&quot; in its theory of reality. In what follows I will
define mysticism, offer a typology of mysticism, and demonstrate how common usage
conflicts with my definition. I will argue that the terms &quot;holistic pluralism&quot;
and &quot;organicism&quot; best describe the world that 20<sup>th</sup> Century physics
has discovered.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>The adjective &quot;mystical&quot; comes from Latin <i>mysticus</i> and Greek <i>mustikos</i>.
For the Greeks it meant one who was initiated into one of the mystery religions. Etymology
alone, however, will mislead us, because this Greek derivation might lead us to believe
that mysticism has something to do with esoteric religious practices. This unfortunately
intensifies the misunderstanding of the mystical as the mysterious or the occult. The
literally meaning of <i>mustikos</i> is &quot;close the mouth&quot;--i.e., to keep
tight-lipped about the religious instruction that one has been given. Mysticism has
generally not been esoteric, and mystics have usually been quite willing to speak about
their experiences.</big></p>

<p><big><i><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>The Oxford English Dictionary</i> (OED) defines the &quot;mystical&quot; as
&quot;spiritual union with God transcending human comprehension.&quot; This I believe is a
good basic definition but it needs to be revised to include those mystics who claim union
with an impersonal reality, such as Plotinus&#146; One or the Hindu Brahman. Both Plotinus
and some Hindus (specifically followers of Advaita Vedanta) believe that in the mystical
experience the individual self is completely dissolved and identified with ultimate
reality. (The medieval mystic Meister Eckhart is the best representative of this view in
the Christian tradition.) In philosophical terms this type of mysticism implies what is
called &quot;absolute monism,&quot; a view that holds that all of reality is a divine
unity, an undifferentiated one, and that individuals and particular things are at most
only derivative or ultimately illusory. While mystics name ultimate reality differently,
they all agree that the mystical experience is ineffable, confirming the second part of
the OED definition. Therefore, we have two necessary but only together sufficient
conditions for a mystical experience: a union with ultimate reality that is ineffable.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>St. Catherine of Genoa, a medieval mystic, speaks of the dissolution of the self
into God in the following way: &quot;My Me is God, nor do I recognize any other Me except
God Himself.&quot; Catherine&#146;s position is a mystical interpretation of Paul&#146;s
famous phrase &quot;Not I, but Christ in me.&quot; This is essentially the same as the
Hindu saying &quot;Not I, but Atman (=Brahman) in me,&quot; or the Mahayana Buddhist
saying &quot;Not I, but the Buddha nature in me.&quot; For the mystic the ego-self is an
illusion; in Christian terms it is the fallen, sinful self. The true soul is the Godhead
or the divine One. As Meister Eckhart said: &quot;The knower and the know are one. Simply
people imagine that they should see God, as if He stood there and they here. This is not
so. God and I, we are one in knowledge.&quot; </big></p>
<b>

<p><big>WHAT MYSTICISM IS NOT</big></b></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>After the 1989 earthquake in San Francisco one commentator said that it had
destroyed the &quot;mystical&quot; beauty of the Bay Area. (On a recent visit I did not
see any evidence of the damage, so this person must have meant only a temporary
interruption in its beauty.) The general experience of beauty is not mystical because it
fulfills only the ineffability criterion. Usually we do not claim that we have become one
with the beautiful object. As with most of our experiences there is certainly a unity in
our perception of beautiful things. Many of our experiences also involve a loss of self,
but to call these experiences, even those of great aesthetic quality and intensity,
mystical would extend the meaning so far as to make it imprecise and unusable.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>I once read an article about Chinese paintings that described the objects as
located in a &quot;mystical&quot; space. Following our definition mystical space would be
undifferentiated, but clearly any painting that could be recognized as such would have
differentiated space. Perhaps the author meant that the paintings&#146; space was in some
way connected to the &quot;nature&quot; mysticism (defined later) of Chinese philosophy,
or maybe he thought that the space was mysterious compared to European paintings. The
confusion of the mysterious and the mystical is obviously playing a role in the
commentator on National Public Radio who claimed that llamas have mystical stress
relieving qualities. <span lang="en-us">Another NPR announcer claimed that 
George Harrison's introduction of the sitar, an Indian instrument, gave the 
Beatle's a new &quot;mystical&quot; sound.</span></big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>When people say that a book, a religion, or a philosophy is &quot;mystical,&quot;
they usually mean that it is deep, profound, speculative, metaphysical, esoteric, or just
plain philosophical. (They could of course be using the word correctly if the book fit our
revised OED definition.) The confusion between metaphysical and mystical is especially
common. To a trained philosopher metaphysics is simply a study of being or reality, which
could range from a belief that all things are material to all things being mental or
spiritual. We can contrast this with the New Age movement, where &quot;metaphysical&quot;
means esoteric or occult teachings, ranging from reincarnation, lost civilizations, to
aliens. This means that, according to our definition, many New Age ideas are not mystical
either. Aside from some Buddhists and the Chinese philosopher Zhuangzi, most mystics
assume a metaphysics, but, as we have seen, a very specific monistic system based on an
ineffable union with spiritual reality. </big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>In our English literature classes we learn that William Blake was a great
&quot;mystical&quot; poet. While is it true that Blake probably did have mystical
experiences, most of his poetry shows that he was a visionary not a mystic. A vision is
full of vivid images and has a narrative line, one that can be explained in words. By
contrast the ultimate mystical experience has little if no content and the mystic
confesses that the experience cannot be expressed in words. Christian mystics speak of
themselves being dissolved in Christ or God, but they also say that Jesus sometimes
appeared to them and spoke to them. I believe that it is essential to distinguish the
first as mystical from the second as visionary, or once again the word
&quot;mystical&quot; loses its meaning.</big></p>
<b>

<p><big>PHYSICS IS NOT MYSTICAL</big></b></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Except for some Buddhist schools, traditional mysticism both East and West has
assumed a substance metaphysics. From Aristotle onwards, a substance has been defined as
an unchanging substrate somehow related to a changing world. Substances ranged from an
immutable spiritual substances such as God and the soul to immutable physical substances
such as the atom. I believe that the discoveries of 20<sup>th</sup> Century physics has
empirically disconfirmed substance metaphysics. Instead of unchanging substances the
universe appears to be a dynamic whole made up of changing processes. Heraclitus, Laozi,
and the Buddha were the great &quot;process&quot; philosophers of the ancient world and
they have now been vindicated by the new &quot;process&quot; physics. This common ground
in process philosophy is the real meeting point of contemporary physics and Eastern
philosophy, at least the Buddhist and Chinese traditions. Unfortunately, Hinduism
maintains Brahman-Atman as spiritual substance and the original Yoga philosophy had a
substance dualism even more extreme than Cartesian dualism. </big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>If we apply strictly the revised OED definition of mysticism to the earliest Yoga
philosophy (i.e., before it was synthesized with Vedanta), we get a very surprising
result. Here there is a basic dualism between matter and spirit in which even higher level
mental activities are placed on the material side, leaving the spiritual self as a very
abstract entity. When yogis reach liberation, their spirits do not fuse with Brahman or
anything equivalent; rather, their disembodied souls float to their own isolated regions
of space. Therefore, there is no experience of unity (except an internal one) and the <i>Yoga
Sutras</i> describe this experience with great confidence. If the yogis are able to speak
in this manner, then their experience is not ineffable.&nbsp; Their experience then fails
on both elements of the OED definition of mysticism.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>There is much value in Fritjof Capra's <i>The Tao of Physics</i> and Gary Zukav's <i>Dancing
Wu Li Masters</i>, but both of them are misusing the term &quot;mysticism&quot; when they
compare contemporary physics with Eastern philosophy. Instead of the word
&quot;mystical,&quot; Capra and Zukav should have used the terms &quot;holistic&quot; or
&quot;organic.&quot; The <i>wu li</i> of Zukav&#146;s title is Chinese for
&quot;physics,&quot; but literally and appropriately it means &quot;patterns of organic
energy.&quot; Zukav claims that Bell's theorem demonstrates that there are no such things
as separate parts, a conclusion that he says is the same as the mystics. I propose that
this is better explained by a theory of organic wholes, with internally related parts, not
mystical wholes with no real individual parts. In a strict sense modern physics is the
very opposite of mysticism, because it has divided up reality into literally hundreds of
discrete subatomic particles, each with its own distinct signature. Each of these
particles is an integral expression of a holistic energy field, but each maintains its identity nonetheless. What we have is a radical pluralism not an
absolute monism. The cosmos is organic unity with diversity, not mystical union without
remainder.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Einstein thought that if we separated paired subatomic particles, we could
eliminate quantum indeterminancy by measuring one particle and making up for the
observational disturbance by assuming that the other particle would remain unchanged.
Quantum theory fooled Einstein, not the other way around. Imagine the cosmos as a gigantic
dance floor and two paired particles are doing a synchronized dance. Even if the particles
were at opposite ends of the cosmos, their movements would match each other perfectly--a
change in one would affect a change in the other. At least two assumptions of classical
physics are undermined completely: (1) there is no action at a distance; and (2) particles
are self-contained, self-sufficient entities externally related to one another. Zukav is
correct is saying that subatomic particles are &quot;connected in an immediate and
intimate way,&quot; but this is not a mystical relation nor a mystical unity. Rather, it
is an internal relation, where one entity is dependent on another (asymmetrical internal
relation) or both are dependent on each other (symmetrical internal relation). Just as our
&quot;dance&quot; particle preserve their individual integrity and are not dissolved into
each other, so too do the terms of internal relations maintain their separate identities.
The mechanical model is based on external relations; the organic model is based on
internal relations; and the mystical model ultimately has no parts that relate at all. The
Buddha&#146;s doctrine of interdependent coorigination is the best Eastern model of
internal relations, so it is Buddhism once again that offers the most constructive
comparisons for the new physics.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Schrodinger's wave mechanics is better conceptualized in organic terms rather than
mystical terms. The wave packets after all are discrete packets, intimately related to
other packets, and are recognizable by instruments. Unwittingly mixing organicism and
mysticism, Zukav himself describes the wave function correctly: &quot;It would be a sort
of organic whole whose parts are changing constantly. . . .&quot; Earlier, he refers to
the organic model when he states that &quot;all that exists by itself is an unbroken
wholeness that present itself to us as webs [patterns] of relations. Individual entities
are idealizations which are correlations made by us.&quot; There are at least two problems
with this passages: (1) the whole cannot be literally unbroken if the parts are
&quot;changing constantly&quot;; and (2) individual particles are not made by us but
constituted by relations. The passage he uses from Henry Stapp clearly indicates this:
&quot;Not a structure built out of independently existing unanalyzable entities [classical
atoms externally related], but rather a web of relationships between elements whose
meanings arise wholly from their relationships to the whole.&quot; This is the
Buddha&#146;s view as well: all things are constituted by their relations to other things.
The Buddha rejects the idealist position that human minds construct reality. </big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>In <i>The Tao of Physics </i>Capra repeatedly claims that Asian mysticism has been
confirmed by contemporary physics. Here is one sample passage: &quot;The harmony between
their views confirms the ancient Indian wisdom that Brahman, the ultimate reality without,
is identical to the Atman, the reality within.&quot; But of course there is no such
confirmation. Atman and Brahman are immutable spiritual substances, and I maintain that
this is incompatible with today&#146;s physics for at least two reasons: (1) contemporary
physics appears to make the idea of substance impossible; and (2) physics is irreducibly
pluralistic not monistic. (It may be said to be monistic only in the sense that all things
come from energy.) One might say that the naked singularity of the moment before the Big
Bang is such a mystical reality, but just a little scrutiny will show what a rash claim
that might be. We know that the singularity would have been infinitely dense, but the
mystics tell us that the divine unity is infinitely expansive. </big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Capra appeals to Tantric Buddhism to make a comparison between Buddhist
interconnectedness and the equivalent idea in physics. The Sanskrit root for the word
Tantra means &quot;to weave,&quot; and to say that reality is like an interwoven fabric is
to imply that it has a warp, a woof, and distinguishable threads. This metaphor again
lends itself to an organic model of pluralistic holism not mystical unity. </big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Zukav believes that the idea reality is dependent upon perception is a mystical
notion that is confirmed by quantum physics. But this idealist philosophy is also found in
Euro-American philosophy where there are many idealists who are not mystics. (To say that
reality is dependent upon perception is not the same thing as claiming that this same
reality is a unity without differentiation.) Instead of Zukav&#146;s idealist
interpretation of quantum mechanics (&quot;our reality is what we choose to make
it&quot;), I side with Stapp&#146;s idea that things are constituted by their
interrelations with all others things (not just human consciousness), which is the most
acceptable Buddhist view as well. Therefore, I reject Zukav&#146;s claim that physics has
become a branch of psychology, or that physics now actually reflects the structure of
consciousness. </big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>The foregoing is also the view of Amit Goswami in his book <i>The Self-Aware
Universe</i>. The premise that the cosmos as a whole is self-conscious is a speculative
idea that has no supporting evidence. On the issue of consciousness I would rather support
a bottom-up emergent evolutionary view, one such as Alfred North Whitehead&#146;s, rather
than a top-down view of the universe as a cosmic mind. In other words, conscious appears
only in those places in the universe where evolution has reached a very complex stage of
development. In addition to the problem of divine consciousness appearing as a <i>deus ex
machina</i>, there is also the issue of internal consistency in Goswami&#146;s own
Vedantist philosophy. In its ultimate form Brahman is an undifferentiated unity: highest
Brahman does not know, is not aware, and does not act in any way. These facts make it very
difficult to understand how Brahman can reduce the wave packet. It is, therefore,
misleading to describe Brahman as a divine mind. It is a divine One, a mystical unity
without any distinctions or qualities whatever. Even if we propose that Brahman is energy
in its most abstract sense, physicists would still ascribe certain qualities to energy,
not least would be power itself.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Zukav also argues that the Many Worlds interpretation of quantum mechanics
&quot;sounds like a quantitative version of a mystical vision of unity.&quot; But, as I
understanding this theory, the Many World concept is not one of unity but of
disunity--namely, all possible worlds contained in the wave function are already
actualized in separate universes. This does remind us of an Asian view, but it not a
mystical one. It is the odd view explained above: the early yogis&#146; belief that each
liberated soul will float freely in spiritual space, each not knowing the other and each
contemplating its own perfection. This is not a universe, but a multiverse.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Capra suggests that the Many World hypothesis can be conceived as an infinite
number of identical worlds, a view expressed in the Hindu story of Indra's pearls. In
Indra's heaven there is a string of pearls and they are so constituted that the entire
cosmos is contained in each one of them. Later Buddhists adapted this by saying that in
every thing has its own identical Buddha nature. The story of Indra&#146;s pearls is used
by Capra and Zukav to link physics and mysticism by means of a holographic analogy. Both
Indra's pearls and holographs, however, still represent a differentiated reality, so they
are not analogous to the absolute monism of the mystical vision. Furthermore, the many
worlds of quantum mechanics would represent instantiations of all the different possible
worlds. In contrast Indra&#146;s pearls and holographs give us copies of the exact same
world.</big></p>
<b>

<p></b><big><strong>FIVE TYPES OF MYSTICISM</strong></big></p>

<p><big><strong><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Theistic Mysticism</strong>. The mystics can be categorized according to
the way in which they describe the ultimate reality they claim they have become one with.
Jewish, Islamic, and Christian mystics say that they have unified themselves with God, so
we can call this &quot;theistic&quot; mysticism. Examples are St. Catherine of Genoa, St.
Theresa, St. John of the Cross, and the Sufi mystics of Islam. As I have already said,
Christian mysticism is sometimes a mix of mystical union and vivid visions. </big></p>

<p><big><strong><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Nontheistic or Monistic Mysticism</strong>. 
<span lang="en-us">Asian</span> mystics,
neo-Platonists like Plotinus, and some Christian mystics, such as Meister Eckhart and
Jacob Boehme, claim that the ultimate reality is an impersonal One. We can call this a
nontheistic or monistic mysticism. Eckhart speaks for them all: &quot;This identity out of
the One into the One and with the One is the source and fountainhead and breaking forth of
glowing love.&quot; Some Buddhists and Eckhart have a provocative version of this type of mysticism.
The Mahayana Buddhists call the highest reality <i>shunyata</i> and Eckhart sometimes
called God, in his medieval German, <i>bloss nit</i>. Both of these terms mean
nothingness--not just emptiness, but a nothingness which is all--a no-thing-ness--no
particular thing. This undifferentiated reality is the focus and true object of the
mystical experience. As Eckhart states: &quot;The Godhead is poor, naked and empty as
thought it were not; it has not, wills not, wants, not works not, gets not. . . the
Godhead is as void as though it were not.&quot; Comparative philosophers have pointed out
the obvious similarity between Eckhart&#146;s Godhead and the Hindu Brahman.</big></p>

<p><big><strong><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Nature Mysticism</strong>. Nature mysticism is the most widespread form of
mystical experience. Many people have had powerful experiences of unity with nature that
they have declared to be ineffable. William Rowe's distinction between introvertive and
extrovertive mysticism clarifies the difference between nature mysticism and the first two
types above. Whereas theistic and monistic mysticism involves a blocking of the senses, a
&quot;self-emptying&quot; of the soul, and a withdrawal from the world, nature mystics
merge with nature and their senses become more acute, seeing things as &quot;they really
are.&quot; Zen Buddhism and Chinese Daoism are very good examples of nature mysticism. We
ordinary folk think that we perceive mountains, streams, and valleys, but Zen techniques
jolt us out of these banal perceptions so that we come to see mountains, streams, and
valleys for the very first time.</big></p>

<p><big><strong><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Performance Mysticism</strong>. Even amateur musicians speak of the
experience of union with their instruments and all performers have to admit that they
cannot tell us in words how it is that they use their fingers or their lips. Performance
mysticism is central to Zhuangzi&#146;s philosophy and his most famous character is Cook
Ding, who knows the joints of the animals he carves so well that he never dulls his knife,
claiming to cut through the nothingness between the bones and ligaments. One might say
that in nature and performance mysticism we are straying from the experience of total
union the theistic and monistic mystics claim. While the criterion of ineffability is
still fulfilled, the world of the nature and performance mystic, especially the latter, is
fully differentiated. One might say that the perception of a beautiful object and the
playing an instrument may approach the same type of unity experience.</big></p>

<p><big><strong><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>Chemical Mysticism</strong>. Some might speculate that the mystical
experience is &quot;all in the head,&quot; and might propose that it is nothing but an
altered state of consciousness. If this is so, then the experience could be created either
by some form of brain stimulation or the ingestion of a drug. In his book <i>The Doors of
Perception</i>, Aldous Huxley reports about his experiments with the drug mescaline and
the altered states of consciousness that it produced. Most drug experiences, however,
appear visionary rather than mystical, because they usually contain vivid content and most
subjects appear to be able to give some account of them. Furthermore, neuroscientists such
as James Austin (his new book is <i>Zen and the Brain</i>) claim that Zen satori cannot be
induced by drugs. But it is possible that we will one day find a way to stimulate the
brain or find a drug that simulates the mystical experience. Such a reductionistic account
is unsatisfying for most religious scholars, but we cannot rule out the possibility.</big></p>

<p><big><span lang="en-us">&nbsp;&nbsp;&nbsp; </span>I am prepared to defend the mystics against a very common charge, one brought
often by scientists and others who prize rational thought. This charge is that the
mystical vision is irrational. I believe that this criticism is misdirected. On the face
of it, there is nothing logically incoherent about a view that reality is an
undifferentiated unity. In fact, the Greek philosopher Parmenides, the only pre-Socratic
thinker who had a clear grasp of the principle of contradiction, used logic to attempt a
proof of absolute monism. Mysticism is not necessarily irrational; rather, it is just
highly improbable given what we know about the world. The other aspect of this charge is
that mystical experiences are irrational because they cannot possibly happen. With some
exceptions of course, I believe that we should believe mystics when they claim that they
have had these experiences. It is the interpretation of the experiences that we can
challenge, and I believe that we can do that very successfully. I am personally convinced
that the total unity they experience is in the mind and not in reality.</big></p>
</font>

<p><font FACE="Times New Roman"><big>A paper presented by Nick Gier at &quot;Pions and
Beyond,&quot; a UI physics conference, April, 1998<span lang="en-us">; also 
published in the proceedings of the conference.</span></big></font></p>
</body>
</html>

