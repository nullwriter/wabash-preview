<html>

<head>
<meta NAME="GENERATOR" CONTENT="Microsoft FrontPage 3.0">
<title></title>
</head>

<body TEXT="#000000" LINK="#0000FF" VLINK="#551A8B" ALINK="#FF0000" BGCOLOR="#FFFFFF">

<p align="center"><big><strong>IS THE NEW PHYSICS REALLY MYSTICAL?</strong> </big></p>

<p align="center"><big><br WP="BR1">
Nicholas F. Gier </big></p>

<p align="center"><big>Department of Philosophy </big></p>

<p align="center"><big>University of Idaho </big></p>

<p align="center"><big>Presented at the conference &quot;Pions and Beyond,&quot; U. of
Idaho, April, 1998</big></p>

<p><big>There is widespread misuse of the word &quot;mystical.&quot; Even religious
scholars do not use the word precisely. In common parlance its meaning is so loose that
the word has lost its power to communicate anything intelligible. The popular books <em>The
Tao of Physics</em> and <em>The Dancing Wu Li Masters</em> have done much to educate the
general public about the wonders of contemporary physics, but their authors are incorrect
in describing it as &quot;mystical&quot; in its theory of reality. In what follows I will
define mysticism, offer a typology of mysticism, and demonstrate how common usage
conflicts with my definition. I will argue that the terms &quot;holistic pluralism&quot;
and &quot;organicism&quot; best describe the world that 20<sup>th</sup> Century physics
has discovered.</big></p>

<p><big>The adjective &quot;mystical&quot; comes from Latin <em>mysticus</em> and Greek <em>mustikos</em>.
For the Greeks it meant one who was initiated into one of the mystery religions. I usually
find etymologies very helpful, but this one is misleading. Theses derivations might lead
us to believe that mysticism has something to do with esoteric religious practices. This
unfortunately intensifies the misunderstanding of the mystical as the mysterious or the
occult. The literal meaning of <em>mustikos</em> is &quot;close the mouth&quot;--i.e., to
keep tight-lipped about the religious instruction that one has been given. Mysticism East
and West has generally not been esoteric, and mystics have usually not kept their
experiences or even the methods to attain them secret. Therefore, there is a significant
difference between esoteric knowledge (kept secret except for those who are initiated) and
the ineffability of the mystical experience.&nbsp; That which is esoteric can be put into
words and known to the chosen ones, but mystical experience can never be known in this
way.</big></p>

<p><big><em>The Oxford English Dictionary</em> (OED) defines the &quot;mystical&quot; as
&quot;spiritual union with God transcending human comprehension.&quot; This I believe is a
good basic definition but it needs to be revised to include those mystics who claim union
with an impersonal reality, such as Plotinus' One or the Hindu Brahman. Both Plotinus and
some Hindus (specifically followers of Advaita Vedanta) believe that in the mystical
experience the individual self is completely dissolved and identified with the ultimate
reality. (The medieval mystic Meister Eckhart is the best representative of this view in
the Christian tradition.) In philosophical terms this type of mysticism implies what is
called &quot;absolute monism,&quot; a view that holds that all of reality is a divine
unity, an undifferentiated one, and that individuals and particular things are only
derivative or ultimately illusory. While mystics name ultimate reality differently, they
all agree that the mystical experience is ineffable, confirming the second part of the OED
definition &quot;transcending human comprehension.&quot; Therefore, we have two necessary
but only together sufficient conditions for a mystical experience: a union with ultimate
reality that is ineffable.</big></p>

<p><big>St. Catherine of Genoa, a medieval mystic, speaks of the dissolution of the self
into God in the following way: &quot;My Me is God, nor do I recognize any other Me except
my God Himself.&quot;<a HREF="mysticism.htm#N_1_"><sup>(1)</sup></a> Catherine's position is a mystical
interpretation of Paul's famous phrase &quot;Not I, but Christ.&quot; This is essentially
the same as the Hindu saying &quot;Not I, but Atman (=Brahman),&quot; or the Buddhist
saying &quot;Not I, but the Buddha nature.&quot; For the mystic the ego-self is an
illusion; in Christian terms it is the fallen, sinful self. The true soul is the Godhead
or the divine One. As Meister Eckhart said: &quot;The knower and the know are one. Simple
people imagine that they should see God, as if He stood there and they here. This is not
so. God and I, we are one in knowledge.&quot;<a HREF="mysticism.htm#N_2_"><sup>(2)</sup></a> </big></p>

<p><big><br WP="BR1">
<strong>WHAT MYSTICISM IS NOT</strong></big></p>

<p><big>After the 1989 earthquake in San Francisco one commentator said that it had
destroyed the &quot;mystical&quot; beauty of the Bay Area. (On a recent visit I did not
see any evidence of the damage, so this person must have meant only a temporary
interruption in its beauty.) The general experience of beauty is not mystical because it
fulfills only the ineffability criterion. Usually we do not claim that we have become one
with the beautiful object. As with most of our experiences there is certainly a unity in
our perception of beautiful things. Many of our experiences also involve a loss of self,
but to call these experiences, even those of great aesthetic quality and intensity,
mystical would extend the meaning so far as to make it imprecise and unusable.</big></p>

<p><big>I once read an article about Chinese paintings that described the objects as
located in a &quot;mystical&quot; space. Following our definition mystical space would be
undifferentiated, but clearly any painting that could be recognized as such would have
differentiated space. Perhaps the author meant that the paintings' space was in some way
connected to the &quot;nature&quot; mysticism (defined below) of Chinese philosophy, or
maybe he thought that the space was mysterious compared to Western paintings. The
confusion of the mysterious and the mystical is obviously playing a role in the
commentator on National Public Radio who claimed that llamas have&quot; mystical&quot;
stress relieving qualities. </big></p>

<p><big>When people say that a book, a religion, or a philosophy is &quot;mystical,&quot;
they usually mean that it is deep, profound, speculative, metaphysical, esoteric, or just
plain philosophical. (They could of course be using the word correctly if the book fit our
OED definition.) The confusion between metaphysical and mystical is especially common. To
a trained philosopher metaphysics is simply a study of being or reality, which could range
from a belief that all things are material to all things being mental or spiritual. We can
contrast this with the New Age movement, where &quot;metaphysical&quot; means esoteric or
occult teachings, ranging from reincarnation, lost civilizations, to alien visitations.
This means that, according to our definition, many New Age ideas are not mystical either.
Aside from some Buddhists and the Chinese philosopher Zhuangzi, most mystics assume a
metaphysics, but, as we have seen, a very specific monistic system based on an ineffable
union with spiritual reality. </big></p>

<p><big>In our English literature classes we learn that William Blake was a great
&quot;mystical&quot; poet, but his major poems describe visions not mystical experiences.<a
HREF="mysticism.htm#N_3_"><sup>(3)</sup></a> A vision is full of vivid images and has a narrative line,
one that can be explained in words. By contrast the ultimate mystical experience has
little if no content and the mystic confesses that the experience cannot be expressed in
words. Christian mystics speak of themselves being dissolved in Christ or God, but they
also say that Jesus sometimes appeared to them and spoke to them. I believe that it is
essential to distinguish the first as mystical from the second as visionary, or once again
the word mystical loses its meaning.</big></p>

<p><big>Although some meditative techniques lead to mystical experiences, there is no
necessary connection between meditation and mysticism. In early Buddhism there are two
types of meditation: calming meditation (<em>samatha-bhavana</em>) and insight meditation
(<em>vipassana-bhavana</em>). The first is designed to calm the passions, to rid people of
their craving selves, and to maximize sympathy for all living beings. The second technique
is more intellectual and results in <em>prajna</em>, the highest form of knowledge the
Buddha attained. It consists of basic propositional knowledge and can be phrased in
sentences such as &quot;all things are impermanent&quot; and &quot;there is no
Atman.&quot; It is clear that neither of these types of meditation leads to mystical
experiences according to the OED definition. The virtue of compassion requires that we
have sympathy for beings other than ourselves. <em>Prajna</em> is not mystical knowledge
for at least two reasons: (1) it is knowledge of a differentiated reality of a myriad
transitory events; and (2) as propositional it is eminently sayable and clearly not
ineffable. With its concept of the Dharmakaya (the universal body of the Buddha) Mahayana
Buddhism does combine meditation and mysticism, but this concept is not found in the early
Buddhist writings.<a HREF="mysticism.htm#N_4_"><sup>(4)</sup></a></big></p>

<p><big><strong>PHYSICS IS NOT MYSTICAL</strong></big></p>

<p><big>Except for some Buddhist and Chinese philosophers, traditional mysticism both Asia
and Euro-America has assumed a substance metaphysics. From Aristotle onwards a substance
has been defined as an unchanging substrate somehow related to a changing world.
Substances ranged from an immutable spiritual substances such as God and the soul to
immutable physical substances such as the atom. The discoveries of 20<sup>th</sup> Century
physics, I believe, offer strong empirical disconfirmation of substance metaphysics.
Instead of unchanging substances the universe appears to be a dynamic whole made up of
changing processes. Heraclitus, Laozi, and the Buddha were the great &quot;process&quot;
philosophers of the ancient world and they have now been vindicated by the new
&quot;process&quot; physics. This common ground in process philosophy is the real meeting
point of contemporary physics and Asian philosophy, at least the Buddhist and Chinese
traditions. Unfortunately, Hinduism maintains Brahman-Atman as spiritual substance and the
original Yoga philosophy had a substance dualism even more extreme than Cartesian dualism.
</big></p>

<p><big>If we apply strictly the revised OED definition of mysticism to the earliest Yoga
philosophy (i.e., before it was synthesized with Vedanta), we get a very surprising
result. Here there is a basic dualism between matter and spirit in which even higher level
mental activities are placed on the material side, leaving the spiritual self as a very
abstract entity. When yogis reach liberation, their spirits do not fuse with Brahman or
anything equivalent; rather, their disembodied souls float to their own isolated regions
of space.<a HREF="mysticism.htm#N_5_"><sup>(5)</sup></a> Therefore, there is no experience of unity
(except an internal one) and the <em>Yoga Sutras</em> describe this experience with great
confidence. The Buddha was also an expert in yogic techniques and his enlightenment does
not appear to meet the two criteria of mysticism either. Like the yogis he explains his
experiences in great detail, discovers that all things flow, and boldly proclaims that
Atman and all other substances are metaphysical fictions. Again there is no fusion with
any ultimate reality and there is no claim to ineffability. Later Buddhism does make the
Buddha into a cosmic being, one with whom all beings can merge in mystical union, but
there is nothing like this in the early sutras.</big></p>

<p><big>There is much value in Fritjof Capra's <em>The Tao of Physics</em> and Gary
Zukav's <em>Dancing Wu Li Masters</em>, but both of them are misusing the term
&quot;mysticism&quot; when they compare contemporary physics with Asian philosophy.
Instead of the word &quot;mystical,&quot; Capra and Zukav should have used the terms
&quot;holistic&quot; or &quot;organic.&quot; The <em>wu li</em> of Zukav's title is
Chinese for &quot;physics,&quot; but literally and appropriately it means &quot;patterns
of organic energy,&quot; and the neo-Confucian philosophers had view of vibratory energy
roughly similar to the new physics. Zukav claims that Bell's theorem demonstrates that
there are no such things as separate parts, a conclusion that he says is the same as the
mystics.<a HREF="mysticism.htm#N_6_"><sup>(6)</sup></a> I propose that this is better explained by a
theory of organic wholes, with internally related parts, not mystical wholes with no real
individual parts. In a strict sense modern physics is the very opposite of mysticism,
because it has divided up reality into literally hundreds of discrete subatomic particles,
each with its own distinct signature. Each of these particles is an instantiation of a
holistic energy field, but each maintains its identity nonetheless. What we have is a
radical pluralism not an absolute monism. The cosmos is an organic unity with diversity,
not a mystical union without remainder.</big></p>

<p><big>Einstein thought that if we separated paired subatomic particles, we could
eliminate quantum indeterminancy by measuring one particle and making up for the
observational disturbance by assuming that the other particle would remain unchanged.
Quantum theory fooled Einstein, not the other way around. Imagine the cosmos as a gigantic
dance floor and two paired particles are doing a synchronized dance. Even if the particles
were at opposite ends of the cosmos, their movements would match each other perfectly--a
change in one would affect a change in the other. At least two assumptions of classical
physics are undermined completely: (1) there is no action at a distance; and (2) particles
are self-contained, self-sufficient entities externally related to one another. Zukav is
correct in saying that subatomic particles are &quot;connected in an immediate and
intimate way,&quot;<a HREF="mysticism.htm#N_7_"><sup>(7)</sup></a> but this is not a mystical relation
nor a mystical unity. Rather, it is an internal relation, where one entity is dependent on
another (asymmetrical internal relation) or both are dependent on each other (symmetrical
internal relation). Just as our &quot;dance&quot; particles preserve their individual
integrity and are not dissolved into each other, so too do the terms of internal relations
maintain their separate identities. The mechanical model is based on external relations;
the organic model is based on internal relations; and the mystical model ultimately has no
parts that can relate at all. The Buddha's doctrine of interdependent coorigination is the
best Asian model of internal relations, so it is Buddhism once again that offers the most
constructive comparisons with the new physics.</big></p>

<p><big>Schrodinger's wave mechanics is best conceptualized in organic terms rather than
mystical terms. The wave packets are discrete packets, intimately related to other
packets, and are recognizable by instruments. Unwittingly mixing organicism and mysticism,
Zukav himself describes the wave function correctly: &quot;It would be a sort of organic
whole whose parts are changing constantly. . . .&quot;<a HREF="mysticism.htm#N_8_"><sup>(8)</sup></a>
Earlier, he refers to the organic model when he states that &quot;all that exists by
itself is an unbroken wholeness that present itself to us as webs [patterns] of relations.
Individual entities are idealizations which are correlations made by us.&quot;<a
HREF="mysticism.htm#N_9_"><sup>(9)</sup></a> There are at least two problems with this passage: (1) the
whole cannot be literally unbroken if the parts are &quot;changing constantly&quot;; and
(2) individual particles are not made by us but constituted by relations. The passage he
uses from Henry Stapp clearly indicates this: &quot;Not a structure built out of
independently existing unanalyzable entities [classical atoms externally related], but
rather a web of relationships between elements whose meanings arise wholly from their
relationships to the whole.&quot;<a HREF="mysticism.htm#N_10_"><sup>(10)</sup></a> This is the Buddha's
view as well: all things are constituted by their relations to other things. The Buddha
rejects the idealist position that human minds construct reality. </big></p>

<p><big>In <em>The Tao of Physics </em>Capra repeatedly claims that Asian mysticism has
been confirmed by contemporary physics. Here is one sample passage: &quot;The harmony
between their views confirms the ancient Indian wisdom that Brahman, the ultimate reality
without, is identical to the Atman, the reality within.&quot;<a HREF="mysticism.htm#N_11_"><sup>(11)</sup></a>
But of course there is no such confirmation. Atman and Brahman are immutable spiritual
substances, and I maintain that this is incompatible with today's physics for at least two
reasons: (1) contemporary physics appears to make the idea of any substance impossible;
and (2) physics is irreducibly pluralistic not monistic. (It may be said to be monistic
only in the sense that all things come from energy.) One might say that the naked
singularity of the moment before the Big Bang is such a mystical reality, but just a
little scrutiny will show what a rash claim that might be. We know that the singularity
would have been infinitely dense, but the mystics tell us that the divine unity is
infinitely expansive. </big></p>

<p><big>Capra believes that Buddhist interconnectedness and the equivalent idea in physics
are found in Tantrism.<a HREF="mysticism.htm#N_12_"><sup>(12)</sup></a> Tantric philosophy is widely
expressed in the worship of the Hindu Goddess, which offers a powerful alternative to the
abstract spiritualism of yogic isolation on the one hand and total dissolution in
Atman-Brahman on the other. The Goddess exhorts her followers to return to the world, to
the body, and to society.<a HREF="mysticism.htm#N_13_"><sup>(13)</sup></a> The Sanskrit root for the
word Tantra means &quot;to weave,&quot; and to say that reality is like an interwoven
fabric is to imply that it has a warp, a woof, and distinguishable threads. This metaphor
again lends itself to an organic model of pluralistic holism not mystical unity. </big></p>

<p><big>Zukav believes that the idea reality is dependent upon perception is a mystical
notion that is confirmed by quantum physics.<a HREF="mysticism.htm#N_14_"><sup>(14)</sup></a> But this
idealist philosophy is also found in Euro-American philosophy where most idealists are not
mystics. (To say that reality is dependent upon perception is not the same thing as
claiming that this same reality is a unity without differentiation.) Instead of Zukav's
idealist interpretation of quantum mechanics (&quot;our reality is what we choose to make
it&quot;),<a HREF="mysticism.htm#N_15_"><sup>(15)</sup></a> I side with Stapp's idea that things are
constituted by their interrelations with all others things (not just human consciousness),
which is the most acceptable Buddhist view as well. Therefore, we must reject Zukav's
claim that physics has become a branch of psychology, or that physics now actually
reflects the structure of consciousness.<a HREF="mysticism.htm#N_16_"><sup>(16)</sup></a> </big></p>

<p><big>The foregoing is also the view of Amit Goswami in his book <em>The Self-Aware
Universe</em>.<a HREF="mysticism.htm#N_17_"><sup>(17)</sup></a> The premise that the cosmos as a whole
is self-conscious is a speculative idea that has no supporting evidence. On the issue of
consciousness I would rather support a bottom-up emergent evolutionary view, one such as
Alfred North Whitehead's, rather than a top-down view of the universe as a cosmic mind. In
other words, conscious appears only in those places in the universe where evolution has
reached a very complex stage of development. In addition to the problem of divine
consciousness appearing as a <em>deus ex machina</em>, there is also the issue of internal
consistency in Goswami's own Vedantist philosophy. In its ultimate form Brahman is an
undifferentiated unity: highest Brahman does not know, is not aware, and does not act in
any way. These facts make it very difficult to understand how Brahman can reduce the wave
packet. It is, therefore, misleading to describe Brahman as a divine mind. It is a divine
One, a mystical unity without any distinctions or qualities whatever. Even if we propose
that Brahman is energy in its most abstract sense, physicists would still ascribe certain
qualities to energy, not least of which would be power itself.</big></p>

<p><big>Zukav also argues that the Many Worlds interpretation of quantum mechanics
&quot;sounds like a quantitative version of a mystical vision of unity.&quot;<a
HREF="mysticism.htm#N_18_"><sup>(18)</sup></a> But, as I understanding this theory, the Many Worlds
concept is not one of unity but of disunity--namely, all possible worlds contained in the
wave function are already actualized in separate universes. This does remind us of an
Asian view, but it not a mystical one. It is the odd view explained above: the early
yogis' belief that each liberated soul will float freely in spiritual space, each not
knowing the other and each contemplating its own perfection. This is not a universe, but a
multiverse and clearly not a mystical unity.</big></p>

<p><big>Capra suggests that the Many World hypothesis can be conceived as an infinite
number of identical worlds, a view expressed in the Hindu story of Indra's pearls.<a
HREF="mysticism.htm#N_19_"><sup>(19)</sup></a> In Indra's heaven there is a string of pearls and they
are so constituted that the entire cosmos is contained in each one of them. Later
Buddhists adapted this by saying that every thing has its own identical Buddha nature. The
story of Indra's pearls is used by Capra and Zukav to link physics and mysticism by means
of a holographic analogy. Both Indra's pearls and holographs, however, still represent a
differentiated reality, so they are not analogous to the absolute monism of the mystical
vision. Furthermore, the many worlds of quantum mechanics would represent instantiations
of all different possible worlds. In contrast Indra's pearls and holographs give us copies
of the exact same world.</big></p>

<p>&nbsp;</p>

<p><big><strong>FIVE TYPES OF MYSTICISM</strong></big></p>

<p><big><strong>Theistic Mysticism. </strong>The mystics can be categorized according to
the way in which they describe the ultimate reality they claim they have become one with.
Jewish, Islamic, and Christian mystics say that they have unified themselves with God, so
we can call this &quot;theistic&quot; mysticism. Examples are St. Catherine of Genoa, St.
Theresa, St. John of the Cross, and the Sufi mystics of Islam. As I have already said,
Christian mysticism is sometimes a mix of mystical union and vivid visions. </big></p>

<p><big><strong>Nontheistic or Monistic Mysticism. </strong>Asian mystics, neo-Platonists
like Plotinus, and some Christian mystics, such as Meister Eckhart and Jacob Boehme, claim
that the ultimate reality is an impersonal One. We can call this a nontheistic or monistic
mysticism. Eckhart speaks for them all: &quot;This identity out of the One into the One
and with the One is the source and fountainhead and breaking forth of glowing love.&quot;<a
HREF="mysticism.htm#N_20_"><sup>(20)</sup></a> </big></p>

<p><big>Some Buddhists and Eckhart have a provocative version of this type of mysticism.
The Mahayana Buddhists call the highest reality <em>shunyata</em> and Eckhart sometimes
called God, in his medieval German, <em>bloss nit</em>. Both of these terms mean
nothingness--not just emptiness, but a nothingness which is all--a no-thing-ness--no
particular thing. This undifferentiated reality is the focus and true object of the
mystical experience. As Eckhart states: &quot;The Godhead is poor, naked and empty as
thought it were not; it has not, wills not, wants, not works not, gets not. . . the
Godhead is as void as though it were not.&quot;<a HREF="mysticism.htm#N_21_"><sup>(21)</sup></a>
Comparative philosophers have pointed out the obvious similarity between Eckhart's Godhead
and the Hindu Brahman.</big></p>

<p><big><strong>Nature Mysticism. </strong>Nature mysticism is the most widespread form of
mystical experience. Many people have had powerful experiences of unity with nature that
they have declared to be ineffable. William Rowe's distinction between introvertive and
extrovertive mysticism clarifies the difference between nature mysticism and the first two
types above.<a HREF="mysticism.htm#N_22_"><sup>(22)</sup></a> Whereas theistic and monistic mysticism
involves a blocking of the senses, a &quot;self-emptying&quot; of the soul, and a
withdrawal from the world, nature mystics merge with nature and their senses become more
acute, seeing things as &quot;they really are.&quot; Zen Buddhism and Chinese Daoism are
very good examples of nature mysticism. We ordinary folk think that we perceive mountains,
streams, and valleys, but Zen techniques jolt us out of these banal perceptions so that we
come to see mountains, streams, and valleys for the very first time.&nbsp; The first
chapter of the <em>Daodejing</em> speaks of the Dao as ultimate reality, beyond God (<em>di</em>)
and nature, and it insists that the Dao is nameless.&nbsp; The ineffability criterion of
the OED definition is even stronger here in Daoism.&nbsp; Even so the Daoist agrees with
the Zen Buddhist in the ability to apply names to the differentiated world of mountains,
streams, and valleys.</big></p>

<p><big><strong>Performance Mysticism</strong>. Even amateur musicians speak of the
experience of union with their instruments and all performers have to admit that they
cannot tell us in words how it is that they use their fingers or their lips. Performance
mysticism is central to Zhuangzi's philosophy and his most famous character is Cook Ding,
who knows the joints of the animals he carves so well that he never dulls his knife,
claiming to cut through the nothingness between the bones and ligaments. One might say
that in nature and performance mysticism we are straying from the experience of total
union the theistic and monistic mystics claim. While the criterion of ineffability is
still fulfilled, the world of the nature and performance mystic, especially the latter, is
fully differentiated. One might say that the perception of a beautiful object and the
playing an instrument may approach the same type of unity experience.</big></p>

<p><big><strong>Chemical Mysticism</strong>. Some might speculate that the mystical
experience is &quot;all in the head,&quot; and might propose that it is nothing but an
altered state of consciousness. If this is so, then the experience could be created either
by some form of brain stimulation or the ingestion of a drug. In his book <em>The Doors of
Perception</em>, Aldous Huxley reports about his experiments with the drug mescaline and
the altered states of consciousness that it produced. Most drug experiences, however,
appear visionary rather than mystical, because they usually contain vivid content and most
subjects appear to be able to give some account of them. Furthermore, neuroscientists such
as James Austin claim that Zen satori cannot be induced by drugs.<a HREF="mysticism.htm#N_23_"><sup>(23)</sup></a>
But it is possible that we will one day find a way to stimulate the brain or find a drug
that simulates the mystical experience. Such a reductionistic account is unsatisfying for
most religious scholars, but we cannot rule out the possibility.</big></p>

<p><big>I am prepared to defend the mystics against a very common charge, one brought
often by scientists and others who prize rational thought. This charge is that the
mystical vision is irrational. I believe that this criticism is misdirected. On the face
of it, there is nothing logically incoherent about a view that reality is an
undifferentiated unity. In fact, the Greek philosopher Parmenides, the only pre-Socratic
thinker who had a clear grasp of the principle of contradiction, used logic to attempt a
proof of absolute monism. Mysticism is not necessarily irrational; rather, it is just
highly improbable given what we know about the world. The other aspect of this charge is
that mystical experiences are irrational because they cannot possibly happen. With some
exceptions of course, I believe that we should believe mystics when they claim that they
have had these experiences. It is the interpretation of the experiences that we can
challenge, and I believe that we can do that very successfully. I am personally convinced
that the total unity they experience is in the mind and not in reality.</big></p>

<p>&nbsp;</p>

<p><big><strong>ENDNOTES</strong></big></p>

<p><br WP="BR1">
<a NAME="N_1_">1. </a><font FACE="Times New Roman">Quoted in Aldous Huxley, <i>The
Perennial Philosophy</i> (New York: Harper &amp; Row, 1970), p. 11.</font></p>

<p><a NAME="N_2_">2. </a><font FACE="Times New Roman">Quoted in ibid., p. 12.</font></p>

<p><a NAME="N_3_">3. </a><font FACE="Times New Roman">I was gratified to learn that Alfred
Kazin agrees with me: &quot;Actually the keystone of Blakes&#146;s creative work is not
mysticism, but vision, and his deepest concern was &#145;that he should get all his vision
down, through all the arts open to him&quot; (<i>William Blake</i>, ed. Alfred Kazin [New
York: Viking Press, 1946]), backcover.</font></p>

<p><a NAME="N_4_">4. </a><font FACE="Times New Roman">I have drawn the two types of
meditation from Damien Keown&#146;s excellent book <i>The Nature of Buddhist Ethics </i>(New
York: St. Martin&#146;s Press, 1992, pp. 77-80), but if I am correct then R. M. Gimello is
incorrect in calling these meditative experiences &quot;mystical&quot; (cited in ibid., p.
81).</font></p>

<p><a NAME="N_5_">5. </a><font FACE="Times New Roman">In my own work I call this
&quot;spiritual Titanism.&quot; See my &quot;Hindu Titanism,&quot; <i>Philosophy East
&amp; West</i> 45:1 (January, 1995), pp. 73-96. See also my <i>Spiritual Titanism: Indian,
Chinese, and Western Perspectives</i> (Albany, NY: State University of New York Press,
2000).</font></p>

<p><a NAME="N_6_">6.&nbsp; </a><font FACE="Times New Roman">Gary Zukav, <i>The Dancing Wu
Li Masters</i> (New York: Bantam Books, 1980), p. 257.</font></p>

<p><a NAME="N_7_">7. </a><font FACE="Times New Roman">Ibid. I owe the dance metaphor to
J�rn Bindslev Hansen, a Danish physicist.</font></p>

<p><a NAME="N_8_">8. </a>Ibid, p. 258..</p>

<p><a NAME="N_9_">9. </a>Ibid., p. 72.</p>

<p><a NAME="N_10_">10. </a>Quoted in ibid.</p>

<p><a NAME="N_11_">11. </a><font FACE="Times New Roman">Fritjof Capra, <i>The Tao of
Physics</i> (Boston: Shambhala, 3<sup>rd</sup> ed., 1991), p. 305</font></p>

<p><a NAME="N_12_">12. </a>Ibid., pp. 139-40. </p>

<p><a NAME="N_13_">13.</a>&nbsp; <font FACE="Times New Roman">See my &quot;The Yogi and
the Goddess,&quot; <i>International Journal of Hindu Studies</i> 1:2 (August, 1997), pp.
265-87. This article as been expanded as Chapter Six in <i>Spiritual Titanism.</i></font></p>

<p><a NAME="N_14_">14. </a>Zukav, p. 29.</p>

<p><a NAME="N_15_">15. </a>Ibid.</p>

<p><a NAME="N_16_">16. </a>Ibid., p. 31.</p>

<p><a NAME="N_17_">17. </a><font FACE="Times New Roman">Amit Goswami, <i>The Self-Aware
Universe</i> (New York: J. P. Tarcher, 1995).</font></p>

<p><a NAME="N_18_">18. </a>Zukav, p. 84.</p>

<p><a NAME="N_19_">19. </a>Capra, p. 296.</p>

<p><a NAME="N_20_">20. </a>Quoted in Huxley, p. 88.</p>

<p><a NAME="N_21_">21. </a>Quoted in Huxley, p. 25.</p>

<p><a NAME="N_22_">22. </a><font FACE="Times New Roman">William L. Rowe, <i>Philosophy of
Religion</i> (Encino, CA: Dickenson Publishing Co., 1987), chap. 5.</font></p>

<p><a NAME="N_23_">23. </a><font FACE="Times New Roman">James M. Austin, <i>Zen and the
Brain</i> (Cambridge, MA: MIT Press, 1998).</font></p>
</body>
</html>


