<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>The Universal Family: More than Metaphor?</title>
</head>

<body>

<h1 align="center">The Universal Family: More than Metaphor?</h1>

<h3 align="right">

<br>
For the Ohio Academy of Religion, March 25, 2000<br>
<br>
Jeffrey Wattles, Department of Philosophy, Kent State University (<a href="mailto:jwattles@kent.edu">jwattles@kent.edu</a>)</H3>

<p align="center"><a href="default.htm"><img border="0" src="home2.gif" width="101" height="40"></a><a href="resources.htm"><img border="0" src="resources2.gif" width="101" height="40"></a></p>
<hr>
<p>
The concept of a humankind as members in a universal family--the fatherhood of God and the brotherhood of man--was proclaimed a century ago as the culmination of theology, an umbrella for interfaith dialogue, an inspiration for social reform, and the essence of religion. The very phrase--"the fatherhood of God and the brotherhood of man"--carried rich implications. It implied the unity and personality of God. It suggested that God, while transcending the believer, is also close; the believer may experience the divine by choosing to relate as a son or daughter to God. The sequence of elements in the phrase implied the primacy of the individual's relationship with God. The social consequence of that primary relationship was the brotherhood of man. Furthermore, talk of brotherhood addressed key challenges to modern society--to dissolve the forces that tear the fabric of humankind, including religious intolerance, nationalism, racism, sexism, economic and political injustice.<br>
<br>
One clarifying remark before going further. Alongside my choice to sustain an older terminology, I affirm the equality of women with men, the right of each person to choose his or her name for God, and the legitimacy of inquiring into the motherhood of God.<br>
<br>
Although the proclamation of the fatherhood of God is most prominent in the teaching of Jesus, many traditions have an interest in the father concept of God. The Bhagavad-Gita presents Krishna revealing his divine nature in chapter 9.17 with the words, "I am the universal father, mother . . . ." One of the most popular texts of Neo-Confucianism, the Western Inscription of Chang Tsai [1020-1077 C.E.], begins, "Heaven is my father and earth is my mother. . . . All people are my brothers and sisters." The twelfth century Japanese prophet Nichiren, in his Dedication to the Lotus, gives a record of a revelatory experience: "Our hearts ache and our sleeves are wet [with tears] until we hear the voice of one who says, "I am your father." A prayer of the Nuer tribe of Africa begins, "Father, it is your universe, it is your will." In the Hebrew Bible, Malachi asks rhetorically, "Have we not all one father? Has not one God created us?"<br>
<br>
A broad concept of family manifests continuing vitality down to the present day. Pittsburgh Pirates fans danced and sang "We are family" as their multi-ethnic team won its way to the 1979 World Series championship. The persistent performing of Beethoven's Ninth Symphony testifies to a longing for affirmations that are easier to sing than to say. The universal family theme is found in Mexican mariachi music and Eastern European Jewish folk music--and heightened when these bands play together. Even the Journal of the American Academy of Religion published a recent article in which Robert Bellah writes about brotherhood without qualification or apology.<br>
<br>
Nevertheless, talk of the fatherhood of God and the brotherhood of man has all but disappeared in North American academic and ecclesial life. Liberals are uneasy about the alleged sexism in the concept of the fatherhood of God, and conservatives are uneasy about the seeming indifference to saving faith in the concept of the brotherhood of man.<br>
<br>
I'm starting research for a book discussing these issues, and I'm hoping for your critical help. My thesis as a religious philosopher is that the message of the universal family, emergent in the New Testament Gospels and elsewhere, is a revelatory proclamation of relationships.<br>
<br>
One line of objection to my thesis runs as follows. Talk of the fatherhood of God is a metaphor. The metaphor transports into a new context language whose primary meaning derives from natural family life. Many metaphors may be used to express the mystery of transcendence, and it is important not to get caught in an idolatrous fixation on any particular metaphor, especially one whose associations with patriarchy are ethically unacceptable. To call God our father is an anthropomorphic projection, which may have some acceptable function for human beings as long as we understand its limitations. Similarly, talk of the brotherhood of man or siblinghood of humankind is another metaphor, just one of many possible ways to express human solidarity with its connotations of caring and cooperation, obligation and commitment, emotional harmony and shared ideals.<br>
<br>
In what follows I first sketch the line I take in defending the revelatory character of the fatherhood of God in conversation with metaphor theory. Next I focus on the experience of relating to other human beings as "brother" and "sister." I present and discuss a series of experiences in which the language of family is used. It is not a question of proving my thesis but rather of keeping a door open for the thought that the metaphoric function of family talk is secondary to its primary analogical function.<br>
<br>
Humble origins as a clue to the Creator's plan<br>
<br>
In many presentations of the theory that the fatherhood of God is a metaphor, it is suggested that the child's experience of the father is the basis for the metaphor. A psychological claim about early childhood development is implicitly used by the metaphor theorist in explaining the tendency to call God "father." The explanation is that the father is the young child's first image of God. Claire Thurston has written that the father is typically the first major Other for the child. The mother is the child's immediate natural universe of origin and the source of nurturing in an experience where mind and body are hardly differentiated. One psychologist who sets forth a psychoanalytic interpretation is Anthony J. De Luca. His book, Freud and Future Religious Experience, emphasizes the logical point that the early childhood origins of the concept of God do not prevent the adult from developing a mature concept of God. The genesis of religious experience need not be its telos. De Luca implies that the mature adult can leave the father concept of God behind. However, an alternate possibility is that the mature adult can develop a mature conception of the fatherhood of God.<br>
<br>
There are undoubtedly many factors that influence the way people refer to God, but what I want to point out is that there is a possible harmony of science and religion here in which the scientific account is accepted, not denied. Put in more general terms, a religiously affirmative person may agree there are indeed evolutionary factors that help us understand the development of religion, and that social scientific explanations of religion often help us understand part of the story.&nbsp;<br>
<br>
It remains open to the religionist, however, to affirm that the evolution of religion is a process whose main lines are part of the Creator's plan. The main principle of harmony between science and religion that I propose is the following: Science explores processes that philosophy can interpret as serving divine purposes. Thus, far from humiliating the fatherhood of God, the psychological insight to which the theorist of metaphor appeals can be viewed as helping to make the case for the fatherhood of God.<br>
<br>
While both analogy theorists and theorists of metaphor would agree that to call God "a rock" is to use a metaphor, the analogy theorist would say that to call God good is to use language analogically. In other words, goodness is a predicate that applies to God first and foremost. The creature is good in a secondary and derivative sense. The creature may be said to participate in the Creator's goodness. What I am proposing is that calling God "father" has its genesis as a metaphor in early childhood, but has its telos as an analogical term. In other words, parental terms apply to God first and foremost. Human parents manifest parenting in a secondary and derivative way pertaining to the creature. The better a human parent is, the more that parent reveals the qualities of the divine Parent. If so, then metaphor theory grasps part of the story, but not the full story of the significance of using parental terms to refer to God.<br>
<br>
Relating to another person as a brother or sister: A spectrum of experiences<br>
<br>
For speakers as well as for hearers, talk of family in a broad sense may be more or less disconnected from reality. For example, if the leader of a company speaks of the employees as a family, the message might not take. The employees might feel manipulated. If a religious leader speaks of fellow believers or of humankind as a family, even though the speaker may be expressing a genuine feeling or insight, the hearers might lack the comparable realization. It might take long practice of the golden rule or the law of love for the neighbor before the feeling of kinship begins to surface.<br>
<br>
Perhaps the most common use of family language in a broad sense expresses a special bond. For example, a person may hear another person express in a spontaneous and fresh way insights or commitments that she shares. She may feel an impulse to exclaim "Sister!" or "Right on, brother!" This experience has a few key features. It involves grasping some characteristic in the other person. The characteristic in question is central to who she experiences the other person to be. Furthermore, she identifies with that characteristic in the other. The characteristic is central to her sense of who she is, too.<br>
<br>
There is another feature of this kind of experience. The characteristic in question is uncommon. The other in whom she rejoices may feel to her like an ally, set apart from the rest of humankind, perhaps as one who realizes something that most of the world does not know. Thus there may be a quasi-partisan quality to the experience. The pressure of living with people who dispute one's central values contributes to a sense of relief in finding people who share those values. To the extent that this is so, the community that is discovered is not the universal family of God, but some more restricted community, however legitimate and important that community may be. All that experiences of this type show is that some people show a tendency to speak in terms of family in circumstances beyond the family in the flesh. But it is one thing to regard others in one's religious group, for example, as brothers and sisters, and another thing to regard humankind as a family.<br>
<br>
The universal family advocate can meet that objection as follows. There is a joy in discovering the beauty of another person. It is true that finding a kindred mind makes it easier to delight in someone. If partisan alliances are sometimes part of the phenomenon, something more is often coming to realization. Not only may the alliance in question may be devoted to the progress of humanity. In addition, the experience of finding a kindred mind or spirit facilitates recognition of the beauty of the unique and wonderful created personality that we often fail to recognize in daily interaction. It is uncommon to enjoy a bright awareness of another person. Nevertheless, it is the business of religion to promote a higher level of living. It takes a considerable transformation to cherish as the core of one's identity characteristics that are shared by everyone else, such as being a unique personality created by God and infinitely loved as a member of a wonderful family.<br>
<br>
Sometimes the impulse to call another person "brother" or "sister" arises from a moment of love. The other person may not have any particular characteristic that functions as a basis for an alliance. There may, for example, be radical differences regarding religion. In this experience there is then no quality of partisanship, no categorical separation of the individual from the mass of humankind. The unique other is simply bathed in a regard of compassion and tenderness and enjoyment.<br>
<br>
How should such an experience be interpreted? According to metaphor theory, experiencing and referring to others as brothers and sisters who are not one�s literal siblings in the flesh involves using a metaphor. The metaphor is based on experiences in the natural family. As one fourth-grader put it bluntly, "When we use figurative language, we don't really mean it."<br>
<br>
If the universal family is a reality, then metaphor theory is reductionist insofar as it implies that there is a complete social-psychological explanation for the broad use of family language, making any reference to a common source in a divine Parent otiose.<br>
<br>
Phenomenologically speaking, however, the experience does not require this explanatory hypothesis. This is so for three reasons. First, the experience presents itself as a recognition of an existing kinship, not as a comparison or act of imagination. Second, family talk in such situations consistently has idealistic connotations, whereas family memories are a mix of positive and negative. Third, some people who have no natural brothers or sisters speak of brothers and sisters in a broad sense.<br>
<br>
The reductionist could develop a more complex hypothesis in response to these points. Where a person had no siblings in the flesh or where the overtones in relating with them were negative, then it is arguable that unconscious desires for fulfilling relationships with siblings supply the energy and the joy of discovering a kindred mind or spirit. Relationships with natural siblings, moreover, perform such an important function that they tend to be cherished despite their manifest imperfections, just as life is cherished despite its pain. In addition, the tendency to use family language can be partly explained by cultural patterns influenced by religion.<br>
<br>
The risk for the complex reductive hypothesis is that the more it appeals to unconscious mechanisms the less obvious it is. Metaphor theory has a flavor of obviousness to it. Of course experience with rocks (together with a striking experience of divine reliability) is the basis for using the word "rock" to refer to God. Of course experience with one's natural family is the basis for the broad use of family terms. However, when a reductive metaphor theory can only be sustained by complex hypotheses, then the self-evidence of the theory is gone.<br>
<br>
The analogy theorist on this topic again claims that social-psychological analysis tells part of the story, but not the heart of the story. The child's experience of the natural family is the runway for the flight of a higher realization. Analogy theory proposes that experiences of recognizing someone as a brother or sister--especially across alleged barriers such as culture, race, and religion--is an intimation of our belonging in a universal family of God.<br>
<br>
It is hardly possible to prove the thesis of the reality of the universal family of God, a thesis which has diminished appeal when the language commonly traditionally to express the concept has been widely criticized as sexist. Nevertheless, the concept remains an enduring option. I have sketched how this thesis can incorporate the psychological insight to which the metaphor theorist appeals, and I have described some experiences of human kinship. Again, the goal here is simply to keep open the possibility of an analogical interpretation of family language.<br>
<br>
What kind of harmony between theorists is possible? The metaphor theorist might profoundly respect family language for God while insisting that the greatest spiritual teachers who used family language were using metaphors. The analogy theorist on this topic will insist on the wonders of actually engaging in relating to God as father and to other persons as the sons and daughters of God. In the end, it is possible to agree that philosophy of language debates are not essential to religious living. What is essential is not our interpretation of language but the quality of our lives. We are free to choose terms reflecting our comprehension of the relationships we have discovered and free to pursue intellectual inquiry in collegial community.</p>
<hr>
<p><b>
Notes</b><br>
<br>
1. <u> The Bhagavad-Gita</u>, tr. Barbara Stoler Miller (New York: Bantam, 1986), 9.17.<br>
<br>
2. In Wing-Tsit Chan, tr. and ed., <u> A Source Book in Chinese Philosophy</u> (Princeton: Princeton University Press, 1963), p. 497.<br>
<br>
3. In William Theodore de Bary, ed., <u> The Buddhist Tradition in India, China, and Japan</u> (New York: Vintage, 1972), p. 349.<br>
<br>
4. Malachi 2.10.<br>
<br>
5. Jeffrey Kaye's August 3, 1999, story of brotherhood in song is saved on the archives of The News Hour with Jim Lehrer found at http://www.pbs.org/newshour/bb/entertainment/july-dec99/mariachi_8-3.html<br>
<br>
6. Robert N. Bellah, "Is There a Common American Culture?" <u> JAAR</u>, 66.3 (Fall 1998), 613-625. The essay was delivered as a plenary address at the AAR meting in San Francisco on November 22, 1997.<br>
<br>
7. My primary source for the theory of analogy is Philip Rolnick's book, <u> Analogical Possibilities</u> (Atlanta: Scholars Press, 1993). The book, which has received over a half-dozen very positive reviews, discusses the Thomist theory of W. Norris Clarke, the Wittgensteinian thought of David Burrell, and the evangelical ideas of Eberhart Jungel.<br>
<br>
8. Totowa: N.J.: Littlefield, Adams &amp; Co., 1977.<br>
<br>
9. Most often, this identifying implies that what one finds in the other is essentially like some experience, insight, or commitment of one's own, though the identifying might arise also in cases where the other expresses in an excellent way an ideal toward which one also strives.<br>
<br>
10. The quote was broadcast on National Public Radio's "All Things Considered," March 22, 2000. The comment was made by a fourth grader, apparently selected by her teacher as an example of successful learning at that level. While a more advanced understanding would be more nuanced, I believe that the student captured the drift of metaphor theory.<br>
</p>
<hr>
<p><small><small>Copyright 2002 - Kent State University - ALL RIGHTS RESERVED<br>
Problems? Questions? Need help? Contact <a href="mailto:deb@dl.kent.edu">deb@dl.kent.edu</a><br>
Course built and delivered by<a href="http://www.kent.edu"> Kent State
University</a><a href="http://business.kent.edu"> </a><a href="http://www.dl.kent.edu">Distributed
Learning</a>.</small></small></p>

</body>

</html>


