<?php
/*
Plugin Name: Admin Welcome Panel
Plugin URI: http://www.soliantconsulting.com
Description: Custom admin welcome panel.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/

class Admin_Welcome_Panel {
  private static $instance;

  public static function getInstance() {
    if (self::$instance == NULL) {
      self::$instance = new self();
    }

    return self::$instance;
  }

  private function __construct() {
    // implement hooks here
    remove_action('welcome_panel','wp_welcome_panel');

    add_action('welcome_panel',array($this,'welcome_panel'));

    add_action('admin_enqueue_scripts',array($this,'add_css'));
  }

  public function welcome_panel(){
    ?>
    <div class="welcome-panel-content">
      <h3><?php _e('Welcome to Admin Wabash Center Website'); ?></h3>
      <p><?php _e('Admin dashboard for Wabash Center!'); ?></p>
    </div>
    <?php
  }

  public function add_css(){
     wp_register_style('admin-welcome-panel-css',plugin_dir_url(__FILE__).'/css/admin-welcome-panel.css');
     wp_enqueue_style('admin-welcome-panel-css');
  }

}

Admin_Welcome_Panel::getInstance();
