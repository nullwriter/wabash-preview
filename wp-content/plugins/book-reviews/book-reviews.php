<?php
/*
Plugin Name: Book Reviews
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the book reviews CPTs.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/

// book_review table
// CREATE TABLE `wp_books_reviews` (
//   `id` int(11) NOT NULL AUTO_INCREMENT,
//   `__fk_BookID` bigint(20) DEFAULT '0',                                      // coming from FM but ignore on WP
//   `title` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `author` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,             // add meta
//   `publisher` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,          // add meta
//   `year` year(4) DEFAULT NULL,                                               // add taxonomy
//   `book_cover_image` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `book_publisher_url` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL, // add meta
//   `isbn` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL,               // add meta
//   `Web_flag` tinyint(1) DEFAULT '0',                                         // coming from FM, 1 post_status = publish 0 post_status = notforweb
//   `reviewed` tinyint(1) DEFAULT '0',                                         // add meta
//   `pages` smallint(5) unsigned NOT NULL DEFAULT '0',                         // add meta
//   `price` decimal(6,2) NOT NULL DEFAULT '0.00',                              // add meta
//   PRIMARY KEY (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*

object(stdClass)[1258]
  public '__fk_BookID' => string '1' (length=1)
  public 'title' => string 'The College Student’s Introduction to Christology' (length=51)
  public 'author' => string 'Loewe, William P.' (length=17)
  public 'publisher' => string 'Liturgical Press - St. John’s Abbey' (length=37)
  public 'year' => string '1996' (length=4)
  public 'book_cover_image' => string md5 of image
  public 'book_publisher_url' => string 'http://www.litpress.org/Search.aspx?q=The+College+Student%E2%80%99s+Introduction+to+Christology&q3.x=0&q3.y=0' (length=109)
  public 'isbn' => string '0-8146-5018-X' (length=13)
  public 'Web_flag' => string '' (length=0)
  public 'reviewed' => string '' (length=0)
  public 'pages' => string '' (length=0)
  public 'price' => string '' (length=0)
  public 'image_name' => string '9D542DF73D5B7A4139D6C2589A5931A8:Untitled.pct' (length=45)
  public 'image_content' => string base64
  public 'image_md5' => string '9D542DF73D5B7A4139D6C2589A5931A8' (length=32)
  public 'image_filename' => string 'books-the-college-students-introduction-to-christology.jpeg' (length=59)

 */

/*
Usefull queries
SELECT * FROM `wp_posts` WHERE post_type="book_reviews"
SELECT * FROM `wp_postmeta` WHERE post_id IN (SELECT ID FROM `wp_posts` WHERE post_type="book_reviews")

Max one postmeta values per post

_thumbnail_id

can have a feature image so post attachment

have a  year taxonomy like tag
have regular tags

*/

/*
DISPLAY
book cover image
Title

Author
Publisher
Year
Tags (if not empty)

Reviewed by
First 50 words of review

 */

/*
SORT
Relevance
Title
Publisher
Book Author
Year
Book Review Author



 */
// Register Custom Post Type
function book_reviews_cpt() {

	$labels = array(
		'name'                  => _x( 'Book Reviews', 'Post Type General Name', 'book_reviews_cpt' ),
		'singular_name'         => _x( 'Book Review', 'Post Type Singular Name', 'book_reviews_cpt' ),
		'menu_name'             => __( 'Book Reviews', 'admin menu', 'book_reviews_cpt' ),
		'name_admin_bar'        => __( 'book_reviews', 'add new on admin bar', 'book_reviews_cpt' ),
		'archives'              => __( 'Item Archives', 'book_reviews_cpt' ),
		'parent_item_colon'     => __( 'Parent book_review:', 'book_reviews_cpt' ),
		'all_items'             => __( 'All Book Reviews', 'book_reviews_cpt' ),
		'add_new_item'          => __( 'Add New Book Review', 'book_reviews_cpt' ),
		'add_new'               => __( 'Add New Book Review', 'book_reviews_cpt' ),
		'new_item'              => __( 'Add New Book Review', 'book_reviews_cpt' ),
		'edit_item'             => __( 'Edit Book Review', 'book_reviews_cpt' ),
		'update_item'           => __( 'Update Book Review', 'book_reviews_cpt' ),
		'view_item'             => __( 'View Book Review', 'book_reviews_cpt' ),
		'search_items'          => __( 'Search Book Reviews', 'book_reviews_cpt' ),
		'not_found'             => __( 'No Book Review found', 'book_reviews_cpt' ),
		'not_found_in_trash'    => __( 'No Book Review found in Trash', 'book_reviews_cpt' ),
		'featured_image'        => __( 'Featured Image', 'book_reviews_cpt' ),
		'set_featured_image'    => __( 'Set featured image', 'book_reviews_cpt' ),
		'remove_featured_image' => __( 'Remove featured image', 'book_reviews_cpt' ),
		'use_featured_image'    => __( 'Use as featured image', 'book_reviews_cpt' ),
		'insert_into_item'      => __( 'Insert into item', 'book_reviews_cpt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'book_reviews_cpt' ),
		'items_list'            => __( 'Items list', 'book_reviews_cpt' ),
		'items_list_navigation' => __( 'Items list navigation', 'book_reviews_cpt' ),
		'filter_items_list'     => __( 'Filter items list', 'book_reviews_cpt' ),
	);
	$args = array(
		'label'                 => __( 'Book Reviews', 'book_reviews_cpt' ),
		'description'           => __( 'Book Review Post Type', 'book_reviews_cpt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail' ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'rewrite'               => array( 'slug' => 'book_reviews' ),
		'menu_icon'	            => 'dashicons-book-alt',
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'capabilities'          => array('create_posts' => false),
		'map_meta_cap'          => true, // Set to `false`, if users are not allowed to edit/delete existing posts
	);
	register_post_type( 'book_reviews', $args );

}
add_action( 'init', 'book_reviews_cpt');

function book_reviews_taxonomies() {

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Book Year', 'taxonomy general name' ),
		'singular_name'              => _x( 'Book Year', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Book Years' ),
		'popular_items'              => __( 'Popular Book Years' ),
		'all_items'                  => __( 'All Book Years' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Book Year' ),
		'update_item'                => __( 'Update Book Year' ),
		'add_new_item'               => __( 'Add New Book Year' ),
		'new_item_name'              => __( 'New Book Year Name' ),
		'separate_items_with_commas' => __( 'Separate book years with commas' ),
		'add_or_remove_items'        => __( 'Add or remove book year' ),
		'choose_from_most_used'      => __( 'Choose from the most used book years' ),
		'not_found'                  => __( 'No book years found.' ),
		'menu_name'                  => __( 'Book Year' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'book-year' ),
	);

	register_taxonomy( 'book-year', 'book_reviews', $args );

}
add_action( 'init', 'book_reviews_taxonomies');


function book_reviews_metaboxes(){
	add_meta_box("book-reviews-author-meta", "Extra Book Review Info", "display_book_reviews_meta_box", "book_reviews", "normal", "high");
}

function display_book_reviews_meta_box(){
  	global $post;


  	wp_nonce_field( basename( __FILE__ ), 'book_reviews_nonce' );

	$book_review = get_book_reviews_object($post->ID);


  ?>

<div class="form-group">
  <label for="book-reviews-author" class="col-sm-2 control-label">Author:</label>
  <div class="col-sm-10">
    <input name="book-reviews-author" id="book-reviews-author" readonly class="form-control" placeholder="Author" value="<?php echo $book_review->author; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="book-reviews-publisher" class="col-sm-2 control-label">Publisher:</label>
  <div class="col-sm-10">
    <input name="book-reviews-publisher" class="form-control" readonly placeholder="Publisher" value="<?php echo $book_review->publisher; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="book-reviews-publisher-url" class="col-sm-2 control-label">Publisher Url:</label>
  <div class="col-sm-10">
    <input name="book-reviews-publisher-url" class="form-control" readonly placeholder="Publisher Url" value="<?php echo $book_review->book_publisher_url; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="book-reviews-isbn" class="col-sm-2 control-label">ISBN:</label>
  <div class="col-sm-10">
    <input name="book-reviews-isbn" class="form-control" readonly placeholder="ISBN" value="<?php echo $book_review->isbn; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="book-reviews-pages" class="col-sm-2 control-label">Pages:</label>
  <div class="col-sm-10">
    <input name="book-reviews-pages" class="form-control" readonly placeholder="Pages" value="<?php echo $book_review->pages; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="book-reviews-price" class="col-sm-2 control-label">Price:</label>
  <div class="col-sm-10">
    <input name="book-reviews-price" class="form-control" readonly placeholder="Price" value="<?php echo $book_review->price; ?>" />
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <div class="checkbox">
      <label for="book-reviews-reviewed">
      <input type="checkbox" name="book-reviews-reviewed" id="book-reviews-reviewed" value="1" <?php if ( !empty ( $book_review->reviewed ) ) { echo ' checked="checked"';} ?> />
      Check if the book has been reviewed.
      </label>
    </div>
  </div>
</div>

<?php add_thickbox(); ?>

<a id="myanchor" href="#TB_inline?width=100%&height=100%&inlineId=modal-window-id" class="thickbox" style="display:none;">Modal Me</a>

<div id="modal-window-id" style="display:none;">
    <p style="padding: 40px; font-size: 15px;">
			Sorry you can't add or edit any book_reviews content from WordPress.<br />
			This content is coming from FileMaker, and you should use it
			if you want to add or modify any content.<br />
			The content is showing here just for your convenience.

		</p>
</div>

<?php
}

add_action('add_meta_boxes_book_reviews', 'book_reviews_metaboxes');

function save_book_reviews_metaboxes( $post_id, $post, $update ) {
   global $wpdb;


// Checks save status
$is_autosave = wp_is_post_autosave( $post_id );
$is_revision = wp_is_post_revision( $post_id );
$is_valid_nonce = ( isset( $_POST[ 'book_reviews_nonce' ] ) && wp_verify_nonce( $_POST[ 'book_reviews_nonce' ], basename( __FILE__ ) ) ) ? TRUE : FALSE;




// Exits script depending on save status
if ($is_autosave || $is_revision || !$is_valid_nonce ) {
		return;
}

$book_review = get_book_reviews_object($post_id);
//$book_review = get_book_reviews_object(70299);//211054

  if($update){


	 //$book_review         = array();
	 $book_review->title = $post->post_title;
	 $book_review->author = $_POST[ 'book-reviews-author' ];
	 $book_review->publisher = $_POST[ 'book-reviews-publisher' ];
	 $book_review->book_publisher_url = $_POST[ 'book-reviews-publisher-url' ];
	 $book_review->isbn = $_POST[ 'book-reviews-isbn' ];
	 $book_review->reviewed = !empty($_POST[ 'book-reviews-reviewed'])?1:0;
	 $book_review->review = $_POST[ 'content' ];
	 $book_review->pages = $_POST[ 'book-reviews-pages' ];
	 $book_review->price = $_POST[ 'book-reviews-price' ];
	 $book_review->reviewer1_name = $_POST['coauthors-main'];
	 if(count($_POST['coauthorsinput'])>1 ){
	 $book_review->reviewer2_name = $_POST['coauthorsinput'][0];
	 }
	 else{
		$book_review->reviewer2_name = '';
	 }

	if(empty($book_review->date_reviewed) || $book_review->date_reviewed === '0000-00-00'){
		 //echo 'here in empty date reviewed<br/>';
		 $book_review->date_reviewed = date("Y-m-d");
	 }

	if(isset($_POST['post_date'])) {// user is overwriten reviewed date
		$book_review->date_reviewed = date("Y-m-d", strtotime($_POST['post_date']));
		//echo 'here in setting date reviewed<br/>';
	}

	 $book_review->post_modified = date("Y-m-d H:i:s");;
  //  echo '<hr/>';


	 if($book_review->post_id!=0){

		// $result=$wpdb->update(
		// 	'wp_books_reviews',
		// 	['review' => $book_review->review, 'reviewed' => $book_review->reviewed],
		// 	['id' => (INT)$book_review->id],
		// 	array( '%s', '%d' ), array( '%d' )
		// );
		$query = $wpdb->prepare('UPDATE wp_books_reviews SET review = %s, reviewed = %d, reviewer1_name = %s, reviewer2_name = %s, post_modified = %s, date_reviewed = %s WHERE id = %d LIMIT 1', $book_review->review,(int)$book_review->reviewed, $book_review->reviewer1_name, $book_review->reviewer2_name, $book_review->post_modified, $book_review->date_reviewed, (int)$book_review->id );
		$result = $wpdb->query($query);

	}


 }
 else{

	 //exit();
	 if($post->post_status=='auto-draft'){
		 return;
	 }
 }


}
add_action( 'save_post_book_reviews', 'save_book_reviews_metaboxes' , 10, 3 );


function is_reviewed($id){
	global $wpdb;

	$query = $wpdb->prepare('SELECT reviewed FROM wp_books_reviews WHERE post_id = %d LIMIT 1', $id);
    $reviewed = $wpdb->get_var($query);


	return $reviewed;
}

function add_book_columns($columns) {
    return array_merge($columns,
          array('reviewed' => 'reviewed'));
}
add_filter('manage_book_reviews_posts_columns' , 'add_book_columns');


function book_posts_custom_columns( $column, $post_id ) {

    switch ( $column ) {

    case 'reviewed' :
        echo is_reviewed($post_id);

        break;
    }
}
add_action('manage_book_reviews_posts_custom_column' , 'book_posts_custom_columns', 10, 2 );

// Register the column as sortable
function book_reviews_posts_column_register_sortable( $columns ) {
    $columns['reviewed'] = 'reviewed';
    return $columns;
}
add_filter( 'manage_edit-book_reviews_sortable_columns', 'book_reviews_posts_column_register_sortable' );




function add_book_reviews_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'book_reviews' === $post->post_type ) {
					wp_register_style( 'book_reviews_custom_css', get_stylesheet_directory_uri() . '/css/book_reviews.css', false, '1.0.0' );
					wp_enqueue_style( 'book_reviews_custom_css' );
					wp_enqueue_script(  'custom_book_reviews_js', get_stylesheet_directory_uri().'/js/book_reviews/custom.js' );
					wp_dequeue_script( 'autosave' );
        }
    }

}
add_action( 'admin_enqueue_scripts', 'add_book_reviews_admin_scripts', 10, 1 );

function remove_book_reviews_quick_edit_and_trash( $actions ) {
	global $post;
	  if ( 'book_reviews' === $post->post_type ) {
			unset($actions['inline hide-if-no-js']);
			unset($actions['trash']);
		}
		return $actions;
}
add_filter('post_row_actions','remove_book_reviews_quick_edit_and_trash',10,1);

function display_excerpt_book_reviews_object($book_review){
    global $wpdb;

    $html = '<p><a href="'.get_permalink($book_review->post_id).'">' .$book_review->title.'</a><br/>';
	$html .=limit_text(wp_strip_all_tags($book_review->review), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_book_reviews_object($item){
	 global $wpdb,$load_more_data;

    /*
    DISPLAY
    book cover image
    Title

    (Book-Review OR Book)
    Author
    Year
    Publisher
    Tags (if not empty)

    Reviewed by
    First 50 words of review

     */

    $single_results_page=is_page_template('template-section-results-page.php') || is_page_template('template-selected-resources-page.php') || $load_more_data;
    $detail_page=is_singular('book_reviews') || is_page_template('template-print-book-review-as-pdf.php');


	//var_dump($item);
	//var_dump($book_post_id);

	$terms = wp_get_post_tags($item->post_id);
	//var_dump($terms);

	//exit();


    $tags = array();

    foreach($terms as $tag){
        if(!empty($tag)) {
			$tags[]='<a href="'.get_category_link( $tag->term_id ).'" >'.$tag->name.'</a>';
        }
    }

    $tags=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$tags);


    //$full_content = force_balance_tags($item->review);
    $full_content = str_replace('\\"','"',force_balance_tags($item->review));

    $excerpt_content= limit_text(strip_tags($item->review, '<br><br/>' ), 70);

    $page = get_page_by_title('PRINT BOOK REVIEW AS PDF');



    //get book cover image
    if(!empty($item->image_url)){
        $img='<img class="img-responsive" src="'.$item->image_url.'" height="150" width="150" title="Cover image" alt="Cover image">';
    }
    else
    {
        $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/books.png'.'" height="150" width="150" title="Book cover image" alt="Book cover image">';
    }

    if(!empty($item->book_publisher_url))
    {
        $publisher_title="<a target='_blank' class=\"publisher_name\" href='".$item->book_publisher_url."'>".$item->title."<span class='icon_external_url'></span></a>";
    }else{
        $publisher_title=$item->title;
    }

	$html = '
<div class="panel card book_reviews-card">
   <div class="panel-body">
      <div class="row is-flex">
         <div class="card-image col-xs-4 col-sm-4 col-md-3">
            '.$img.'
         </div>
         <div class="card-select col-xs-2 col-xs-offset-6 col-sm-2 col-sm-offset-6 col-md-1 col-md-offset-0 col-md-push-8">';

        $html .= '<div class="card-checkbox">
                        <input value="'.$item->post_id.'" id="resource_'.$item->post_id.'" class="case" type="checkbox">
                    </div>';

    $html .= ' </div>
         <div class="card-info col-xs-12 col-sm-12 col-md-8 col-md-pull-1">
            <h3 class="card-title">
              '.$publisher_title.'
            </h3>
            <p class="card-meta">
                <span>'.(!empty($item->review)?'Book-Review':'Book').'</span><br>
				'.(!empty($item->author)?'<span>'.$item->author.'</span><br>':'').''
				.((!empty($item->year) && $item->year!='0000')?'<span>'.$item->year.'</span><br>':'').''
                .(!empty($item->publisher)?'<span>'.$item->publisher.'</span><br>':'').''
                .(!empty($item->review)?'<a class="link-pdf" target="_blank" href="'.get_permalink($page->ID).'/?id='.$item->post_id.'"><img class="icon-pdf" alt="icon" title="Download Review as a PDF" src="'.get_stylesheet_directory_uri().'/images/icon-pdf.png"/></a>':'');
				if(!empty($tags) ) {
					$html.= '<span>Tags: '.$tags.'</span><br>';
				}
				if (empty($item->reviewed)){
                    $html.= '<strong><span>Available for review</span></strong><br>';
                }
            $html.= '</p>
         </div>
      </div>';
        $html .= '
      <div class="row">
         <div class="card-content-container col-xs-12">';
		 	if(!empty($item->review)){
				if(empty($item->reviewer1_name) && !empty($item->reviewer2_name)){
						$item->reviewer1_name=$item->reviewer2_name;
						$item->reviewer2_name='';
				}
            $reviewers = stripslashes($item->reviewer1_name).(!empty( $item->reviewer2_name)?' and '.stripslashes($item->reviewer2_name):'');
            $html .='<div class="card-content">';

            $dateReviewed = "";
            if ( ((bool)strtotime($item->date_reviewed)) ) {
                $dateReviewed = $item->date_reviewed;
            }

			if(!$detail_page){
				$html .='<div class="less-more-content excerpt" data-id="'.$item->id.'">
				<strong>Reviewed by: </strong>' . $reviewers . '<br>
				'. (!empty($dateReviewed) ? '<strong>Date Reviewed: </strong>' . $dateReviewed . '<br>' : '') .'
				<div id="excerpt-content-'.$item->id.'">
					'.$excerpt_content.'
				</div>
				<div id="full-content-'.$item->id.'" class="full-content">
					'.$full_content.'
				</div>';

			  $html .='	<div id="' . $item->id . '" class="more-link-single-results2">
					  <a id="anchor-'. $item->id .'" title="Read More" href="javascript:void(0)">More</a>
					  <i class="fa fa-caret-down" aria-hidden="true"></i>
				  </div>
				  </div>';
			}
			else{
				$html .='<div>
				<strong>Reviewed by: </strong>' . $reviewers . '<br>
				<div>' . $full_content . '</div>
				</div>';
			}
			$html .= '</div>';
		}
		$html .= '
		</div>
      </div>
   </div>
</div>';


        return $html;


}

function display_book_reviews_sort_panel($sort_term){
	global $post_types,$search_string;

  $html='
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="post" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />
    <div class="col-md-12">
			  <div class="panel panel-default">
				  <div class="panel-heading">
					  <div>
						  <span class="panel-title">Sort by:</span>
					  </div>
				  </div>
				  <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="weight" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked"' : '').'
                                   id="relevance_radio">
								  <label for="relevance_radio">Relevance</label>
							  </div>
						  </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="radio ">
                                    <input type="radio" name="sort_term" value="date_reviewed" '.(($sort_term=='date_reviewed')? 'checked="checked"' : '').'
                                           id="year_radio">
                                    <label for="year_radio">Date of Review</label>
							  </div>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
	  </form>
	';
  return $html;
}


function get_book_reviews_object($id){
    global $wpdb;

	$query = $wpdb->prepare('SELECT * FROM wp_books_reviews WHERE Web_flag = 1 AND post_id = %d LIMIT 1', $id);
    $book_review = $wpdb->get_row($query);


	return $book_review;

}

function get_book_reviews_list($limit=0,$sort_term=''){
	global $wpdb;

	$sort_direction='ASC';


	if(!in_array($sort_term,array('title', 'author', 'publisher', 'year', 'post_modified', 'date_reviewed'))){
		$sort_term='';
	}
	if(in_array($sort_term,array('year', 'post_modified', 'date_reviewed'))){
		$sort_direction='DESC';
	}


	$query = $wpdb->prepare('SELECT * FROM wp_books_reviews JOIN wp_posts ON wp_posts.ID = wp_books_reviews.post_id WHERE Web_flag = 1 AND wp_posts.post_status = "publish" AND reviewed = %d'.(!empty($sort_term)?" ORDER BY wp_books_reviews.$sort_term $sort_direction":'').($limit?" LIMIT $limit":''), 1);
	//var_dump($query);
    $book_reviews = $wpdb->get_results($query);


	return $book_reviews;

}

function get_book_available_for_review_list($limit=0,$sort_term=''){
	global $wpdb;

	$sort_direction='ASC';


	if(!in_array($sort_term,array('title', 'author', 'publisher', 'year', 'post_modified'))){
		$sort_term='';
	}
	if(in_array($sort_term,array('year', 'post_modified'))){
		$sort_direction='DESC';
	}

	$query = $wpdb->prepare('SELECT * FROM wp_books_reviews JOIN wp_posts ON wp_posts.ID = wp_books_reviews.post_id WHERE Web_flag = 1 AND wp_posts.post_status = "publish" AND reviewed = %d'.(!empty($sort_term)?" ORDER BY wp_books_reviews.$sort_term $sort_direction":'').($limit?" LIMIT $limit":''), 0);
	//var_dump($query);
	$books = $wpdb->get_results($query);


	return $books;
}

function search_book_reviews($search_string, $sort_by = 'weight', $limit='', $offset='',$book_reviewed_filter='',$book_filter=''){
	global $wpdb;



	$limit_sql='';
    $offset_sql='';
    $sql_reviewed='';
    /*If filter by reviewed only*/
    if($book_reviewed_filter==1 and $book_filter!=1)
    {
        $sql_reviewed=' AND reviewed=1 ';
    }
    /*If filter by book only*/
    if($book_reviewed_filter!=1 and $book_filter==1)
    {
        $sql_reviewed=' AND reviewed=0 ';
    }
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
	}
	if($sort_by!= 'weight' && $sort_by!= 'year' && $sort_by!= 'date_reviewed' && $sort_by!= 'post_modified'){
		$sort_direction='ASC';
	}
	else{
		$sort_direction='DESC';
	}


	// var_dump($search_string);
	// var_dump($sort_by);
	// var_dump($sort_direction);
	// exit();


	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }
	//var_dump($terms_array);

	// where do you want to search
	$fields = array('title', 'review', 'author',  'publisher', 'reviewer1_name', 'reviewer2_name');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_books_reviews.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_books_reviews.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
	//exit();

	$sql="SELECT wp_books_reviews.*,
	if(
		wp_books_reviews.title = '$search_string',  100,
	         IF(
	             wp_books_reviews.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_books_reviews.review =  '$search_string',  40,
	        IF(
	             wp_books_reviews.review LIKE '%$search_string%', 30,

	                 if( {$terms_query['review']}, 7, 0)

	          )


	)
	+
    IF(
        wp_books_reviews.author =  '$search_string',  20,
            IF(
                 wp_books_reviews.author LIKE '%$search_string%', 10,

                     if( {$terms_query['author']}, 5, 0)

              )


    )
	+
	IF(
		wp_books_reviews.reviewer1_name =  '$search_string',  20,
			IF(
				 wp_books_reviews.reviewer1_name LIKE '%$search_string%', 10,

					 if( {$terms_query['reviewer1_name']}, 5, 0)

			  )


	)
	+
	IF(
		wp_books_reviews.reviewer2_name =  '$search_string',  20,
			IF(
				 wp_books_reviews.reviewer2_name LIKE '%$search_string%', 10,

					 if( {$terms_query['reviewer2_name']}, 5, 0)

			  )


	)
	+
	IF(
		wp_books_reviews.publisher =  '$search_string',  20,
			IF(
				 wp_books_reviews.publisher LIKE '%$search_string%', 10, 0



			  )


	) AS weight
	FROM wp_books_reviews
	WHERE wp_books_reviews.Web_flag = 1 $sql_reviewed
	HAVING weight > 0";
    if ($sort_by == 'title'){
        $sql .= "
	ORDER BY regex_replace('[^a-zA-Z\-]','',$sort_by) $sort_direction" . $limit_sql . $offset_sql;
    }else {
        $sql .= "
	ORDER BY $sort_by $sort_direction" . $limit_sql . $offset_sql;
    }

	//var_dump($sql);

	//exit();

	$book_reviews = $wpdb->get_results($sql);
	//var_dump($grants);

	//exit();
	//
	return $book_reviews;


}

function search_book_reviews_total($search_string,$book_reviewed_filter='', $book_filter=''){
	global $wpdb;


	// var_dump($search_string);

	// var_dump($sort_by);
	//
	// exit();
    $sql_reviewed='';
    if($book_reviewed_filter==1 and $book_filter!=1)
    {
        $sql_reviewed=' AND reviewed=1 ';
    }
    /*If filter by book only*/
    if($book_reviewed_filter!=1 and $book_filter==1)
    {
        $sql_reviewed=' AND reviewed=0 ';
    }

	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	//var_dump($terms_array);

	// where do you want to search
	$fields = array('title', 'review', 'author',  'publisher', 'reviewer1_name', 'reviewer2_name');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_books_reviews.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_books_reviews.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
	//exit();

	$sql="SELECT wp_books_reviews.id,
	if(
		wp_books_reviews.title = '$search_string',  100,
	         IF(
	             wp_books_reviews.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_books_reviews.review =  '$search_string',  40,
	        IF(
	             wp_books_reviews.review LIKE '%$search_string%', 30,

	                 if( {$terms_query['review']}, 7, 0)

	          )


	)
	+
    IF(
        wp_books_reviews.author =  '$search_string',  20,
            IF(
                 wp_books_reviews.author LIKE '%$search_string%', 10,

                     if( {$terms_query['author']}, 5, 0)

              )


    )
	+
	IF(
		wp_books_reviews.reviewer1_name =  '$search_string',  20,
			IF(
				 wp_books_reviews.reviewer1_name LIKE '%$search_string%', 10,

					 if( {$terms_query['reviewer1_name']}, 5, 0)

			  )


	)
	+
	IF(
		wp_books_reviews.reviewer2_name =  '$search_string',  20,
			IF(
				 wp_books_reviews.reviewer2_name LIKE '%$search_string%', 10,

					 if( {$terms_query['reviewer2_name']}, 5, 0)

			  )


	)
	+
	IF(
		wp_books_reviews.publisher =  '$search_string',  20,
			IF(
				 wp_books_reviews.publisher LIKE '%$search_string%', 10, 0



			  )


	) AS weight
	FROM wp_books_reviews
	WHERE wp_books_reviews.Web_flag = 1 $sql_reviewed
	HAVING weight > 0";


	//var_dump($sql);

	//exit();

	$book_reviews = $wpdb->get_results($sql);
	//var_dump($grants);

	//exit();
	//
	return COUNT($book_reviews);


}
