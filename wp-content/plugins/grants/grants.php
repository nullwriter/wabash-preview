<?php
/*
Plugin Name: Grants
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the Grant CPTs.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/

// CREATE TABLE IF NOT EXISTS `wp_grants` (
//   `id` int(11) NOT NULL AUTO_INCREMENT,
//   `__fk_Grant_t` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `title` text COLLATE utf8mb4_unicode_ci,
//   `institution` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `project_director` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `type` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `year` year(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `topics` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `Web_flag` tinyint(1) DEFAULT '0',
//   `proposal_abstract` text COLLATE utf8mb4_unicode_ci,
//   `learning_abstract` text COLLATE utf8mb4_unicode_ci,
//   PRIMARY KEY (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

// Register Custom Post Type
function grants_cpt() {

	$labels = array(
		'name'                  => _x( 'Grants', 'Post Type General Name', 'grants_cpt' ),
		'singular_name'         => _x( 'Grant', 'Post Type Singular Name', 'grants_cpt' ),
		'menu_name'             => __( 'Grants', 'admin menu', 'grants_cpt' ),
		'name_admin_bar'        => __( 'Grants', 'add new on admin bar', 'grants_cpt' ),
		'archives'              => __( 'Item Archives', 'grants_cpt' ),
		'parent_item_colon'     => __( 'Parent Grant:', 'grants_cpt' ),
		'all_items'             => __( 'All Grants', 'grants_cpt' ),
		'add_new_item'          => __( 'Add New Grant', 'grants_cpt' ),
		'add_new'               => __( 'Add New Grant', 'grants_cpt' ),
		'new_item'              => __( 'Add New Grant', 'grants_cpt' ),
		'edit_item'             => __( 'Edit Grant', 'grants_cpt' ),
		'update_item'           => __( 'Update Grant', 'grants_cpt' ),
		'view_item'             => __( 'View Grant', 'grants_cpt' ),
		'search_items'          => __( 'Search Grants', 'grants_cpt' ),
		'not_found'             => __( 'No Grant found', 'grants_cpt' ),
		'not_found_in_trash'    => __( 'No Grant found in Trash', 'grants_cpt' ),
		'featured_image'        => __( 'Featured Image', 'grants_cpt' ),
		'set_featured_image'    => __( 'Set featured image', 'grants_cpt' ),
		'remove_featured_image' => __( 'Remove featured image', 'grants_cpt' ),
		'use_featured_image'    => __( 'Use as featured image', 'grants_cpt' ),
		'insert_into_item'      => __( 'Insert into item', 'grants_cpt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'grants_cpt' ),
		'items_list'            => __( 'Items list', 'grants_cpt' ),
		'items_list_navigation' => __( 'Items list navigation', 'grants_cpt' ),
		'filter_items_list'     => __( 'Filter items list', 'grants_cpt' ),
	);
	$args = array(
		'label'                 => __( 'Grants', 'grants_cpt' ),
		'description'           => __( 'Grant Post Type', 'grants_cpt' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'rewrite'               => array( 'slug' => 'grants' ),
		'menu_icon'	            => 'dashicons-welcome-learn-more',
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'capabilities'          => array('create_posts' => false),
		'map_meta_cap'          => true, // Set to `false`, if users are not allowed to edit/delete existing posts
	);
	register_post_type( 'grants', $args );

}
add_action( 'init', 'grants_cpt');

function grants_taxonomies() {

	// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => 'Types',
			'singular_name'     => 'Type',
			'search_items'      => 'Search Types',
			'all_items'         => 'All Types',
			'parent_item'       => 'Parent Type',
			'parent_item_colon' => 'Parent Type:',
			'edit_item'         => 'Edit Type',
			'update_item'       => 'Update Type',
			'add_new_item'      => 'Add New Type',
			'new_item_name'     => 'New Type Name',
			'menu_name'         => 'Types',
		);

		$args = array(
			'hierarchical'        => true,
			'labels'              => $labels,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'query_var'           => true,
			'public'              => true,
			'publicly_queryable'  => true,
			'has_archive'         => true,
			'rewrite'             => array( 'slug' => 'grant-type' ),
		);

		register_taxonomy('grant-type','grants',$args);

	// Add new taxonomy, make it hierarchical (like categories)

	$labels = array(
		'name'                       => _x( 'Topics', 'taxonomy general name' ),
		'singular_name'              => _x( 'Topic', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Topics' ),
		'all_items'                  => __( 'All Topics' ),
		'parent_item'                => __( 'Parent Topic' ),
		'parent_item_colon'          => __( 'Parent Topic:' ),
		'edit_item'                  => __( 'Edit Topic' ),
		'update_item'                => __( 'Update Topic' ),
		'add_new_item'               => __( 'Add New Topic' ),
		'new_item_name'              => __( 'New Topic Name' ),
		'separate_items_with_commas' => __( 'Separate Topics with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Topics' ),
		'choose_from_most_used'      => __( 'Choose from the most used Topics' ),
		'not_found'                  => __( 'No Topics found.' ),
		'menu_name'                  => __( 'Topics' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		//'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		// 'show_in_nav_menus'	=> false,
		'public'                => true,
		'publicly_queryable'    => true,
		'has_archive'           => true,
		'rewrite'               => array( 'slug' => 'grant-topic' ),
	);


	register_taxonomy( 'grant-topic', 'grants', $args );


	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Grant Year', 'taxonomy general name' ),
		'singular_name'              => _x( 'Grant Year', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Grant Years' ),
		'popular_items'              => __( 'Popular Grant Years' ),
		'all_items'                  => __( 'All Grant Years' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Grant Year' ),
		'update_item'                => __( 'Update Grant Year' ),
		'add_new_item'               => __( 'Add New Grant Year' ),
		'new_item_name'              => __( 'New Grant Year Name' ),
		'separate_items_with_commas' => __( 'Separate grant years with commas' ),
		'add_or_remove_items'        => __( 'Add or remove grant year' ),
		'choose_from_most_used'      => __( 'Choose from the most used grant years' ),
		'not_found'                  => __( 'No grant years found.' ),
		'menu_name'                  => __( 'Grant Year' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'grant-year' ),
	);

	register_taxonomy( 'grant-year', 'grants', $args );

}
add_action( 'init', 'grants_taxonomies');


function grants_metaboxes(){
  add_meta_box("grants-learning-abstract-meta", "Grant Info", "display_grants_meta_box", "grants", "normal", "high");


}

function display_grants_meta_box(){
   	global $post;

	wp_nonce_field( basename( __FILE__ ), 'grants_nonce' );

	$grant = get_grant_object($post->ID);

?>


<div class="form-group">
  <label for="grants-institution" class="col-sm-2 control-label">Institution:</label>
  <div class="col-sm-10">
    <input name="grants-institution" id="grants-institution" readonly class="form-control" placeholder="Institution" value="<?php echo $grant->institution; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="grants-project-director" class="col-sm-2 control-label">Project Director:</label>
  <div class="col-sm-10">
    <input name="grants-project-director" id="grants-project-director" readonly class="form-control" placeholder="Separate by comma is more than one" value="<?php echo $grant->project_director; ?>" />
  </div>
</div>
<div class="form-group">
  <label for="grants-learning-abstract" class="col-sm-2 control-label">Proposal Abstract:</label>
  <div class="col-sm-10">
    <textarea name="grants-learning-abstract" readonly placeholder="Learning Abstract" rows="5" class="form-control"><?php echo $grant->proposal_abstract; ?></textarea>
  </div>
</div>
<div class="form-group">
  <label for="grants-learning-abstract" class="col-sm-2 control-label">Learning Abstract:</label>
  <div class="col-sm-10">
    <textarea name="grants-learning-abstract" readonly placeholder="Learning Abstract" rows="5" class="form-control"><?php echo $grant->learning_abstract; ?></textarea>
  </div>
</div>


<?php add_thickbox(); ?>

<a id="myanchor" href="#TB_inline?width=100%&height=100%&inlineId=modal-window-id" class="thickbox" style="display:none;">Modal Me</a>

<div id="modal-window-id" style="display:none;">
    <p style="padding: 40px; font-size: 15px;">Sorry you can't add or edit any grant content from WordPress.<br />
			This content is coming from FileMaker, and you should use it
			if you want to add or modify any content.<br />
			The content is showing here just for your convenience.

		</p>
</div>

  <?php
}

add_action('add_meta_boxes_grants', 'grants_metaboxes');

function save_grants_metaboxes( $post_id ) {


// Checks save status
$is_autosave = wp_is_post_autosave( $post_id );
$is_revision = wp_is_post_revision( $post_id );
$is_valid_nonce = ( isset( $_POST[ 'grants_nonce' ] ) && wp_verify_nonce( $_POST[ 'grants_nonce' ], basename( __FILE__ ) ) ) ? TRUE : FALSE;

// Exits script depending on save status
if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
    return;
}



}

add_action( 'save_post_grants', 'save_grants_metaboxes' );


function add_grants_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'grants' === $post->post_type ) {
					wp_register_style( 'grants_custom_css', get_stylesheet_directory_uri() . '/css/grants.css', false, '1.0.0' );
					wp_enqueue_style( 'grants_custom_css' );
					wp_enqueue_script(  'grants_reviews_js', get_stylesheet_directory_uri().'/js/grants/custom.js' );
					wp_dequeue_script( 'autosave' );
        }
    }




}
add_action( 'admin_enqueue_scripts', 'add_grants_admin_scripts', 10, 1 );


add_filter('manage_grants_posts_columns' , 'add_grants_columns');

function add_grants_columns($columns) {
    //unset($columns['author']);
    return array_merge($columns,
          array('grants-institution' => 'Institution'));
}

add_action('manage_grants_posts_custom_column' , 'grants_posts_custom_columns', 10, 2 );

function grants_posts_custom_columns( $column, $post_id ) {
	$grant = get_grant_object($post_id);

    switch ( $column ) {

    case 'grants-institution' :

        echo $grant->institution;

        break;
    }
}

function remove_grants_quick_edit_and_trash( $actions ) {
	global $post;
	  if ( 'grants' === $post->post_type ) {
			unset($actions['inline hide-if-no-js']);
			unset($actions['trash']);
		}
		return $actions;
}
add_filter('post_row_actions','remove_grants_quick_edit_and_trash',10,1);


function display_excerpt_grant_object($grant){
    global $wpdb;


    $html = '<p><a href="'.get_permalink($grant->post_id).'">' .$grant->title.'</a><br/>';
    $html .=limit_text(wp_strip_all_tags($grant->proposal_abstract), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_grant_object($item){
    global $load_more_data;

	$single_results_page=is_page_template('template-section-results-page.php') || is_page_template('template-selected-resources-page.php') || $load_more_data;
    $detail_page=is_singular('grants') && !$single_results_page;

	// get type
	$terms = array_map('trim', explode('|', $item->type));

	$types = array();
	if(!empty($terms)){
		foreach($terms as $type){
		  if(!empty($type)) {
			  $term = get_term_by('name', $type, 'grant-type');
			  $types[]='<a href="'.get_category_link( $term->term_id ).'" target="_blank">'.$term->name.'</a>';
		  }
		}

		$types=implode(' | ',$types);

	}

	// get topics
	$terms = array_map('trim', explode('|', $item->topics));

	$topics = array();
	foreach($terms as $topic){
	    if(!empty($topic)) {
	        $term = get_term_by('name', $topic, 'grant-topic');
	        $topics[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
	    }
	}

	$topics=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$topics);

    $full_content = '<strong>Proposal abstract :</strong><br>'.force_balance_tags($item->proposal_abstract).'<br/><br/><strong>Learning Abstract :</strong><br>'.force_balance_tags($item->learning_abstract);

    $excerpt_content= '<strong>Proposal abstract :</strong><br>'.limit_text(strip_tags($item->proposal_abstract, '<br><br/>' ), 70);



    $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-grant.jpg'.'" title="Grants cover image" height="150" width="150" alt="Grants cover image">';

	$html = '
<div class="panel card grants-card">
   <div class="panel-body">
      <div class="row is-flex">
         <div class="card-image col-xs-4 col-sm-4 col-md-3">
            '.$img.'
         </div>
         <div class="card-select col-xs-2 col-xs-offset-6 col-sm-2 col-sm-offset-6 col-md-1 col-md-offset-0 col-md-push-8">';

        $html .= '<div class="card-checkbox">
                        <input value="'.$item->post_id.'" id="resource_'.$item->post_id.'" class="case" type="checkbox">
                    </div>';

    $html .= ' </div>
         <div class="card-info col-xs-12 col-sm-12 col-md-8 col-md-pull-1">
            <h3 class="card-title">
              '.$item->title.'
            </h3>
            <p class="card-meta">
                <span>Awarded Grant</span><br>
				'.(!empty($item->project_director)?'<span>'.$item->project_director.'</span><br>':'').''
				.(!empty($item->institution)?'<span>'.$item->institution.'</span><br>':'').''
				.(!empty($types)?'<span>'.$types.'</span><br>':'').''
				.((!empty($item->year) && $item->year!='0000')?'<span>'.$item->year.'</span><br>':'');
				if(!empty($topics) ) {
					$html.= '<span>Topics: '.$topics.'</span><br>';
				}
            $html.= '</p>
         </div>
      </div>';
        $html .= '
      <div class="row">
         <div class="card-content-container col-xs-12">
            <div class="card-content">';

			if(!$detail_page){
				$html .='
                <div class="less-more-content excerpt" data-id="'.$item->id.'">
                    <div id="excerpt-content-'.$item->id.'">
                        '.$excerpt_content.'
                    </div>
                    <div id="full-content-'.$item->id.'" class="full-content">
                        '.$full_content.'
                    </div>
                    <div id="'.$item->id.'" class="more-link-single-results2">
                        <a id="anchor-'.$item->id.'" title="Read More" href="javascript:void(0)">More</a>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </div>
                </div>';
            }
            else{
                $html .=$full_content;
            }
            $html.= '
            </div>
         </div>
      </div>
   </div>
</div>';

	return $html;

}

function display_grants_sort_panel($sort_term){
	global $search_string, $post_types;

  $html='
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="post" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />
    <div class="col-md-12">
 	<div class="panel panel-default">
 	   <div class="panel-heading">
                <div>
                    <span class="panel-title">Sort by:</span>
                </div>
 	   </div>
 	   <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="radio">
                            <input type="radio" name="sort_term" value="weight" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked"' : '').'
                                   id="relevance_radio">
                            <label for="relevance_radio">Relevance</label>
                    </div>
				</div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="radio">
                            <input type="radio" name="sort_term" value="institution" id="format_radio" '.(($sort_term=='institution')? 'checked="checked"' : '').'>
                            <label for="format_radio">Institution Name</label>
				</div>
			</div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="radio">
                            <input type="radio" name="sort_term" value="type" id="title_radio" '.(($sort_term=='type')? 'checked="checked"' : '').'>
                            <label for="title_radio">Institution Type</label>
                   </div>
			   </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			   <div class="radio ">
                            <input type="radio" name="sort_term" value="year" '.(($sort_term=='year')? 'checked="checked"' : '').'
                                   id="year_radio">
                            <label for="year_radio">Year</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12"">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <span class="panel-title">Filter by:</span>
                    <button id="clear_filters2" title="Clear Filters" type="button">
                        <span class="hidden">Clear checkboxes</span>Clear
                    </button>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
 		            
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Undergraduate School" class="checkbox">
                            <input class="styled" type="checkbox" name="gs-college-uni" id="gs-college-uni" ' . ((!empty($_POST['gs-college-uni']) || !empty($_GET['gs-college-uni'])) ? 'checked="checked" ' : '') . '>
                            <label for="gs-college-uni">Undergraduate School</label>
			   </div>
			</div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Theological School" class="checkbox">
                            <input class="styled" type="checkbox" name="gs-theo-school" id="gs-theo-school" ' . ((!empty($_POST['gs-theo-school']) || !empty($_GET['gs-theo-school'])) ? 'checked="checked" ' : '') . '>
                            <label for="gs-theo-school">Theological School</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Non-Degree Agency" class="checkbox">
                            <input class="styled" type="checkbox" name="gs-agencies" id="gs-agencies" ' . ((!empty($_POST['gs-agencies']) || !empty($_GET['gs-agencies'])) ? 'checked="checked" ' : '') . '>
                            <label for="gs-agencies">Non-Degree Agency</label>
                        </div>
 		  </div>
 		            
 	   </div>
 	</div>
 	    </div>
 	</div>
</form>
    ';
  return $html;
}

function get_grant_object($id){
	global $wpdb;

	$query = $wpdb->prepare('SELECT * FROM wp_grants WHERE Web_flag = 1 AND post_id = %d LIMIT 1', $id);
    $grant = $wpdb->get_row($query);

    return $grant;
}

function search_grant($search_string, $subtypes = array(), $sort_by = 'weight',$limit='', $offset=''){
	global $wpdb;

	// var_dump($search_string);
	// var_dump($sub_type);
	// var_dump($sort_by);
	//
	// exit();
	$limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
	}
	if($sort_by!= 'weight' && $sort_by != 'year'){
		$sort_direction='ASC';
	}
	else{
		$sort_direction='DESC';
	}

	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }
	//var_dump($terms_array);

	// where do you want to search
	$fields = array('title', 'proposal_abstract', 'learning_abstract', 'institution', 'project_director', 'topics', 'type');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_grants.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_grants.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
	//exit();

	$sql="SELECT wp_grants.*,
	if(
		wp_grants.title = '$search_string',  100,
	         IF(
	             wp_grants.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_grants.proposal_abstract =  '$search_string',  40,
	        IF(
	             wp_grants.proposal_abstract LIKE '%$search_string%', 30,

	                 if( {$terms_query['proposal_abstract']}, 5, 0)

	          )


	)
	+
	IF(
	    wp_grants.learning_abstract =  '$search_string',  20,
	        IF(
	             wp_grants.learning_abstract LIKE '%$search_string%', 15,

	                 if( {$terms_query['learning_abstract']}, 3, 0)

	          )


	)
	+
	IF(
	    wp_grants.institution =  '$search_string',  3,
	        IF(
	             wp_grants.institution LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
	    wp_grants.project_director =  '$search_string',  3,
	        IF(
	             wp_grants.project_director LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
	    wp_grants.topics =  '$search_string',  3,
	        IF(
	             wp_grants.topics LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
		wp_grants.type =  '$search_string',  3,
			IF(
				 wp_grants.type LIKE '%$search_string%', 2, 0

			  )


	) AS weight
	FROM wp_grants WHERE wp_grants.Web_flag = 1 ";

    if (!empty($subtypes)) {
    $sql .= "AND wp_grants.type IN ('" . implode("','", $subtypes) . "') ";
    }

	$sql .= "HAVING weight > 0 ";

    if ($sort_by == 'title'){
        $sql .= "
	ORDER BY regex_replace('[^a-zA-Z\-]','',$sort_by) $sort_direction" . $limit_sql . $offset_sql;
    }else {
        $sql .= "
	ORDER BY $sort_by $sort_direction" . $limit_sql . $offset_sql;
    }

	// if(!empty($limit)){
	// 	$sql .= " LIMIT $limit";
	// }
	//var_dump($sql);
	//exit();

	$grants = $wpdb->get_results($sql);
	//var_dump($grants);

	//exit();
	//
	return $grants;


}

function search_grant_total($search_string, $subtypes = array()){
	global $wpdb;


	// var_dump($search_string);
	// var_dump($sub_type);
	// var_dump($sort_by);
	//
	// exit();


	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	//var_dump($terms_array);

	// where do you want to search
	$fields = array('title', 'proposal_abstract', 'learning_abstract', 'institution', 'project_director', 'topics', 'type');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_grants.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_grants.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
	//exit();

	$sql="SELECT wp_grants.id,
	if(
		wp_grants.title = '$search_string',  100,
	         IF(
	             wp_grants.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_grants.proposal_abstract =  '$search_string',  40,
	        IF(
	             wp_grants.proposal_abstract LIKE '%$search_string%', 30,

	                 if( {$terms_query['proposal_abstract']}, 5, 0)

	          )


	)
	+
	IF(
	    wp_grants.learning_abstract =  '$search_string',  20,
	        IF(
	             wp_grants.learning_abstract LIKE '%$search_string%', 15,

	                 if( {$terms_query['learning_abstract']}, 3, 0)

	          )


	)
	+
	IF(
	    wp_grants.institution =  '$search_string',  3,
	        IF(
	             wp_grants.institution LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
	    wp_grants.project_director =  '$search_string',  3,
	        IF(
	             wp_grants.project_director LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
	    wp_grants.topics =  '$search_string',  3,
	        IF(
	             wp_grants.topics LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
		wp_grants.type =  '$search_string',  3,
			IF(
				 wp_grants.type LIKE '%$search_string%', 2, 0

			  )


	) AS weight
	FROM wp_grants
	WHERE wp_grants.Web_flag = 1 ";

	if(!empty($subtypes)){
		$sql.=" AND wp_grants.type IN ('" . implode("','", $subtypes) . "') ";
	}

    $sql .= "HAVING weight > 0";

	// if(!empty($limit)){
	// 	$sql .= " LIMIT $limit";
	// }
	//var_dump($sql);

	//exit();

	$grants = $wpdb->get_results($sql);
	//var_dump($grants);

	//exit();
	//
	return COUNT($grants);


}
