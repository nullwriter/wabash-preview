<?php
/*
Plugin Name: Scholarship on Teaching
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the Scholarship on Teaching CPTs.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/

// CREATE TABLE `wp_scholarship_of_teaching` (
// `id` int(11) NOT NULL AUTO_INCREMENT,
// `title` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `author` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `publication_date` date DEFAULT NULL,
// `isbn_issn` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `web_recommended` tinyint(1) unsigned DEFAULT '0',
// `web_type` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `publisher` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `call_number` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `wc_number` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `format` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `image` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `url_article_link` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `url_bookreview` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `url_ttr_article_link` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `url_web_resource` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `additional_info` text COLLATE utf8mb4_unicode_ci,
// `table_of_contents_info` text COLLATE utf8mb4_unicode_ci,
// `image_url` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
// `Web_flag` tinyint(1) DEFAULT '0',
// `recid` int(11) DEFAULT '0',
// `topic_list` text COLLATE utf8mb4_unicode_ci,
// PRIMARY KEY (`id`)
// ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*
web_type

Book
Journal Issue
Article
Web
TTR
Tactic
 */

/*
SORT
Relevance
Date
Format
Author
Title
Recommended
 */

// Register Custom Post Type
function scholarship_cpt() {

	$labels = array(
		'name'                  => _x( 'Scholarship on Teaching', 'Post Type General Name', 'scholarship_cpt' ),
		'singular_name'         => _x( 'Scholarship on Teaching', 'Post Type Singular Name', 'scholarship_cpt' ),
		'menu_name'             => __( 'Scholarship on Teaching', 'admin menu', 'scholarship_cpt' ),
		'name_admin_bar'        => __( 'Scholarship on Teaching', 'add new on admin bar', 'scholarship_cpt' ),
		'archives'              => __( 'Item Archives', 'scholarship_cpt' ),
		'parent_item_colon'     => __( 'Parent Scholarship on Teaching:', 'scholarship_cpt' ),
		'all_items'             => __( 'All Scholarship on Teachings', 'scholarship_cpt' ),
		'add_new_item'          => __( 'Add New Scholarship on Teaching', 'scholarship_cpt' ),
		'add_new'               => __( 'New Scholarship on Teaching', 'scholarship_cpt' ),
		'new_item'              => __( 'New Scholarship on Teaching', 'scholarship_cpt' ),
		'edit_item'             => __( 'Edit Scholarship on Teaching', 'scholarship_cpt' ),
		'update_item'           => __( 'Update Scholarship on Teaching', 'scholarship_cpt' ),
		'view_item'             => __( 'View Scholarship on Teaching', 'scholarship_cpt' ),
		'search_items'          => __( 'Search Scholarship on Teachings', 'scholarship_cpt' ),
		'not_found'             => __( 'No Scholarship on Teaching found', 'scholarship_cpt' ),
		'not_found_in_trash'    => __( 'No Scholarship on Teaching found in Trash', 'scholarship_cpt' ),
		'featured_image'        => __( 'Featured Image', 'scholarship_cpt' ),
		'set_featured_image'    => __( 'Set featured image', 'scholarship_cpt' ),
		'remove_featured_image' => __( 'Remove featured image', 'scholarship_cpt' ),
		'use_featured_image'    => __( 'Use as featured image', 'scholarship_cpt' ),
		'insert_into_item'      => __( 'Insert into item', 'scholarship_cpt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'scholarship_cpt' ),
		'items_list'            => __( 'Items list', 'scholarship_cpt' ),
		'items_list_navigation' => __( 'Items list navigation', 'scholarship_cpt' ),
		'filter_items_list'     => __( 'Filter items list', 'scholarship_cpt' ),
	);
	$args = array(
		'label'                 => __( 'Scholarship on Teaching', 'scholarship_cpt' ),
		'description'           => __( 'Scholarship on Teaching Post Type', 'scholarship_cpt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'rewrite'               => array( 'slug' => 'scholarship' ),
		'menu_icon'	            => 'dashicons-megaphone',
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'capabilities'          => array('create_posts' => false),
		'map_meta_cap'          => true, // Set to `false`, if users are not allowed to edit/delete existing posts
	);
	register_post_type( 'scholarship', $args );

}
add_action( 'init', 'scholarship_cpt');

function scholarship_taxonomies() {

	// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => 'Types',
			'singular_name'     => 'Type',
			'search_items'      => 'Search Types',
			'all_items'         => 'All Types',
			'parent_item'       => 'Parent Type',
			'parent_item_colon' => 'Parent Type:',
			'edit_item'         => 'Edit Type',
			'update_item'       => 'Update Type',
			'add_new_item'      => 'Add New Type',
			'new_item_name'     => 'New Type Name',
			'menu_name'         => 'Types',
		);

		$args = array(
			'hierarchical'        => true,
			'labels'              => $labels,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'query_var'           => true,
			'public'              => true,
			'publicly_queryable'  => true,
			'has_archive'         => true,
			'rewrite'             => array( 'slug' => 'scholarship-type' ),
		);

		register_taxonomy('scholarship-type','scholarship',$args);

		// Add new taxonomy, make it hierarchical (like categories)

		$labels = array(
			'name'                       => _x( 'Topics', 'taxonomy general name' ),
			'singular_name'              => _x( 'Topic', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Topics' ),
			'all_items'                  => __( 'All Topics' ),
			'parent_item'                => __( 'Parent Topic' ),
			'parent_item_colon'          => __( 'Parent Topic:' ),
			'edit_item'                  => __( 'Edit Topic' ),
			'update_item'                => __( 'Update Topic' ),
			'add_new_item'               => __( 'Add New Topic' ),
			'new_item_name'              => __( 'New Topic Name' ),
			'separate_items_with_commas' => __( 'Separate Topics with commas' ),
			'add_or_remove_items'        => __( 'Add or remove Topics' ),
			'choose_from_most_used'      => __( 'Choose from the most used Topics' ),
			'not_found'                  => __( 'No Topics found.' ),
			'menu_name'                  => __( 'Topics' ),
		);

		$args = array(
			'hierarchical'          => true,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			//'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			// 'show_in_nav_menus'	=> false,
			'public'                => true,
			'publicly_queryable'    => true,
			'has_archive'           => true,
			'rewrite'               => array( 'slug' => 'scholarship-topic' ),
		);


		register_taxonomy( 'scholarship-topic', 'scholarship', $args );


		// Add new taxonomy, NOT hierarchical (like tags)
		$labels = array(
			'name'                       => _x( 'Web Recommended', 'taxonomy general name' ),
			'singular_name'              => _x( 'Web Recommended', 'taxonomy singular name' ),
			'search_items'               => __( 'Search Web Recommended' ),
			'popular_items'              => __( 'Popular Web Recommended' ),
			'all_items'                  => __( 'All Web Recommended' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Web Recommended' ),
			'update_item'                => __( 'Update Web Recommended' ),
			'add_new_item'               => __( 'Add New Web Recommended' ),
			'new_item_name'              => __( 'New Web Recommended Name' ),
			'separate_items_with_commas' => __( 'Separate Web Recommended with commas' ),
			'add_or_remove_items'        => __( 'Add or remove Web Recommended' ),
			'choose_from_most_used'      => __( 'Choose from the most used Web Recommended' ),
			'not_found'                  => __( 'No Web Recommended found.' ),
			'menu_name'                  => __( 'Web Recommended' ),
		);

		$args = array(
			'hierarchical'          => false,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'scholarship-web-recommended' ),
		);

		register_taxonomy( 'scholarship-web-recommended', 'scholarship', $args );

}
add_action( 'init', 'scholarship_taxonomies');


function scholarship_metaboxes(){
	add_meta_box("scholarship-meta", "Scholarship On Teaching Info", "display_scholarship_meta_box", "scholarship", "normal", "high");


}


function display_scholarship_meta_box(){
	global $post;


	wp_nonce_field( basename( __FILE__ ), 'scholarship_nonce' );



	$scholarship = get_scholarship_object($post->ID);
	if(!empty($scholarship)){
		$scholarship->resource_url=!empty($scholarship->url_article_link)?$scholarship->url_article_link:(!empty($scholarship->url_bookreview)?$scholarship->url_bookreview:(!empty($scholarship->url_ttr_article_link)?$scholarship->url_ttr_article_link:(!empty($scholarship->url_web_resource)?$scholarship->url_web_resource:'')));

	}
	else{
		$scholarship         = new StdClass();
		$scholarship->author = '';
		$scholarship->publication_date  = '';
		$scholarship->isbn_issn  = '';
		$scholarship->publisher = '';
		$scholarship->call_number  = '';
		$scholarship->wc_number  = '';
		$scholarship->format = '';
		$scholarship->additional_info  = '';
		$scholarship->table_of_contents_info  = '';
		$scholarship->resource_url  = '';
	}





  ?>


	<div class="form-group">
	  <label for="scholarship-author" class="col-sm-2 control-label">Author:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-author" id="scholarship-author" readonly class="form-control" placeholder="Author" value="<?php echo $scholarship->author; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-publication-date" class="col-sm-2 control-label">Publication Date:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-publication-date" id="scholarship-publication-date" readonly class="form-control" placeholder="Publication Date" value="<?php echo $scholarship->publication_date; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-isbn-issn" class="col-sm-2 control-label">ISBN or ISSN:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-isbn-issn" id="scholarship-isbn-issn" readonly class="form-control" placeholder="ISBN or ISSN" value="<?php echo $scholarship->isbn_issn; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-publisher" class="col-sm-2 control-label">Publisher:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-publisher" id="scholarship-publisher" readonly class="form-control" placeholder="Publisher" value="<?php echo $scholarship->publisher; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-call-number" class="col-sm-2 control-label">Call Number:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-call-number" id="scholarship-call-number" readonly class="form-control" placeholder="Call Number" value="<?php echo $scholarship->call_number; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-wc-number" class="col-sm-2 control-label">WC Number:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-wc-number" id="scholarship-wc-number" readonly class="form-control" placeholder="WC Number" value="<?php echo $scholarship->wc_number; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-format" class="col-sm-2 control-label">Format:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-format" id="scholarship-format" readonly class="form-control" placeholder="Format" value="<?php echo $scholarship->format; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-resource-url" class="col-sm-2 control-label">Resource Url:</label>
	  <div class="col-sm-10">
	    <input name="scholarship-resource-url" id="scholarship-resource-url" readonly class="form-control" placeholder="Resource Url" value="<?php echo $scholarship->resource_url; ?>" />
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-additional-info" class="col-sm-2 control-label">Additional Info:</label>
	  <div class="col-sm-10">
	    <textarea name="scholarship-additional-info" id="scholarship-additional-info" readonly placeholder="Additional Info" rows="5" class="form-control"><?php echo $scholarship->additional_info; ?></textarea>
	  </div>
	</div>
	<div class="form-group">
	  <label for="scholarship-table-of-contents-info" class="col-sm-2 control-label">Table of contents Info (only for books):</label>
	  <div class="col-sm-10">
	    <textarea name="scholarship-table-of-contents-info" id="scholarship-table-of-contents-info" readonly placeholder="Table of contents Info" rows="5" class="form-control"><?php echo $scholarship->table_of_contents_info; ?></textarea>
	  </div>
	</div>


	<?php add_thickbox(); ?>

	<a id="myanchor" href="#TB_inline?width=100%&height=100%&inlineId=modal-window-id" class="thickbox" style="display:none;">Modal Me</a>

	<div id="modal-window-id" style="display:none;">
	    <p style="padding: 40px; font-size: 15px;">
				Sorry you can't add or edit any scholarship on teaching content from WordPress.<br />
				This content is coming from FileMaker, and you should use it
				if you want to add or modify any content.<br />
				The content is showing here just for your convenience.

			</p>
	</div>

	<?php
}


add_action('add_meta_boxes_scholarship', 'scholarship_metaboxes');

function save_scholarship_metaboxes( $post_id ) {


// Checks save status
$is_autosave = wp_is_post_autosave( $post_id );
$is_revision = wp_is_post_revision( $post_id );
$is_valid_nonce = ( isset( $_POST[ 'scholarship_nonce' ] ) && wp_verify_nonce( $_POST[ 'scholarship_nonce' ], basename( __FILE__ ) ) ) ? TRUE : FALSE;

// Exits script depending on save status
if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
    return;
}



}

add_action( 'save_post_scholarship', 'save_scholarship_metaboxes' );

function add_scholarship_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'scholarship' === $post->post_type ) {
					wp_register_style( 'scholarship_custom_css', get_stylesheet_directory_uri() . '/css/scholarship.css', false, '1.0.0' );
					wp_enqueue_style( 'scholarship_custom_css' );
					wp_enqueue_script(  'custom_scholarship_js', get_stylesheet_directory_uri().'/js/scholarship/custom.js' );
					wp_dequeue_script( 'autosave' );
        }
    }




}
add_action( 'admin_enqueue_scripts', 'add_scholarship_admin_scripts', 10, 1 );

function remove_scholarship_quick_edit_and_trash( $actions ) {
	global $post;
	  if ( 'scholarship' === $post->post_type ) {
			unset($actions['inline hide-if-no-js']);
			unset($actions['trash']);
		}
		return $actions;
}
add_filter('post_row_actions','remove_scholarship_quick_edit_and_trash',10,1);

function remove_scholarship_add_new_menu() {

    remove_submenu_page('edit.php?post_type=scholarship','post-new.php?post_type=scholarship');

}
add_action('admin_menu','remove_scholarship_add_new_menu');

function display_excerpt_scholarship_object($scholarship){

    $url = get_permalink($scholarship->post_id);
    if ($scholarship->web_type == 'Web') {
        $url = $scholarship->url_web_resource;
    }

	$html = '<p><a href="'.$url.'">' .$scholarship->title.'</a><br/>';
	$html .=limit_text(wp_strip_all_tags($scholarship->additional_info), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_scholarship_object($item){
	global $load_more_data;

	//var_dump($load_more_data);

	$single_results_page=is_page_template('template-section-results-page.php') || is_page_template('template-selected-resources-page.php') || $load_more_data;
	$detail_page=is_singular() && !$single_results_page;

	if(!empty($item->image_url)){
        $img='<img class="img-responsive" src="'.$item->image_url.'" title="Cover image" alt="Cover image">';
    }
    else{
		switch($item->web_type){
			  case 'Book':
				  $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-book.png'.'" title="Book cover image" alt="Book cover image">';
				  break;
			  case 'Journal Issue':
				  $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-journal.png'.'" title="Journal cover image" alt="Journal cover image">';
				  break;
			  case 'Article':
				  $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-article.png'.'" title="Article cover image" alt="Article cover image">';
				  break;
			  case 'Web':
				  $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-websites.png'.'" title="Web cover image" alt="Web cover image">';
				  break;
			  case 'TTR':
				  $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-ttr.png'.'" title="TTR cover image" alt="TTR cover image">';
				  break;
			  case 'Tactic':
				  $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-teaching-tactic.png'.'" title="Tactics cover image" alt="Tactics cover image">';
				  break;

		  }
    }


	// get type
	$terms = array_map('trim', explode('|', $item->web_type));

	$types = array();
	foreach($terms as $type){
	  if(!empty($type)) {
		  $term = get_term_by('name', $type, 'scholarship-type');
          //$types[]='<a href="'.get_category_link( $term->term_id ).'" target="_blank">'.$term->name.'</a>';
          $types[]=$term->name;
      }
	}

	$types=implode(' | ',$types);

	// get topics
	$terms = array_map('trim', explode('|', $item->topic_list));

	$topics = array();
	foreach($terms as $topic){
		if(!empty($topic)) {
			$term = get_term_by('name', $topic, 'scholarship-topic');
            $topics[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
		}
	}

	$topics=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$topics);

    $title=$item->title;
	$url=!empty($item->url_article_link)?$item->url_article_link:(!empty($item->url_bookreview)?$item->url_bookreview:(!empty($item->url_ttr_article_link)?$item->url_ttr_article_link:(!empty($item->url_web_resource)?$item->url_web_resource:'')));

	if($types == "Web"){
        $url = $item->url_web_resource;
	}

	// if(!empty($item->url_article_link)){
	// 	$url=$item->url_article_link;
	// }
	// elseif(!empty($item->url_bookreview)){
	// 		$url=$item->url_bookreview;
	// }
	// elseif(!empty($item->url_ttr_article_link)){
	// 		$url=$item->url_ttr_article_link;
	// }
	// elseif(!empty($item->url_web_resource)){
	// 		$url=$item->url_web_resource;
	// }
	// else{
	// 	$url='';
	// }
	if(!empty($url)) {
        $title="<a target='_blank' href='".$url."'>".$item->title."<i class='icon_external_url'></i></a>";
    }
    //var_dump($url);

    $full_content = '<strong>Additional Info:</strong><br/>'.force_balance_tags($item->additional_info).(!empty($item->table_of_contents_info)?'<br/><br/><strong>Table Of Content:</strong><br/>'.force_balance_tags($item->table_of_contents_info):'');

    $excerpt_content= '<strong>Additional Info:</strong><br/>'.limit_text(strip_tags($item->additional_info, '<br><br/>' ), 70);

	$more=($full_content==$excerpt_content)?false:true;

	$html = '
	<div class="panel card scholarship-card">
	<div class="panel-body">
	  <div class="row is-flex">
		 <div class="card-image col-xs-4 col-sm-4 col-md-3">
			'.$img.'
		 </div>
		 <div class="card-select col-xs-2 col-xs-offset-6 col-sm-2 col-sm-offset-6 col-md-1 col-md-offset-0 col-md-push-8">';
		 if ($item->web_recommended == '1') {
			 $html .= ' <img id="image-normal" class="img-responsive" src="' . get_stylesheet_directory_uri() . '/images/tree.png' . '" title="Wabash tree" alt="Wabash tree">';
		 }


			$html .= '<div class="card-checkbox">
						<input value="'.$item->post_id.'" id="resource_'.$item->post_id.'" class="case" type="checkbox">
					</div>';


		$html .= ' </div>
		 <div class="card-info col-xs-12 col-sm-12 col-md-8 col-md-pull-1">
			<h3 class="card-title">
				'.$title.'
			</h3>
			<p class="card-meta">
			<span>'.$types.'</span><br>
			'.(!empty($item->author)?'<span>'.$item->author.'</span><br>':'').''
			.((!empty($item->publication_date) && $item->publication_date!='0000')?'<span>'.$item->publication_date.'</span><br>':'').''
			.(!empty($item->publisher)?'<span>'.$item->publisher.'</span><br>':'').''
			.(!empty($item->call_number)?'<span>'.$item->call_number.'</span><br>':'');
			if(!empty($topics) ) {
				$html.= '<span>Topics: '.$topics.'</span><br>';
			}
			$html.= '</p>
		 </div>
	  </div>
	  <div class="row">
		 <div class="card-content-container col-xs-12">
			<div class="card-content">';

			if(!$detail_page){
				$html .='
				<div class="less-more-content excerpt" data-id="'.$item->id.'">
		            <div id="excerpt-content-'.$item->id.'">
		                '.$excerpt_content.'
		            </div>
		            <div id="full-content-'.$item->id.'" class="full-content">
		                '.$full_content.'
		            </div>';
					if($more){
						$html.='<div id="'.$item->id.'" class="more-link-single-results2">
			                <a id="anchor-'.$item->id.'" title="Read More" href="javascript:void(0)">More</a>
			                <i class="fa fa-caret-down" aria-hidden="true"></i>
			            </div>';
					}

		        $html.= '</div>';
			}
			else{
				$html .=$full_content;
			}
			$html.= '</div>
		 </div>
	  </div>
	</div>
	</div>';

	return $html;

}

function display_scholarship_sort_panel($sort_term) {
	global $post_types,$search_string;

    $html = '
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="get" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />
    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div>
                                    <span class="panel-title">Sort by:</span>
                                </div>
                            </div>
                            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio">
                            <input type="radio" name="sort_term" checked="checked" value="weight" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked"' : '').'
                                   id="relevance_radio">
                            <label for="relevance_radio">Relevance</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio">
                            <input type="radio" name="sort_term" value="web_type" id="format_radio" '.(($sort_term=='web_type')? 'checked="checked"' : '').'>
                            <label for="format_radio">Format</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio">
                            <input type="radio" name="sort_term" value="title" id="title_radio" '.(($sort_term=='title')? 'checked="checked"' : '').'>
                            <label for="title_radio">Title</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio">
                            <input type="radio" name="sort_term" value="publication_date" '.(($sort_term=='publication_date')? 'checked="checked"' : '').'
                                   id="year_radio">
                            <label for="year_radio">Publication date</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio">
                            <input type="radio" name="sort_term" value="author" id="author_radio" '.((!empty($sort_term) && $sort_term == 'author') ? 'checked="checked" ' : '') . '>
                            <label for="author_radio">Author</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio">
                            <input type="radio" name="sort_term" value="web_recommended" ' . ((!empty($sort_term) && $sort_term == 'web_recommended') ? 'checked="checked" ' : '') . '
                                   id="recommended_radio">
                            <label for="recommended_radio">Recommended</label>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
    <div class="col-md-12">
                        <div class="panel panel-default">
						<div class="panel-heading">
							<div>
								<span class="panel-title">Filter by:</span>
                    <button id="clear_filters2" title="Clear Filters" type="button">
                        <span class="hidden">Clear checkboxes</span>Clear
								</button>
							 </div>
						    </div>
                            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div title="Website" class="checkbox">
                            <input class="styled" type="checkbox" name="st-web" ' . ((!empty($_POST['st-web']) || !empty($_GET['st-web'])) ? 'checked="checked" ' : '') . '
                                   id="st-web_checkbox">
                            <label for="st-web_checkbox">Website</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div title="Journal" class="checkbox">
                            <input class="styled" type="checkbox" name="st-journal" ' . ((!empty($_POST['st-journal']) || !empty($_GET['st-journal'])) ? 'checked="checked" ' : '') . '
                                    id="st-journal_checkbox">
                            <label for="st-journal_checkbox">Journal Issue</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div title="Book" class="checkbox">
                            <input class="styled" type="checkbox" name="st-book" ' . ((!empty($_POST['st-book']) || !empty($_GET['st-book'])) ? 'checked="checked" ' : '') . '
                                   id="st-book_checkbox">
                            <label for="st-book_checkbox">Book</label>
                                        </div>
                                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div title="Article" class="checkbox">
                            <input class="styled" type="checkbox" name="st-article" ' . ((!empty($_POST['st-article']) || !empty($_GET['st-article'])) ? 'checked="checked" ' : '') . '
                                   id="st-article_checkbox">
                            <label for="st-article_checkbox">Article</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div title="TTR" class="checkbox">
                                            <input class="styled" type="checkbox" name="st-ttr" ' . ((!empty($_POST['st-ttr']) || !empty($_GET['st-ttr'])) ? 'checked="checked" ' : '') . '
                                            id="st-ttr_checkbox">
                                            <label for="st-article_checkbox">TTR</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div title="Tactic" class="checkbox">
                                            <input class="styled" type="checkbox" name="st-tactic" ' . ((!empty($_POST['st-tactic']) || !empty($_GET['st-tactic'])) ? 'checked="checked" ' : '') . '
                                            id="st-tactic_checkbox">
                                            <label for="st-tactic_checkbox">Tactic</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </form>
    ';
  return $html;
}

function display_ttr_sort_panel($sort_term){
global $post_types,$search_string;

    $html = '
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="post" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />
    <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <div>
                  <span class="panel-title">Sort by:</span>
               </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="radio">
                            <input type="radio" name="sort_term" checked="checked" value="weight" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked"' : '').'
                                   id="relevance_radio">
                            <label for="relevance_radio">Relevance</label>
                     </div>
                  </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="radio">
                            <input type="radio" name="sort_term" value="web_type" id="format_radio" '.(($sort_term=='web_type')? 'checked="checked"' : '').'>
                            <label for="format_radio">Format</label>
                     </div>
                  </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="radio">
                            <input type="radio" name="sort_term" value="title" id="title_radio" '.(($sort_term=='title')? 'checked="checked"' : '').'>
                            <label for="title_radio">Title</label>
                     </div>
                  </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="radio">
                            <input type="radio" name="sort_term" value="publication_date" '.(($sort_term=='publication_date')? 'checked="checked"' : '').'
                                   id="year_radio">
                            <label for="year_radio">Publication date</label>
                     </div>
                  </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="radio">
                            <input type="radio" name="sort_term" value="author" id="author_radio" '.((!empty($sort_term) && $sort_term == 'author') ? 'checked="checked" ' : '') . '>
                            <label for="author_radio">Author</label>
                     </div>
                  </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <div class="radio">
                            <input type="radio" name="sort_term" value="web_recommended" ' . ((!empty($sort_term) && $sort_term == 'web_recommended') ? 'checked="checked" ' : '') . '
                                   id="recommended_radio">
                            <label for="recommended_radio">Recommended</label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <div>
                  <span class="panel-title">Filter by:</span>
                    <button id="clear_filters2" title="Clear Filters" type="button">
                        <span class="hidden">Clear checkboxes</span>Clear
                  </button>
               </div>
            </div>
            <div class="panel-body">
                <div class="row">                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Journal" class="checkbox">
                            <input class="styled" type="checkbox" name="st-tactic" ' . ((!empty($_POST['st-tactic']) || !empty($_GET['st-tactic'])) ? 'checked="checked" ' : '') . '
                                    id="st-tactic">
                            <label for="st-tactic">Teaching Tactics Only</label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
	';
    return $html;
}


function get_scholarship_object($id){
    global $wpdb;

	$query = $wpdb->prepare('SELECT * FROM wp_scholarship_of_teaching WHERE Web_flag = 1 AND post_id = %d LIMIT 1', $id);
    $scholarship = $wpdb->get_row($query);

	return $scholarship;
}

function search_scholarship($search_string, $sub_type = array(), $type = 'scholarship', $sort_by = 'weight', $limit='', $offset=''){
	global $wpdb;

	if (empty($sub_type)) {
	    if ($type == 'scholarship') {
			$sub_type = array( 'Web', 'Journal Issue','Book', 'Article', 'TTR', 'Tactic' );
        } else {
			$sub_type = array( 'TTR', 'Tactic' );
        }
    }

	$limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
	}
	if($sort_by!= 'weight' && $sort_by!= 'web_recommended' && $sort_by != 'publication_date'){
		$sort_direction='ASC';
	}
	else{
		$sort_direction='DESC';
	}

	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

    $search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }

	// where do you want to search
	$fields = array('isbn_issn', 'call_number', 'title', 'additional_info', 'author',  'publisher', 'web_type',  'topic_list');


	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_scholarship_of_teaching.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_scholarship_of_teaching.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
  //exit();

	$sql="SELECT wp_scholarship_of_teaching.*,
	if(
		wp_scholarship_of_teaching.isbn_issn = '$search_string',  200, 0
	)
	+
	if(
		wp_scholarship_of_teaching.call_number = '$search_string',  200, 0
	)
	+
	if(
		wp_scholarship_of_teaching.title = '$search_string',  100,
	         IF(
	             wp_scholarship_of_teaching.title LIKE '%$search_string%', 50, 0

	          )
	)
	+
	IF(
	    wp_scholarship_of_teaching.additional_info =  '$search_string',  40,
	        IF(
	             wp_scholarship_of_teaching.additional_info LIKE '%$search_string%', 30, 0

	          )


	)
	+
	IF(
		wp_scholarship_of_teaching.author =  '$search_string',  20,
			IF(
				 wp_scholarship_of_teaching.author LIKE '%$search_string%', 10, 0

			  )


	)
	+
	IF(
		wp_scholarship_of_teaching.publisher =  '$search_string',  20,
			IF(
				 wp_scholarship_of_teaching.publisher LIKE '%$search_string%', 10, 0



			  )


	)
	+
	IF(
	    wp_scholarship_of_teaching.web_type =  '$search_string', 3, 0

	)
	+
	IF(
	    wp_scholarship_of_teaching.topic_list =  '$search_string',  3,
	        IF(
	             wp_scholarship_of_teaching.topic_list LIKE '%$search_string%', 2, 0



	          )


	) AS weight
	FROM wp_scholarship_of_teaching
    WHERE wp_scholarship_of_teaching.Web_flag = 1";

	//var_dump($sub_type);
    $sql.=" AND web_type IN ('" . implode("','", $sub_type) . "')";
	$sql.="
	HAVING weight > 0";

	if ($sort_by == 'title'){
        $sql.="
	ORDER BY regex_replace('[^a-zA-Z\-]','',$sort_by) $sort_direction".$limit_sql.$offset_sql;
    } else if ($sort_by == 'author') {
		$sql .= "
	ORDER BY CASE WHEN $sort_by = '' THEN 2 ELSE 1 END, $sort_by $sort_direction" . $limit_sql . $offset_sql;
    } else {
		$sql .= "
	ORDER BY $sort_by $sort_direction" . $limit_sql . $offset_sql;
	}
	//var_dump($sql);

	$scholarships = $wpdb->get_results($sql);
	//var_dump($wor);
	//
	return $scholarships;


}

function search_scholarship_total($search_string, $sub_type = array(), $type = 'scholarship'){
	global $wpdb;

	if (empty($sub_type)) {
		if ($type == 'scholarship') {
			$sub_type = array( 'Web', 'Journal Issue','Book', 'Article', 'TTR', 'Tactic' );
		} else {
			$sub_type = array( 'TTR', 'Tactic' );
		}
	}

	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	// where do you want to search
	$fields = array('isbn_issn', 'call_number', 'title', 'additional_info', 'author',  'publisher', 'web_type',  'topic_list');


	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_scholarship_of_teaching.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_scholarship_of_teaching.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
  //exit();

	$sql="SELECT wp_scholarship_of_teaching.id,
	if(
		wp_scholarship_of_teaching.isbn_issn = '$search_string',  200, 0
	)
	+
	if(
		wp_scholarship_of_teaching.call_number = '$search_string',  200, 0
	)
	+
	if(
		wp_scholarship_of_teaching.title = '$search_string',  100,
	         IF(
	             wp_scholarship_of_teaching.title LIKE '%$search_string%', 50, 0

	          )
	)
	+
	IF(
	    wp_scholarship_of_teaching.additional_info =  '$search_string',  40,
	        IF(
	             wp_scholarship_of_teaching.additional_info LIKE '%$search_string%', 30, 0

	          )


	)
	+
	IF(
		wp_scholarship_of_teaching.author =  '$search_string',  20,
			IF(
				 wp_scholarship_of_teaching.author LIKE '%$search_string%', 10, 0

			  )


	)
	+
	IF(
		wp_scholarship_of_teaching.publisher =  '$search_string',  20,
			IF(
				 wp_scholarship_of_teaching.publisher LIKE '%$search_string%', 10, 0



			  )


	)
	+
	IF(
	    wp_scholarship_of_teaching.web_type =  '$search_string', 3, 0

	)
	+
	IF(
	    wp_scholarship_of_teaching.topic_list =  '$search_string',  3,
	        IF(
	             wp_scholarship_of_teaching.topic_list LIKE '%$search_string%', 2, 0



	          )


	) AS weight
	FROM wp_scholarship_of_teaching
    WHERE wp_scholarship_of_teaching.Web_flag = 1";

	//var_dump($sub_type);

    $sql.=" AND web_type IN ('" . implode("','", $sub_type) . "')";
	$sql.="
	HAVING weight > 0";
	//var_dump($sql);

	$scholarships = $wpdb->get_results($sql);
	//var_dump($wor);
	//
	return COUNT($scholarships);


}
