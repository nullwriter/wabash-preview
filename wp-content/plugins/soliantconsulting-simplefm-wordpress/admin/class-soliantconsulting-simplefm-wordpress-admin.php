<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.soliantconsulting.com
 * @since      1.0.0
 *
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/admin
 * @author     Jeremiah Small <jsmall@soliantconsulting.com>
 */
class Soliantconsulting_Simplefm_Wordpress_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;


    protected $options;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Soliantconsulting_Simplefm_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Soliantconsulting_Simplefm_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/soliantconsulting-simplefm-wordpress-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Soliantconsulting_Simplefm_Wordpress_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Soliantconsulting_Simplefm_Wordpress_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/soliantconsulting-simplefm-wordpress-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function admin_menu() {
        add_options_page( 'Filemaker Sync Settings', 'FM Sync', 'manage_options', 'soliant-fm-sync', array($this, 'my_plugin_options') );
    }

    public function page_init() {
        register_setting(
            'my_option_group', // Option group
            'soliant-fm-sync', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );


        add_settings_section(
            'setting_section_id', // ID
            'Main Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'soliant-fm-sync' // Page
        );

        add_settings_field(
            'sync_url', // ID
            'FM Url', // Title
            array( $this, 'fm_url_callback' ), // Callback
            'soliant-fm-sync', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'timezone_offset', // ID
            'Filemaker Server Timezone', // Title
            array( $this, 'fm_timezone_callback' ), // Callback
            'soliant-fm-sync', // Page
            'setting_section_id' // Section
        );
    }
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['fm_url'] ) )
            $new_input['fm_url'] = sanitize_text_field($input['fm_url']);

        if( isset( $input['fm_timezone'] ) )
            $new_input['fm_timezone'] = sanitize_text_field($input['fm_timezone']);

        return $new_input;
    }
    function my_plugin_options() {
        if ( !current_user_can( 'manage_options' ) )  {
            wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
        }
        $this->options = get_option( 'soliant-fm-sync' );


        require_once('partials/soliantconsulting-simplefm-wordpress-admin-display.php');
    }

    public function fm_url_callback()
    {
        printf(
            '<input type="text" id="fm_url" name="soliant-fm-sync[fm_url]" class="large-text" value="%s" />',
            isset( $this->options['fm_url'] ) ? esc_attr( $this->options['fm_url']) : ''
        );
    }
    public function fm_timezone_callback()
    {
        printf(
            '<input type="text" id="fm_timezone" name="soliant-fm-sync[fm_timezone]" class="regular-text" value="%s" />',
            isset( $this->options['fm_timezone'] ) ? esc_attr( $this->options['fm_timezone']) : ''
        );
    }
}
