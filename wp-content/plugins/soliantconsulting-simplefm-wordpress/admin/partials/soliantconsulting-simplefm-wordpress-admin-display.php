<?php

/**
* Provide a admin area view for the plugin
*
* This file is used to markup the admin-facing aspects of the plugin.
*
* @link       https://www.soliantconsulting.com
* @since      1.0.0
*
* @package    Soliantconsulting_Simplefm_Wordpress
* @subpackage Soliantconsulting_Simplefm_Wordpress/admin/partials
*/
?>
<div class="wrap">
    <h1>Filemaker Sync Settings</h1>
    <form method="post" action="options.php">
        <?php
        // This prints out all hidden setting fields
        settings_fields( 'my_option_group' );
        do_settings_sections( 'soliant-fm-sync' );
        submit_button();//submit button
        ?>
    </form>
</div>