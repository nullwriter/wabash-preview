<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.soliantconsulting.com
 * @since      1.0.0
 *
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/includes
 * @author     Jeremiah Small <jsmall@soliantconsulting.com>
 */
class Soliantconsulting_Simplefm_Wordpress_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
