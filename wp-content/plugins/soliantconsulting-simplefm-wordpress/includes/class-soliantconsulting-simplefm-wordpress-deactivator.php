<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.soliantconsulting.com
 * @since      1.0.0
 *
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/includes
 * @author     Jeremiah Small <jsmall@soliantconsulting.com>
 */
class Soliantconsulting_Simplefm_Wordpress_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
