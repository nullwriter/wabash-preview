<?php
/**
 * This source file is subject to the MIT license that is bundled with this package in the file LICENSE.txt.
 *
 * @package   SimpleFM
 * @copyright Copyright (c) 2007-2016 Soliant Consulting, Inc. (http://www.soliantconsulting.com)
 * @author    jsmall@soliantconsulting.com
 *
 * PREREQUISITES
 *  To run this example file, you will need an environment with PHP7+, Composer, and FileMaker Server 15 running on
 *  localhost with the default FMServer_Sample file hosted.
 *
 * INSTRUCTIONS
 *  In the terminal, run the following commands
 *      cd {project-root}/doc/filemaker
 *      composer install
 *      php -S 0.0.0.0:8080 -t ./ ./simplefm_example.php
 *
 *  The last command starts the internal PHP web server. Now when you visit the browser you should see the SimpleFM
 *  Example page displayed in your browser.
 */

require_once(__DIR__ . '/../../../vendor/autoload.php');

// Load the WordPress library.
require_once( dirname(__FILE__) . '/../../../wp-load.php' );

// Set up the WordPress query.
wp();

echo '<pre>';
WP_Filesystem();
$sync = new \Soliant\SimpleFMWordpress\Infrastructure\Sync($wpdb, $wp_filesystem);
$sync->syncAllTables();
