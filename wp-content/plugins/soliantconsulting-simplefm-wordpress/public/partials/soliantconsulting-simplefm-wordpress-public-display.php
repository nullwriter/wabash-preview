<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.soliantconsulting.com
 * @since      1.0.0
 *
 * @package    Soliantconsulting_Simplefm_Wordpress
 * @subpackage Soliantconsulting_Simplefm_Wordpress/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
