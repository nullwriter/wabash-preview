<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.soliantconsulting.com
 * @since             1.0.0
 * @package           Soliantconsulting_Simplefm_Wordpress
 *
 * @wordpress-plugin
 * Plugin Name:       SimpleFM WordPress
 * Plugin URI:        https://github.com/soliantconsulting/SimpleFM-WordPress
 * Description:       Plugin to set FM integration with WP and help generate an Application Form.
 * Version:           1.0.0
 * Author:            Jeremiah Small
 * Author URI:        https://www.soliantconsulting.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       soliantconsulting-simplefm-wordpress
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-soliantconsulting-simplefm-wordpress-activator.php
 */
function activate_soliantconsulting_simplefm_wordpress() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-soliantconsulting-simplefm-wordpress-activator.php';
	Soliantconsulting_Simplefm_Wordpress_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-soliantconsulting-simplefm-wordpress-deactivator.php
 */
function deactivate_soliantconsulting_simplefm_wordpress() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-soliantconsulting-simplefm-wordpress-deactivator.php';
	Soliantconsulting_Simplefm_Wordpress_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_soliantconsulting_simplefm_wordpress' );
register_deactivation_hook( __FILE__, 'deactivate_soliantconsulting_simplefm_wordpress' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-soliantconsulting-simplefm-wordpress.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_soliantconsulting_simplefm_wordpress() {

	$plugin = new Soliantconsulting_Simplefm_Wordpress();
	$plugin->run();

}
run_soliantconsulting_simplefm_wordpress();
