<?php
namespace Soliant\SimpleFMWordpress\Infrastructure;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
// ini_set('xdebug.var_display_max_depth', -1);
// ini_set('xdebug.var_display_max_children', -1);
// ini_set('xdebug.var_display_max_data', -1);
// ini_set('memory_limit', '1024M');
//
//   function myoutput($str){
//      global $debug;
//      if(!$debug){
//          return;
//      }
//        echo $str;
//        ob_flush();
//        flush();
//   }

use Http\Client\Curl\Client;
use Soliant\SimpleFM\Collection\CollectionInterface;
use Soliant\SimpleFM\Client\ResultSet\ResultSetClient;
use Soliant\SimpleFM\Connection\Command;
use Soliant\SimpleFM\Connection\Connection;
use Zend\Diactoros\Uri;
use Litipk\BigNumbers\Decimal;
use DateTimeZone;
use wpdb;
use WP_Filesystem_Base;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\MemoryUsageProcessor;
use Monolog\Processor\WebProcessor;

class Sync
{
    /** @var array settings last sync times from soliant-fm-sync-times */
    protected $lastSync;

    /** @var ResultSetClientInterface Filemaker client */
    protected $result ;

    /** @var array settings from soliant-fm-sync */
    protected $settings;

    /** @var DateTimeZone Timezone of filemaker server */
    protected $timezone;

    /** @var wpdb  */
    protected $wpdb;

    /** @var log  */
    protected $log;

    /** @var WP_Filesystem_Base  */
    protected $wp_filesystem;

    public function __construct(wpdb $wpdb, WP_Filesystem_Base $wp_filesystem)
    {
        $this->wpdb = $wpdb;
        $this->wp_filesystem = $wp_filesystem;

        $this->settings = get_option('soliant-fm-sync');

        $httpClient = new Client();
        $uri = new Uri($this->settings['fm_url']);
        $database = 'wc_web_Overview';
        $identityHandler = null;
        $logger = null;
        $connection = new Connection(
            $httpClient,
            $uri,
            $database,
            $identityHandler,
            $logger
        );

        $this->timezone = new DateTimeZone($this->settings['fm_timezone']);

        $this->resultSetClient = new ResultSetClient(
            $connection,
            $this->timezone
        );

        $this->lastSync = get_option('soliant-fm-sync-times');

        $this->set_logger();
    }



    protected function set_logger($level = Logger::DEBUG){


        $info=wp_upload_dir();
        $path=$info['basedir'];

        $log_dir = $info['basedir'].'/logs/';
        // log channel
        $this->log = new Logger('sync');

        // handler
        // $browserConsoleHandler = new Monolog\Handler\BrowserConsoleHandler($level);
        // $browserConsoleHandler->setFormatter(new Tatocaster\Monolog\Formatter\JsonPrettyUnicodePrintFormatter());
        //
        // $logFileHandler = new Monolog\Handler\StreamHandler($log_dir.'sf.log',$level);

        $logRotatingFileHandler = new RotatingFileHandler($log_dir.'fm.log', 7, $level);

        //$this->log->pushHandler($browserConsoleHandler);
        $this->log->pushHandler($logRotatingFileHandler);

        // processor
        $this->log->pushProcessor(new IntrospectionProcessor());
        $this->log->pushProcessor(new MemoryUsageProcessor());
        $this->log->pushProcessor(new WebProcessor());

    }

    protected function persistSyncTimes()
    {
        update_option('soliant-fm-sync-times', $this->lastSync);
    }

    protected function getLastSync($tableName) : string
    {
        if (isset($this->lastSync[$tableName])) {
            $lastSync = $this->lastSync[$tableName];
        } else {
            $lastSync = new \DateTime('-1 days', $this->timezone);// default to a day in the past
        }

        return $lastSync->format('m/d/Y G:i:s');
    }

    protected function setLastSync(string $tableName, \DateTime $sync) {
        $this->lastSync[$tableName] = $sync;
        $this->persistSyncTimes();
    }

    public function syncAllTables()
    {
         $synced = $this->syncGrant();
         echo 'Synced ' .$synced . " Grant records\n";
         $this->log->info('Synced ' .$synced . " Grant records\n");
         $synced = $this->syncBook();
         echo 'Synced ' . $synced . " Book records\n";
         $this->log->info('Synced ' . $synced . " Book records\n");
         $synced = $this->syncScholarship();
         echo 'Synced ' . $synced . " Scholarship records\n";
         $this->log->info('Synced ' . $synced . " Scholarship records\n");
    }

    public function syncScholarship() {
        $newSync = new \DateTime('-1 minutes', $this->timezone);
        //$newSync = new \DateTime('-15 year', $this->timezone);
        try {
            $records = $this->getScholarshipToSync($this->getLastSync('scholarship'));
            //$records = $this->getScholarshipToSync($newSync->format('m/d/Y G:i:s'));
        } catch (\Throwable $t) {
            echo 'Error Pulling Scholarship records from FM: ' . $t->getMessage();
            $this->log->error('Error Pulling Scholarship records from FM: ' . $t->getMessage());
            return false;
        }
        //echo "number of records ".COUNT($records). "\n";
        //exit();
        $syncedRecords = 0;
        foreach ($records as $row) {
          //var_dump($row);
          $row['recid']=!empty($row['recid'])?$row['recid']->asInteger():null;
          $row['Web Flag']=!empty($row['Web Flag'])?$row['Web Flag']->asInteger():null;
          $row['WC Number']=!empty($row['WC Number'])?$row['WC Number']->innerValue():null;
          $row['Web Recommended']=!empty($row['Web Recommended'])?$row['Web Recommended']->asInteger():null;
          $row['Publication Date']=!empty($row['Publication Date'])?$row['Publication Date']->asInteger():null;

            try {
                $wp_scholarship = [
                    'title'                  => $row['Title'],
                    'author'                 => $row['Author'],
                    'publication_date'       => $row['Publication Date'],
                    'isbn_issn'              => $row['ISBN_ISSN'],
                    'web_recommended'        => ($row['Web Recommended'] == 1 ?: 0),
                    'web_type'               => $row['Web Type_WordPress'],
                    'publisher'              => $row['Publisher'],
                    'call_number'            => $row['Call Number'],
                    'wc_number'              => $row['WC Number'],
                    'format'                 => $row['Format'],
                    'image'                  => '',
                    'url_article_link'       => $row['Article Link'],
                    'url_bookreview'         => $row['URL_BookReview'],
                    'url_ttr_article_link'   => $row['URL_TTR Article Link'],
                    'url_web_resource'       => $row['URL_Web_Resource'],
                    'additional_info'        => $row['Additional Info_WordPress'],
                    'table_of_contents_info' => $row['Table of Contents Info'],
                    'image_url'              => '',
                    'Web_flag'               => ($row['Web Flag'] == 1 ?: 0),
                    'recid'                  => $row['recid'],
                    'topic_list'             => $row['TopicNames_c'],
                    'post_id' => 0,
                ];
                $image['stream'] = $row['Image'];
                $meta = explode(':', $row['Image_Sync_metadata_c']);
                if (count($meta) == 2) {
                    $image['md5'] = $meta[0];
                    $image['filename'] = $meta[1];
                } else {
                    $image['md5'] = null;
                    $image['filename'] = null;
                }
                //var_dump($wp_scholarship);
                //var_dump($image);
                //exit();
                if(!empty($row['Title'])){
                    $this->updateScholarshipPost($wp_scholarship, $image);
                    $syncedRecords++;
                }


            } catch (\Throwable $t) {
                echo 'Error pushing Scholarship post: ' . json_encode($row) . ' - '. $t->getMessage();
                $this->log->error('Error pushing Scholarship post: ' . json_encode($row) . ' - '. $t->getMessage());
            }
        }
        $this->setLastSync('scholarship', $newSync);
        return $syncedRecords;
    }

    public function syncBook()
    {
        $newSync = new \DateTime('-1 minutes', $this->timezone);
        //$newSync = new \DateTime('-15 year', $this->timezone);
        try {
            $records = $this->getBookToSync($this->getLastSync('book'));
            //$records = $this->getBookToSync($newSync->format('m/d/Y G:i:s'));
        } catch (\Throwable $t) {
            echo 'Error Pulling Book records from FM: ' . $t->getMessage();
            $this->log->error('Error Pulling Book records from FM: ' . $t->getMessage());
            return false;
        }
        //var_dump(COUNT($records));
        echo 'Getting '.(COUNT($records)).' book records to sync'. "<br/>\n";
        $this->log->info('Getting '.(COUNT($records)).' book records to sync'. "\n");
        $syncedRecords = 0;
        foreach ($records as $row) {
            //var_dump($row);
            //exit();
            $row['__kp_BookID']=!empty($row['__kp_BookID'])?$row['__kp_BookID']->innerValue():null;
            $row['Date']=!empty($row['Date'])?$row['Date']->asInteger():null;
            $row['Web_Flag']=!empty($row['Web_Flag'])?$row['Web_Flag']->asInteger():null;
            $row['Reviewed_Flag']=!empty($row['Reviewed_Flag'])?$row['Reviewed_Flag']->asInteger():null;
            $row['Price']=!empty($row['Price'])?$row['Price']->asFloat():null;
            try {
                $wp_book = [
                    '__fk_BookID'        => $row['__kp_BookID'],
                    'title'              => $row['Title'],
                    'author'             => $row['Author'],
                    'publisher'          => $row['Publisher'],
                    'year'               => $row['Date'],
                    'book_cover_image'   => '',
                    'image_url'          => '',
                    'book_publisher_url' => $row['Book url'],
                    'isbn'               => $row['ISBN'],
                    'Web_flag'           => ($row['Web_Flag'] == 1 ? 1 : 0),
                    'reviewed'           => ($row['Reviewed_Flag'] == 1 ? 1 : 0),
                    'date_reviewed'      => null,
                    'pages'              => $row['Pages'],
                    'price'              => $row['Price'],
                    'review'             => '',
                    'reviewer1_name'     => $row['Reviewer1_Name'],
                    'reviewer1_email'    => $row['Reviewer1_Email'],
                    'reviewer1_institution' => $row['Reviewer1_Institution'],
                    'reviewer2_name'     => $row['Reviewer2_Name'],
                    'reviewer2_email'    => $row['Reviewer2_Email'],
                    'reviewer2_institution' => $row['Reviewer2_Institution'],
                    'post_id' => 0,
                ];
                $image['stream'] = $row['Image'];
                $meta = explode(':', $row['Image_Sync_metadata_c']);
                if (count($meta) == 2) {
                    $image['md5'] = $meta[0];
                    $image['filename'] = $meta[1];
                } else {
                    $image['md5'] = null;
                    $image['filename'] = null;
                }
                $this->updateBookPost($wp_book, $image);
                $syncedRecords++;
                //exit();
            } catch (\Throwable $t) {
                echo 'Error pushing Book post: ' . json_encode($row) . ' - '. $t->getMessage();
                $this->log->error('Error pushing Book post: ' . json_encode($row) . ' - '. $t->getMessage());
            }
        }
        $this->setLastSync('book', $newSync);
        return $syncedRecords;
    }

    /**
     * Sync Grant table from filemaker to wordpress
     */
    public function syncGrant()
    {
        $newSync = new \DateTime('-1 minutes', $this->timezone);
        try {
            $records = $this->getGrantToSync($this->getLastSync('grant'));
        } catch (\Throwable $t) {
            echo 'Error Pulling Grant records from FM: ' . $t->getMessage();
            $this->log->error('Error Pulling Grant records from FM: ' . $t->getMessage());
            return false;
        }
        //myoutput( "number of records ".COUNT($records). "<br/>");
        //exit();
        $syncedRecords = 0;
        foreach ($records as $row) {
          //var_dump($row);

          $row['Web_Flag']=!empty($row['Web_Flag'])?$row['Web_Flag']->asInteger():null;
          $row['Grant Year']=!empty($row['Grant Year'])?$row['Grant Year']->asInteger():null;

            try {
                $wp_grants = [
                    '__fk_Grant_t'      => $row['__pk_Grant_t'],
                    'title'             => $row['Title'],
                    'institution'       => $row['Organization::Organization Name'],
                    'project_director'  => $row['ContactNameList_c'],
                    'type'              => $row['Organization::Web_Export_Type'],
                    'year'              => $row['Grant Year'],
                    'topics'            => $row['TopicNameList_c'],
                    'Web_flag'          => ($row['Web_Flag'] == 1 ? 1 : 0),
                    'proposal_abstract' => $row['Description'],
                    'learning_abstract' => $row['Web Learning Abstract'],
                    'post_id' => 0,
                ];
                $this->updateGrantPost($wp_grants);
                $syncedRecords++;
            } catch (\Throwable $t) {
                echo 'Error pushing grant post: ' . json_encode($row) . ' - '. $t->getMessage();
                $this->log->error('Error pushing grant post: ' . json_encode($row) . ' - '. $t->getMessage());
            }
        }
        $this->setLastSync('grant', $newSync);
        return $syncedRecords;
    }

    /**
     * Pulls filemaker records modified after $lastSync
     * @param string $lastSync the last sync time.
     * @return CollectionInterface
     */
    protected function getGrantToSync(string $lastSync)  : CollectionInterface
    {
        return $this->getRecordsToSync($lastSync, 'Grant');
    }

    /**
     * Pulls filemaker records modified after $lastSync
     * @param string $lastSync the last sync time.
     * @return CollectionInterface
     */
    protected function getBookToSync(string $lastSync)  : CollectionInterface
    {
        return $this->getRecordsToSync($lastSync, 'BookReview');
    }

    /**
     * Pulls filemaker records modified after $lastSync
     * @param string $lastSync the last sync time.
     * @return CollectionInterface
     */
    protected function getScholarshipToSync(string $lastSync)  : CollectionInterface
    {
        return $this->getRecordsToSync($lastSync, 'ScholarshipOnTeaching');
    }

    protected function getRecordsToSync(string $lastSync, string $layout) : CollectionInterface
    {
        $command = new Command($layout, ['z_HostModificationStamp' => $lastSync, 'z_HostModificationStamp.op' => 'gte', '-find' => null]);
        //$command = new Command($layout, ['-max'=>100,'-findall' => null]);

        $records = $this->resultSetClient->execute($command);
        return $records;
    }

    protected function updateScholarshipPost(array $wp_scholarship, array $image)
    {
        $query = $this->wpdb->prepare('SELECT id, image, post_id FROM wp_scholarship_of_teaching WHERE recid = %s LIMIT 1', $wp_scholarship['recid']);
        $scholarship = $this->wpdb->get_row($query);
        if ($scholarship == null){
            $this->wpdb->replace(
                'wp_scholarship_of_teaching',
                $wp_scholarship
            );
            $scholarship = $this->wpdb->get_row($query);
        } else  {
          unset($wp_scholarship['image']);
          unset($wp_scholarship['image_url']);
          unset($wp_scholarship['post_id']);


            $this->wpdb->update('wp_scholarship_of_teaching', $wp_scholarship, ['id' => $scholarship->id]);
        }
        $scholarshipID = $scholarship->id;


        if (empty($scholarship->post_id)) {
            $post = array(
                'post_title'    => wp_strip_all_tags( $wp_scholarship['title'] ),
                'post_content'  => '',
                'post_status'   => ($wp_scholarship['Web_flag'] == 1 ? 'publish' : 'notforweb'),
                'post_author'   => 1,
                'post_type' => 'scholarship'
            );
            $postId = wp_insert_post($post);
            $this->wpdb->update('wp_scholarship_of_teaching', ['post_id' => $postId], ['id' => $scholarship->id]);
            echo  "Created scholarship post: " . $postId . " from recid: " . $wp_scholarship['recid'] . "<br/>\n";
            $this->log->info( "Created scholarship post: " . $postId . " from recid: " . $wp_scholarship['recid'] . "\n");
        } else {
            $postId = $scholarship->post_id;
            $post = array(
                'ID' => $postId,
                'post_title' => wp_strip_all_tags( $wp_scholarship['title'] ),
                'post_status' =>  ($wp_scholarship['Web_flag'] == 1 ? 'publish' : 'notforweb'),
            );
            wp_update_post($post);
            echo "Updating scholarship post: " . $postId . " from recid: " . $wp_scholarship['recid'] . "with url_web_resource ".$wp_scholarship['url_web_resource']."\n";
            $this->log->info( "Updating scholarship post: " . $postId . " from recid: " . $wp_scholarship['recid'] . "\n");
        }

        if ($image['md5'] && $scholarship->image != $image['md5']) {
            /** @var \Soliant\SimpleFM\Client\ResultSet\Transformer\StreamProxy $stream */
            $stream = $image['stream'];
            $imageContents = $stream->getContents();


            $ext = pathinfo($image['filename'], PATHINFO_EXTENSION);
            $ext = strtolower($ext);


           if (!empty($imageContents)) {
             if($ext=='pct'){
               echo 'wrong format for image (pct): skipping this image '. "\n";
               $this->log->error( 'wrong format for image (pct): skipping this image '. "\n");

             }
             else{
               if($ext=='jpg'){
                 $ext='jpeg';
               }
               $filename='scholarship-'.sanitize_title($wp_scholarship['title']).'.'.$ext;


               $image_url = $this->attachImage($filename, $imageContents, $postId);

            $this->wpdb->update(
                'wp_scholarship_of_teaching',
                ['image' => $image['md5'],'image_url' => $image_url],
                ['id'=> $scholarshipID]
            );
               echo 'Updated scholarship post image: ' . $postId . " New md5 " . $image['md5'] . "\n";
               $this->log->info( 'Updated scholarship post image: ' . $postId . " New md5 " . $image['md5'] . "\n");
             }

            }

        }

        if (!empty($wp_scholarship['web_type'])) {
            wp_set_object_terms($postId, $wp_scholarship['web_type'], 'scholarship-type');
        }

        if (!empty($wp_scholarship['web_recommended'])) {
            wp_set_object_terms($postId, $wp_scholarship['web_recommended'], 'scholarship-web-recommended');
        }

        if (!empty($wp_scholarship['topic_list'])) {
            $topics = explode('|', $wp_scholarship['topic_list']);
            wp_set_object_terms($postId, $topics, 'scholarship-topic');
        }

        //exit();
    }

    protected function updateBookPost(array $wp_book, array $image)
    {
        $query = $this->wpdb->prepare('SELECT id, book_cover_image, post_id FROM wp_books_reviews WHERE __fk_BookID = %s LIMIT 1', $wp_book['__fk_BookID']);
        $book = $this->wpdb->get_row($query);
        if ($book == null){
            echo 'Creating new book_review records on WP'. "<br/>\n";
            $this->log->info( 'Creating new book_review records on WP'. "\n");
            $this->wpdb->replace(
                'wp_books_reviews',
                $wp_book
            );
            $book = $this->wpdb->get_row($query);
        } else  {
          unset($wp_book['book_cover_image']);
          unset($wp_book['image_url']);
          unset($wp_book['date_reviewed']);
          unset($wp_book['review']);
          unset($wp_book['post_id']);

            $this->wpdb->update('wp_books_reviews', $wp_book, ['id' => $book->id]);
            echo 'Updating book_review records on WP'. "<br/>\n";
            $this->log->info( 'Updating book_review records on WP'. "\n");
        }
        $bookID = $book->id;

        //var_dump($book);

      if (empty($book->post_id)) {
            $post = array(
                'post_title'    => wp_strip_all_tags( $wp_book['title'] ),
                'post_content'  => '',
                'post_status'   => ($wp_book['Web_flag'] == 1 ? 'publish' : 'notforweb'),
                'post_author'   => 1,
                'post_type' => 'book_reviews'
            );
            $postId = wp_insert_post($post);
            $this->wpdb->update('wp_books_reviews', ['post_id' => $postId], ['id' => $book->id]);
            echo "Created book post: " . $postId . " from __fk_BookID: " . $wp_book['__fk_BookID'] . "<br/>\n";
            $this->log->info( "Created book post: " . $postId . " from __fk_BookID: " . $wp_book['__fk_BookID'] . "\n");
        } else {
            //update an existing post
            $postId = $book->post_id;
            $post = array(
                'ID' => $postId,
                'post_title' => wp_strip_all_tags( $wp_book['title'] ),
                'post_status' =>  ($wp_book['Web_flag'] == 1 ? 'publish' : 'notforweb'),
            );
            wp_update_post($post);
            echo "Updating book post: " . $postId . " from __fk_BookID: " . $wp_book['__fk_BookID'] . "<br/>\n";
            $this->log->info("Updating book post: " . $postId . " from __fk_BookID: " . $wp_book['__fk_BookID'] . "\n");
        }

        //check if there's an image and if it's changed.
        if ($image['md5'] && $book->book_cover_image != $image['md5']) {
            /** @var \Soliant\SimpleFM\Client\ResultSet\Transformer\StreamProxy $stream */
            $stream = $image['stream'];
            $imageContents = $stream->getContents();
             $ext = pathinfo($image['filename'], PATHINFO_EXTENSION);
             $ext = strtolower($ext);


            if (!empty($imageContents)) {
              if($ext=='pct'){
                echo 'wrong format for image (pct): skipping this image '. "<br/>\n";
                $this->log->error('wrong format for image (pct): skipping this image '. "\n");

              }
              else{
                  if($ext=='jpg'){
                               $ext='jpeg';
                  }
                  $filename='books-'.sanitize_title($wp_book['title']).'.'.$ext;


                  $image_url = $this->attachImage($filename, $imageContents, $postId);

                  $this->wpdb->update(
                      'wp_books_reviews',
                      ['book_cover_image' => $image['md5'],'image_url' => $image_url],
                      ['id'=> $bookID]
                  );
                  echo 'Updated book post image: ' . $postId . " New md5 " . $image['md5'] . "<br/>\n";
                  $this->log->info('Updated book post image: ' . $postId . " New md5 " . $image['md5'] . "\n");
              }

            }



        }

        if (!empty($wp_book['year'])) {
            wp_set_object_terms($postId, $wp_book['year'], 'book-year');
            echo "Updating book post " . $postId . " year: " . $wp_book['year'] . "<br/>\n";
            $this->log->info("Updating book post " . $postId . " year: " . $wp_book['year'] . "\n");
        }
    }

    /**
     * Update a post based on $wp_grants.
     * @param array $wp_grants
     */
    protected function updateGrantPost(array $wp_grants)
    {
        // create or update wp_grants record
        $query = $this->wpdb->prepare('SELECT id, post_id FROM wp_grants WHERE __fk_Grant_t = %s LIMIT 1', $wp_grants['__fk_Grant_t']);
        $grant = $this->wpdb->get_row($query);
        if ($grant == null){
            $this->wpdb->replace(
                'wp_grants',
                $wp_grants
            );
            $grant = $this->wpdb->get_row($query);
        } else  {
            unset($wp_grants['post_id']);

            $this->wpdb->update('wp_grants', $wp_grants, ['id' => $grant->id]);
        }
        $grantID = $grant->id;


        if (empty($grant->post_id)) {
            //create a new post
            $post = array(
                'post_title'    => wp_strip_all_tags( $wp_grants['title'] ),
                'post_content'  => '',
                'post_status'   =>  ($wp_grants['Web_flag'] == 1 ? 'publish' : 'notforweb'),
                'post_author'   => 1,
                'post_type' => 'grants'
            );
            $postId = wp_insert_post($post);
            $this->wpdb->update('wp_grants', ['post_id' => $postId], ['id' => $grant->id]);
            echo "Created grant post: " . $postId . " from __fk_Grant_t: " . $wp_grants['__fk_Grant_t'] . " with Web_Flag ".$wp_grants['Web_flag']."\n";
            $this->log->info("Created grant post: " . $postId . " from __fk_Grant_t: " . $wp_grants['__fk_Grant_t'] . "\n");
        } else {
            //update an existing post
            $postId = $grant->post_id;
            $post = array(
                'ID' => $postId,
                'post_title' => wp_strip_all_tags( $wp_grants['title'] ),
                'post_status' =>  ($wp_grants['Web_flag'] == 1 ? 'publish' : 'notforweb'),
            );
            wp_update_post($post);
            echo "Updating grant post: " . $postId . " __fk_Grant_t: " . $wp_grants['__fk_Grant_t'] .  " with Web_Flag ".$wp_grants['Web_flag']."\n";
            $this->log->info("Updating grant post: " . $postId . " __fk_Grant_t: " . $wp_grants['__fk_Grant_t'] . "\n");
        }

        //add/update meta data
        if (!empty($wp_grants['type'])) {
            wp_set_object_terms($postId, $wp_grants['type'], 'grant-type');
        }

        if (!empty($wp_grants['year'])) {
            wp_set_object_terms($postId, $wp_grants['year'], 'grant-year');
        }


        if (!empty($wp_grants['topics'])) {
            $topics = explode('|', $wp_grants['topics']);
            wp_set_object_terms($postId, $topics, 'grant-topic');
        }
    }

    private function writeFile($fullpath, $contents) {
        $fp = @fopen( $fullpath, 'wb' );
        if ( ! $fp ) {
            echo 'Failed to open file for writing: ' . $fullpath ."\n";
            $this->log->error('Failed to open file for writing: ' . $fullpath ."\n");
            return false;
        }
        mbstring_binary_safe_encoding();

        $data_length = strlen( $contents );

        $bytes_written = fwrite( $fp, $contents );

        reset_mbstring_encoding();

        fclose( $fp );
        if ( $data_length !== $bytes_written )
            return false;

        return true;
    }

    protected function attachImage($filename, $contents, $parent_post_id) {
        // $filename should be the path to a file in the upload directory.
        $wp_upload_dir = wp_upload_dir();

        //var_dump($wp_upload_dir);


        $fullpath = $wp_upload_dir['path'] . '/' . $filename;
        $url = $wp_upload_dir['url']. '/' . $filename;


        $this->writeFile(
            $fullpath,
            $contents
        );

        // Check the type of file. We'll use this as the 'post_mime_type'.
        $filetype = wp_check_filetype( basename( $fullpath ), null );

        // Get the path to the upload directory.


        // Prepare an array of post data for the attachment.
        $attachment = array(
            'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
            'post_mime_type' => $filetype['type'],
            'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
            'post_content'   => '',
            'post_status'    => 'inherit'
        );

        // Insert the attachment.
        $attach_id = wp_insert_attachment( $attachment, $fullpath, $parent_post_id );

        // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
        require_once( ABSPATH . 'wp-admin/includes/image.php' );

        // Generate the metadata for the attachment, and update the database record.

        $attach_data = wp_generate_attachment_metadata( $attach_id, $fullpath);
        wp_update_attachment_metadata( $attach_id, $attach_data );

        set_post_thumbnail( $parent_post_id, $attach_id );

        return $url;
    }


}
