<?php
/*
Plugin Name: Syllabi
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the Syllabi CPTs.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/
// Register Custom Post Type
function syllabi_cpt() {

  $labels = array(
    'name'                  => _x( 'Syllabi', 'Post Type General Name', 'syllabi_cpt' ),
    'singular_name'         => _x( 'syllabus', 'Post Type Singular Name', 'syllabi_cpt' ),
    'menu_name'             => __( 'Syllabi', 'admin menu', 'syllabi_cpt' ),
    'name_admin_bar'        => __( 'Syllabi', 'add new on admin bar', 'syllabi_cpt' ),
    'archives'              => __( 'Item Archives', 'syllabi_cpt' ),
    'parent_item_colon'     => __( 'Parent Syllabus:', 'syllabi_cpt' ),
    'all_items'             => __( 'All Syllabi', 'syllabi_cpt' ),
    'add_new_item'          => __( 'Add New Syllabus', 'syllabi_cpt' ),
    'add_new'               => __( 'New Syllabus', 'syllabi_cpt' ),
    'new_item'              => __( 'New Syllabus', 'syllabi_cpt' ),
    'edit_item'             => __( 'Edit Syllabus', 'syllabi_cpt' ),
    'update_item'           => __( 'Update Syllabus', 'syllabi_cpt' ),
    'view_item'             => __( 'View Syllabus', 'syllabi_cpt' ),
    'search_items'          => __( 'Search Syllabi', 'syllabi_cpt' ),
    'not_found'             => __( 'No Syllabi found', 'syllabi_cpt' ),
    'not_found_in_trash'    => __( 'No Syllabi found in Trash', 'syllabi_cpt' ),
    'featured_image'        => __( 'Featured Image', 'syllabi_cpt' ),
    'set_featured_image'    => __( 'Set featured image', 'syllabi_cpt' ),
    'remove_featured_image' => __( 'Remove featured image', 'syllabi_cpt' ),
    'use_featured_image'    => __( 'Use as featured image', 'syllabi_cpt' ),
    'insert_into_item'      => __( 'Insert into item', 'syllabi_cpt' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'syllabi_cpt' ),
    'items_list'            => __( 'Items list', 'syllabi_cpt' ),
    'items_list_navigation' => __( 'Items list navigation', 'syllabi_cpt' ),
    'filter_items_list'     => __( 'Filter items list', 'syllabi_cpt' ),
  );
  $args = array(
    'label'                 => __( 'Syllabi', 'syllabi_cpt' ),
    'description'           => __( 'Syllabi Post Type', 'syllabi_cpt' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor',  ),
    'taxonomies'            => array( 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'rewrite'               => array( 'slug' => '/resource/syllabi' ),
    'menu_icon'              => 'dashicons-clipboard',
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'syllabi', $args );

}
add_action( 'init', 'syllabi_cpt');

function syllabi_taxonomies() {


  // Add new taxonomy, make it hierarchical (like categories)
    $label_fields =  array(
        'name'                       => _x( 'Fields', 'taxonomy general name' ),
        'singular_name'              => _x( 'Field', 'taxonomy singular name' ),
        'search_items'               => __( 'Search Fields' ),
        'all_items'                  => __( 'All Fields' ),
        'parent_item'                => __( 'Parent Field' ),
        'parent_item_colon'          => __( 'Parent Field:' ),
        'edit_item'                  => __( 'Edit Field' ),
        'update_item'                => __( 'Update Field' ),
        'add_new_item'               => __( 'Add New Field' ),
        'new_item_name'              => __( 'New Field Name' ),
        'separate_items_with_commas' => __( 'Separate Field with commas' ),
        'add_or_remove_items'        => __( 'Add or remove Fields' ),
        'choose_from_most_used'      => __( 'Choose from the most used Fields' ),
        'not_found'                  => __( 'No Fields found.' ),
        'menu_name'                  => __( 'Fields' ),
    );

    $args_field  = array(
        'hierarchical'          => true,
        'labels'                => $label_fields,
        'show_ui'               => true,
        'show_admin_column'     => true,
        //'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        // 'show_in_nav_menus'  => false,
        'public'                => true,
        'publicly_queryable'    => true,
        'has_archive'           => true,
        'rewrite'               => array( 'slug' => 'syllabi-field' ),
    );

  $label_programs =  array(
      'name'                       => _x( 'Programs', 'taxonomy general name' ),
      'singular_name'              => _x( 'Program', 'taxonomy singular name' ),
      'search_items'               => __( 'Search Programs' ),
      'all_items'                  => __( 'All Programs' ),
      'parent_item'                => __( 'Parent Program' ),
      'parent_item_colon'          => __( 'Parent Program:' ),
      'edit_item'                  => __( 'Edit Program' ),
      'update_item'                => __( 'Update Program' ),
      'add_new_item'               => __( 'Add New Program' ),
      'new_item_name'              => __( 'New Program Name' ),
      'separate_items_with_commas' => __( 'Separate Program with commas' ),
      'add_or_remove_items'        => __( 'Add or remove Programs' ),
      'choose_from_most_used'      => __( 'Choose from the most used Programs' ),
      'not_found'                  => __( 'No Programs found.' ),
      'menu_name'                  => __( 'Programs' ),
  );

  $args_program  = array(
      'hierarchical'          => true,
      'labels'                => $label_programs,
      'show_ui'               => true,
      'show_admin_column'     => true,
      //'update_count_callback' => '_update_post_term_count',
      'query_var'             => true,
      // 'show_in_nav_menus'  => false,
      'public'                => true,
      'publicly_queryable'    => true,
      'has_archive'           => true,
      'rewrite'               => array( 'slug' => 'syllabi-program' ),
  );

  $labels = array(
    'name'                       => _x( 'Topics', 'taxonomy general name' ),
    'singular_name'              => _x( 'Topic', 'taxonomy singular name' ),
    'search_items'               => __( 'Search Topics' ),
    'all_items'                  => __( 'All Topics' ),
    'parent_item'                => __( 'Parent Topic' ),
    'parent_item_colon'          => __( 'Parent Topic:' ),
    'edit_item'                  => __( 'Edit Topic' ),
    'update_item'                => __( 'Update Topic' ),
    'add_new_item'               => __( 'Add New Topic' ),
    'new_item_name'              => __( 'New Topic Name' ),
    'separate_items_with_commas' => __( 'Separate Topics with commas' ),
    'add_or_remove_items'        => __( 'Add or remove Topics' ),
    'choose_from_most_used'      => __( 'Choose from the most used Topics' ),
    'not_found'                  => __( 'No Topics found.' ),
    'menu_name'                  => __( 'Topics' ),
  );

  $args = array(
    'hierarchical'          => true,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    //'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    // 'show_in_nav_menus'  => false,
    'public'                => true,
    'publicly_queryable'    => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'syllabi-topic' ),
  );


  register_taxonomy( 'syllabi-topic', 'syllabi', $args );
  register_taxonomy( 'syllabi-program', 'syllabi', $args_program );
  register_taxonomy( 'syllabi-field', 'syllabi', $args_field );

}
add_action( 'init', 'syllabi_taxonomies');


function syllabi_metaboxes(){
  add_meta_box("syllabi-url-meta", "Syllabus Url", "display_syllabi_url_meta_box", "syllabi", "normal", "high");
  add_meta_box("syllabi-recommended-meta", "Featured Syllabus", "display_syllabi_recommended_meta_box", "syllabi", "normal", "high");
  add_meta_box("syllabi-instructor-meta", "Syllabus Instructor", "display_syllabi_instructor_meta_box", "syllabi", "normal", "high");
  add_meta_box("syllabi-institution-meta", "Syllabus Institution", "display_syllabi_institution_meta_box", "syllabi", "normal", "high");
  add_meta_box("syllabi-course_type-meta", "Syllabus Course Type", "display_syllabi_course_type_meta_box", "syllabi", "normal", "high");
  add_meta_box("syllabi-course_year-meta", "Syllabus Course Year", "display_syllabi_course_year_meta_box", "syllabi", "normal", "high");
  add_meta_box("syllabi-course_term-meta", "Syllabus Course Term", "display_syllabi_course_term_meta_box", "syllabi", "normal", "high");

}

function display_syllabi_url_meta_box(){
  global $post;
  $custom = get_post_custom($post->ID);
  $syllabi_url =  !empty($custom["syllabi-url"][0])?$custom["syllabi-url"][0]:'';

  ?>
  <label for="syllabi-url">Url:</label>
  <input name="syllabi-url" id="syllabi-url" value="<?php echo $syllabi_url; ?>" />
  <?php
}


function display_syllabi_recommended_meta_box() {
  global $post;
  wp_nonce_field( basename( __FILE__ ), 'syllabi_nonce' );
  $custom = get_post_custom($post->ID);
  $recommended =  !empty($custom["featured-syllabi"][0])?$custom["featured-syllabi"][0]:'';

  ?>
  <label for="featured-syllabi">
            <input type="checkbox" name="featured-syllabi" id="featured-syllabi" value="1" <?php if ( !empty ( $recommended ) && $recommended==1 ) { echo ' checked="checked"';} ?> />
            Check if this is a recommended Syllabus
        </label>
  <?php
}

function display_syllabi_instructor_meta_box(){
    global $post;
    $custom = get_post_custom($post->ID);
    $syllabi_instructor =  !empty($custom["syllabi-instructor"][0])?$custom["syllabi-instructor"][0]:'';

    ?>
    <label for="syllabi-instructor">Instructor:</label>
    <input name="syllabi-instructor" id="syllabi-instructor" value="<?php echo $syllabi_instructor; ?>" />
    <?php
}

function display_syllabi_institution_meta_box(){
    global $post;
    $custom = get_post_custom($post->ID);
    $syllabi_institution =  !empty($custom["syllabi-institution"][0])?$custom["syllabi-institution"][0]:'';

    ?>
    <label for="syllabi-institution">Institution:</label>
    <input name="syllabi-institution" id="syllabi-institution" value="<?php echo $syllabi_institution; ?>" />
    <?php
}

function display_syllabi_course_type_meta_box(){
    global $post;
    $custom = get_post_custom($post->ID);
    $syllabi_course_type =  !empty($custom["syllabi-course_type"][0])?$custom["syllabi-course_type"][0]:'';

    ?>
    <label for="syllabi-course_type">Course Type:</label>
    <input name="syllabi-course_type" id="syllabi-course_type" value="<?php echo $syllabi_course_type; ?>" />
    <?php
}

function display_syllabi_course_year_meta_box(){
    global $post;
    $custom = get_post_custom($post->ID);
    $syllabi_course_year =  !empty($custom["syllabi-course_year"][0])?$custom["syllabi-course_year"][0]:'';

    ?>
    <label for="syllabi-course_year">Course Year:</label>
    <input name="syllabi-course_year" id="syllabi-course_year" value="<?php echo $syllabi_course_year; ?>" />
    <?php
}

function display_syllabi_course_term_meta_box(){
    global $post;
    $custom = get_post_custom($post->ID);
    $syllabi_course_term =  !empty($custom["syllabi-course_term"][0])?$custom["syllabi-course_term"][0]:'';

    ?>
    <label for="syllabi-course_term">Course Term:</label>
    <input name="syllabi-course_term" id="syllabi-course_term" value="<?php echo $syllabi_course_term; ?>" />
    <?php
}


add_action('admin_init', 'syllabi_metaboxes');

function save_syllabi_metaboxes( $post_id ) {


    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'syllabi_nonce' ] ) && wp_verify_nonce( $_POST[ 'syllabi_nonce' ], basename( __FILE__ ) ) ) ? TRUE : FALSE;

    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
    return;
    }

    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'featured-syllabi' ] ) ) {
    update_post_meta( $post_id, 'featured-syllabi', 1 );
    }
    else{
    update_post_meta( $post_id, 'featured-syllabi', 0 );
    }

    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'syllabi-url' ] ) ) {
    update_post_meta( $post_id, 'syllabi-url', $_POST[ 'syllabi-url' ]  );
    }

    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'syllabi-instructor' ] ) ) {
    update_post_meta( $post_id, 'syllabi-instructor', $_POST[ 'syllabi-instructor' ]  );
    }

    if( isset( $_POST[ 'syllabi-institution' ] ) ) {
    update_post_meta( $post_id, 'syllabi-institution', $_POST[ 'syllabi-institution' ]  );
    }

    if( isset( $_POST[ 'syllabi-course_type' ] ) ) {
    update_post_meta( $post_id, 'syllabi-course_type', $_POST[ 'syllabi-course_type' ]  );
    }

    if( isset( $_POST[ 'syllabi-course_year' ] ) ) {
    update_post_meta( $post_id, 'syllabi-course_year', $_POST[ 'syllabi-course_year' ]  );
    }

    if( isset( $_POST[ 'syllabi-course_term' ] ) ) {
    update_post_meta( $post_id, 'syllabi-course_term', $_POST[ 'syllabi-course_term' ]  );
    }


}

add_action( 'save_post_syllabi', 'save_syllabi_metaboxes' );

add_filter('manage_syllabi_posts_columns' , 'add_syllabi_columns');

function add_syllabi_columns($columns) {
    //unset($columns['author']);
    return array_merge($columns,
          array('featured-syllabi' => 'Recommended'));
}

add_action('manage_syllabi_posts_custom_column' , 'syllabi_posts_custom_columns', 10, 2 );

function syllabi_posts_custom_columns( $column, $post_id ) {

    switch ( $column ) {

    case 'featured-syllabi' :
        echo get_post_meta($post_id, 'featured-syllabi', true);

        break;
    }
}

// Register the column as sortable
add_filter( 'manage_edit-syllabi_sortable_columns', 'syllabi_posts_column_register_sortable' );
function syllabi_posts_column_register_sortable( $columns ) {
    $columns['featured-syllabi'] = 'featured-syllabi';
    return $columns;
}

add_filter( 'pre_get_posts','syllabi_posts_column_orderby');
function syllabi_posts_column_orderby( $query ) {
    if( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');
    if ( 'featured-syllabi' == $orderby ) {
        //alter the query args
        $query->set('meta_key',$orderby);
        $query->set('orderby','meta_value');
    }
}//end _column_

function display_excerpt_syllabi_object($syllabus){

    // get tags
    $terms = array_map('trim', explode('|', $syllabus->tags));

    $tags = array();
    $tag_names = array();
    foreach($terms as $tag){
        if(!empty($tag)) {
            $term = get_term_by('name', $tag, 'post_tag');
            $tags[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
            $tag_names[]=$term->name;
        }
    }
    if(in_array('internal-syllabi',$tag_names)){

            $title="<a  href='".$syllabus->syllabi_url."'>".$syllabus->title."</a>";
    }
    else{
        $title="<a target='_blank' href='".$syllabus->syllabi_url."'>".$syllabus->title."</a>";
    }
	 $html = '<p>'.$title.'<br/>';
   $html .=limit_text(wp_strip_all_tags($syllabus->annotation), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_syllabi_object($item){
    global $load_more_data;

    $single_results_page=is_page_template('template-section-results-page.php') || $load_more_data;
    $detail_page=is_singular() && !$single_results_page;

    $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/syllabi_icon.png'.'" title="Syllabi cover image" alt="Syllabi cover image">';



    $topics = array();

    // get topics
    if (!empty($item->topics)) {
    $terms = array_map('trim', explode('|', $item->topics));


    foreach($terms as $topic){
        if(!empty($topic)) {
            $term = get_term_by('name', $topic, 'syllabi-topic');
            $topics[]='<a href="'.get_category_link( $term->term_id ).'" >'.$term->name.'</a>';
        }
    }

    $topics=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$topics);
    }

    $tags = array();
    $tag_names = array();
    // get tags
    if (!empty($item->topics)) {
    $terms = array_map('trim', explode('|', $item->tags));


    foreach($terms as $tag){
        if(!empty($tag)) {
            $term = get_term_by('name', $tag, 'post_tag');
            $tags[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
            $tag_names[]=$term->name;
        }
    }

    $tags=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$tags);
    }
    $title='';
     if (!empty($item->title)) {
    if(in_array('internal-syllabi',$tag_names)){

            $title="<a  href='".$item->syllabi_url."'>".$item->title."</a>";

    }
    else{
        $title="<a target='_blank' href='".$item->syllabi_url."'>".$item->title."<i class='icon_external_url'></i></a>";
    }
    }
    
    $programs = array();

    if (!empty($item->programs)){
        $programs = array_map('trim', explode('|', $item->programs));
    }

    $programs_syllabi = array();
    foreach($programs as $program){
        if(!empty($program)) {
            $term = get_term_by('name', $program, 'syllabi-program');
            $programs_syllabi[]='<a href="'.get_category_link( $term->term_id ).'" >'.$term->name.'</a>';
        }
    }

    $fields = array();
    if (!empty($item->fields)){
        $fields = array_map('trim', explode('|', $item->fields));
    }
    $fields_syllabi = array();
    foreach($fields as $field){
        if(!empty($field)) {
            $term = get_term_by('name', $field, 'syllabi-field');
            $fields_syllabi[]='<a href="'.get_category_link( $term->term_id ).'" >'.$term->name.'</a>';
        }
    }

    $fields_syllabi=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$fields_syllabi);
    $programs_syllabi=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$programs_syllabi);
    
    $content='';
    if (!empty($item->annotation)) {
        $content = do_shortcode($item->annotation);
    }

    $course_level_format = $item->programs;

$html = '
<div class="panel card syllabi-card">
   <div class="panel-body">
      <div class="row is-flex">
         <div class="card-image col-xs-4 col-sm-4 col-md-3">
            '.$img.'
         </div>
         <div class="card-select col-xs-2 col-xs-offset-6 col-sm-2 col-sm-offset-6 col-md-1 col-md-offset-0 col-md-push-8">';
         if (!empty($item->featured_syllabi) && $item->featured_syllabi == '1') {
             $html .= ' <img id="image-normal" class="img-responsive" src="' . get_stylesheet_directory_uri() . '/images/tree.png' . '" title="Wabash tree" alt="Wabash tree">';
         }

if (!empty($item->id)) {
            $html .= '<div class="card-checkbox">
                        <input value="'.$item->id.'" id="resource_'.$item->id.'" class="case" type="checkbox">
                    </div>';

 }
        $html .= ' </div>
         <div class="card-info col-xs-12 col-sm-12 col-md-8 col-md-pull-1">
            <h3 class="card-title">
                '.$title.'
            </h3>
            <p class="card-meta">
            Syllabus<br>';
            if(!empty($topics) ) {
             $html.= '<span>Topics: '.$topics.'</span><br>';
           }
           if(!empty($tags) ) {
             $html.= '<span>Tags: '.$tags.'</span><br>';
           }
            if(!empty($fields_syllabi)){
                $html.= '<span>Fields: '.$fields_syllabi.'</span><br>';
            }
            if(!empty($programs_syllabi)){
                $html.= '<span>Course Level-Format: '.$programs_syllabi.'</span><br>';
            }
            $html.= '<div class="row">';
           if (!empty($item->instructor)){
             $html.= '<div class="col-md-6 col-xs-6 col-sm-6"><span>Instructor: '.$item->instructor.'</span></div>';
             if(empty($item->institution)){
                 $html.='<br>';
             }
           }
           if (!empty($item->institution)){
             $html.= '<div class="col-md-6 col-xs-6 col-sm-6"><span>Institution: '.$item->institution.'</span></div><br>';
           }
           $html.='</div>';
           $html.='<div class="row" style="margin-top: 0;">';
            if (!empty($item->course_term)){
                $html.= '<div class="col-md-6 col-xs-4 col-sm-4"><span>Course Term: '.$item->course_term.'</span></div>';
            }
           if (!empty($item->course_year)){
             $html.= '<div class="col-md-6 col-xs-4 col-sm-4"><span>Course Year: '.$item->course_year.'</span></div><br>';
             if(empty($item->course_term)){
                 $html.='<br>';
             }
           }
    $html.='</div>';
           $html.= '</p>
         </div>
      </div>
      <div class="row">
         <div class="card-content-container col-xs-12">
            <div class="card-content">
                <p>' . $content . '</p>
            </div>
         </div>
      </div>
   </div>
</div>';

    return $html;



}

function display_syllabi_sort_panel($sort_term){
	global $post_types,$search_string;

	$html = '
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="post" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />    
    <div class="col-md-12">
         <div class="panel panel-default">
            <div class="panel-heading">
               <div>
                  <span class="panel-title">Filter by:</span>
                    <button id="clear_filters2" title="Clear Filters" type="button">
                        <span class="hidden">Clear checkboxes</span>Clear
                  </button>
               </div>
            </div>
            <div class="panel-body">               
                <div style="margin-bottom: 1rem;">
                    <span class="panel-title">Course Level-Format</span>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Undergrad" class="checkbox">
                            <input class="styled" type="checkbox" name="sy-undergrad" ' . ((!empty($_POST['sy-undergrad']) || !empty($_GET['sy-undergrad'])) ? 'checked="checked" ' : '') . '
                                   id="sy-undergrad">
                            <label for="sy-undergrad">Undergraduate</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Graduate" class="checkbox">
                            <input class="styled" type="checkbox" name="sy-grad" ' . ((!empty($_POST['sy-grad']) || !empty($_GET['sy-grad'])) ? 'checked="checked" ' : '') . '
                                    id="sy-grad">
                            <label for="sy-grad">Graduate</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Intro Course" class="checkbox">
                            <input class="styled" type="checkbox" name="sy-intro-course" ' . ((!empty($_POST['sy-intro-course']) || !empty($_GET['sy-intro-course'])) ? 'checked="checked" ' : '') . '
                                    id="sy-intro-course">
                            <label for="sy-intro-course">Introductory</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Online" class="checkbox">
                            <input class="styled" type="checkbox" name="sy-online" ' . ((!empty($_POST['sy-online']) || !empty($_GET['sy-online'])) ? 'checked="checked" ' : '') . '
                                    id="sy-online">
                            <label for="sy-online">Online Only</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Online and F2F" class="checkbox">
                            <input class="styled" type="checkbox" name="sy-online-f2f" ' . ((!empty($_POST['sy-online-f2f']) || !empty($_GET['sy-online-f2f'])) ? 'checked="checked" ' : '') . '
                                    id="sy-online-f2f">
                            <label for="sy-online-f2f">Online and F2F</label>
                        </div>
                    </div>
               </div>
            </div>
         </div>
      </div>
   </form>
	';
  return $html;
}

function get_syllabi_object($id){
    global $wpdb;

    $query = $wpdb->prepare('SELECT * FROM wp_v_syllabi WHERE id = %s LIMIT 1', $id);
    $syllabus = $wpdb->get_row($query);

	 return $syllabus;
}

function search_syllabi($search_string, $subtype = array(), $sort_by = 'weight', $limit='', $offset=''){
	global $wpdb;

    $limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
    }

  if($sort_by!= 'weight'){
		$sort_direction='ASC';
	}
	else{
		$sort_direction='DESC';
	}

  $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
  $clean_string = preg_replace('/\s+/', ' ', $clean_string);

  $search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }
	// where do you want to search
	$fields = array('title', 'annotation', 'topics', 'tags');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_syllabi.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_syllabi.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);


	$sql="SELECT wp_v_syllabi.*,
	if(
		wp_v_syllabi.title = '$search_string',  100,
	         IF(
	             wp_v_syllabi.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_syllabi.annotation =  '$search_string',  40,
	        IF(
	             wp_v_syllabi.annotation LIKE '%$search_string%', 30,

	                 if( {$terms_query['annotation']}, 5, 0)

	          )


	)
	+
	IF(
	    wp_v_syllabi.topics =  '$search_string',  3,
	        IF(
	             wp_v_syllabi.topics LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
		wp_v_syllabi.tags =  '$search_string',  3,
			IF(
				 wp_v_syllabi.tags LIKE '%$search_string%', 2, 0

			  )


	) AS weight
	FROM wp_v_syllabi
	WHERE wp_v_syllabi.post_status = 'publish' ";

    if (!empty($subtype)) {

        $sql .= "AND ";
        $i = 0;

        foreach($subtype as $type) {
            if ($i > 0) {
                $sql .= "OR ";
            }
            $sql .= "wp_v_syllabi.programs LIKE '".$type."' ";
            $i++;
        }
    }

    $sql .= "HAVING weight > 0
	ORDER BY $sort_by $sort_direction".$limit_sql.$offset_sql;

  //var_dump($sql);

	$syllabi = $wpdb->get_results($sql);
	//var_dump(COUNT($syllabi));
	//
	return $syllabi;


}

function search_syllabi_total($search_string, $subtype = array()){
	global $wpdb;


  $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	// where do you want to search
	$fields = array('title', 'annotation', 'topics', 'tags');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_syllabi.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_syllabi.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);


	$sql="SELECT wp_v_syllabi.id,
	if(
		wp_v_syllabi.title = '$search_string',  100,
	         IF(
	             wp_v_syllabi.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_syllabi.annotation =  '$search_string',  40,
	        IF(
	             wp_v_syllabi.annotation LIKE '%$search_string%', 30,

	                 if( {$terms_query['annotation']}, 5, 0)

	          )


	)
	+
	IF(
	    wp_v_syllabi.topics =  '$search_string',  3,
	        IF(
	             wp_v_syllabi.topics LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
		wp_v_syllabi.tags =  '$search_string',  3,
			IF(
				 wp_v_syllabi.tags LIKE '%$search_string%', 2, 0

			  )


	) AS weight
	FROM wp_v_syllabi
	WHERE wp_v_syllabi.post_status = 'publish' ";

	if (!empty($subtype)) {

		$sql .= "AND ";
		$i = 0;

		foreach($subtype as $type) {
			if ($i > 0) {
				$sql .= "OR ";
			}
			$sql .= "wp_v_syllabi.programs LIKE '".$type."' ";
			$i++;
		}
	}

	$sql .= "HAVING weight > 0";

  //var_dump($sql);

	$syllabi = $wpdb->get_results($sql);
	//var_dump(COUNT($syllabi));
	//
	return COUNT($syllabi);


}
