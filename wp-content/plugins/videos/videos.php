<?php
/*
Plugin Name: Videos
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the Videos CPTs.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/

// Register Custom Post Type
function video() {

	$labels = array(
		'name'                  => _x( 'Videos', 'Post Type General Name', 'video' ),
		'singular_name'         => _x( 'Videos', 'Post Type Singular Name', 'video' ),
		'menu_name'             => __( 'Videos', 'admin menu', 'video' ),
		'name_admin_bar'        => __( 'Videos', 'add new on admin bar', 'video' ),
		'archives'              => __( 'Item Archives', 'video' ),
		'parent_item_colon'     => __( 'Parent Website:', 'video' ),
		'all_items'             => __( 'All Videos', 'video' ),
		'add_new_item'          => __( 'Add New Video', 'video' ),
		'add_new'               => __( 'New Video', 'video' ),
		'new_item'              => __( 'View', 'video' ),
		'edit_item'             => __( 'Edit Video', 'video' ),
		'update_item'           => __( 'Update Video', 'video' ),
		'view_item'             => __( 'View Video', 'video' ),
		'search_items'          => __( 'Search Videos', 'video' ),
		'not_found'             => __( 'No Videos found', 'video' ),
		'not_found_in_trash'    => __( 'No Videos found in Trash', 'video' ),
		'featured_image'        => __( 'Featured Image', 'video' ),
		'set_featured_image'    => __( 'Set featured image', 'video' ),
		'remove_featured_image' => __( 'Remove featured image', 'video' ),
		'use_featured_image'    => __( 'Use as featured image', 'video' ),
		'insert_into_item'      => __( 'Insert into item', 'video' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'video' ),
		'items_list'            => __( 'Items list', 'video' ),
		'items_list_navigation' => __( 'Items list navigation', 'video' ),
		'filter_items_list'     => __( 'Filter items list', 'video' ),
	);
	$args = array(
		'label'                 => __( 'Videos', 'video' ),
		'description'           => __( 'Videos Post Type', 'video' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor',  'comments', ),
		'taxonomies'            => array( 'post_tag',),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'rewrite'               => array( 'slug' => '/resource/videos' ),
		'menu_icon'	            => 'dashicons-format-video',
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'video', $args );
    flush_rewrite_rules();

}
add_action( 'init', 'video');

function video_metaboxes(){
  add_meta_box("video-content-meta", "Video Content", "display_video_content_meta_box", "video", "normal", "high");
 }

function display_video_content_meta_box(){
  global $post;
  wp_nonce_field( basename( __FILE__ ), 'video_nonce' );
  $custom = get_post_custom($post->ID);
  $video_content = !empty($custom["video-content"][0])?$custom["video-content"][0]:'';
  ?>
  <div class="form-group">
	<label for="video-content" class="col-sm-2 control-label">Video Content:</label>
	<div class="col-sm-10">
	  <textarea name="video-content" id="video-content" rows="5" class="form-control"><?php echo trim($video_content); ?></textarea>
	</div>
  </div>
  <?php
}


add_action('admin_init', 'video_metaboxes');

function save_video_metaboxes( $post_id ) {


// Checks save status
$is_autosave = wp_is_post_autosave( $post_id );
$is_revision = wp_is_post_revision( $post_id );
$is_valid_nonce = ( isset( $_POST[ 'video_nonce' ] ) && wp_verify_nonce( $_POST[ 'video_nonce' ], basename( __FILE__ ) ) ) ? TRUE : FALSE;

// Exits script depending on save status
if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
    return;
}

// Checks for input and sanitizes/saves if needed
if( !empty( $_POST[ 'video-content' ] ) ) {
    update_post_meta( $post_id, 'video-content', trim($_POST[ 'video-content' ]) );
}



}

add_action( 'save_post_video', 'save_video_metaboxes' );

function add_video_admin_scripts( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'video' === $post->post_type ) {
					wp_register_style( 'video_custom_css', get_stylesheet_directory_uri() . '/css/video.css', false, '1.0.0' );
					wp_enqueue_style( 'video_custom_css' );
					wp_dequeue_script( 'autosave' );
        }
    }




}
add_action( 'admin_enqueue_scripts', 'add_video_admin_scripts', 10, 1 );


function display_excerpt_video_object($video){

    $html = '<p><a href="'.get_permalink($video->id).'">' .$video->title.'</a><br/>';


	$html .=limit_text(wp_strip_all_tags($video->video_content), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_video_object($item){
    global $load_more_data;

    $html='';

	$single_results_page=is_page_template('template-section-results-page.php') || is_page_template('template-selected-resources-page.php') || $load_more_data;
    $detail_page=is_singular() && !$single_results_page;
	global $load_more_data;

	$img='<img class="single_results_video_icon img-responsive" src="'.get_stylesheet_directory_uri().'/images/video_icon.png'.'" title="Video icon" alt="Video icon">';


    // get tags
    $terms = array_map('trim', explode('|', $item->tags));

    $tags = array();
    $tag_names = array();
    foreach($terms as $tag){
        if(!empty($tag)) {
            $term = get_term_by('name', $tag, 'post_tag');
            $tags[]='<a href="'.get_category_link( $term->term_id ).'" >'.$term->name.'</a>';
            $tag_names[]=$term->name;
        }
    }

    $tags=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$tags);

    if ($detail_page) {
        $title = $item->title;
    }else{
        $title = '<a class="" rel="bookmark" href="'.get_permalink($item->id).'">'.$item->title.'</a>';
    }

  $html = '
  <div class="panel card video-card">
   <div class="panel-body">
      <div class="row is-flex">
	  <div class="card-info col-xs-11 col-sm-11 col-md-10">
		 <h4 itemprop="headline" class="single_results_video_title video_title_play">
		    ' . $title . '
		  </h4>

		<p class="card-meta">
		 '.$img.'<span>Video</span><br>';

	   if(!empty($tags) ) {
		   $html.= '<span>Tags: '.$tags.'</span><br>';
	   }
		 $html.= '</p>
	  </div>
         <div class="card-select col-xs-1 col-sm-1 col-md-1 col-md-offset-1">';


            $html .= '<div class="card-checkbox">
                        <input value="'.$item->id.'" id="resource_'.$item->id.'" class="case" type="checkbox">
                    </div>';


        $html .= ' </div>

      </div>
	  <div class="row">
         <div class="card-content-container col-xs-12">
            <div class="video-wrapper">
              ' . do_shortcode($item->video_code). '
            </div>
         </div>
      </div>
      <div class="row">
         <div class="card-content-container col-xs-12">
            <div class="card-content">
              <p>' . $item->video_content. '</p>
            </div>
         </div>
      </div>
   </div>
  </div>';

    return $html;

}

function display_video_sort_panel($sort_term){
  $html='';
  return $html;
}

function get_video_content($content){
  $content = preg_replace('/\[(embedyt)\].*?\[\/\1\] ?/', '', $content);

  return $content;
}

function get_video_object($id){
    global $wpdb;

    $query = $wpdb->prepare('SELECT wp_v_videos.*  FROM wp_v_videos WHERE id = %s LIMIT 1', $id);
    $video = $wpdb->get_row($query);

 	return $video;
}

function search_videos($search_string, $limit='', $offset=''){
	global $wpdb;
    $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

    $terms_array = explode(' ', $clean_string);

    $stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }

    $limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
    }


	$sql="SELECT wp_v_videos.*,
	if(
		wp_v_videos.title = '$search_string',  100,
	         IF(
	             wp_v_videos.title LIKE '%$search_string%', 50, 0

	          )
	)
	+
	IF(
	    wp_v_videos.video_content =  '$search_string',  40,
	        IF(
	             wp_v_videos.video_content LIKE '%$search_string%', 30, 0
	          )


	) AS weight
	FROM wp_v_videos
	HAVING weight > 0
	ORDER BY weight DESC".$limit_sql.$offset_sql;


    //var_dump($sql);

	$videos = $wpdb->get_results($sql);
	//var_dump($syllabi);
	//
	return $videos;


}

function search_videos_total($search_string){
	global $wpdb;
    $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

    $terms_array = explode(' ', $clean_string);

    $stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	$limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
    }


	$sql="SELECT wp_v_videos.id,
	if(
		wp_v_videos.title = '$search_string',  100,
	         IF(
	             wp_v_videos.title LIKE '%$search_string%', 50, 0

	          )
	)
	+
	IF(
	    wp_v_videos.video_content =  '$search_string',  40,
	        IF(
	             wp_v_videos.video_content LIKE '%$search_string%', 30, 0
	          )


	) AS weight
	FROM wp_v_videos
	HAVING weight > 0";


    //var_dump($sql);

	$videos = $wpdb->get_results($sql);
	//var_dump($syllabi);
	//
	return COUNT($videos);


}
