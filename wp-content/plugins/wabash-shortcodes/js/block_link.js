jQuery( document ).ready(function()
{
    //Check if inside there is more links
    jQuery(".block-link").click(function(e) {
        e.preventDefault();
        var senderElement = e.target;
        if(e.target.tagName=="A")
        {
            var href=e.target;
            if(e.target.target==='')
            {
                target="self";
            }
            else
            {
                var target=e.target.target;
            }
        }
        else
        {
            var urlTarget=jQuery(this).attr('id');
            var urlTargetsplit=urlTarget.split('|');
            var href=urlTargetsplit[0];
            var target=urlTargetsplit[1];
        }
        if(target=='_blank')
        {
            window.open(href,target);
        }
        else
        {
            location.href=href;
        }
    });
});