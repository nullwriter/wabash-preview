(function () {

    tinymce.PluginManager.add('bs3_panel', function (editor, url) {
        // example of a dashicons icon labelled button with a sub menu with
        // the sub menu items being labelled with an icon as well as text
        editor.addButton('layouts_button', {
            title: 'Choose layouts',
            type: 'menubutton',
            icon: 'icon layouts-icon',
            text: 'Layouts',
            menu: [{
                text: 'Sidebar layout',
                icon: 'icon dashicons-align-left',
                value: 'Sidebar layout',
                onclick: function () {
                    editor.insertContent('<p>[row]<br />\
[column lg="9" md="9" sm="6" xs="12" ]<br />\
Text<br />\
[/column]<br />\
[column lg="3" md="3" sm="6" xs="12" ]<br />\
Text<br />\
[/column]<br />\
[/row]</p>');
                }
            },
                {
                    text: 'Portfolio layout',
                    icon: 'icon dashicons-align-left',
                    value: 'Portfolio layout',
                    onclick: function () {
                        editor.insertContent('<p>[row]<br />\
 [column lg="9" md="9" sm="6" xs="12" ]<br />\
 Text 1<br />\
 [/column]<br />\
 [column lg="3" md="3" sm="6" xs="12" ]<br />\
 Text 2<br />\
 [/column]<br />\
 [/row]</p>\
<p>[row]<br />\
 [column lg="4" md="4" sm="12" xs="12" ]<br />\
 [panel style="panel-default box"]<br />\
 [panel-header]<br />\
 <span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
 [/panel-header]<br />\
 [panel-content]<br />\
 [thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
 <p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
 <a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
 [/panel-content]<br />\
 [/panel]<br />\
 [/column]<br />\
 [column lg="4" md="4" sm="12" xs="12" ]<br />\
 [panel style="panel-default box"]<br />\
 [panel-header]<br />\
 <span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
 [/panel-header]<br />\
 [panel-content]<br />\
 [thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
 <p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
 <a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
 [/panel-content]<br />\
 [/panel]<br />\
 [/column]<br />\
 [column lg="4" md="4" sm="12" xs="12" ]<br />\
 [panel style="panel-default box"]<br />\
 [panel-header]<br />\
 <span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
 [/panel-header]<br />\
 [panel-content]<br />\
 [thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
 <p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
 <a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
 [/panel-content]<br />\
 [/panel]<br />\
 [/column]<br />\
 [/row]</p>');
                    }
                },
                {
                    text: 'Gallery layout',
                    icon: 'icon dashicons-layout',
                    value: 'Gallery layout',
                    onclick: function () {
                        editor.insertContent('<p>[row]<br />\
[column lg="4" md="4" sm="12" xs="12" ]<br />\
[panel style="panel-default box"]<br />\
[panel-header]<br />\
<span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
[/panel-header]<br />\
[panel-content]<br />\
[thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
<p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
<a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
[/panel-content]<br />\
[/panel]<br />\
[/column]<br />\
[column lg="4" md="4" sm="12" xs="12" ]<br />\
[panel style="panel-default box"]<br />\
[panel-header]<br />\
<span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
[/panel-header]<br />\
[panel-content]<br />\
[thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
<p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
<a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
[/panel-content]<br />\
[/panel]<br />\
[/column]<br />\
[column lg="4" md="4" sm="12" xs="12" ]<br />\
[panel style="panel-default box"]<br />\
[panel-header]<br />\
<span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
[/panel-header]<br />\
[panel-content]<br />\
[thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
<p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
<a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
[/panel-content]<br />\
[/panel]<br />\
[/column]<br />\
[/row]</p>');
                    }
                }]
        });


        // example of a dashicons icon labelled button with a sub menu with
        // the sub menu items being labelled with an icon as well as text
        editor.addButton('pushortcodes', {
            title: 'Choose shortcode',
            type: 'menubutton',
            icon: 'icon shortcodes-icon',
            text: 'Shortcodes',
            menu: [{
                text: 'Staff',
                icon: 'icon dashicons-admin-users',
                value: 'Staff',
                onclick: function () {
                    editor.insertContent('<p>[wb_staff2 list_type="list_all" image="no" email="email1,email2"]</p>');
                }
            },
                {
                    text: 'Also of Interest',
                    icon: 'icon dashicons-info',
                    value: 'Also of Interest',
                    onclick: function () {
                        editor.insertContent('<p>[of_interest_widget text="Also of Interest"]\
										<ul>\
										<li><a href="http://wabashcenter.wabash.edu">first ink</a></li>\
										<li><a href="http://wabashcenter.wabash.edu">second link</a></li>\
										<li><a href="http://wabashcenter.wabash.edu">third link</a></li>\
										</ul>\
										[/of_interest_widget]</p>');
                    }
                },
                {
                    text: 'Text only widget',
                    icon: 'icon dashicons-media-default',
                    value: 'Text only widget',
                    onclick: function () {
                        editor.insertContent('<p>[text_only_widget]</p><p>Your content</p><p>[/text_only_widget]</p>');
                    }
                },
                {
                    text: 'Tout button',
                    icon: 'icon dashicons-tag',
                    value: 'Tout button',
                    onclick: function () {
                        editor.insertContent('<p>[tout_button url="http://www.example.com/"]Your content[/tout_button]</p>');
                    }
                },
                {
                    text: 'image',
                    icon: 'icon dashicons-format-image',
                    value: 'image',
                    onclick: function () {
                        editor.insertContent('<p>[floating_image align="left" src="https://placeimg.com/350/218/any" alt="my picture"]</p>');
                    }
                },
                {
                    text: 'Accordion',
                    icon: 'icon dashicons-list-view',
                    value: 'Accordion Spoiler',
                    onclick: function () {
                        editor.insertContent('<p>[accordion]<br />[su_spoiler title="Spoiler title 1" open="yes"] Spoiler content 1[/su_spoiler]<br />\
[su_spoiler title="Spoiler title 2"] Spoiler content 2[/su_spoiler]<br />\
[su_spoiler title="Spoiler title 3"] Spoiler content 3[/su_spoiler]<br />[/accordion]</p>');
                    }
                },
                {
                    text: 'Box',
                    icon: 'icon dashicons-welcome-widgets-menus',
                    value: 'box',
                    onclick: function () {
                        editor.insertContent('[panel style="panel-default box"]<br />\
 [panel-header]<br />\
 <span style="color: #a6192e;">First-name <strong>Last-name</strong></span><br />\
 [/panel-header]<br />\
 [panel-content]<br />\
 [thumbnail target="_self" alt="some picture" src="https://placeimg.com/320/115/any"]</p>\
<p><span style="color: #A29A8B;"><strong>Director</strong></span></p>\
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\
<a href="http://wabashcenter.wabash.edu" class="pull-right">Read more</a><br />\
 [/panel-content]<br />\
 [/panel]<br />');
                    }
                },
                {
                    text: 'Events Widget',
                    icon: 'icon dashicons-calendar-alt',
                    value: 'Events Widget',
                    onclick: function () {
                        editor.insertContent('<section class="widget tribe-events-list-widget">\
   <h3 class="widgettitle widget-title">Upcoming Events &amp; Deadlines</h3>\
   <p>[ecs-list-events]</p>\
   <p class="tribe-events-widget-link">\
     <a rel="bookmark" href="http://live-wabash.pantheonsite.io/events/">View All Events</a>\
   </p>');
                    }
                },
                {
                    text: 'Custom Search Widget',
                    icon: 'icon dashicons-search',
                    value: 'Custom Search Widgett',
                    onclick: function () {
                        editor.insertContent('<p>[custom_search type="grants"]</p>');
                    }
                },
                {
                    text: 'Video',
                    icon: 'icon dashicons-format-video',
                    value: 'Video',
                    onclick: function () {
                        editor.insertContent('<p><div class="panel panel-default">\
  <div class="panel-heading">Your Video Title</div>\
      <div class="panel-body">\
  [su_youtube_advanced url="https://www.youtube.com/watch?v=tetQjpoM01o"]\
        <p>Content below video</p>\
  </div>\
</div></p>');
                    }
                },
                {
                    text: 'Recent Photos',
                    icon: 'icon dashicons-format-video',
                    value: 'Recent Photos',
                    onclick: function () {
                        editor.insertContent('<p>\
  [recent_photos  img_media_url=""]\
        </p>');
                    }
                },
                {
                    text: 'Button Browser',
                    icon: 'icon dashicons dashicons-editor-kitchensink',
                    value: 'Button Browser',
                    onclick: function () {
                        editor.insertContent('<p>\
  [custom_buttons color="#6b9132" title="test" smallTitle="small-title-test" link="" target="blank"]\
        </p>');
                    }
                },
                ,
                {
                    text: 'Staff Carousel',
                    icon: 'icon dashicons dashicons-image-flip-horizontal',
                    value: 'Staff Carousel',
                    onclick: function () {
                        editor.insertContent('<p>\
  				[staff-carousel list="all" emails=""]\
        </p>');
                    }
                }
                ,
                {
                    text: 'Custom Accordion',
                    icon: 'icon dashicons-list-view',
                    value: 'Custom Accordion',
                    onclick: function () {
                        editor.insertContent('[custom-accordion bgcolor="#a6192e" title="title" header="h2"]<ul><li>Item 1</li></ul>[/custom-accordion]');
                    }
                }
                ,
                {
                    text: 'WOR lists',
                    icon: 'icon dashicons-cloud',
                    value: 'WOR lists',
                    onclick: function () {
                        editor.insertContent('[wor-lists browse="" type="" parent-topic="" letter=""]');
                    }
                }
                ,
                {
                    text: 'Scholarship lists',
                    icon: 'icon dashicons-book-alt',
                    value: 'Scholarship lists',
                    onclick: function () {
                        editor.insertContent('[scholarship-list]');
                    }
                }
                ,
                {
                    text: 'Syllabi lists',
                    icon: 'icon dashicons-clipboard',
                    value: 'Syllabi lists',
                    onclick: function () {
                        editor.insertContent('[syllabi-lists browse="" type="" parent-topic="" letter=""]');
                    }
                }
                ,
                {
                    text: 'Grants lists',
                    icon: 'icon dashicons-welcome-learn-more',
                    value: 'Grants lists',
                    onclick: function () {
                        editor.insertContent('[grants-list]');
                    }
                }
                ,
                {
                    text: 'List Books Reviews',
                    icon: 'icon dashicons-book-alt',
                    value: 'List Books Reviews',
                    onclick: function () {
                        editor.insertContent('[book-reviews-list limit="5" orderBy="post_date" withReviews="yes"]');
                    }
                }
                ,
                {
                    text: 'Featured Video',
                    icon: 'icon dashicons-format-video',
                    value: 'Featured Video',
                    onclick: function () {
                        editor.insertContent('[featured-video width="100%"]');
                    }
                }
                ,
                {
                    text: 'Lastest Videos',
                    icon: 'icon dashicons-playlist-video',
                    value: 'Lastest Videos',
                    onclick: function () {
                        editor.insertContent('[lastest-videos limit="4" columns="2"]');
                    }
                }
                ,
                {
                    text: 'Block Links',
                    icon: 'icon dashicons-admin-links',
                    value: 'Block Links',
                    onclick: function () {
                        editor.insertContent('[block-link target="" url=""][/block-link]');
                    }
                }
                ,
                {
                    text: 'Lastest Syllabi ',
                    icon: 'icon dashicons-list-view',
                    value: 'Lastest Syllabi',
                    onclick: function () {
                        editor.insertContent('[lastest-syllabi limit="5" columns="2"]');
                    }
                }
            ]
        });
    });
})();
