(function($) {
    $(document).ready(function() {
        jQuery('.responsive').carousel({
            interval: false,
            wrap:false
        });
        //Fancy Box
        jQuery(".fancybox").fancybox({
            helpers: {
                overlay: {
                    locked: false
                }
            }
        });

        $('.responsive').slick({
            dots: false,
            infinite: true,
            speed: 200,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

    });
}(jQuery));

