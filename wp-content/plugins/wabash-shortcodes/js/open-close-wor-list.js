jQuery( document ).ready(function()
{
	jQuery( document ).ajaxStop(function() {
	  jQuery('html,body').animate({
        scrollTop: jQuery("#wor-wrapper-result").offset().top},
        'slow');
	});
	jQuery(document).on("click", ".ajax_table", function(e) 
	{ 

	
		/*jQuery('#personal').hide();
		jQuery('#subject').hide();
		jQuery('#materials').show();*/
		e.preventDefault();
		//alert(my_js_data.ajaxurl);
		
		jQuery('#wor-wrapper-result').hide();
			
		 jQuery.ajax({
        type : 'post',
        url : my_js_data.ajaxurl,
        data : {
			action:my_js_data.action,
            browse : jQuery(this).attr('href')
        },
        success : function( response ) {
            jQuery('#wor-wrapper-result').html(response);
			
			jQuery('#wor-wrapper-result').show();
			//jQuery('#websites_table').DataTable().ajax.reload();
			jQuery('#websites_table').dataTable({
                        "aaSorting":[],
                        "bSortClasses":false,
                        "asStripeClasses":['even','odd'],
                        "bSort":true,
                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 3 ]
                            }
                        ]
                    });
        }
		
    });
		
		
		
		
	
	});
	
	jQuery('#subject').show();
	
	jQuery(document).on("click", "#button-materials", function() 
	{ 
		jQuery('#personal').hide();
		jQuery('#subject').hide();
		jQuery('#materials').show();
		jQuery('#wor-wrapper-result').hide();
	
	});
	jQuery(document).on("click", "#button-subject", function() 
	{ 
		jQuery('#personal').hide();
		jQuery('#materials').hide();
		jQuery('#subject').show();
		jQuery('#wor-wrapper-result').hide();
	
	});
	jQuery(document).on("click", "#button-personal", function() 
	{ 
		jQuery('#personal').show();
		jQuery('#materials').hide();
		jQuery('#subject').hide();
		jQuery('#wor-wrapper-result').hide();
	
	});
	
});