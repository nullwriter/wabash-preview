jQuery(window).load(function() {
    //Check the parents Divs Size
function resizeText() {
    jQuery('.staff-carousel-body-container').each(function () {
        if(jQuery(this).width() < 500)
        {
            jQuery(this).find('.staff-carousel-title').css('font-size','11px');
            jQuery(this).find('.position-title').css('font-size','11px');
        }
        else
        {
            jQuery(this).find('.staff-carousel-title').css('font-size','18px');
            jQuery(this).find('.position-title').css('font-size','15px');
        }
    });
}
    jQuery(window).resize(function() {
        resizeText();
        resizeBoxAImg();
    });

function resizeBoxAImg()
{
    jQuery('.staff-carousel-body-container').each(function(){
        // Cache the minimum for IMG
        var minHeightBox = [];
        // Select and loop the elements you want to equalise
        jQuery(".staff-carousel-img", this).each(function(){

            if(jQuery(this).height()>0)
            {
                minHeightBox.push(jQuery(this).height());
            }
        });
        // Set the height of all those children to whichever was highest
        var minimunHeight=Math.min.apply(Math,minHeightBox);

        jQuery(".staff-carousel-img",this).height(minimunHeight);
    });
}
    resizeBoxAImg();
resizeText();
});
