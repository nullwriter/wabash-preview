jQuery(document).ready(function ($) {
    /*Youtube video play on click title (Video Single Results Page)*/
    $('.video_title_play').on('click', function(ev) {
        //Iframe youtube to play
        var iframeYoutubeSrc=$(this).parents().eq(1).find('iframe');
        var splitSrc=iframeYoutubeSrc.attr('src').split('&');
        var separatorVideoSrc='?';
        var iframeSrc='';
        //If there is some options in the video URL
        if(splitSrc.length>1){
            separatorVideoSrc='&';
            //If we found some autoplay, we must to remove it in order to add autoplay=1
            var index = $.inArray('autoplay=0', splitSrc);
            if (index > -1) {
                splitSrc.splice(index, 1)
            }
            iframeSrc=splitSrc.join('&');
        }
        else
        {
            iframeSrc=iframeYoutubeSrc.attr('src');
        }
        iframeYoutubeSrc.attr('src',iframeSrc+separatorVideoSrc+'autoplay=1');
        ev.preventDefault();
    });
});