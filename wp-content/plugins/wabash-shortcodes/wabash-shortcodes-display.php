<style type="text/css">
	#ad {
		float: right;
		width: 300px;
		height: 250px;
		margin: 0 20px 10px 20px;
	}
	#btn {
		background-color: #1e8cbe;
		font-size: 13px;
		width: 40%;
		margin-top: 40px;
		margin-left: auto;
		margin-right: auto;
		color: #FFF;
		text-align: center;
		padding: 10px 20px;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		cursor: pointer;
	}
	#btn:hover {
		background-color: #0074a2;
	}

	#follow-up {
		margin-top: 20px;
	}

	label {
		display: block;
		margin-top: 20px;
	}
	label input {
		margin-left: 20px;
	}
	div.resource_body {
  padding: 20px;
}
.clearfix {
  clear: both;
}
.checkbox label::before {
  background-color: #fff;
  border: 1px solid #707070;
  border-radius: 3px;
  bottom: 0;
  content: "";
  display: inline-block;
  height: 18px;
  left: 0;
  margin-left: -24px;
  position: absolute;
  top: 0;
  transition: border 0.15s ease-in-out 0s, color 0.15s ease-in-out 0s;
  width: 18px;
}
.checkbox label::after {
  color: #a6192e;
  display: inline-block;
  font-size: 15px;
  height: 28px;
  left: 0;
  margin-left: -24px;
  position: absolute;
  top: -1px;
  width: 28px;
}

.post_panel .checkbox {
  min-height: 45px;
  padding-left: 0;
}
.post_panel .checkbox > label {
  width: 90%;
}
.post_panel .resource_title {
  float: right;
  width: 100%;
}
.post_panel {
  background-color: white;
  border: 1px solid #ccc;
  float: left;
  margin: 0 10px 20px;
  width: 45%;
}
.post_panel .checkbox > label {
  margin-top: 1px;
}
.resource_title {
  font-size: 12px;
}
#adminmenu div.wp-menu-image.dashicons-tagcloud::before {
    color: #D5D421;
}
#posts-list .mix{
    display: none;
}
#posts-list .filter {
  float: none;
  margin: 0 5px;
}
.sort, .filter {
    background: white none repeat scroll 0 0;
    border-bottom: 1px solid #f2f2f2;
    border-top: 1px solid transparent;
    color: #555555;
    display: inline-block;
    vertical-align: middle;
}
.sort:hover, .filter:hover {
    color: #68b8c4;
}
.sort.active, .filter.active {
    background: #68b8c4 none repeat scroll 0 0;
    border-bottom: 0 none;
    border-top: 1px solid #03899c;
    color: white;
    font-weight: 700;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice {
	background-clip: padding-box;
  background-color: #3276b1;
  border: 1px solid #2a6395;
  color: #fff;
  cursor: default;
  line-height: 18px;
  margin: 4px 0 3px 5px;
  padding: 1px 28px 1px 8px;
  position: relative;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice__remove::before {
    color: #fff;
    content: "";
}

.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
	-moz-osx-font-smoothing: grayscale;
	display: block;
	font-family: FontAwesome;
	font-size: 15px;
	font-style: normal;
	font-weight: 400;
	line-height: 1;
	margin: 0;
	min-height: 20px;
	min-width: 21px;
	padding: 0;
	position: absolute;
	right: 3px;
	text-decoration: none !important;
	top: 3px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover {
  background: rgba(0, 0, 0, 0.3) none repeat scroll 0 0;
}
#categories .checkbox > label {
  padding: 5px 10px;
	margin-top: 15px;
}
#categories .checkbox label::before {
  top: 4px;
}
#categories .checkbox label::after {
  top: 3px;
}
.race-matters-in-the-classroom{
	 background-color: #F7FFEE;
}
.stories-from-the-front{
	 background-color: #FFFFEE;
}
.surprises-when-lecturing-less-and-teaching-more{
	 background-color: #EEF4FF;
}
.coulda-woulda-shoulda{
	 background-color: #FFF4FF;
}
.theological-school-deans{
	 background-color: #EDFCFF;
}
.sort, .filter {
  background: white none repeat scroll 0 0;
  border: 1px solid #f2f2f2;
  color: #555555;
  display: inline-block;
  padding: 5px;
  vertical-align: middle;
}
</style>

<h2>Wabash Shortcodes</h2>

<div>

	<p>
		Wabash Shortcodes Documentation.
	</p>
	<p>
		Coming Soon.
	</p>
</div>
