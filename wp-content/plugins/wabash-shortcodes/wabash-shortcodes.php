<?php

/*
Plugin Name: Wabash-Shortcodes
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for Wabash shortcodes.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/

class GWP_bs3_panel_shortcode
{
    /**
     * $shortcode_tag
     * holds the name of the shortcode tag
     * @var string
     */
    public $shortcode_tag = 'bs3_panel';

    /**
     * __construct
     * class constructor will set the needed filter and action hooks
     *
     * @param array $args
     */
    function __construct($args = array())
    {
        //add shortcode
        add_shortcode($this->shortcode_tag, array($this, 'shortcode_handler'));
        add_shortcode('bootstrap_col', array($this, 'bootstrap_col_shortcode_handler'));
        add_shortcode('widget', array($this, 'widget'));
        add_shortcode('wb_staff', array($this, 'wabash_staff_shortcode_handler'));
        add_shortcode('text_only_widget', array($this, 'text_only_widget_shortcode_handler'));
        add_shortcode('of_interest_widget', array($this, 'of_interest_widget_shortcode_handler'));
        add_shortcode('accordion', array($this, 'accordion'));
        add_shortcode('tout_button', array($this, 'tout_button_shortcode_handler'));
        add_shortcode('custom_search', array($this, 'custom_search_shortcode_handler'));
        add_shortcode('floating_image', array($this, 'floating_image_shortcode_handler'));
        add_shortcode('recent_photos', array($this, 'recent_photos_shortcode_handler'));
        add_shortcode('custom_buttons', array($this, 'custom_buttons'));
        //add_shortcode('wor-list', array($this, 'lists_websites_on_religion'));
        //add_shortcode('wor-wrapper', array($this, 'wrapper_result_website_on_religion'));
        add_shortcode('wb_staff2', array($this, 'wabash_staff2'));
        add_shortcode('staff-carousel', array($this, 'staff_carousel'));
        add_shortcode('custom-accordion', array($this, 'custom_accordion'));
        add_shortcode('wor-lists', array($this, 'custom_lists_websites_on_religion'));
        add_shortcode('scholarship-list', array($this, 'custom_lists_scholarship'));
        add_shortcode('syllabi-lists', array($this, 'custom_lists_syllabi'));
        add_shortcode('grants-list', array($this, 'custom_lists_grants'));
        add_shortcode('book-reviews-list', array($this, 'book_reviews_list'));
        add_shortcode('featured-video', array($this, 'feature_videoCPT'));
        add_shortcode('lastest-videos', array($this, 'lastestVideos'));
        add_shortcode('block-link', array($this, 'block_link'));
        add_shortcode('lastest-syllabi', array($this, 'lastestSyllabi'));

        if (is_admin()) {
            // We want it called within the header so we use admin_head
            add_action('admin_head', array($this, 'admin_head'));
            add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
            add_action('admin_footer', array($this, 'pu_get_shortcodes'));
        }
        // Define the menu element
        add_action('admin_menu', array($this, 'my_wabash_shortcodes_plugin_menu'));
    }

    /**
     * shortcode_handler
     * @param  array $atts shortcode attributes
     * @return string
     * @internal param string $content shortcode content
     */


    function accordion($atts, $content = null)
    {
        //To check if script is already enqueued
        $handle = 'other-shortcodes';
        $list = 'enqueued';
        if (!wp_script_is($handle, $list)) {
            wp_register_script('other-shortcodes', plugins_url() . '/shortcodes-ultimate/assets/js/other-shortcodes.js');
            wp_enqueue_script('other-shortcodes');
            wp_register_style('box-shortcodes', plugins_url() . '/shortcodes-ultimate/assets/css/box-shortcodes.css', array(), '1', 'all');
            wp_enqueue_style('box-shortcodes');
        }
        $handle = 'box-shortcodes';
        $list = 'enqueued';
        if (!wp_script_is($handle, $list)) {
            wp_register_style('box-shortcods', plugins_url() . '/shortcodes-ultimate/assets/css/box-shortcodes.css', array(), '1', 'all');
            wp_enqueue_style('box-shortcodes');
        }
        $output = '<div class="su-accordion">';
        $finalContent=str_replace('[su_spoiler','[su_spoiler style="fancy"',$content);
        $output .= do_shortcode($finalContent);
        $output .= '</div>';
        return $output;
    }
    function recent_photos_shortcode_handler($atts)
    {
        global $wpdb;
        $this->carousel_scripts();
        $table_name = $wpdb->prefix . "posts";
        /* wpdb class should not be called directly.global $wpdb variable is an instantiation of the class already set up to talk to the WordPress database */
        if (!isset($atts['img_media_url']) || $atts['img_media_url'] == '') {
            $sql = "SELECT * FROM " . $table_name . " WHERE post_type='attachment' ORDER BY 'post_date' DESC LIMIT 20";
            $result = $wpdb->get_results($sql);
        } else {
            $imgsUrlComma = explode(',', $atts['img_media_url']);
        }

        $maxwidth = 120;
        $maxheight = 80;
        $CarouselClass=uniqid();
        /* If you require you may print and view the contents of $result object */
        $output = '<div class="carouselRecentPhotos ' . $CarouselClass. '">
	        <div class="responsive slider" style="">';
        $index = 1;
        $output .= '<div class="item active">';
        if (isset($atts['img_media_url']) && $atts['img_media_url'] != '') {
            foreach ($imgsUrlComma as $img) {
                //GET THE ID BY IMG URL
                $ImgId=$this->get_attachment_id($img);


                //GET THE IMG AND SIZES
                $imgSmall=wp_get_attachment_image_src($ImgId);
                $imgLarge=wp_get_attachment_image_src($ImgId,'large');


                if ($index == 2) {
                    $index = 1;
                    $output .= '</div>';
                    $output .= '<div class="item">';
                }

                $output .= '<a class="fancybox" href="' . $imgLarge[0] . '" >
                                <img class="img-responsive img-thumbnail recent_photos_small" src="'.$imgLarge[0].'" title=""/>
                            </a>';

                ++$index;
            }
        } else {
            foreach ($result as $row) {
                //GET THE ID BY IMG URL
                $ImgId=$this->get_attachment_id($row->guid);
                //GET THE IMG AND SIZES
                $imgSmall=wp_get_attachment_image_src($ImgId);
                $imgLarge=wp_get_attachment_image_src($ImgId,'large');


                if ($index == 2) {
                    $index = 1;
                    $output .= '</div>';
                    $output .= '<div class="item">';
                }

                $output .= '<a class="fancybox" href="' . $imgLarge[0] . '" >
                                <img class="img-responsive img-thumbnail recent_photos_small" src="'.$imgLarge[0].'" title=""/>
                            </a>';

                ++$index;
            }
        }
        $output .= '</div></div>
        </div>';
        //return shortcode output
        return $output;
    }

    /**
     * floating_image_shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function floating_image_shortcode_handler($atts)
    {
        //Attributes
        $align = '';
        extract(shortcode_atts(array('align' => 'left', 'alt' => 'some picture', 'src' => 'https://placeimg.com/350/218/any',), $atts));
        //start html
        $output = '<img class="img-responsive';
        if ($align == 'left') {
            $output .= ' pull-left"';
        }
        if ($align == 'right') {
            $output .= ' pull-right"';
        }
        $output .= ' alt="' . $alt . '" src="' . $src . '">';
        //add closing div tag
        return $output;
    }

    /**
     * custom_search_shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function custom_search_shortcode_handler($atts, $content = null)
    {
        /*Check if parameters are empty*/
        if (empty($atts['type'])) {
            $error = "The parameter <strong>Type</strong> can not be empty or not exist (<i>Custom Search Widget Shortcode</i>)";
            return $error;
        }
        /*Count how many CPTs come into the type parameters 1 mean single result search page
        and more general search in order to search in the selected CPTs*/
        $howCPTs = explode(',', $atts['type']);
        $totalCPTs = count($howCPTs);
        $cptToSearch = '';
        $subTypesToSearch = '';
        if (!empty($atts['type'])) {

            if ($totalCPTs == 1) {
                preg_match_all("/\{(.+)\}/", $atts['type'], $subTypesFound);
                if (isset($subTypesFound[1][0])) {
                    $subtypesExplodes = explode('|', $subTypesFound[1][0]);
                    if (isset($subTypesFound[1]) && $subTypesFound[1] != '') {
                        foreach ($subtypesExplodes as $subtype) {
                            $subTypesToSearch .= '<input type="hidden" value="on" name="' . $subtype . '">';
                        }
                    }
                }
                $pt = explode('{', $atts['type']);
                $pt = $pt[0];
                $cptToSearch = '<input type="hidden" value="' . $pt . '" name="pt">';
                $url = get_site_url() . '/single-results/';
                $inputName = 'st';
            } else {
                foreach ($howCPTs as $cpt) {
                    //SUBTYPES FOREACH CPT
                    preg_match_all("/\{(.+)\}/", $cpt, $subTypesFound);
                    if (isset($subTypesFound[1][0])) {
                        $subtypesExplodes = explode('|', $subTypesFound[1][0]);
                        if (isset($subTypesFound[1]) && $subTypesFound[1] != '') {
                            foreach ($subtypesExplodes as $subtype) {
                                $subTypesToSearch .= '<input type="hidden" value="on" name="' . $subtype . '">';
                            }
                        }
                    }
                    $cptExplode = explode('{', $cpt);
                    $finalCptName = $cptExplode[0];
                    $cptToSearch .= '<input type="hidden" value="' . $finalCptName . '" name="post-type[]">';
                    $url = get_site_url();
                    $inputName = 's';
                }
            }
            $finalText = array();
            foreach ($howCPTs as $cpt) {
                $pt = explode('{', $cpt);
                $textExplode = explode('_', $pt[0]);
                $TextUc = array();

                foreach ($textExplode as $text) {
                    $TextUc[] = ucwords($text);
                }
                //IF SUBTYPES
                /*if(isset($subTypesFound[1]) && $subTypesFound[1]!='' )
                {
                    $subTypesText=array();
                    foreach($subtypesExplodes as $subtype)
                    {
                        $subTypesToSearch.= '<input type="hidden" value="on" name="'.$subtype.'">';
                        $subTypesText[]=$subtype;
                    }
                    $subTypesText='('.implode(',',$subTypesText).')';
                }
                $finalText[] = implode(' ', $TextUc).$subTypesText;*/
                $finalText[] = implode(' ', $TextUc);
            }
            $text = implode(', ', $finalText);
        }

        //Start html
        if (!empty($atts['text'])){
            $text = $atts['text'];
        }else{
            $text = 'Search within '.$text;
        }
        $output = '<form itemprop="potentialAction" itemscope="" itemtype="http://schema.org/SearchAction" style="margin: 20px 0 20px 0px;" class="form-inline" id="custom_search_form"
             name="search_form" action="' . $url . '" method="'.($totalCPTs>1?'get':'post').'">
             ' . $cptToSearch . '
             ' . $subTypesToSearch . '
        <input type="search" class="form-control" itemprop="query-input" placeholder=" ' . $text . '" value="" id="searchform-777" name="' . $inputName . '">
        <button class="btn custom-search-btn" type="submit">
                                                                             <i class="fa fa-search"></i>
                                                                     </button>
                                                             </span>';

        $output .= '</form>';
        //add closing div tag
        //return shortcode output
        return $output;
    }

    /**
     * shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function tout_button_shortcode_handler($atts, $content = null)
    {
        // Attributes
        $url = '';
        extract(shortcode_atts(array('url' => '#'), $atts));
        //start html
        $output = '<a href="' . $url . '" class="btn btn-tout"><i class="fa fa-share"></i>' . trim(do_shortcode($content)) . '</a>';
        //add closing div tag
        //return shortcode output
        return $output;
    }

    /**
     * shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function of_interest_widget_shortcode_handler($atts, $content = null)
    {
        // Attributes
        $text = '';
        extract(shortcode_atts(array('text' => 'Also of Interest'), $atts));
        //start html
        $output = <<<MARKUP
	 <div class="panel panel-default of-interest">
			<div class="panel-heading red-wabash" style="color: #fff;">
				 <i class="fa fa-info-circle" style="margin-right: 7px; font-size: 20px; position: relative; top: 2px;"></i><strong>
MARKUP;
        $output .= $text . '</strong>
			</div>';

        $output .= '<div class="panel-body">' . trim(do_shortcode($content)) . '</div>';
        //add closing div tag
        $output .= '</div>';
        //return shortcode output
        return $output;
    }

    /**
     * shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function text_only_widget_shortcode_handler($atts, $content = null)
    {
        // Attributes
        extract(shortcode_atts(array('header' => 'no', 'footer' => 'no', 'type' => 'default',), $atts));
        //make sure the panel type is a valid styled type if not revert to default
        $panel_types = array('primary', 'success', 'info', 'warning', 'danger', 'default');
        $type = in_array($type, $panel_types) ? $type : 'default';
        //start panel markup
        $output = '<div class="panel panel-' . $type . '">';
        //check if panel has a header
        if ('no' != $header)
            $output .= '<div class="panel-heading">' . $header . '</div>';
        //add panel body content and allow shortcode in it
        $output .= '<div class="panel-body">' . trim(do_shortcode($content)) . '</div>';
        //check if panel has a footer
        if ('no' != $footer)
            $output .= '<div class="panel-footer">' . $footer . '</div>';
        //add closing div tag
        $output .= '</div>';
        //return shortcode output
        return $output;
    }

    /**
     * shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function wabash_staff_shortcode_handler($atts, $content = null)
    {
        global $wpdb;
        /* wpdb class should not be called directly.global $wpdb variable is an instantiation of the class already set up to talk to the WordPress database */
        $result = $wpdb->get_results("SELECT * FROM wp_wabash_staff "); /*mulitple row results can be pulled from the database with get_results function and outputs an object which is stored in $result */
        /* If you require you may print and view the contents of $result object */
        $output = <<<MARKUP
        <div id="accordion">
MARKUP;
        foreach ($result as $row) {
            $output .= '<div class="staff">
                                <div>
                                   <i class="fa fa-caret-down"></i>
                                   <a aria-controls="collapse' . $row->id . '" data-parent="#accordion" data-toggle="collapse" href="#collapse' . $row->id . '">';
            $output .= $row->firstname . "  " . $row->lastname;
            $output .= ' </a>
                                 </div>
                                 <div class="collapse" id="collapse' . $row->id . '">
                                    <p>';
            $output .= '<strong>' . $row->position . '</strong><br/>
                                    ' . $row->phone . '<br/>
                               ' . $row->email . '
                                        </p>';
            $output .= '</div>
                              </div>';

        }
        $output .= <<<MARKUP
        </div>
MARKUP;
        //return shortcode output
        return $output;
    }

    /**
     * shortcode_handler
     * @param  array $atts shortcode attributes
     * @param  string $content shortcode content
     * @return string
     */
    function shortcode_handler($atts, $content = null)
    {
        //Attributes
        extract(shortcode_atts(array('header' => 'no', 'footer' => 'no', 'type' => 'default'), $atts));
        //Make sure the panel type is a valid styled type if not revert to default
        $panel_types = array('primary', 'success', 'info', 'warning', 'danger', 'default');
        $type = in_array($type, $panel_types) ? $type : 'default';
        //start panel markup
        $output = '<div class="panel panel-' . $type . '">';
        //check if panel has a header
        if ('no' != $header)
            $output .= '<div class="panel-heading">' . $header . '</div>';
        //add panel body content and allow shortcode in it
        $output .= '<div class="panel-body">' . trim(do_shortcode($content)) . '</div>';
        //check if panel has a footer
        if ('no' != $footer)
            $output .= '<div class="panel-footer">' . $footer . '</div>';
        //add closing div tag
        $output .= '</div>';
        //return shortcode output
        return $output;
    }

    function bootstrap_col_shortcode_handler($atts, $content = null)
    {
        // Attributes
        extract(shortcode_atts(array('class' => 'col-sm-1'), $atts));
        $result = '<div class="' . $class . '">';
        $result .= do_shortcode($content);
        $result .= '</div>';
        return force_balance_tags($result);
    }

    function widget($atts)
    {
        global $wp_widget_factory;
        extract(shortcode_atts(array('widget_name' => FALSE), $atts));
        $widget_name = wp_specialchars(strip_tags($widget_name));
        $instance = array();
        $id = 2;
        ob_start();
        the_widget($widget_name, $instance, array('widget_id' => 'arbitrary-instance-' . $id,
            'before_widget' => '<div class="widget">',
            'after_widget' => '</div>',
            'before_title' => '',
            'after_title' => ''
        ));
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    function my_wabash_shortcodes_plugin_menu()
    {
        add_menu_page('Wabash Shortcodes Options', 'Wabash Shortcodes', 'manage_options', 'wabash-shortcodes', array($this, 'wabash_shortcodes_options'), 'dashicons-tagcloud');
    }

    function wabash_shortcodes_options()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }
        include 'wabash-shortcodes-display.php';
    }

    function admin_enqueue_scripts()
    {
        wp_enqueue_style('bs3_panel_shortcode', plugins_url('css/mce-button.css', __FILE__));
    }

    function pu_get_shortcodes()
    {
        global $shortcode_tags;
        echo '<script type="text/javascript">
        var shortcodes_button = new Array();';
        $count = 0;
        foreach ($shortcode_tags as $tag => $code) {
            echo "shortcodes_button[{$count}] = '{$tag}';";
            $count++;
        }
        echo '</script>';
    }

    function custom_buttons($atts, $content = null)
    {
        $handle = 'content-shortcodes.css';
        $list = 'enqueued';
        if (wp_script_is($handle, $list)) {
            return;
        } else {
            wp_register_style('custom_buttons', plugins_url() . '/shortcodes-ultimate/assets/css/content-shortcodes.css', array(), '1', 'all');
            wp_enqueue_style('custom_buttons');
        }
        $url = "javascript:void(0)";
        if (isset($atts['link'])) {
            $url = $atts['link'];
        }
        if (isset($atts['target'])) {
            $target = $atts['target'];
        } else {
            $target = "_self";
        }
        $output = '
	 <div class="su-button-center"><a target="' . $target . '" href="' . $url . '" class="su-button su-button-style-default su-button-wide" style="color:#FFFFFF;background-color:' . $atts['color'] . ';border-color:#000;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px">
	 <span style="color:#FFFFFF;padding:0px 16px;font-size:13px;line-height:26px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;text-shadow:0px 0px 0px #FFF;-moz-text-shadow:0px 0px 0px #FFF;-webkit-text-shadow:0px 0px 0px #FFF"> ' . $atts["title"] . '<small style="padding-bottom:6px;color:#FFFFFF">' . $atts["smalltitle"] . ' </small></span></a></div>';
        return $output;
    }

    function carousel_scripts()
    {
        //Load JS and CSS files in here
        wp_register_script('fancy_box_script', plugins_url().'/wabash-shortcodes/js/jquery.fancybox.pack.js', array('jquery'), '', true);
        wp_enqueue_script('fancy_box_script');
        wp_register_style('carousel_style', plugins_url() . '/wabash-shortcodes/css/carousel-recent-photos.css', array(), '1', 'all');
        wp_enqueue_style('carousel_style');
        wp_register_style('fancy_box_style', plugins_url() . '/wabash-shortcodes/css/fancybox/jquery.fancybox.css', array(), '1', 'all');
        wp_enqueue_style('fancy_box_style');
        wp_register_script('carousel_script', plugins_url() . '/wabash-shortcodes/js/my-carousel-script.js', array('jquery'), '1', true);
        wp_enqueue_script('carousel_script');
        //Slick carousel setup
        wp_register_style('slick_style', plugins_url() . '/wabash-shortcodes/css/slick.css', array(), '1', 'all');
        wp_enqueue_style('slick_style');
        wp_register_style('slick_theme_style', plugins_url() . '/wabash-shortcodes/css/slick-theme.css', array(), '1', 'all');
        wp_enqueue_style('slick_theme_style');
        wp_register_script('slick_script', plugins_url() . '/wabash-shortcodes/js/slick.js', array('jquery'), '1', true);
        wp_enqueue_script('slick_script');
    }
    function website_all_scriptsandstyles()
    {
        //Load JS and CSS files in here
        wp_register_script('website_custom_js', get_stylesheet_directory_uri() . '/js/websites/custom.js', array('jquery'), '1', true);
        wp_register_script('other-shortcodes', plugins_url() . '/shortcodes-ultimate/assets/js/other-shortcodes.js', array('jquery'), '1', true);
        wp_register_script('website_religion_js2', plugins_url() . '/wabash-shortcodes/js/open-close-wor-list.js', array('jquery'), '1', true);
        wp_register_style('website_religion_css', plugins_url() . '/shortcodes-ultimate/assets/css/content-shortcodes.css', array(), '1', 'all');
        wp_register_style('box-shortcodes', plugins_url() . '/shortcodes-ultimate/assets/css/box-shortcodes.css', array(), '1', 'all');
        wp_register_style('website_custom_css', get_stylesheet_directory_uri() . '/css/shortcodes.css', array(), '1', 'all');
        wp_register_script('website_religion_datatable-js', get_stylesheet_directory_uri() . '/js/jquery.dataTables.min.js', array('jquery'), '1', true);
        wp_register_style('website_religion_datatable-css', get_stylesheet_directory_uri() . '/css/jquery.dataTables.min.css', array(), '1', 'all');
        wp_enqueue_script('other-shortcodes');
        wp_enqueue_script('website_religion_js2');
        wp_enqueue_script('website_religion_datatable-js');
        wp_enqueue_style('website_religion_css');
        wp_enqueue_style('box-shortcodes');
        wp_enqueue_style('website_religion_datatable-css');
    }
    //function case: Material Type
    function material_type($browse,$letter){
        global $wpdb; // for sql queries
        $output = '';
        $sql = "
                    select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where terms.name='" . $browse . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $parent_term = $wpdb->get_results($sql); //Get Material Type
        $sql_final = "select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $parent_term[0]->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $list = $wpdb->get_results($sql_final); //Get Material Type childrens
        if (!empty($list)) {
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/
                if ($topic->name == '') {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'website-topic');
                $output .= '<li><a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case: Subject Headings
    function subject_headings($browse,$letter){
        global $wpdb; // for sql queries
        $output = '';
        $sql = "select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where terms.name='" . $browse . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $parent_term = $wpdb->get_results($sql); //get Subject Hedings object
        $sql_final = "select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $parent_term[0]->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $parents = $wpdb->get_results($sql_final); //get Subject Hedings childrens
        foreach ($parents as $key => $value) {
            $sql_aux = "select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $value->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
            $result_aux = $wpdb->get_results($sql_aux);
            $list[$key][] = $result_aux;
        }
        $i = 0; //while indexes
        $j = 0;
        if (!empty($list)) {
            $output = '<ul>';
            while ($i < count($list)) {
                $output .= '<li>' . $parents[$i]->name;
                $output .= '<ul>';
                while ($j < count($list[$i][0])) {
                    $term = get_term_by('name', $list[$i][0][$j]->name, 'website-topic');
                    $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                    //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $list[$i][0][$j]->name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $list[$i][0][$j]->name . '</a></li>';
                    $j += 1;
                }
                $output .= '</ul>';
                $output .= '</li>';
                $j = 0;
                $i += 1;
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case: Subject Headings + parent-topic
    function subject_headings_parent_topic($browse,$parent_topic,$letter){
        global $wpdb; // for sql queries
        $output = '';
        $sql = "
                        select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where terms.name='" . $browse . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";

        $parent_term = $wpdb->get_results($sql);
        $sql2 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term[0]->term_id . " and term_taxonomy.term_id=term.term_id and name='" . $parent_topic . "'";
        $parent_term2 = $wpdb->get_results($sql2);
        $sql3 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term2[0]->term_id . " and term_taxonomy.term_id=term.term_id ";
        $list = $wpdb->get_results($sql3);
        if (!empty($list)) {
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/
                if ($topic->name == '') {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'website-topic');
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $name . '</a></li>';
            }
            $output .= '</ul>';
        }

        return $output;
    }

    //function case: Subject Headings + letter
    function subject_headings_letter ($browse,$letter){
        global $wpdb; // for sql queries
        $output = '';
        $sql = "
                          select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where terms.name='" . $browse . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $parent_term = $wpdb->get_results($sql); //get Subject Hedings object
        $sql_final = "
                          select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $parent_term[0]->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $parents = $wpdb->get_results($sql_final); //get Subject Hedings childrens
        $letterSyllabi = '(name like "' . $letter . '%" OR name like "\\"' . $letter . '%") AND ';
        //$sql4 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where " . $letterSyllabi . " parent=" . $parent_term2[0]->term_id . " and term_taxonomy.term_id=term.term_id " . $orderSyllabi . " and count>0";
        foreach ($parents as $key => $value) {
            $sql_aux = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where " . $letterSyllabi . " parent=" . $value->term_id . " and term_taxonomy.term_id=term.term_id  and count>0";;
            $result_aux = $wpdb->get_results($sql_aux);
            $list[$key][] = $result_aux;
        }
        $i = 0;
        $j = 0;
        if (!empty($list)) {
            $output = '<ul>';
            while ($i < count($list)) {
                //$output .= '<li>'.$parents[$i]->name;
                //$output .= '<ul>';
                while ($j < count($list[$i][0])) {
                    $term = get_term_by('name', $list[$i][0][$j]->name, 'website-topic');
                    $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                    //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $list[$i][0][$j]->name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $list[$i][0][$j]->name . '</a></li>';
                    $j += 1;
                }
                //$output .= '</ul>';
                //$output .= '</li>';
                $j = 0;
                $i += 1;
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case: Subject Headings + type=alphabetic

    function subject_headings_alphabetic($browse,$letter){
        $output = '';
        $main_term = get_term_by('name', $browse, 'website-topic');
        $childrens = get_term_children($main_term->term_id, 'website-topic');
        $list = array();
        foreach ($childrens as $termchild) {
            $valuesChilds = get_term_by('id', $termchild, 'website-topic');
            $list[] = $valuesChilds->name;
        }
        natcasesort($list);
        if (!empty($list)) {
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/

                if (empty($topic->name)) {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'website-topic');
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $name . '</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case Personal Subject Headings
    function personal_subject_headings($browse,$letter){
        global $wpdb; // for sql queries
        $output = '';
        $main_term = get_term_by('name', $browse, 'website-topic');
        $parent = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $main_term->term_id . " and term_taxonomy.term_id=term.term_id ";
        $parent_term = $wpdb->get_results($parent);
        $parents = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term[0]->term_id . " and term_taxonomy.term_id=term.term_id ";
        $parent_chrono = $wpdb->get_results($parents);
        foreach ($parent_chrono as $key => $value) {
            $sql_aux = "
                              select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $value->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
            $result_aux = $wpdb->get_results($sql_aux);

            $list[$key][] = $result_aux;
        }
        $i = 0; //while indexes
        $j = 0;
        if (!empty($list)) {
            $output = '<ul>';
            while ($i < count($list)) {
                $output .= '<li>' . $parent_chrono[$i]->name;
                $output .= '<ul>';
                while ($j < count($list[$i][0])) {
                    $term = get_term_by('name', $list[$i][0][$j]->name, 'website-topic');
                    $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                    //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $list[$i][0][$j]->name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $list[$i][0][$j]->name . '</a></li>';
                    $j += 1;
                }
                $output .= '</ul>';
                $output .= '</li>';
                $j = 0;
                $i += 1;
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case: Personal SUbject Headings + type=Chronological List
    function personal_subject_headigns_chrono($letter){
        global $wpdb; // for sql queries
        $output = '';
        $to_1400 = get_term_by('name', 'to 1400', 'website-topic'); // First
        $born_1401 = get_term_by('name', 'born 1401-1865', 'website-topic'); // Second
        $born_1866 = get_term_by('name', 'born 1866-1900', 'website-topic'); //Third
        $born_1901 = get_term_by('name', 'born 1901-', 'website-topic'); //Last
        // to - 1400
        $sql_aux = "
                              select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $to_1400->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $to_1400_childrens = $wpdb->get_results($sql_aux);
        $list[] = $to_1400_childrens;

        // born 1401-1865
        $sql_aux = "
                              select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $born_1401->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $born_1401_childrens = $wpdb->get_results($sql_aux);
        $list[] = $born_1401_childrens;

        // born 1866-1900
        $sql_aux = "
                              select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $born_1866->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $born_1866_childrens = $wpdb->get_results($sql_aux);
        $list[] = $born_1866_childrens;

        // born 1901-
        $sql_aux = "
                              select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where parent='" . $born_1901->term_id . "' and term_taxonomy.taxonomy='website-topic' and term_taxonomy.term_id=terms.term_id";
        $born_1901_childrens = $wpdb->get_results($sql_aux);
        $list[] = $born_1901_childrens;

        $i = 0; //while indexes
        $j = 0;
        if (!empty($list)) {
            $output = '<ul>';
            while ($i < count($list)) {
                while ($j < count($list[$i])) {
                    $term = get_term_by('name', $list[$i][$j]->name, 'website-topic');
                    $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                    //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $list[$i][$j]->name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $list[$i][$j]->name . '</a></li>';
                    $j += 1;
                }
                $j = 0;
                $i += 1;
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case: Personal Subject Headings + type=alphabetic
    function personal_subject_headings_alphabetic($browse,$letter){
        $output ='';
        $main_term = get_term_by('name', $browse, 'website-topic');
        $childrens = get_term_children($main_term->term_id, 'website-topic');
        $list = array();
        foreach ($childrens as $termchild) {
            $valuesChilds = get_term_by('id', $termchild, 'website-topic');
            if ($valuesChilds->parent != $main_term->term_id) {
                $list[] = $valuesChilds->name;
            }
        }
        natcasesort($list);
        if (!empty($list)) {
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/
                if (empty($topic->name)) {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'website-topic');
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $name . '</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function case: Personal Sunject Headings + letter
    function personal_subject_headings_letter($browse,$letter){
        $output = '';
        $main_term = get_term_by('name', $browse, 'website-topic');
        $childrens = get_term_children($main_term->term_id, 'website-topic');
        $list = array();
        foreach ($childrens as $termchild) {
            $valuesChilds = get_term_by('id', $termchild, 'website-topic');
            if ($valuesChilds->parent != $main_term->term_id) {
                $fstletter = substr($valuesChilds->name, 0, 1);
                if ($fstletter == $letter) {
                    $list[] = $valuesChilds->name;
                }
            }
        }
        if (!empty($list)) {
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/
                if (empty($topic->name)) {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'website-topic');
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $name . '</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function Personal Subject Headings + parent-topic
    function personal_subject_headings_parent_topic($parent_topic,$letter){
        $output = '';
        $main_term = get_term_by('name', $parent_topic, 'website-topic');
        $childrens = get_term_children($main_term->term_id, 'website-topic');
        $list = array();
        foreach ($childrens as $termchild) {
            $valuesChilds = get_term_by('id', $termchild, 'website-topic');
            $list[] = $valuesChilds->name;
        }
        if (!empty($list)) {
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/
                if (empty($topic->name)) {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'website-topic');
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
                //$output .= '<li><a style="color:black!important" href="single-results/?st=topic:' . $name . '&pt=website_on_religion&orderSy=' . $order . '&letter=' . $letter . '">' . $name . '</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    //function for each
    function check_browse_cases ($case,$browse,$letter,$parent_topic){
        $output =''; //final html output
        switch ($case){

            case 'Material Types':
                $output = $this->material_type($browse,$letter,$parent_topic);
                break;

            case 'invalid Material Types':
                $output = '<p style="color: red"> For this browse option (Material Types) you should not put parameters besides browse</p>';
                break;

            case 'Subject Headings':
                $output = $this->subject_headings($browse,$letter);
                break;

            case 'Subject Headigns + parent-topic':
                $output = $this->subject_headings_parent_topic($browse,$parent_topic,$letter);
                break;

            case 'Subject Headigns + letter':
                $output = $this->subject_headings_letter ($browse,$letter);
                break;

            case 'invalid type + letter':
                $output = '<p style="color: red"> choose either letter parameter or type parameter, but not both</p>';
                break;

            case 'invalid: parent-topic + parameter (type or letter)':
                $output = '<p style="color: red"> Wrong combination, you should not put parent-topic with any other parameter</p>';
                break;

            case 'Subject Headings invalid type != alphabetic':
                $output = '<p style="color: red"> type must have the value of "alphabetic"  for this browse option (Subject Headings)</p>';
                break;

            case 'Subject Headings + type=alphabetic':
               $output = $this->subject_headings_alphabetic($browse,$letter);
                break;

            case 'Personal Subject Headings':
                $output = $this->personal_subject_headings($browse,$letter);
                break;

            case 'Personal Subject Headings + type=chrono':
                $output = $this->personal_subject_headigns_chrono($letter);
                break;

            case 'Personal Subject Headings + type=alphabetic':
                $output = $this->personal_subject_headings_alphabetic($browse,$letter);
                break;

            case 'Personal Subject Headings + letter':
                $output = $this->personal_subject_headings_letter($browse,$letter);
                break;

            case 'Personal Subject Headings + parent-topic':
                $output = $this->personal_subject_headings_parent_topic($parent_topic,$letter);
                break;

        }

        return $output;
    }

    //check Subject Headings cases
    function check_subject_cases ($parent_topic,$type,$letter){
        $case = '';
        if (empty($parent_topic) && empty($type) && empty($letter)) {
            $case = 'Subject Headings';
        }
        //case =  Subject Headigns + parent-topic
        if (!empty($parent_topic) && empty($type) && empty($letter)) {
            $case = 'Subject Headigns + parent-topic';
        }
        if (empty($parent_topic) && (!empty($type) || !empty($letter))) {
            //case invalid type + letter
            if (!empty($letter) && !empty($type)) {
                $case = 'invalid type + letter';
            }
            //case =  Subject Headigns + letter
            if (!empty($letter) && empty($type)) {
                $case = 'Subject Headigns + letter';
            }
            //case invalid type != alphabetic
            if ( !empty($type) && 'alphabetic' != $type) {
                $case = 'Subject Headings invalid type != alphabetic';
            }
            //case Subject Headings + type=alphabetic
            if (!empty($type) && empty($letter) && $type == 'alphabetic') {
                $case = 'Subject Headings + type=alphabetic';

            }
        }
        //invalid case parent-topic + parameter (type or letter)
        if (!empty($parent_topic) && (!empty($type) || !empty($letter))) {
            $case = 'invalid: parent-topic + parameter (type or letter)';
        }
        return $case;
    }

    //check Personal Subject Headings cases
    function check_personal_subject_cases ($parent_topic,$type,$letter){
        $case = '';
        //Case browse
        if (empty($parent_topic) && empty($type) && empty($letter)) {
            $case = 'Personal Subject Headings';

        }
        //Case browse + type = Chronological List
        if (empty($parent_topic) && empty($letter) && !empty($type) && 'Chronological List' == $type) {
            $case = 'Personal Subject Headings + type=chrono';
        }
        //Case browse + type = alphabetic
        if (!empty($type) && empty($letter) && $type == 'alphabetic' && empty($parent_topic)) {
            $case = 'Personal Subject Headings + type=alphabetic';
        }
        //Case browse + letter
        if (empty($parent_topic) && empty($type) && !empty($letter)) {
            $case = 'Personal Subject Headings + letter';
        }
        // Case browse + parent-topic
        if (!empty($parent_topic) && empty($type) && empty($letter)) {
            $case = 'Personal Subject Headings + parent-topic';
        }
        //Invalid cases
        if (!empty($parent_topic) && (!empty($letter) || !empty($type))) {
            $case = 'invalid: parent-topic + parameter (type or letter)';
        }
        if (empty($parent_topic) && !empty($letter) && !empty($type)) {
            $case = 'invalid type + letter';
        }
        return $case;
    }

    function custom_lists_websites_on_religion($atts, $content = null)
    {
        $browse = $atts['browse'];
        $parent_topic ='';
        $type ='';
        $letter = '';
        if (!empty($atts['parent-topic'])){
            $parent_topic = $atts['parent-topic'];
        }
        if (!empty($atts['type'])){
            $type = $atts['type'];
        }
        if (!empty($atts['letter'])){
            $letter = $atts['letter'];
        }

        $case = ''; // variable to each case depending on browse values

        if (empty($browse)) {
            return $output = '<p style="color: red">browse parameter can not be empty, go to wabash shortcode documentation for more information</p>';
        }
        switch ($browse) {

            case  'Material Types' :
                $case = 'Material Types';

                // invalid case
                if (!empty($parent_topic) || !empty($letter) || !empty($type)) {
                    $case = 'invalid Material Types';
                }
                break;

            case 'Subject Headings' :
                //case =  Subject Headigns
                $case = $this->check_subject_cases($parent_topic,$type,$letter);
                break;

            case 'Personal Subject Headings' :
                $case = $this->check_personal_subject_cases ($parent_topic,$type,$letter);

                break;

        }
        $output = $this->check_browse_cases($case,$browse,$letter,$parent_topic);

        return $output;
    }

    function custom_lists_scholarship($atts, $content = null)
    {
        $post_type = 'scholarship';
        $taxonomy_name = 'scholarship-topic';
        $terms_by_taxonomy = get_terms($taxonomy_name);
        $output = '';
        if (!empty($terms_by_taxonomy)) {
            $output .= '<ul>';
            foreach ($terms_by_taxonomy as $term) {
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    function custom_lists_syllabi($atts, $content = null)
    {
        global $wpdb;
        $browse = $atts['browse'];
        $parent_topic = '';
        if (!empty($atts['parent-topic'])){
            $parent_topic = $atts['parent-topic'];
        }
        $type = '';
        if (!empty($atts['type'])){
            $type = $atts['type'];
        }
        $letter = '';
        if (!empty($atts['letter'])){
            $letter = $atts['letter'];
        }

        $taxonomy_name = 'syllabi-topic';
        if (!empty($type)  && !empty($parent_topic)) {
            $sql = "select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where terms.name='" . $browse . "' and term_taxonomy.taxonomy='" . $taxonomy_name . "' and term_taxonomy.term_id=terms.term_id";
            $parent_term = $wpdb->get_results($sql);
            $sql2 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term[0]->term_id . " and term_taxonomy.term_id=term.term_id and name='" . $type . "'";
            $parent_term2 = $wpdb->get_results($sql2);
            $sql3 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term2[0]->term_id . " and term_taxonomy.term_id=term.term_id and name='" . $parent_topic . "'";
            $parent_term3 = $wpdb->get_results($sql3);
            $letterSyllabi = '';
            if (!empty($letter)) {
                $letterSyllabi = '(name like "' . $letter . '%" OR name like "\\"' . $letter . '%") AND ';
            }
            $sql4 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where " . $letterSyllabi . " parent=" . $parent_term3[0]->term_id . " and term_taxonomy.term_id=term.term_id and count>0";
            $parent_term4 = $wpdb->get_results($sql4);
            $list = $parent_term4;
        } elseif (!empty($browse) && !empty($parent_topic)) {
            $sql = "select * from wp_terms as terms,wp_term_taxonomy as term_taxonomy where terms.name='" . $browse . "' and term_taxonomy.taxonomy='" . $taxonomy_name . "' and term_taxonomy.term_id=terms.term_id";
            $parent_term = $wpdb->get_results($sql);
            $sql2 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term[0]->term_id . " and term_taxonomy.term_id=term.term_id and name='" . $parent_topic . "'";
            $parent_term2 = $wpdb->get_results($sql2);
            $sql3 = "SELECT * FROM wp_term_taxonomy as term_taxonomy,wp_terms as term where parent=" . $parent_term2[0]->term_id . " and term_taxonomy.term_id=term.term_id and count>0";
            $parent_term3 = $wpdb->get_results($sql3);
            $list = $parent_term3;
        } elseif ((empty($type) || $type == 'alphabetic') && empty($parent_topic)) {
            $main_term = get_term_by('name', $browse, $taxonomy_name);
            $childrens = get_term_children($main_term->term_id, $taxonomy_name);
            //print_r($childrens);
            $list = array();
            foreach ($childrens as $termchild) {
                $valuesChilds = get_term_by('id', $termchild, $taxonomy_name);
                if ($letter != '') {
                    $fstletter = substr($valuesChilds->name, 0, 1);
                    if ($fstletter == $letter) {
                        $list[] = $valuesChilds->name;
                    }
                } else {
                    $list[] = $valuesChilds->name;
                }
            }
            if ($type == 'alphabetic') {
                natcasesort($list);
            }
        }
        if (!empty($list)) {
            $termName = array();
            $output = '<ul>';
            foreach ($list as $topic) {
                //REORDERED ARRAY*/
                if (empty($topic->name)) {
                    $name = $topic;
                } else {
                    $name = $topic->name;
                }
                $term = get_term_by('name', $name, 'syllabi-topic');
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    function custom_lists_grants($atts, $content = null)
    {
        $post_type = 'grants';
        $taxonomy_name = 'grant-topic';
        $terms_by_taxonomy = get_terms($taxonomy_name);
        if (!empty($terms_by_taxonomy)) {
            $output = '<ul>';
            foreach ($terms_by_taxonomy as $term) {
                $output .= '<li><a style="color:black" href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a></li>';
            }
            $output .= '</ul>';
        }
        return $output;
    }

    function wabash_staff2($atts, $content = null)
    {
        global $wpdb;
        $this->carousel_scripts();
        if (!empty($atts['list_type']) && $atts['list_type'] == 'list_all' && empty($atts['email'])) {
            $table_name = $wpdb->prefix . "wabash_staff";
            $sql = "SELECT * FROM " . $table_name . " WHERE 1 ORDER BY staff_order";
            $staff_data = $wpdb->get_results($sql);
            $upload_dir = wp_upload_dir();
            $target_dir = $upload_dir['baseurl'] . '/';
            $i = 0;
            $output = '';
            foreach ($staff_data as $staff_member) {
                if ($i++ % 3 == 0) $output .= '<div class="row">';
                $output .= '<div class="col-md-4">
								  <div class="panel panel-default box">
									  <div class="panel-heading"><span style="color: #a6192e; font-size: 20px;">' . $staff_member->firstname . ' <strong>' . $staff_member->lastname . '</strong></span></div>
									  <div class="panel-body">';
                if ($atts['image'] == 'yes') {
                    $output .= '<div><a href="' . get_bloginfo('url') . '/about/staff-profiles?' . $staff_member->id . '" >';
                    $output .= '<img class="card-img-top img-responsive" src="' . $target_dir . $staff_member->card_photo . '" alt="image" /></a></div>';
                    $totalText = '100';
                } else {
                    $totalText = 400;
                }
                $output .= '<div><h4 style="color: #a29b8b;">' . $staff_member->position . '</h4>
											  ' . force_balance_tags(substr($staff_member->description, 0, $totalText)) . ' <span class="pull-right" style="color: #a6192e;"><a href="' . get_bloginfo('url') . '/about/staff-profiles?' . $staff_member->id . '" >Read more</a></span>
										  </div>
									  </div>
								  </div>
							  </div>';
                if ($i % 3 == 0)
                    $output .= "</div>";
            }
            if ($i % 3 != 0) $output .= "</div>";
            wp_reset_postdata();
        } elseif (!empty($atts['list_type']) && $atts['list_type'] == 'by_emails' && !empty($atts['email'])) {
            $emailsComma = explode(',', $atts['email']);
            $separator = '';
            $emailsSql = '';
            foreach ($emailsComma as $emails) {
                $emailsSql .= $separator . "email='" . $emails . "'";
                $separator = ' or ';
            }
            $table_name = $wpdb->prefix . "wabash_staff";
            $sql = "SELECT * FROM " . $table_name . " WHERE " . $emailsSql;
            $staff_data = $wpdb->get_results($sql);
            // Add our custom loop
            $upload_dir = wp_upload_dir();
            $target_dir = $upload_dir['baseurl'] . '/';
            $i = 0;
            $output = '';
            foreach ($staff_data as $staff_member) {
                if ($i++ % 3 == 0) {
                    $output .= '<div class="row">';
                }
                $output .= '<div class="col-md-4">
								  <div class="panel panel-default">
									  <div class="panel-heading"><span style="color: #a6192e; font-size: 20px;">' . $staff_member->firstname . ' <strong>' . $staff_member->lastname . '</strong></span></div>
									  <div class="panel-body">';
                if ($atts['image'] == 'yes') {
                    $output .= '<div><a href="' . get_bloginfo('url') . '/about/staff-profiles?' . $staff_member->id . '" >';
                    $output .= '<img class="card-img-top img-responsive" src="' . $target_dir . $staff_member->card_photo . '" alt="image" /></a></div>';
                    $totalText = '100';
                } else {
                    $totalText = 400;
                }
                $output .= '<div><h4 style="color: #a29b8b;">' . $staff_member->position . '</h4>
											  ' . force_balance_tags(substr($staff_member->description, 0, $totalText)) . '
											  <span class="pull-right" style="color: #a6192e;"><a href="' . get_bloginfo('url') . '/about/staff-profiles?' . $staff_member->id . '" >Read more</a></span>
										  </div>
									  </div>
								  </div>
							  </div>';
                if ($i % 3 == 0) {
                    $output .= "</div>";
                }
            }

            if ($i % 3 != 0) {
                $output .= "</div>";
            }
            wp_reset_postdata();
        }
        return $output;
    }

    function staff_carousel($atts, $content = null)
    {
        global $wpdb;
        if((empty($atts['list'])))
        {
            return "<strong>Some parameters are empty, please check again the shortcode (Staff Carousel)</strong>";
        }
        else if($atts['list']=="emails" && $atts['emails']=="")
        {
            return "<strong>List Type emails need paramter emails to be filled (Staff Carousel)</strong>";
        }
        wp_register_style('staff-carousel-style', plugins_url() . '/wabash-shortcodes/css/staff-carousel.css', array(), '1', 'all');
        wp_enqueue_style('staff-carousel-style');
        wp_register_style('staff-carousel-responsive-style', plugins_url() . '/wabash-shortcodes/css/jcarousel.responsive.css', array(), '1', 'all');
        wp_enqueue_style('staff-carousel-responsive-style');
        wp_register_script('jCarousel-script', plugins_url() . '/wabash-shortcodes/js/jquery.jcarousel.min.js', array('jquery'), '1', true);
        wp_enqueue_script('jCarousel-script');
        wp_register_script('jCarousel-responsive-script', plugins_url() . '/wabash-shortcodes/js/jcarousel.responsive.js', array('jquery'), '1', true);
        wp_enqueue_script('jCarousel-responsive-script');
        $uploadUrl = wp_upload_dir();
        $table_name = $wpdb->prefix . "wabash_staff";
        if ($atts['list'] == 'emails') {
            $emailsComma = explode(',', $atts['emails']);
            $separator = '';
            $emailsSql = '';
            foreach ($emailsComma as $emails) {
                $emailsSql .= $separator . "email='" . trim($emails) . "'";
                $separator = ' or ';
            }
            $sql = "SELECT * FROM " . $table_name . " WHERE " . $emailsSql;
        } else {
            $sql = "SELECT * FROM " . $table_name;
        }
        $staff_data = $wpdb->get_results($sql);
        $carouselClass=uniqid();
        $output = '<div class="wrapper">
                    <div class="jcarousel-wrapper">
                        <div class="jcarousel">';
        $index = 0;
        $output .= '<ul>';
        foreach ($staff_data as $staff_member)
        {
            $output .= ' <li class="staff-carousel-body-container">
                              <div class="staff-carousel-box">
                                  <div class="staff-carousel-img-container">
                                    <a class="staff-member-image-link" href="' . get_bloginfo('url') . '/about/staff-profiles?' . $staff_member->id . '" >
                                    <img class="img-responsive text-center staff-carousel-img" src="' . $uploadUrl['baseurl'] . '/' . $staff_member->card_photo . '" alt="image" /></a>
									  </div>
                                  <div id="title-container">
                                        <span class="staff-carousel-title" id="staff-carousel-title">' . $staff_member->firstname . ' <strong>' . $staff_member->lastname . '</strong></span>
									  </div>
                                  <div id="position-container">
                                      <h5 class="position-title" id="position-title">' . $staff_member->position . '</h5>
                                  </div>
								  </div>
					    </li>';
            $index++;
        }
        $output .= '</li>
		</div>
                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
		</div>
		</div>';
        return $output;
    }

    function custom_accordion($atts, $content = null)
    {
        //To check if script is already enqueued
        $handle = 'other-shortcodes';
        if(empty($atts['bgcolor']))
        {
            return 'The Background Color parameter is not filled <strong>(Custom Accordion Shortcode)</strong>';
        }
        $list = 'enqueued';
        if (!wp_script_is($handle, $list)) {
            wp_register_script('other-shortcodes', plugins_url() . '/shortcodes-ultimate/assets/js/other-shortcodes.js');
            wp_enqueue_script('other-shortcodes');
            wp_register_style('box-shortcodes', plugins_url() . '/shortcodes-ultimate/assets/css/box-shortcodes.css', array(), '1', 'all');
            wp_enqueue_style('box-shortcodes');
        }
        $handle = 'box-shortcodes';
        $list = 'enqueued';
        if (!wp_script_is($handle, $list)) {
            wp_register_style('box-shortcods', plugins_url() . '/shortcodes-ultimate/assets/css/box-shortcodes.css', array(), '1', 'all');
            wp_enqueue_style('box-shortcodes');
        }
        $headerOpen = '';
        $headerClose = '';
        if (isset($atts['header']) && $atts['header'] != '') {
            $headerOpen = '<' . $atts['header'] . ' style="margin-top:5px;margin-bottom:5px;color:#fff">';
            $headerClose = '</' . $atts['header'] . '>';
        }
        $output = '<div class="su-accordion" style="margin-top:50px">
				  <div class="su-spoiler su-spoiler-style-fancy su-spoiler-icon-plus">
				  <div class="su-spoiler-title" style="background-color:'.$atts['bgcolor'].' "><span style="color:#fff" class="su-spoiler-icon"></span>' . $headerOpen . $atts['title'] . $headerClose . '</div>
				  <div class="su-spoiler-content su-clearfix">';
        $output .= do_shortcode($content);
        $output .= '</div></div></div>';
        return $output;
    }

    function book_reviews_list($atts, $content = null)
    {

        wp_register_style('book-reviews', get_stylesheet_directory_uri() . '/css/book_reviews.css', array(), '1', 'all');
        wp_enqueue_style('book-reviews');
        wp_register_script('fix-text-script', get_stylesheet_directory_uri().'/js/FitText/jquery.fittext.js', array('jquery'), '1', true);
        wp_enqueue_script('fix-text-script');
        wp_register_script('book_reviews_script', plugins_url().'/wabash-shortcodes/js/book_reviews.js', array('jquery'), '1', true);
        wp_enqueue_script('book_reviews_script');

        $books=array();

        if (isset($atts['withreviews']) && $atts['withreviews'] == 'no') {
            $books=get_book_available_for_review_list($atts['limit'],$atts['orderby']);
        } else{
            $books=get_book_reviews_list($atts['limit'],$atts['orderby']);
        }

        $html='';

        foreach($books as $book){
            $html.=display_tile_book_reviews_object($book);
        }

        return $html;
    }

    function feature_videoCPT($atts, $content = null)
    {
        global $wpdb;
        wp_register_style('featured-video-style', plugins_url() . '/wabash-shortcodes/css/featured-video.css', array(), '1', 'all');
        wp_enqueue_style('featured-video-style');
        wp_register_script('videos-script', plugins_url() . '/wabash-shortcodes/js/videos-shortcode.js', array('jquery'), '1', true);
        wp_enqueue_script('videos-script');
        wp_register_script('featured-video-script', get_stylesheet_directory_uri() . '/js/featured-video.js', array('jquery'), '1', true);
        wp_enqueue_script('featured-video-script');
        $lastestVideoSql = "SELECT * FROM " . $wpdb->prefix . "posts where post_type='video' and post_status!='auto-draft' order by post_date DESC limit 1";
        $lastestVideoData = $wpdb->get_results($lastestVideoSql);
        $pattern = "#<\s*?iframe\b[^>]*>(.*?)</iframe\b[^>]*>#s";
        preg_match($pattern, do_shortcode($lastestVideoData[0]->post_content), $video);
        $content = preg_replace($pattern, '', $lastestVideoData[0]->post_content);


        $tag = wp_get_post_tags($lastestVideoData[0]->ID);
        $tagNames=array();
        foreach($tag as $tagsValues)
        {
            $tagNames[]=$tagsValues->name;
        }
        $tags=implode(' | ',$tagNames);
        //ALIGN VIDEO
        /*if(isset($atts['align'])) {
            if ($atts['align'] == 'center') {
                $align = 'margin:0 auto';
            } elseif ($atts['align'] == 'left') {
                $align = 'float:left';
            } elseif ($atts['align'] == 'right') {
                $align = 'float:right';
            }
        }
        else {
            $align = 'text-left';
        }*/
        //VIDEO SIZES
        if (isset($atts['width'])) {
            $newWidth = $atts['width'];
        } else {
            $newWidth = 'auto';
        }
        $video = preg_replace(array('/width="\d+"/i', '/height="\d+"/i'), array(sprintf('width="%s"', '100%'), sprintf('height="%s"', '100%')), $video[0]);
        //$output = '<a href="' . $lastestVideoData[0]->guid . '" class="link_title_featured_videos">';
                     $output = '<div style="width:' . $newWidth . '" class="featured_videos_parent_container">
                        <div class="container_title_videos video_title_play">' . $lastestVideoData[0]->post_title . '</div>
                        <img class="detail_page_video_icon" src="'.get_stylesheet_directory_uri().'/images/video_icon.png"><h5>Video</h5>
                    <div style="width:100%" id="featured_video">
                    <div id="content_featured_video">
					' . $video . '
				</div>
				</div>
				<div class="featured_content">
					' . strip_tags(do_shortcode($content)) . '
				</div>
				<footer class="tags-footer"><strong>Tags:</strong> '.$tags.'</footer>
				</div>';
        $output.='</a>';
        return $output;
    }

    function lastestVideos($atts, $content = null)
    {
        wp_register_script('videos-script', plugins_url() . '/wabash-shortcodes/js/videos-shortcode.js', array('jquery'), '1', true);
        wp_enqueue_script('videos-script');
        wp_register_style('lastest-videos-style', plugins_url() . '/wabash-shortcodes/css/lastest_videos.css', array(), '1', 'all');
        wp_enqueue_style('lastest-videos-style');
        $limit = $atts['limit'];

        global $wpdb;
        if (empty($limit)) {
            return 'The limit attribute cant be empty, please check the shortcode <strong>(Lastest Videos)</strong>';
        } else {
            if (empty($atts['columns']) || $atts['columns'] > 3) {
                $columns = 2;
            }
            else
                {
                    $columns = $atts['columns'];
                }
        $lastestVideoSql = "SELECT * FROM " . $wpdb->prefix . "posts where post_type='video' and post_status!='auto-draft' order by post_date DESC limit " . $limit . " offset 1";
        $lastestVideoData = $wpdb->get_results($lastestVideoSql);
                $output = '[row class="lastest_videos_main_container"]';
        $index = 0;
        foreach ($lastestVideoData as $videoData) {
                    $tag = wp_get_post_tags($videoData->ID);
                    $tagNames=array();
                    foreach($tag as $tagsValues)
                    {
                        $tagNames[]='<a href="javascript:void(0)">'.$tagsValues->name.'</a>';
                    }

                    $tags=implode(' | ',$tagNames);
                    if ($index == $columns) {
                $output .= '[/row]';
                        $output .= '[row]';
                $index = 0;
            }
                    $offset = '0';
                    if ($columns == 3) {
                        $cols = 12 / $columns;
                    } else {
                        $cols = 12 / $columns - 1;
                        if ($cols < 11) {
                            $offset = '1';
                        }
                    }
                    //$output .= '<a href="' . $videoData->guid . '" class="link_title_videos">';
                    $output .= '<div class="lastest_videos_boxes col-lg-'.$cols.' col-lg-offset-'.$offset.' col-md-'.$cols.' col-md-offset-'.$offset.' col-xs-12 col-sm-12">
                                <div class="lastest_videos_inner_container">
                                <div class="container_title_videos video_title_play">
                           ' . $videoData->post_title . '</div>
                           <div><img class="detail_page_video_icon" src="'.get_stylesheet_directory_uri().'/images/video_icon.png"><h5>Video</h5></div>';

            $cleanPost = strip_tags($videoData->post_content);
            $content = str_replace(array('[row]', '[/row]'), '', $cleanPost);
            $contentVideo = do_shortcode($content);
            $contentV = preg_replace(
                array('/class="col.+"/i'),
                        array(sprintf('class="%s"', 'lastest_videos_video_container')),
                        $contentVideo, 1);
            $contentV = preg_replace(
                array('/class="col.+"/i'),
                        array(sprintf('class="%s"', 'lastest_videos_text')),
                $contentV);
                    $contentV = preg_replace("!<iframe([^>]+?)>!",'<div class="video-wrapper"><iframe$1>',$contentV);
                    $contentV = preg_replace("!</iframe>!","</iframe></div>",$contentV);
                    $output .= $contentV . '<footer class="tags-footer"><strong>Tags:</strong> '.$tags.'</footer></div>
                    </div>';
                    //$output .='</a>';
            $index++;
        }
        $output .= '[/row]';
        return do_shortcode($output);
    }
    }

    function block_link($atts = [], $content)
    {
        wp_register_script('block_link_scripts', plugins_url().'/wabash-shortcodes/js/block_link.js');
        wp_enqueue_script('block_link_scripts');
        //onclick='javascript:window.open(\"".$atts['url']."\",\"".$atts['target']."\")'
        $html="<div class='block-link' id='".(!empty($atts['url'])?$atts['url']:'')."|".$atts['target']."'>".do_shortcode(trim($content))."</div>";
        return $html;
    }

    function lastestSyllabi($atts = [], $content)
    {
        wp_register_style('syllaby-style',  plugins_url().'/wabash-shortcodes/css/syllabi.css', array(), '1', 'all');
        wp_enqueue_style('syllaby-style');
        global $wpdb;
        $limit=$atts['limit'];
        if(empty($limit))
        {
            return 'The parameter limit cant be empty <strong>(Lastest Syllabi Shortcode)</strong>';
        }
        if(empty($atts['columns']) || $atts['columns']>2)
        {
            return 'Columns parameter cant be empty or greater than 2 <strong>(Lastest Syllabi Shortcode)</strong>';
        }
        else
        {
            $cols=12/$atts['columns'];
            $sql = "SELECT * FROM wp_posts WHERE post_type='syllabi' order by ID DESC LIMIT ".$limit." ";
            $result = $wpdb->get_results($sql); //get Subject Hedings object
            $html='<div class="row">';
            $index=0;
            foreach ($result as $post)
            {
                if($index==$atts['columns'])
                {
                    $index=0;
                    $html.='</div>';
                    $html.='<div class="row">';
                }
                    $topics = array();
                    $terms = wp_get_object_terms($post->ID, 'syllabi-topic');
                    //print_r($terms);
                    foreach($terms as $topic)
                    {
                        if(!empty($topic->name))
                        {
                            //print_r($topic);
                            $topics_link=get_category_link( $topic->term_id );
                            $topics[]='<a target="_blank" href="'.$topics_link.'">'.$topic->name.'</a>';
                            $topics_links[]=get_category_link( $topic->term_id );
                        }
                    }

                    $topics=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$topics);

                    $meta = get_post_meta($post->ID);
                    /*echo '<pre>';
                    print_r($meta);
                    echo '</pre>';*/
                    $getYear = date("Y", strtotime($post->post_date));
                    $tag = wp_get_post_tags($post->ID);
                    $tagNames = array();
                    foreach ($tag as $tagsValues) {
                        $tagNames[] = '<a href="javascript:void(0)">' . $tagsValues->name . '</a>';
                    }
                    $tags = implode(' | ', $tagNames);
                    $content = $post->post_content;
                    $content = do_shortcode($content);

                    $title = $post->post_title;
                    $syllaby_url = "javascript:void(0)";
                    $classTitle = "";
                    if (!empty($meta['syllabi-url'][0])) {
                        $syllaby_url = $meta['syllabi-url'][0];
                        $classTitle = "link_title_tiles";
                        $title = "<a target='_blank' href='" . $syllaby_url . "'>" . $post->post_title . "<span class='icon_external_url'></span></a>";
                    }

                    $year = '';
                    if (!empty($getYear) && $getYear !== '0000') {
                        $year = $getYear;
                    }
                    $imgRecommended = '';
                    if ($meta['featured-syllabi'][0] == '1') {
                        $imgRecommended = '<img class="img-responsive tree-img-single-results" src="' . get_stylesheet_directory_uri() . '/images/tree.png' . '" title="Wabash tree" alt="Wabash tree">';
                    }
                    $img = '<img class="img-responsive syllaby-cover-img" src="' . get_stylesheet_directory_uri() . '/images/syllabi_icon.png' . '" title="Syllabi cover image" alt="Syllabi cover image">';
                    $html.= '<div class="col-lg-'.$cols.'">
                    <div class="lastest-syllaby-main-container">
                            <article class="page-' . $post->ID . '">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        ' . $img . '
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <h4 class="' . $classTitle . '" itemprop="headline">' . $title . '</h4>
                        <p class="book_year">' . $year . '</p>';


                        if(!empty($topics) ) {
                            $html.= '<p><strong>Topics:</strong> ' . $topics . '</p>';
                        }
                        if(!empty($tags)) {
                            $html.= '<p><strong>Tags:</strong> ' . $tags . '</p>';
                        }

                    $html .= '</div>
                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                         ' . $imgRecommended . '
                    </div>';

                    $html .= '</div><div itemprop="text" class="square-padding clearfix syllabi_content_container">
                        ' . $post->post_content . '
                    </div>';
                    $html .= '</article></div></div>';
                    $index++;
            }
            $html.='</div>';
        }
        return $html;
    }

    function admin_head()
    {
        // check user permissions
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
            return;
        }
        // check if WYSIWYG is enabled
        if ('true' == get_user_option('rich_editing')) {
            add_filter('mce_external_plugins', array($this, 'mce_external_plugins'));
            add_filter('mce_buttons', array($this, 'mce_buttons'));
        }
    }

    /**
     * mce_external_plugins
     * Adds our tinymce plugin
     * @param  array $plugin_array
     * @return array
     */
    function mce_external_plugins($plugin_array)
    {
        $plugin_array[$this->shortcode_tag] = plugins_url('js/mce-button.js', __FILE__);
        return $plugin_array;
    }

    /**
     * mce_buttons
     * Adds our tinymce button
     * @param  array $buttons
     * @return array
     */
    function mce_buttons($buttons)
    {
        array_push($buttons, 'layouts_button');
        array_push($buttons, 'pushortcodes');
        return $buttons;
    }
    /**
     * admin_enqueue_scripts
     * Used to enqueue custom styles
     * @return void
     */
    //TO GET THE ATTACHMENT ID BY NOW USED BY RECENT PHOTOS CAROUSEL
    function get_attachment_id( $url ) {
        $attachment_id = 0;
        $dir = wp_upload_dir();

        if ( preg_match( "/\/wp-content\/uploads\//", $url, $match ) ) { // Is URL in uploads directory?
            $file = basename( $url );

            $query_args = array(
                'post_type'   => 'attachment',
                'post_status' => 'inherit',
                'fields'      => 'ids',
                'meta_query'  => array(
                    array(
                        'value'   => $file,
                        'compare' => 'LIKE',
                        'key'     => '_wp_attachment_metadata',
                    ),
                )
            );
            $query = new WP_Query( $query_args );
            if ( $query->have_posts() ) {
                foreach ( $query->posts as $post_id ) {
                    $meta = wp_get_attachment_metadata( $post_id );
                    $original_file       = basename( $meta['file'] );
                    $cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
                    if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
                        $attachment_id = $post_id;
                        break;
                    }
                }
            }
        }
        return $attachment_id;
    }
}//end class
new GWP_bs3_panel_shortcode();
function pu_get_shortcodes()
{
    global $shortcode_tags;
    echo '<script type="text/javascript">
    var shortcodes_button = new Array();';
    $count = 0;
    foreach ($shortcode_tags as $tag => $code) {
        echo "shortcodes_button[{$count}] = '{$tag}';";
        $count++;
    }
    echo '</script>';
}
