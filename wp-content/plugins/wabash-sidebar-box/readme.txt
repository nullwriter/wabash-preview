=== Wabash Sidebar Box ===
An adaption of the great AH sidebar box plugin

== Description ==

== Installation ==

1. Upload contents of the directory to /wp-content/plugins/ (or use the automatic installer)
1. Activate the plugin through the 'Plugins' menu in WordPress
1. In Appearance->Widgets, add »Wabash Sidebar Box« widget to any sidebar.


== Frequently Asked Questions ==

None at this point.




== Changelog ==

= 1.0.0 =
