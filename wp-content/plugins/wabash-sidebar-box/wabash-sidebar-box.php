<?php
/*
Plugin Name: Wabash Sidebar Box
Description: Creates a new and simple to use widget that adds a sidebar box with recent posts, last comments, categories, popular posts, a tag cloud and the archives to the sidebar.
Version: 1.0.0
Author:      Luis Noguera
License:     GPL2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  wabash_sidebar
Domain Path: /languages
*/

/**
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation.  You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/**
 * Registers our Widget.
 */
add_action( 'widgets_init', function(){
	register_widget( 'wabash_sidebar_box' );
});



/**
 * Include the required files
 */
require_once dirname( __FILE__ ) . '/includes/recent-comments.php';
require_once dirname( __FILE__ ) . '/includes/get-first-image.php';



/**
 * Loads the Textdomain for the german translation
 */
function wabash_sidebar_box_load_plugin_textdomain() {
    load_plugin_textdomain( 'wabash_sidebar', FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'wabash_sidebar_box_load_plugin_textdomain' );




### Function: Calculate Post Views
add_action( 'wp_head', 'process_postviews' );
function process_postviews() {
	global  $post;
	if( is_int( $post ) ) {
		$post = get_post( $post );
	}
	if( ! wp_is_post_revision( $post ) && ! is_preview() ) {
		if( is_singular( 'post' ) ) {
			$id = intval( $post->ID );
			if ( !$post_views = get_post_meta( $post->ID, 'views', true ) ) {
				$post_views = 0;
			}
			$should_count = true;

				$bots = array
				(
				'Google Bot' => 'google'
				, 'MSN' => 'msnbot'
				, 'Alex' => 'ia_archiver'
				, 'Lycos' => 'lycos'
				, 'Ask Jeeves' => 'jeeves'
				, 'Altavista' => 'scooter'
				, 'AllTheWeb' => 'fast-webcrawler'
				, 'Inktomi' => 'slurp@inktomi'
				, 'Turnitin.com' => 'turnitinbot'
				, 'Technorati' => 'technorati'
				, 'Yahoo' => 'yahoo'
				, 'Findexa' => 'findexa'
				, 'NextLinks' => 'findlinks'
				, 'Gais' => 'gaisbo'
				, 'WiseNut' => 'zyborg'
				, 'WhoisSource' => 'surveybot'
				, 'Bloglines' => 'bloglines'
				, 'BlogSearch' => 'blogsearch'
				, 'PubSub' => 'pubsub'
				, 'Syndic8' => 'syndic8'
				, 'RadioUserland' => 'userland'
				, 'Gigabot' => 'gigabot'
				, 'Become.com' => 'become.com'
				, 'Baidu' => 'baiduspider'
				, 'so.com' => '360spider'
				, 'Sogou' => 'spider'
				, 'soso.com' => 'sosospider'
				, 'Yandex' => 'yandex'
				);
				$useragent = isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';
				foreach ( $bots as $name => $lookfor ) {
					if ( ! empty( $useragent ) && ( stristr( $useragent, $lookfor ) !== false ) ) {
						$should_count = false;
						break;
					}
				}

			if( $should_count && ( ( !defined( 'WP_CACHE' ) || !WP_CACHE ) ) ) {
				update_post_meta( $id, 'views', ( $post_views + 1 ) );
				do_action( 'postviews_increment_views', ( $post_views + 1 ) );
			}
		}
	}
}

### Function: Round Numbers To K (Thousand), M (Million) or B (Billion)
function postviews_round_number( $number, $min_value = 1000, $decimal = 1 ) {
	if( $number < $min_value ) {
		return number_format_i18n( $number );
	}
	$alphabets = array( 1000000000 => 'B', 1000000 => 'M', 1000 => 'K' );
	foreach( $alphabets as $key => $value )
		if( $number >= $key ) {
			return round( $number / $key, $decimal ) . '' . $value;
		}
}

### Function Show Post Views Column in WP-Admin
function add_postviews_column($defaults) {
		$defaults['views'] = 'Views';
		return $defaults;
}
add_filter('manage_post_posts_columns' , 'add_postviews_column');


### Functions Fill In The Views Count
function add_postviews_column_content($column_name) {
	if( 'views' == $column_name ) {
		echo subh_get_post_view( get_the_ID() );
	}
}
add_action('manage_post_posts_custom_column' , 'add_postviews_column_content', 10, 2 );

function subh_get_post_view( $postID ) {
	$count_key = 'views';
	$count     = get_post_meta( $postID, $count_key, true );
	if ( '' == $count  ) {
		delete_post_meta( $postID, $count_key );
		add_post_meta( $postID, $count_key, '0' );

		return '0 View';
	}elseif ( 1 == $count ){
		return '1 View';
	}

	return postviews_round_number($count). ' Views';
}


// Register the column as sortable
add_filter( 'manage_edit-post_sortable_columns', 'my_sortable_view_column' );
function my_sortable_view_column( $columns ) {
	$columns['views'] = 'views';

	return $columns;
}



### To order posts in WP Admin
/* Sort posts in wp_list_table by column in ascending or descending order. */
add_action( 'pre_get_posts', 'my_view_orderby' );
function my_view_orderby( $query ) {
	if( ! is_admin() )
		return;

	$orderby = $query->get( 'orderby');

	if( 'views' == $orderby ) {
		$query->set('meta_key','views');
		$query->set('orderby','meta_value_num');
	}
}

/**
 * Create the widget
 */
if ( !class_exists( 'Wabash_Sidebar_Box' ) ) {

class Wabash_Sidebar_Box extends WP_Widget {

/**
 * Register widget with WordPress.
 */
	public function __construct() {
		parent::__construct(
	 		'wabash_sidebar_box', // Base ID
			'Wabash Sidebar Box', // Name
			array( 'description' => __( 'Adds a Sidebar Box with recent posts, last comments, categories, popular posts, a tag cloud and the archives to the sidebar. No Options.', 'wabash_sidebar' ), ) // Args
		);


/**
 * Registers Stylesheets and JavaScript
*/
    if ( !function_exists( 'wabash_register_script_style' ) ) {

		function wabash_register_script_style() {

			wp_register_script( 'wabash_sidebar_box', plugins_url( '/public/js/aside-script.js', __FILE__ ), array( 'jquery' ) );
			wp_register_style( 'wabash_sidebar_box', plugins_url('/public/css/aside-style.css', __FILE__) );
            wp_enqueue_style('font-awesome', '//opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css');
			wp_enqueue_script('wabash_sidebar_box');
			wp_enqueue_style('wabash_sidebar_box');
}

		// Adding the javascript and css only if widget in use
			if ( is_active_widget( false, false, $this->id_base, true ) ) {

				add_action( 'wp_enqueue_scripts', 'wabash_register_script_style' );
			}
	   }
    }


    /**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract($args);
		$title = apply_filters('widget_title', empty($instance['title']) ? __('') : $instance['title'], $instance, $this->id_base);

		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;

?>
<div id="sidebarbox">
		<ul id="tabMenu">
			<li class="posts selected" title="<?php  _e(' Recent Posts', 'wabash_sidebar');?>"><i class="fa fa-clock-o" aria-hidden="true"></i></li>
			<li class="commentz" title="<?php  _e(' Last Comments', 'wabash_sidebar');?>"><i class="fa fa-comments" aria-hidden="true"></i></li>
			<li class="popular" title="<?php  _e(' Popular Posts', 'wabash_sidebar');?>"><i class="fa fa-heart" aria-hidden="true"></i></li>
			<li class="category" title="<?php  _e(' Categories', 'wabash_sidebar');?>"><i class="fa fa-folder-open" aria-hidden="true"></i></li>
			<li class="random" title="<?php  _e(' Tag Cloud', 'wabash_sidebar');?>" style="display:none;"><i class="fa fa-tags" aria-hidden="true"></i></li>
            <li class="archiveslist" title="<?php  _e(' Archives', 'wabash_sidebar');?>"><i class="fa fa-archive" aria-hidden="true"></i></li>
		</ul>
		<div class="boxBody">
			<div id="posts">
				<h5 class="tabmenu_header">
				<?php  _e(' Recent Posts', 'wabash_sidebar');?>
				</h5>
				<ul class="recent_articles">
					<?php
							$evo = new WP_Query('showposts=5&ignore_sticky_posts=1');
							while ($evo->have_posts()) : $evo->the_post(); ?>
						<li class="clearfix">
						<a href="<?php the_permalink();?>" title="<?php the_title();?>">
							<?php evolution_post_thumbnails(); ?>
						</a>
						<span class="title-link">
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
							<?php the_title();?>
							</a>
						</span>
						<p><?php echo substr(get_the_excerpt(), 0,65); ?></p>
							<?php endwhile;?>
					<?php wp_reset_query(); ?>
				</li>
				</ul>
			</div>
			<div id="commentzz">
				<h5 class="tabmenu_header">
				<?php  _e(' Last Comments', 'wabash_sidebar');?>
				</h5>
				<ul class="wet_recent_comments">
					<?php evolution_recent_comments();?>
				</ul>
			</div>
			<div id="popular">
					<h5 class="tabmenu_header">
					<?php  _e(' Popular Posts', 'wabash_sidebar');?>
					</h5>
					<ul id="popular-posts">
						<?php
								$pop = new WP_Query(array( 'posts_per_page' => 5, 'meta_key' => 'views', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ));
								while ($pop->have_posts()) : $pop->the_post(); ?>
						<li>
							<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<?php evolution_post_thumbnails(); ?>
							</a>
							<span>
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
								<?php the_title();?>
								</a>
							</span>
							<p>
								Posted by
								<strong>
								<?php the_author() ?>
								</strong> on the
								<?php the_time('F jS, Y') ?> with
								<?php echo subh_get_post_view( get_the_ID() );?>
							</p>
						</li>
						<?php endwhile;?>
						<?php wp_reset_query(); ?>
					</ul>
				</div>

			<div id="category">
				<h5 class="tabmenu_header">
				<?php  _e(' Categories', 'wabash_sidebar');?>
				</h5>
				<ul class="category_list">
					<?php wp_list_categories('show_count=1&title_li=&hierarchical = 0');?>
				</ul>
			</div>
			<div id="random">
				<h5 class="tabmenu_header">
				<?php  _e(' Tag Cloud', 'wabash_sidebar');?>
				</h5>
				<?php if (function_exists('wp_tag_cloud')) { ?>
				<span id="sidebar-tagcloud">
					<?php wp_tag_cloud('smallest=10&largest=18');?>
				</span>
				<?php }?>
			</div>
            <div id="archiveslist">
				<h5 class="tabmenu_header">
				<?php  _e(' Monthly Archives', 'wabash_sidebar');?>
				</h5>
            <ul>
            <?php wp_get_archives('monthly&show_post_count=1', '', 'html', '', '', TRUE); ?>
            </ul>
			</div>
		</div><!-- end div.boxBody -->
		<div class="boxBottom">
		</div>
	</div><!-- end div.box -->
<?php
		echo $after_widget;
	}

   /**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);

		return $instance;
	}


    /**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );

		$title = strip_tags($instance['title']);
?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'wabash_sidebar'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>
<?php
	   }
    }
}
