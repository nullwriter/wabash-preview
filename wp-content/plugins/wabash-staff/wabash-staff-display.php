<?php
if (isset($_GET['id'])) {
    $welcome_msj = 'Update Wabash Staff member,fill out the form below';
    $submitMsj = 'Update Staff Member';
    $firstName = $memberData[0]->firstname;
    $lastName = $memberData[0]->lastname;
    $position = $memberData[0]->position;
    $description = $memberData[0]->description;
    $email = $memberData[0]->email;
    $phone = $memberData[0]->phone;
    $fax = $memberData[0]->fax;
    $resume = $memberData[0]->resume;
    $main_photo = $memberData[0]->main_photo;
    $card_photo = $memberData[0]->card_photo;
    $formNameId='update_staff_form';
} else {
    $welcome_msj = 'To add new wabash staff member, fill out the form below.';
    $submitMsj = 'Add New Staff';
    $formNameId='new_staff_form';
    $firstName = '';
    $lastName = '';
    $position = '';
    $description = '';
    $email = '';
    $phone = '';
    $fax = '';
    $resume = '';
    $main_photo = '';
    $card_photo = '';
}
?>
<style type="text/css">
    #wpbody-content {

        padding-right: 20px;

    }

    body {

        background-color: #f1f1f1;

    }
</style>
<div class="">
    <h2>Wabash Staff</h2>
    <div>
        <p>
            Enter your staff data.
        </p>
        <p>
            <?php
            echo $welcome_msj;
            ?>
        </p>
    </div>
    <div name="success" class="alert alert-success" style="display: none">
        <strong>Success!</strong> Staff member saved/updated...
    </div>
    <div name="error" class="alert alert-danger" style="display: none">
        <strong>Error!</strong> saving/updating Staff member
    </div>
    <form id="<?php echo $formNameId;?>" name="<?php echo $formNameId;?>" method="post" action="" enctype="multipart/form-data" class="form-horizontal">
        <input type="hidden" value="<?php echo $_GET['id'] ?>" name="id_member" id="id_member">
        <div class="">
            <div class="col-sm-7">
                <div class="form-group">
                    <div class="col-xs-6">
                        <label for="firstname">Firstname</label>
                        <input required value="<?php echo $firstName ?>" type="text" placeholder="Enter firstname..." name="firstname" id="firstname" class="form-control input-lg">
                    </div>
                    <div class="col-xs-6">
                        <label for="lastname">Lastname</label>
                        <input required value="<?php echo $lastName ?>" type="text" placeholder="Enter lastname..." name="lastname" id="lastname" class="form-control input-lg">
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="position">Position</label>
                        <input required value="<?php echo $position ?>" type="text" placeholder="Enter position..." name="position" id="position" class="form-control input-lg">
                    </div>
                </div>
            </div>
        </div>
        <div class="">
            <div class="col-sm-7">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="description">Bio</label>
                        <textarea required placeholder="Enter details about the new staff member.." rows="12" name="description" id="description" class="form-control input-lg"><?php echo $description ?></textarea>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="email">Email</label>
                        <input required value="<?php echo $email ?>" type="email" placeholder="Email" name="email" id="email" class="form-control input-lg">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="phone">Phone</label>
                        <input value="<?php echo $phone ?>" type="text" placeholder="Phone" name="phone" id="phone" class="form-control input-lg">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="fax">Fax</label>
                        <input type="text" value="<?php echo $fax ?>" placeholder="Fax" name="fax" id="fax" class="form-control input-lg">
                    </div>
                </div>
                <div class="form-group">
                    <?php
                        $upload_dir = wp_upload_dir();
                        if($resume!='')
                        {
                            echo "<a href='".$upload_dir["baseurl"]."/".$resume."' target='_blank' class='btn btn-default' aria-label='Left Align'>
                                    View Resume<span class='glyphicon glyphicon-list-alt' aria-hidden='true'></span>
                                </a>";
                        }
                    ?>
                    <div class="col-xs-12">
                        <label for="fax">Upload resume pdf</label>
                        <input type="file" name="resume" id="resume" class="filestyle" data-buttonName="btn-primary">
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="col-xs-12">
                <div class="col-xs-5">
                    <?php
                        if($main_photo!='')
                        {
                            echo "<a href='".$upload_dir["baseurl"]."/".$main_photo."' target='_blank'>
                                <img style='width:30%' src='".$upload_dir["baseurl"]."/".$main_photo."'>                   
                            </a>";
                        }
                    ?>
                    <div class="form-group">
                        <label for="main_image">Upload Main Image</label>
                        <input type="file" class="dropify" id="main_image" name="main_image">
                    </div>
                </div>
                <div class="col-xs-5 col-xs-offset-2">
                    <?php
                        if($card_photo!='')
                        {
                            echo "<a href='".$upload_dir["baseurl"]."/".$card_photo."' target='_blank'>
                                    <img style='width:50%' src='".$upload_dir["baseurl"]."/".$card_photo."'>                   
                                </a>";
                        }
                    ?>
                    <div class="form-group">
                        <label for="card_image">Upload Card Image</label>
                        <input type="file" class="dropify" id="card_image" name="card_image">
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div name="success" class="alert alert-success" style="display: none">
                    <strong>Success!</strong> Staff member saved/updated...
                </div>
                <div name="error" class="alert alert-danger" style="display: none">
                    <strong>Error!</strong> saving/updating Staff member
                </div>
            </div>
        </div>
        <div>
            <div class="col-xs-12">
                <div class="form-group">
                    <div class="col-xs-12 col-md-2">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check push-5-r"></i> <?php echo $submitMsj ?></button>
                    </div>
                    <?php
                    if (isset($_GET['id']))
                    {
                    ?>
                    <div class="col-xs-12 col-md-2">
                        <button id="delete_member" name="delete_member" class="btn btn-danger"><i class="fa fa-ban push-5-r"></i> Delete Staff member</button>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </form>


</div>
