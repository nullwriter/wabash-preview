<?php
/*
Plugin Name: Wabash-Staff
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the Wabash staff.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/
// Define the menu element
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
global $my_menu_wast_hook_akt;
global $my_menu_wast_hook_akt2;

add_action('admin_menu', 'my_wast_menu');

function my_wast_menu()
{
    global $my_menu_wast_hook_akt;
    global $my_menu_wast_hook_akt2;
    $my_menu_wast_hook_akt = add_menu_page('Wabash Staff Options', 'Wabash Staff', 'manage_options', 'wabash-staff', 'wast_options', 'dashicons-businessman');
    $my_menu_wast_hook_akt2=add_submenu_page('wabash-staff', 'List Staff Members', 'List Staff Members', 'manage_options', 'list-staff', 'wast_options2');
    add_action('admin_enqueue_scripts', 'wast_adding_scripts');
    add_action('admin_enqueue_scripts', 'wast_adding_scripts2');
}

function wast_options()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    if ($_GET['id']) {
        global $wpdb;
        $table_name = $wpdb->prefix . "wabash_staff";
        $sql = "select * from $table_name where id=" . $_GET['id'];
        $memberData = $wpdb->get_results($sql);
    }
    include 'wabash-staff-display.php';
}

function wast_options2()
{
    if (!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
    }
    include 'wabash-staff-list.php';
}
function wast_adding_scripts2($hook_suffix)
{
    global $my_menu_wast_hook_akt2;
    if ($hook_suffix != $my_menu_wast_hook_akt2) return;
    wp_register_script('custom_wast_list_js', plugins_url('wast-list.js', __FILE__), array(), '1', true);
    wp_enqueue_script('custom_wast_list_js');
    /*wp_register_script('bootstrap_tooltip_js', plugins_url('bootstrap-tooltip.js', __FILE__), array(), '1', true);
    wp_enqueue_script('bootstrap_tooltip_js');*/
    wp_register_script('bootstrap_js', plugins_url('bootstrap.min.js', __FILE__), array(), '1', true);
    wp_enqueue_script('bootstrap_js');
    wp_register_script('bootstrap_confirmation_js', plugins_url('bootstrap-confirmation-2.4-min.js', __FILE__), array(), '1', true);
    wp_enqueue_script('bootstrap_confirmation_js');
    wp_register_style('wast_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array(), '1', 'all');
    wp_enqueue_style('wast_bootstrap');
    wp_register_style('wast_css', plugins_url('css/wast.css', __FILE__), array(), '1', 'all');
    wp_enqueue_style('wast_css');
}
// Function to enqueue the js file
function wast_adding_scripts($hook_suffix)
{
    global $my_menu_wast_hook_akt;
    // exit function if not on my own options page!
    // $my_menu_hook_akt is generated when creating the options page, e.g.,
    // $my_menu_hook_akt = add_menu_page(...), add_submenu_page(...), etc
    //echo $hook_suffix;
    if ($hook_suffix != $my_menu_wast_hook_akt) return;
//Load JS and CSS files in here
    wp_register_script('wast_jqueryjs', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js', array(), '1', true);
    wp_register_script('dropify_js', plugins_url('dropify.min.js', __FILE__), array(), '1', true);
    wp_register_style('dropify_css', plugins_url('css/dropify.min.css', __FILE__), array(), '1', 'all');
    wp_register_script('custom_wast_js', plugins_url('wast.js', __FILE__), array(), '1', true);
    wp_register_style('wast_fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', array(), '1', 'all');
    wp_register_script('wast_ajaxreadyjs', plugins_url('jquery-ajaxreadystate.js', __FILE__), array(), '1', true);
    wp_register_style('wast_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array(), '1', 'all');
    wp_register_script('wast_jqueryform', 'http://malsup.github.com/jquery.form.js', array(), '1', true);
    wp_register_script('bootstrap_filestyle_js', plugins_url('bootstrap-filestyle.min.js', __FILE__), array(), '1', true);
    wp_enqueue_script('wast_jqueryjs');
    wp_enqueue_script('custom_wast_js');
    wp_enqueue_style('wast_fontawesome');
    wp_enqueue_script('wast_ajaxreadyjs');
    wp_enqueue_style('wast_bootstrap');
    wp_enqueue_script('dropify_js');
    wp_enqueue_style('dropify_css');
    wp_enqueue_script('wast_jqueryform');
    wp_enqueue_script('bootstrap_filestyle_js');
}

// Function to get the list of posts, called through ajax
if (isset($_POST['action']) && $_POST['action'] == "add_new_staff")
    add_action('wp_ajax_add_new_staff', 'add_new_staff_callback');
elseif (isset($_POST['action']) && $_POST['action'] == "delete_staff")
    add_action('wp_ajax_delete_staff', 'delete_staff_callback');
else
    add_action('wp_ajax_update_staff', 'update_staff_callback');
function add_new_staff_callback()
{
    global $wpdb; // this is how you get access to the database
    $table_name = $wpdb->prefix . "wabash_staff";
    $staff_count = $wpdb->get_var('SELECT COUNT(*) FROM ' . $table_name);
    $upload_dir = wp_upload_dir();
    $target_dir = $upload_dir['basedir'] . '/staff/';
    //Main Image
    if(isset($_FILES['main_image']['name'])) {
        $uniqMainImgName = uniqid();
        $target_main_image_file = $target_dir . 'images/' . $uniqMainImgName . '-' . basename($_FILES['main_image']['name']);
        $my_post['main_photo'] = 'staff/images/' . $uniqMainImgName . '-' . basename($_FILES['main_image']['name']);

        if (move_uploaded_file($_FILES['main_image']['tmp_name'], $target_main_image_file)) {
            $my_post['main_photo_msg'] = 'The file ' . basename($_FILES['main_image']['name']) . ' has been uploaded.';
        } else {
            if (isset($_FILES['main_image']['tmp_name'])) {
                $my_post['error'] .= '|Cannot upload (Main Image)';
            }
        }
    }
    else{
        $my_post['main_photo']='';
    }
    //Card Image
    if(isset($_FILES['card_image']['name'])) {
        $uniqCardImgName = uniqid();
        $target_card_image_file = $target_dir . 'images/' . $uniqCardImgName . '-' . basename($_FILES['card_image']['name']);
        $my_post['card_photo'] = 'staff/images/' . $uniqCardImgName . '-' . basename($_FILES['card_image']['name']);
        if (move_uploaded_file($_FILES['card_image']['tmp_name'], $target_card_image_file)) {
            $my_post['card_photo_msg'] = 'The file ' . basename($_FILES['card_image']['name']) . ' has been uploaded.';
        } else {
            if (isset($_FILES['card_image']['tmp_name'])) {
                $my_post['error'] .= '|Cannot upload (Card Image)';
            }
        }
    }
    else{
        $my_post['card_photo'] ='';
    }
    //Resume file
    if(isset($_FILES['resume']['name'])) {
        $uniqResumeName = uniqid();
        $target_resume_file = $target_dir . 'pdfs/' . $uniqResumeName . '-' . basename($_FILES['resume']['name']);
        $my_post['resume'] = 'staff/pdfs/' . $uniqResumeName . '-' . basename($_FILES['resume']['name']);
        if (move_uploaded_file($_FILES['resume']['tmp_name'], $target_resume_file)) {
            $my_post['resume_msg'] = 'The file ' . basename($_FILES['resume']['name']) . ' has been uploaded.';
        } else {
            if (isset($_FILES['resume']['tmp_name'])) {
                $my_post['error'] .= '|Cannot upload (Resume)';
            }
        }
    }
    else{
        $my_post['resume']='';
    }
    $my_post['staff_order'] = (INT)$staff_count + 1;
    $my_post['error'] = '';

    //$posts = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."posts WHERE (`post_type` = 'post' OR `post_type`='page')", OBJECT);

    $wpdb->insert($table_name,
        array('firstname' => $_POST['firstname'], 'lastname' => $_POST['lastname'], 'position' => $_POST['position'], 'phone' => $_POST['phone'], 'fax' => $_POST['fax'], 'email' => $_POST['email'], 'description' => $_POST['description'],
            'main_photo' => $my_post['main_photo'], 'card_photo' => $my_post['card_photo'], 'resume' => $my_post['resume'], 'staff_order' => $my_post['staff_order'])
    );
    if ($wpdb->last_error != '') {
        $my_post['error'] .= '|' . $wpdb->last_error;
    }
    $my_post['squery'] = $wpdb->last_query;

    echo json_encode($my_post);
    die(); // this is required to return a proper result
}

function update_staff_callback()
{
    //print_r($_POST);
    //exit;
    global $wpdb; // this is how you get access to the database
    $table_name = $wpdb->prefix . "wabash_staff";
    $staff_count = $wpdb->get_var('SELECT COUNT(*) FROM ' . $table_name);
    $upload_dir = wp_upload_dir();
    $target_dir = $upload_dir['basedir'] . '/staff/';

    $arrayToUpdate= array('firstname' => $_POST['firstname'],
        'lastname' => $_POST['lastname'],
        'position' => $_POST['position'],
        'phone' => $_POST['phone'],
        'fax' => $_POST['fax'],
        'email' => $_POST['email'],
        'description' => $_POST['description']);

    //Main Image
    if(isset($_FILES['main_image']['name'])) {
        $uniqMainImgName = uniqid();
        $target_main_image_file = $target_dir . 'images/' . $uniqMainImgName . '-' . basename($_FILES['main_image']['name']);
        $my_post['main_photo'] = 'staff/images/' . $uniqMainImgName . '-' . basename($_FILES['main_image']['name']);
        $arrayToUpdate['main_photo']=$my_post['main_photo'];
        if (move_uploaded_file($_FILES['main_image']['tmp_name'], $target_main_image_file)) {
            $my_post['main_photo_msg'] = 'The file ' . basename($_FILES['main_image']['name']) . ' has been uploaded.';
        } else {
            if (isset($_FILES['main_image']['tmp_name'])) {
                $my_post['error'] .= '|Cannot upload (Main Image)';
            }
        }
    }
    else{
        $my_post['main_photo']='';
    }
    //Card Image
    if(isset($_FILES['card_image']['name'])) {
        $uniqCardImgName = uniqid();
        $target_card_image_file = $target_dir . 'images/' . $uniqCardImgName . '-' . basename($_FILES['card_image']['name']);
        $my_post['card_photo'] = 'staff/images/' . $uniqCardImgName . '-' . basename($_FILES['card_image']['name']);
        $arrayToUpdate['card_photo']=$my_post['card_photo'];
        if (move_uploaded_file($_FILES['card_image']['tmp_name'], $target_card_image_file)) {
            $my_post['card_photo_msg'] = 'The file ' . basename($_FILES['card_image']['name']) . ' has been uploaded.';
        } else {
            if (isset($_FILES['card_image']['tmp_name'])) {
                $my_post['error'] .= '|Cannot upload (Card Image)';
            }
        }
    }
    else{
        $my_post['card_photo'] ='';
    }
    //Resume file
    if(isset($_FILES['resume']['name'])) {
        $uniqResumeName = uniqid();
        $target_resume_file = $target_dir . 'pdfs/' . $uniqResumeName . '-' . basename($_FILES['resume']['name']);
        $my_post['resume'] = 'staff/pdfs/' . $uniqResumeName . '-' . basename($_FILES['resume']['name']);
        $arrayToUpdate['resume']=$my_post['resume'];
        if (move_uploaded_file($_FILES['resume']['tmp_name'], $target_resume_file)) {
            $my_post['resume_msg'] = 'The file ' . basename($_FILES['resume']['name']) . ' has been uploaded.';
        } else {
            if (isset($_FILES['resume']['tmp_name'])) {
                $my_post['error'] .= '|Cannot upload (Resume)';
            }
        }
    }
    else{
        $my_post['resume']='';
    }
    //DELETE PREVIOUS FILES
    $query='SELECT main_photo,card_photo,resume FROM '.$table_name.' where id='.$_POST['id_member'];
    $results = $wpdb->get_results($query);
    //DELETE PREVIOUS FILES FROM FOLDER
    $mainImgDelete='';
    if($_FILES['main_image']['name']!='')
    {
        $mainImgDelete=$results[0]->main_photo;
    }
    if($_FILES['card_image']['name']!='')
    {
        $cardImgDelete=$results[0]->card_photo;
    }
    if($_FILES['resume']['name']!='')
    {
        $resumeDelete=$results[0]->resume;
    }
    deleteFiles($mainImgDelete,$cardImgDelete,$resumeDelete);
    //DELETE USER
    $wpdb->update($table_name, $arrayToUpdate, array('id' => $_POST['id_member']));
    $my_post['error']=$wpdb->last_error;
    $my_post['squery'] = $wpdb->last_query;
    echo json_encode($my_post);
    die(); // this is required to return a proper result*/
    exit;
}

function delete_staff_callback()
{
    //SELECT MEMBER IMAGES AND FILES TO DELETE
    if(isset($_POST['id_member']))
    {
        $id_member = $_POST['id_member'];
    }
    else {
        $id_member = explode('-', $_POST['id']);
        $id_member = $id_member[1];
    }
    global $wpdb;
    $table_name = $wpdb->prefix . "wabash_staff";
    $query='SELECT main_photo,card_photo,resume FROM '.$table_name.' where id='.$id_member;
    $results = $wpdb->get_results($query);
    //DELETE FILES FROM FOLDER
    deleteFiles($results[0]->main_photo,$results[0]->card_photo,$results[0]->resume);
    //DELETE USER
    $wpdb->delete($table_name, array('id' => $id_member));
}

function deleteFiles($main_image, $card_image, $resume)
{
    $upload_dir = wp_upload_dir();
    $target_dir = $upload_dir['basedir'] . '/';
    //Main image
    if($main_image!='' && file_exists($target_dir.$main_image))
    {
        chmod($target_dir.$main_image, 0777);
        unlink($target_dir.$main_image);
    }
    //Card Image
    if($card_image!='' && file_exists($target_dir.$card_image))
    {
        chmod($target_dir.$card_image, 0777);
        unlink($target_dir.$card_image);
    }
    //Resume
    if($resume!='' && file_exists($target_dir. $resume))
    {
        chmod($target_dir.$resume, 0777);
        unlink($target_dir.$resume);
    }
}
