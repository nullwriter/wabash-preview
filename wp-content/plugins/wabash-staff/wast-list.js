jQuery(document).ready(function() {
    jQuery('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        popout:true,
        singleton:true,
        onConfirm: function() {
            var data = {
                'action': 'delete_staff',
                'id': this.attr('id')
            };

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post(ajaxurl, data, function(response) {
                //console.log(response);
                location.reload();
            });
        },
    });
});
