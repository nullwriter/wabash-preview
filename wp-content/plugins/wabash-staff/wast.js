jQuery(document).ready(function() {
      var options = {
            data: { action: "add_new_staff"},
            url:        ajaxurl,
            success:    function(response)
            {
                var result=JSON.parse(response);
                if(result['error']!='')
                {
                    var errors=result['error'].split('|');
                    var errorsComma=errors.join(',')
                    $('div[name=error]').each(function(){
                        $(this).show();
                        $(this).html( $(this).html()+' with the following message:<strong> '+errorsComma+'</strong> check server status...');
                    });
                }
                else
                {
                    //Clear input file dropify
                    var drDestroy = $('#main_image').dropify();
                    //console.log(drDestroy);
                    drDestroy = drDestroy.data('dropify');
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                        drDestroy.init();
                    }

                    var drDestroy = $('#card_image').dropify();
                    drDestroy = drDestroy.data('dropify');
                    if (drDestroy.isDropified()) {
                        drDestroy.destroy();
                        drDestroy.init();

                    }

                    $('div[name=success]').each(function(){
                        $(this).show();
                    });
                    $("#new_staff_form")[0].reset();
                    window.scrollTo(0,0);
                    setTimeout(function() {
                        $('div[name=success]').each(function(){
                            $(this).fadeOut(5000);
                        });
                    }, 10000);
                }
            }
        };

        // bind to the form's submit event
        jQuery('#new_staff_form').submit(function() {
            // inside event callbacks 'this' is the DOM element so we first
            // wrap it in a jQuery object and then invoke ajaxSubmit
            $(this).ajaxSubmit(options);

            // !!! Important !!!
            // always return false to prevent standard browser submit and page navigation
            return false;
        });

//Update
    var options2 = {
        data: { action: "update_staff"},
        url:        ajaxurl,
        success:    function(response)
        {
            //alert(response);
            var result=JSON.parse(response);
            //console.log(result);
            if(result['error']!='')
            {
                var errors=result['error'].split('|');
                var errorsComma=errors.join(',')
                $('div[name=error]').each(function(){
                    $(this).show();
                    $(this).html( $(this).html()+' with the following message:<strong> '+errorsComma+'</strong> check server status...');
                });
            }
            else
            {
                //Clear input file dropify
                var drDestroy = $('#main_image').dropify();
                //console.log(drDestroy);
                drDestroy = drDestroy.data('dropify');
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                    drDestroy.init();
                }

                var drDestroy = $('#card_image').dropify();
                drDestroy = drDestroy.data('dropify');
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                    drDestroy.init();

                }

                $('div[name=success]').each(function(){
                    $(this).show();
                });
                $("#update_staff_form")[0].reset();
                window.scrollTo(0,0);
                setTimeout(function() {
                    $('div[name=success]').each(function(){
                        $(this).fadeOut(5000);
                    });
                }, 10000);
            }
            setTimeout(function() {
                location.reload();
            }, 2500);

        }
    };

    // bind to the form's submit event
    jQuery('#update_staff_form').submit(function() {
        // inside event callbacks 'this' is the DOM element so we first
        // wrap it in a jQuery object and then invoke ajaxSubmit
        $(this).ajaxSubmit(options2);
        // !!! Important !!!
        // always return false to prevent standard browser submit and page navigation
        return false;
    });
    //Delete
    $( "#delete_member" ).on( "click", function() {
        var data = {
            'action': 'delete_staff'
            ,id_member:jQuery('#id_member').val()
        };
        jQuery.post(ajaxurl, data, function(response) {
            //console.log(response);
            location.href='admin.php?page=list-staff';
        });
        return false;
    });
    jQuery('.dropify').dropify();
});
