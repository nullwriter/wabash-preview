<?php
/*
Plugin Name: Websites on Religion
Plugin URI: http://www.soliantconsulting.com
Description: Custom plugin for the Websites on Religion CPTs.
Author: soliantconsulting
Version: 1.0
Author URI: http://www.soliantconsulting.com
*/
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// Register Custom Post Type
function wor_cpt() {

	$labels = array(
		'name'                  => _x( 'Website On Religion', 'Post Type General Name', 'wor_cpt' ),
		'singular_name'         => _x( 'Website on Religion', 'Post Type Singular Name', 'wor_cpt' ),
		'menu_name'             => __( 'Websites on Religion', 'admin menu', 'wor_cpt' ),
		'name_admin_bar'        => __( 'Website on Religion', 'add new on admin bar', 'wor_cpt' ),
		'archives'              => __( 'Item Archives', 'wor_cpt' ),
		'parent_item_colon'     => __( 'Parent Website:', 'wor_cpt' ),
		'all_items'             => __( 'All Websites', 'wor_cpt' ),
		'add_new_item'          => __( 'Add New Website', 'wor_cpt' ),
		'add_new'               => __( 'New Website', 'wor_cpt' ),
		'new_item'              => __( 'New Website', 'wor_cpt' ),
		'edit_item'             => __( 'Edit Website', 'wor_cpt' ),
		'update_item'           => __( 'Update Website', 'wor_cpt' ),
		'view_item'             => __( 'View Website', 'wor_cpt' ),
		'search_items'          => __( 'Search Websites on Religion', 'wor_cpt' ),
		'not_found'             => __( 'No websites on religion found', 'wor_cpt' ),
		'not_found_in_trash'    => __( 'No websites on religion found in Trash', 'wor_cpt' ),
		'featured_image'        => __( 'Featured Image', 'wor_cpt' ),
		'set_featured_image'    => __( 'Set featured image', 'wor_cpt' ),
		'remove_featured_image' => __( 'Remove featured image', 'wor_cpt' ),
		'use_featured_image'    => __( 'Use as featured image', 'wor_cpt' ),
		'insert_into_item'      => __( 'Insert into item', 'wor_cpt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'wor_cpt' ),
		'items_list'            => __( 'Items list', 'wor_cpt' ),
		'items_list_navigation' => __( 'Items list navigation', 'wor_cpt' ),
		'filter_items_list'     => __( 'Filter items list', 'wor_cpt' ),
	);
	$args = array(
		'label'                 => __( 'Websites on Religion', 'wor_cpt' ),
		'description'           => __( 'Websites on Religion Post Type', 'wor_cpt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'rewrite'               => array( 'slug' => 'websites-on-religion' ),
		'menu_icon'	            => 'dashicons-cloud',
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'website_on_religion', $args );

}
add_action( 'init', 'wor_cpt');

function wor_taxonomies() {


	// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => 'Types',
			'singular_name'     => 'Type',
			'search_items'      => 'Search Types',
			'all_items'         => 'All Types',
			'parent_item'       => 'Parent Type',
			'parent_item_colon' => 'Parent Type:',
			'edit_item'         => 'Edit Type',
			'update_item'       => 'Update Type',
			'add_new_item'      => 'Add New Type',
			'new_item_name'     => 'New Type Name',
			'menu_name'         => 'Types',
		);

		$args = array(
			'hierarchical'        => true,
			'labels'              => $labels,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'query_var'           => true,
			'public'              => true,
			'publicly_queryable'  => true,
			'has_archive'         => true,
			'rewrite'             => array( 'slug' => 'website-type' ),
		);

		register_taxonomy('website-type','website_on_religion',$args);

	// Add new taxonomy, make it non-hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Topics', 'taxonomy general name' ),
		'singular_name'              => _x( 'Topic', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Topics' ),
		'all_items'                  => __( 'All Topics' ),
		'parent_item'                => __( 'Parent Topic' ),
		'parent_item_colon'          => __( 'Parent Topic:' ),
		'edit_item'                  => __( 'Edit Topic' ),
		'update_item'                => __( 'Update Topic' ),
		'add_new_item'               => __( 'Add New Topic' ),
		'new_item_name'              => __( 'New Topic Name' ),
		'separate_items_with_commas' => __( 'Separate Topics with commas' ),
		'add_or_remove_items'        => __( 'Add or remove Topics' ),
		'choose_from_most_used'      => __( 'Choose from the most used Topics' ),
		'not_found'                  => __( 'No Topics found.' ),
		'menu_name'                  => __( 'Topics' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		//'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		// 'show_in_nav_menus'	=> false,
		'public'                => true,
		'publicly_queryable'    => true,
		'has_archive'           => true,
		'rewrite'               => array( 'slug' => 'website-topic' ),
	);


	register_taxonomy( 'website-topic', 'website_on_religion', $args );

}
add_action( 'init', 'wor_taxonomies');


function wor_metaboxes(){
  add_meta_box("website-url-meta", "Website Url", "display_website_url_meta_box", "website_on_religion", "normal", "high");
  add_meta_box("website-recommended-meta", "Featured Website", "display_website_recommended_meta_box", "website_on_religion", "normal", "high");
}

function display_website_url_meta_box(){
  global $post;
  $custom = get_post_custom($post->ID);
  $website_url = !empty($custom["website-url"][0])?$custom["website-url"][0]:'';
  ?>
  <label for="website-url">Url:</label>
  <input name="website-url" value="<?php echo $website_url; ?>" />
  <?php
}

function display_website_recommended_meta_box() {
  global $post;
  wp_nonce_field( basename( __FILE__ ), 'wor_nonce' );
  $custom = get_post_custom($post->ID);
  $recommended = !empty($custom["featured-website"][0])?$custom["featured-website"][0]:'';

  ?>
  <label for="featured-website">
            <input type="checkbox" name="featured-website" id="featured-website" value="1" <?php if ( !empty ( $recommended ) && $recommended==1 ) { echo ' checked="checked"';} ?> />
            Check if this is a recommended website
        </label>
  <?php
}

add_action('admin_init', 'wor_metaboxes');

function save_wor_metaboxes( $post_id ) {


// Checks save status
$is_autosave = wp_is_post_autosave( $post_id );
$is_revision = wp_is_post_revision( $post_id );
$is_valid_nonce = ( isset( $_POST[ 'wor_nonce' ] ) && wp_verify_nonce( $_POST[ 'wor_nonce' ], basename( __FILE__ ) ) ) ? TRUE : FALSE;

// Exits script depending on save status
if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
    return;
}

// Checks for input and sanitizes/saves if needed
if( isset( $_POST[ 'featured-website' ] ) ) {
    update_post_meta( $post_id, 'featured-website', 1 );
}
else{
    update_post_meta( $post_id, 'featured-website', 0 );
}

// Checks for input and sanitizes/saves if needed
if( isset( $_POST[ 'website-url' ] ) ) {
    update_post_meta( $post_id, 'website-url', $_POST[ 'website-url' ]  );
}

}

add_action( 'save_post_website_on_religion', 'save_wor_metaboxes' );

add_filter('manage_website_on_religion_posts_columns' , 'add_website_on_religion_columns');

function add_website_on_religion_columns($columns) {
    //unset($columns['author']);
    return array_merge($columns,
          array('featured-website' => 'Recommended'));
}

add_action('manage_website_on_religion_posts_custom_column' , 'website_on_religion_posts_custom_columns', 10, 2 );

function website_on_religion_posts_custom_columns( $column, $post_id ) {

    switch ( $column ) {

    case 'featured-website' :
        echo get_post_meta($post_id, 'featured-website', true);

        break;
    }
}

// Register the column as sortable
add_filter( 'manage_edit-website_on_religion_sortable_columns', 'website_on_religion_posts_column_register_sortable' );
function website_on_religion_posts_column_register_sortable( $columns ) {
    $columns['featured-website'] = 'featured-website';
    return $columns;
}

add_filter( 'pre_get_posts','website_on_religion_posts_column_orderby');
function website_on_religion_posts_column_orderby( $query ) {
    if( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');
    if ( 'featured-website' == $orderby ) {
        //alter the query args
        $query->set('meta_key',$orderby);
        $query->set('orderby','meta_value');
    }
}//end _column_


function display_excerpt_wor_object($wor){
    // get tags
    $terms = array_map('trim', explode('|', $wor->tags));

    $tags = array();
    $tag_names = array();
    foreach($terms as $tag){
        if(!empty($tag)) {
            $term = get_term_by('name', $tag, 'post_tag');
            $tags[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
            $tag_names[]=$term->name;
        }
    }
    if(in_array('internal-website-on-religion',$tag_names)){

            $title = '<a href="'.$wor->website_url.'">' .$wor->title.'</a>';
    }
    else{
        $title="<a target='_blank' href='".$wor->website_url."'>".$wor->title."</a>";
    }

    $html = '<p>'.$title.'<br/>';
    $html .=limit_text(wp_strip_all_tags($wor->annotation), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_wor_object($item){
  global $load_more_data;

  $single_results_page=is_page_template('template-section-results-page.php') || $load_more_data;
  $detail_page=is_singular() && !$single_results_page;

  switch($item->type){
        case 'biblio':
            $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-bibliography.png'.'" title="Biblio cover image" alt="Biblio cover image">';
            break;
        case 'image':
            $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-image.png'.'" title="Image cover image" alt="Image cover image">';
            break;
        case 'e-text':
            $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-etext.png'.'" title="Etext cover image" alt="Etext cover image">';
            break;
        case 'e-journal':
            $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-journal.png'.'" title="Journal cover image" alt="Journal cover image">';
            break;
        case 'web':
            $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/icon-websites.png'.'" title="Web cover image" alt="Web cover image">';
            break;

    }



  // get type
  $terms = array_map('trim', explode('|', $item->type));

  $types = array();
  foreach($terms as $type){
    if(!empty($type)) {
        $term = get_term_by('name', $type, 'website-type');
        $types[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
    }
  }

  $types=implode(' | ',$types);

  // get topics
  $terms = array_map('trim', explode('|', $item->topics));

  $topics = array();
  foreach($terms as $topic){
      if(!empty($topic)) {
          $term = get_term_by('name', $topic, 'website-topic');
          $topics[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
      }
  }

  $topics=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$topics);

  // get tags
  $terms = array_map('trim', explode('|', $item->tags));

  $tags = array();
  $tag_names = array();
  foreach($terms as $tag){
      if(!empty($tag)) {
          $term = get_term_by('name', $tag, 'post_tag');
          $tags[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
          $tag_names[]=$term->name;
      }
  }

  $tags=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$tags);


  if(in_array('internal-website-on-religion',$tag_names)){

          $title="<a  href='".$item->website_url."'>".$item->title."</a>";

  }
  else{
      $title="<a target='_blank' href='".$item->website_url."'>".$item->title."<i class='icon_external_url'></i></a>";
  }


  $content = do_shortcode($item->annotation);


$html = '
<div class="panel card website_on_religion-card">
 <div class="panel-body">
    <div class="row is-flex">
       <div class="card-image col-xs-4 col-sm-4 col-md-3">
          '.$img.'
       </div>
       <div class="card-select col-xs-2 col-xs-offset-6 col-sm-2 col-sm-offset-6 col-md-1 col-md-offset-0 col-md-push-8">';
       if ($item->featured_website == '1') {
           $html .= ' <img id="image-normal" class="img-responsive" src="' . get_stylesheet_directory_uri() . '/images/tree.png' . '" title="Wabash tree" alt="Wabash tree">';
       }


          $html .= '<div class="card-checkbox">
                      <input value="'.$item->id.'" id="resource_'.$item->id.'" class="case" type="checkbox">
                  </div>';


      $html .= ' </div>
       <div class="card-info col-xs-12 col-sm-12 col-md-8 col-md-pull-1">
          <h3 class="card-title">
              '.$title.'
          </h3>
          <p class="card-meta">
          <span>'.$types.'</span><br>';
		  if(!empty($topics) ) {
			  $html.= '<span>Topics: '.$topics.'</span><br>';
		  }
		  if(!empty($tags) ) {
			  $html.= '<span>Tags: '.$tags.'</span><br>';
		  }
          $html.= '</p>
       </div>
    </div>
    <div class="row">
       <div class="card-content-container col-xs-12">
          <div class="card-content">
            <p>' . $content . '</p>
          </div>
       </div>
    </div>
 </div>
</div>';

  return $html;

}

function display_website_on_religion_sort_panel($sort_term){
	global $post_types,$search_string;

	$html = '
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="post" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <span class="panel-title">Sort by:</span>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="weight" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked"' : '').'
                                   id="relevance_radio">
                            <label for="relevance_radio">Relevance</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="type" id="format_radio" '.(($sort_term=='type')? 'checked="checked"' : '').'>
                            <label for="format_radio">Media Type</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="featured_website" id="title_radio" '.(($sort_term=='featured_website')? 'checked="checked"' : '').'>
                            <label for="title_radio">Recommended</label>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <span class="panel-title">Filter by:</span>
                    <button id="clear_filters2" title="Clear Filters" type="button">
                        <span class="hidden">Clear checkboxes</span>Clear
                    </button>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Website" class="checkbox">
                            <input class="styled" type="checkbox" name="wt-biblio" ' . ((!empty($_POST['wt-biblio']) || !empty($_GET['wt-biblio'])) ? 'checked="checked" ' : '') . '
                                   id="st-web_checkbox">
                            <label for="st-web_checkbox">Biblio</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Journal" class="checkbox">
                            <input class="styled" type="checkbox" name="wt-e-journal" ' . ((!empty($_POST['wt-e-journal']) || !empty($_GET['wt-e-journal'])) ? 'checked="checked" ' : '') . '
                                    id="st-journal_checkbox">
                            <label for="st-journal_checkbox">E-Journal</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Book" class="checkbox">
                            <input class="styled" type="checkbox" name="wt-e-text" ' . ((!empty($_POST['wt-e-text']) || !empty($_GET['wt-e-text'])) ? 'checked="checked" ' : '') . '
                                   id="wt-e-journal_checkbox">
                            <label for="st-book_checkbox">E-text</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Article" class="checkbox">
                            <input class="styled" type="checkbox" name="wt-image" ' . ((!empty($_POST['wt-image']) || !empty($_GET['wt-image'])) ? 'checked="checked" ' : '') . '
                                    id="st-article_checkbox">
                            <label for="st-article_checkbox">Image</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Book" class="checkbox">
                            <input class="styled" type="checkbox" name="wt-web" ' . ((!empty($_POST['wt-web']) || !empty($_GET['wt-web'])) ? 'checked="checked" ' : '') . '
                                   id="st-book_checkbox">
                            <label for="st-book_checkbox">Web</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
    ';
	return $html;
}

function display_website_on_religion_sort_panel_old($sort_term){
global $post_types,$search_string;

  $html='
  <div class="row">
              <form class="form_single_results_sort_filter_by" method="post" action="">
			  	<input type="hidden" name="st" value="'.$search_string.'" />
				<input type="hidden" name="pt" value="'.key($post_types).'" />
				<div class="col-md-6">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                                  <div>
                                      <span class="panel-title">Sort by:</span>
                                  </div>
                          </div>
                          <div class="panel-body">
                              <div class="row hidden-sm hidden-xs">
                                  <div class="col-sm-4">
                                      <div class="radio">
                                          <input type="radio" name="sort_term" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked" ' : '').' value="weight" id="relevance_radio">
                                          <label for="relevance_radio"><i title="Relevance" class="fa fa-list-ol" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                                  <div class="col-sm-4">
                                      <div class="radio">
                                          <input type="radio" name="sort_term" '.((!empty($sort_term) && $sort_term=='type')? 'checked="checked" ' : '').' value="type" id="media_type_radio">
                                          <label for="media_type_radio"><i title="Media Type" class="fa fa-archive" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                                  <div class="col-sm-4">
                                      <div class="radio">
                                          <input type="radio" name="sort_term" '.((!empty($sort_term) && $sort_term=='featured_website')? 'checked="checked" ' : '').' value="featured_website" id="recommended_radio">
                                          <label for="recommended_radio"><img class="img_sort_recommended img-responsive" src="'.get_stylesheet_directory_uri().'/images/tree.png"></label>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
                                  <div class="button-group-navigation text-center">
                                      <li><a title="Relevance" class="btn_mobile_single_search single_sort'.((empty($sort_term) || $sort_term=='weight')? ' active' : '').'" name="relevance" href="javascript:void(0)"><i  class="fa fa-list-ol" aria-hidden="true"></i></a></li>
                                      <li><a title="Media Type" class="btn_mobile_single_search single_sort'.(($sort_term=='type')? ' active' : '').'" name="media_type" href="javascript:void(0)"><i  class="fa fa-archive" aria-hidden="true"></i></a></li>
                                      <li><a title="Recommended" class="btn_mobile_single_search single_sort'.(($sort_term=='featured_website')? ' active' : '').'" name="featured_website" href="javascript:void(0)"><img  class="img_sort_recommended img-responsive" src="'.get_stylesheet_directory_uri().'/images/tree.png"></a></li>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="panel panel-default">
                          <div class="panel-heading">
  							<div>
  								<span class="panel-title">Filter by:</span>
  								<button id="clear_filters" title="Clear Filters" class="btn">
  									<span class="hidden">Clear checkboxes</span><i class="fa fa-eraser"></i>
  								</button>
  							 </div>
  						</div>
                          <div class="panel-body">
                              <div class="row hidden-sm hidden-xs">
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <div title="Biblio" class="checkbox">
                                          <input class="styled" type="checkbox" name="wt-biblio" ' . ((!empty($_POST['wt-biblio']) || !empty($_GET['wt-biblio'])) ? 'checked="checked" ' : '') . ' id="wt-biblio_checkbox">
                                          <label for="wt-biblio_checkbox"><i class="fa fa-book" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <div title="E-Journal" class="checkbox">
                                          <input class="styled" type="checkbox" name="wt-e-journal" ' . ((!empty($_POST['wt-e-journal']) || !empty($_GET['wt-e-journal'])) ? 'checked="checked" ' : '') . ' id="wt-e-journal_checkbox">
                                          <label for="wt-e-journal_checkbox"><i class="fa fa-newspaper-o" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <div title="E-Text" class="checkbox">
                                          <input class="styled" type="checkbox" name="wt-e-text" ' . ((!empty($_POST['wt-e-text']) || !empty($_GET['wt-e-text'])) ? 'checked="checked" ' : '') . ' id="wt-e-text_checkbox">
                                          <label for="wt-e-text_checkbox"><i class="fa fa-file-text" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <div title="Image" class="checkbox">
                                          <input class="styled" type="checkbox" name="wt-image" ' . ((!empty($_POST['wt-image']) || !empty($_GET['wt-image'])) ? 'checked="checked" ' : '') . ' id="wt-image_checkbox">
                                          <label for="wt-image_checkbox"><i class="fa fa-file-image-o" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                      <div title="Web" class="checkbox">
                                          <input class="styled" type="checkbox" name="wt-web" ' . ((!empty($_POST['wt-web']) || !empty($_GET['wt-web'])) ? 'checked="checked" ' : '') . ' id="wt-web_checkbox">
                                          <label for="wt-web_checkbox"><i class="fa fa-globe" aria-hidden="true"></i></label>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-sm-12 col-xs-12 hidden-md hidden-lg">
                                  <div class="button-group-navigation text-center">
                                      <div>
                                          <li><a title="Biblio" class="btn_mobile_single_search single_filter'.((!empty($_POST['wt-biblio']) || !empty($_GET['wt-biblio']))? ' active' : '').'" name="wt-web" href="javascript:void(0)"><i class="fa fa-book" aria-hidden="true"></i></a></li>
                                          <li><a title="E-Journal" class="btn_mobile_single_search single_filter'.((!empty($_POST['wt-e-journal']) || !empty($_GET['wt-e-journal']))? ' active' : '').'" name="wt-e-journal" href="javascript:void(0)"><i class="fa fa-newspaper-o" aria-hidden="true"></i></a></li>
                                          <li><a title="E-Text" class="btn_mobile_single_search single_filter'.((!empty($_POST['wt-e-text']) || !empty($_GET['wt-e-text']))? ' active' : '').'" name="wt-e-text" href="javascript:void(0)"><i class="fa fa-file-text" aria-hidden="true"></i></a></li>
                                          <li><a title="Image" class="btn_mobile_single_search single_filter'.((!empty($_POST['wt-image']) || !empty($_GET['wt-image']))? ' active' : '').'" name="wt-image" href="javascript:void(0)"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></li>
                                          <li><a title="Web" class="btn_mobile_single_search single_filter'.((!empty($_POST['wt-web']) || !empty($_GET['wt-web']))? ' active' : '').'" name="wt-web" href="javascript:void(0)"><i class="fa fa-globe" aria-hidden="true"></i></a></li>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </form>
          </div>
    ';

  return $html;
}

function get_website_on_religion_object($id){
    global $wpdb;

    $query = $wpdb->prepare('SELECT * FROM wp_v_website_on_religion WHERE id = %s LIMIT 1', $id);
    $wor = $wpdb->get_row($query);

	 return $wor;
}

function search_website_on_religion($search_string, $sub_type = array(), $sort_by = 'weight', $limit='', $offset=''){
	global $wpdb;
    $limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
	}
    if ($sort_by != 'weight' && $sort_by != 'featured_website') {
        $sort_direction = 'ASC';
    } else {
        $sort_direction = 'DESC';
	}

	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }
	// where do you want to search
	$fields = array('title', 'annotation', 'type',  'topics', 'tags');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_website_on_religion.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_website_on_religion.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
  //exit();

	$sql="SELECT wp_v_website_on_religion.*,
	if(
		wp_v_website_on_religion.title = '$search_string',  100,
	         IF(
	             wp_v_website_on_religion.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_website_on_religion.annotation =  '$search_string',  40,
	        IF(
	             wp_v_website_on_religion.annotation LIKE '%$search_string%', 30,

	                 if( {$terms_query['annotation']}, 5, 0)

	          )


	)
	+
	IF(
	    wp_v_website_on_religion.type =  '$search_string',  3,
	        IF(
	             wp_v_website_on_religion.type LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
	    wp_v_website_on_religion.topics =  '$search_string',  3,
	        IF(
	             wp_v_website_on_religion.topics LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
		wp_v_website_on_religion.tags =  '$search_string',  3,
			IF(
				 wp_v_website_on_religion.tags LIKE '%$search_string%', 2, 0

			  )


	) AS weight
	FROM wp_v_website_on_religion ";

	//var_dump($sub_type);
	if(!empty($sub_type)){
		$sql.="WHERE type IN ('" . implode("','", $sub_type) . "')";
	}
	$sql.="
	HAVING weight > 0";
	if ($sort_by == 'title'){
        $sql .= "
	ORDER BY regex_replace('[^a-zA-Z\-]','',$sort_by) $sort_direction" . $limit_sql . $offset_sql;
    }else {
        $sql .= "
	ORDER BY $sort_by $sort_direction" . $limit_sql . $offset_sql;
    }
	//var_dump($sql);

	$wor = $wpdb->get_results($sql);
	//var_dump($wor);
	//
	return $wor;
}

function search_website_on_religion_total($search_string, $sub_type = array()){
	global $wpdb;

	$clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	// where do you want to search
	$fields = array('title', 'annotation', 'type',  'topics', 'tags');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_website_on_religion.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_website_on_religion.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);
  //exit();

	$sql="SELECT wp_v_website_on_religion.id,
	if(
		wp_v_website_on_religion.title = '$search_string',  100,
	         IF(
	             wp_v_website_on_religion.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_website_on_religion.annotation =  '$search_string',  40,
	        IF(
	             wp_v_website_on_religion.annotation LIKE '%$search_string%', 30,

	                 if( {$terms_query['annotation']}, 5, 0)

	          )


	)
	+
	IF(
	    wp_v_website_on_religion.type =  '$search_string',  3,
	        IF(
	             wp_v_website_on_religion.type LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
	    wp_v_website_on_religion.topics =  '$search_string',  3,
	        IF(
	             wp_v_website_on_religion.topics LIKE '%$search_string%', 2, 0

	          )


	)
	+
	IF(
		wp_v_website_on_religion.tags =  '$search_string',  3,
			IF(
				 wp_v_website_on_religion.tags LIKE '%$search_string%', 2, 0

			  )


	) AS weight
	FROM wp_v_website_on_religion ";

	//var_dump($sub_type);
	if(!empty($sub_type)){
		$sql.="WHERE type IN ('" . implode("','", $sub_type) . "')";
	}
	$sql.="
	HAVING weight > 0";
	//var_dump($sql);

	$wors = $wpdb->get_results($sql);
	//var_dump($wor);
	//
	return COUNT($wors);


}
