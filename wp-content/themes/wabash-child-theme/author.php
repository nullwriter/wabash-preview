<?php

/*
Template Name: author
*/

//* Remove the post info function
remove_action( 'genesis_loop', 'genesis_do_loop' );

error_reporting(E_ALL ^ E_DEPRECATED);

// FORCE FULL WIDTH LAYOUT
add_filter ( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );

function mythemename_all_scriptsandstyles()
{
    wp_register_style('customsearchcss', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');
    wp_enqueue_style('customsearchcss');
    wp_register_style('panel', get_stylesheet_directory_uri().'/css/panel.css', array(), '1', 'all');
    wp_enqueue_style('panel');


    wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
    wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
    wp_enqueue_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
    wp_enqueue_script('clipboard', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);

    $home_url = array(
        'home' => home_url()
    );
    wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );
}

add_action('wp_enqueue_scripts', 'mythemename_all_scriptsandstyles');


// Add our custom loop
add_action('genesis_loop', 'cd_goh_loop');
function cd_goh_loop()
{
    //$paged   = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    global $post;
    global $query_args; // grab the current wp_query() args
    global $author;
   global $paged;
    /*var_dump($paged);
    exit;*/

    $args = array(
        'author'           => $author,
        'post_type' => 'post',
        'post_status' => 'publish',
        'paged'          => $paged // respect pagination
    );

    echo display_custom_panel();

    $html = '';

    $loop = new WP_Query( $args );
    if( $loop->have_posts() ) {
        // loop through posts
        while( $loop->have_posts() ): $loop->the_post();
            if ( 'post' == get_post_type() ) {

                $blog = get_post_object(get_the_ID());
                $html = display_tile_blog_object($blog);
                echo $html;

            }elseif ( 'page' == get_post_type() ) {

                $program = get_program_object(get_the_ID());
                $html = display_tile_program_object($program);
                echo $html;

            }elseif ( 'book_reviews' == get_post_type() ) {

                $book=get_book_reviews_object(get_the_ID());
                $html = display_tile_book_reviews_object($book);
                echo $html;

            }elseif ( 'grants' == get_post_type() ) {

                $grant=get_grant_object(get_the_ID());
                $html = display_tile_grant_object($grant);
                echo $html;

            }elseif ( 'syllabi' == get_post_type() ) {

                $syllabus=get_syllabi_object(get_the_ID());
                $html = display_tile_syllabi_object($syllabus);
                echo $html;

            }elseif ( 'website_on_religion' == get_post_type() ) {

                $wor = get_website_on_religion_object(get_the_ID());
                $html = display_tile_wor_object($wor);
                echo $html;

            }elseif ( 'scholarship' == get_post_type() ) {

                $scholar=get_scholarship_object(get_the_ID());
                $html = display_tile_scholarship_object($scholar);
                echo $html;

            }elseif ( 'video' == get_post_type() ) {

                $video = get_video_object(get_the_ID());
                $html = display_tile_video_object($video);
                echo $html;

            }
        endwhile;
        do_action( 'genesis_after_endwhile' );
    }
    //echo $loop->request;
    wp_reset_postdata();
    /* If you want to show posts from a category only and no subcategory posts, use 'category_name' => 'category-slug', instead of 'cat' => 8, for example, 'category_name' => 'articles', */
}
genesis();
