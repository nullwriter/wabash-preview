<?php
/*
Custom template for date queries
*/


wp_register_style('custom-search', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');
wp_enqueue_style('custom-search');

wp_register_style('panel', get_stylesheet_directory_uri().'/css/panel.css', array(), '1', 'all');
wp_enqueue_style('panel');


wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
wp_enqueue_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
wp_enqueue_script('clipboard', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);
$home_url = array(
    'home' => home_url()
);
wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );

//* Remove the post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
//* Remove the author box on single posts
remove_action( 'genesis_after_entry', 'genesis_do_author_box_single', 8 );
//* Remove the post meta function
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
//Remove Title
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'sk_do_single_search_loop');



function sk_do_single_search_loop()
{
    global $paged; // current paginated page
    echo '<div class="container mixed-results">';
    echo '<div class="row">';
    echo '<!-- this is the paging loader, now is hidden, it will be shown when we scroll to bottom -->
		  <div id="loader"><img src="' . get_stylesheet_directory_uri() . '/images/ajax-loader.gif"></div>';



    $html = display_custom_panel();

    if (have_posts()) :
        $count=0;
        while (have_posts()) : the_post();
            $count++;
            $post_id=get_the_ID();
            $item = get_post_object($post_id);
            $html .= display_tile_blog_object($item);
        endwhile;
        echo display_results_panel('Posts', $count, $html);
        // display sidebar
        $html = '<aside class="sidebar sidebar-primary widget-area col-sm-3">';
        $page = get_page_by_title( "custom-sidebar-post");
        $html .=apply_filters( 'the_content', $page->post_content );
        $html .='</aside>';
        $html .='</div>';
        $html .='</div>';
        echo $html;
    endif;
}
genesis();
