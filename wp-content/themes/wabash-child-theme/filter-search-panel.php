<?php
/*
Template Name: filter-search-panel
*/
error_reporting(E_ALL);
ini_set('display_errors', 1);
function single_results_all_scriptsandstyles(){
    //Load JS and CSS files in here

    wp_register_script('singleResults', get_stylesheet_directory_uri().'/js/search/singleResults2.js');
    wp_register_script('share', get_stylesheet_directory_uri().'/js/search/share.js');
    wp_register_script('infinitescroll', get_stylesheet_directory_uri().'/js/search/infinite_scroll.js');
    wp_register_script('videos_script', get_stylesheet_directory_uri().'/js/videos.js');
    wp_register_script('clipboard-script', get_stylesheet_directory_uri().'/js/clipboard.min.js');
    wp_register_script('single_results_right_sidebar_script', get_stylesheet_directory_uri().'/js/single_result_filter_panel2.js');
    wp_register_style('awesomecheckbox', get_stylesheet_directory_uri().'/css/awesome-bootstrap-checkbox.css', array(), '1', 'all');
    wp_register_style('customsearchcss', get_stylesheet_directory_uri().'/css/custom_search2.css', array(), '1', 'all');
    wp_register_style('single_results_right_sidebar_css', get_stylesheet_directory_uri() . '/css/single_result_filter_panel2.css', false, '1.0.0', 'all');

    $home_url = array(
        'home' => home_url()
    );
    wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );

    wp_enqueue_script('singleResults');
    wp_enqueue_script('infinitescroll');
    wp_enqueue_script('videos_script');
    wp_enqueue_script('clipboard-script');
    wp_enqueue_style('awesomecheckbox');
    wp_enqueue_style('customsearchcss');
    /*wp_enqueue_script('share');*/
    wp_enqueue_style('single_results_right_sidebar_css');
    wp_enqueue_script('single_results_right_sidebar_script');

    //Infinite Scroll
    $translation_array = array( 'template_url' => get_stylesheet_directory_uri() );
    wp_localize_script( 'infinitescroll', 'url_object', $translation_array );
}
add_action('wp_enqueue_scripts', 'single_results_all_scriptsandstyles');
get_header();
if ( have_posts() ) : while ( have_posts() ) : the_post();
    the_content();
endwhile; else: ?>
    <p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
get_footer();
