<?php
/**
 * Front page template
 *
 * @package      Wabash Bootstrap Genesis
 * @author       soliantconsulting
 * @link         http://www.soliantconsulting.com/
 * @copyright    Copyright (c) 2015, soliantconsulting
 * @license      GPL-2.0+
 */

genesis();
