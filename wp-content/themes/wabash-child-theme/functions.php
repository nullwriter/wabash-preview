<?php
/**
 * Theme customizations
 *
 * @package   Wabash Bootstrap Genesis
 * @author    soliantconsulting
 * @link      http://www.soliantconsulting.com
 * @copyright Copyright (c) 2015, soliantconsulting
 * @license   GPL-2.0+
 */
 //* Start the engine
 //include_once( get_template_directory() . '/lib/init.php' );

 //* Child theme (do not remove)
 define('CHILD_THEME_NAME', 'Wabash Bootstrap Genesis');
 define('CHILD_THEME_URL', 'http://www.soliantconsulting.com/');
 define('CHILD_THEME_VERSION', '1.0.0');

if (!function_exists('write_log')) {

	function write_log($log) {
		if (true === WP_DEBUG) {
			if (is_array($log) || is_object($log)) {
				error_log(print_r($log, true));
			} else {
				error_log($log);
			}
		}
	}

}

/**
  * Include all php files in the /includes directory
  *
  * https://gist.github.com/theandystratton/5924570
  */
 add_action('genesis_setup', 'bsg_load_lib_files', 15);

function bsg_load_lib_files()
{

    foreach ( glob(dirname(__FILE__) . '/lib/*.php') as $file ) {
        include $file;
    }
}

add_action('genesis_after_header', 'add_my_header_widget_area', 15);

function add_my_header_widget_area()
{
    if (function_exists('dynamic_sidebar')) {
        dynamic_sidebar('Extra Header Widget Area');
        dynamic_sidebar('Header Image Widget Area');
    }
}

// Add div.wrap inside of div#inner
function child_before_inside_header()
{
    echo '<div class="wrap-inner-header container">';
}

add_action('genesis_header', 'child_before_inside_header', 5);

// Add div.wrap inside of div#inner
function child_after_inside_header()
{
    echo '</div>';
}

add_action('genesis_header', 'child_after_inside_header', 15);


// Add div.wrap inside of div#inner
function child_before_header()
{
    echo '<div class="wrap-header">';
}

add_action('genesis_before_header', 'child_before_header');

function child_after_header()
{
    echo '</div><!-- end .wrap -->';
}

add_action('genesis_after_header', 'child_after_header', 15);


add_action('wp_enqueue_scripts', 'cyb_enqueue_scripts');



function cyb_enqueue_scripts() {
    //Change the key and url with yours
    /*wp_register_script('website_religion_js2', plugins_url().'/wabash-shortcodes/js/open-close-wor-list.js', array('jquery'), '1', true);*/
     //
      wp_enqueue_script('website_religion_js2');
     // wp_enqueue_script('gpmap');
     // wp_enqueue_script('map');
     /*wp_enqueue_script('website_religion_js');
     wp_enqueue_style('website_religion_css');
     wp_enqueue_script('website_custom_js');
     wp_enqueue_style('website_custom_css');*/

    //Localize script data to be used in my-js.js
    $scriptData = array();
    $scriptData['ajaxurl'] = '../wor-result-wrapper';
    $scriptData['action'] = '';

    wp_localize_script( 'website_religion_js2', 'my_js_data', $scriptData );

}

// Add Google Tag Manager code immediately below opening <body> tag
add_action( 'genesis_before', 'sk_google_tag_manager' );
function sk_google_tag_manager() { ?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WMXP56K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php }

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

    //REMOVE AUTHOR BOX
    add_action('wp_loaded', 'override_author_box');
    function override_author_box()
    {
        if(is_singular('book_reviews') || is_singular('video')) {
            return;
        }
            remove_action('genesis_after_entry', 'gcap_author_box', 1);
    }

    //ADD CUSTOM AUTHOR BOX AUTHOR
    add_action('wp_loaded', 'my_plugin_override');
    function my_plugin_override()
    {
        if(is_singular('book_reviews') || is_singular('video') || is_singular('syllabi') || is_singular('website_on_religion') || is_singular('scholarship') || is_singular('grants') || is_archive()) {
        return;
        }
            remove_action('genesis_after_entry', 'gcap_author_box', 1);
            add_action('genesis_after_entry', 'custom_gcap_author_box', 1);
    }

    function custom_gcap_author_box()
    {
        if(is_singular('book_reviews') || is_singular('video') || is_singular('syllabi') || is_singular('website_on_religion') || is_singular('scholarship') || is_singular('grants')) {
            return;
        }
        if (!is_single())
		return;

        if (function_exists('get_coauthors')) {

		$authors = get_coauthors();
            foreach ($authors as $author) {
                custom_gcap_do_author_box('single', $author);
		}
        } else custom_gcap_do_author_box('single', get_the_author_ID());
	}

    function custom_gcap_do_author_box($context = '', $author, $echo = true)
    {
        if (!$author)
		return;

        $gravatar_size = apply_filters('genesis_author_box_gravatar_size', 70, $context);
        $gravatar = get_avatar($author->user_email, $gravatar_size);
        $description = wpautop($author->description);

	    //* The author box markup, contextual
        if (genesis_html5()) {

            $title = apply_filters('genesis_author_box_title', sprintf('%s <span itemprop="name">%s</span>', __('About', 'genesis'), $author->display_name), $context);

            $pattern = sprintf('<section %s>', genesis_attr('author-box'));
		$pattern .= '%s<h2 class="author-box-title">%s</h2>';
		$pattern .= '<div class="author-box-content" itemprop="description">%s</div>';
		$pattern .= '</section>';

        } else {

            $title = apply_filters('genesis_author_box_title', sprintf('<strong>%s %s</strong>', __('About', 'genesis'), $author->display_name), $context);

		$pattern = 'single' === $context ? '<div class="author-box"><div>%s %s<br />%s</div></div>' : '<div class="author-box">%s<h1>%s</h1><div>%s</div></div>';

	}

        $output = apply_filters('genesis_author_box', sprintf($pattern, $gravatar, $title, $description), $context, $pattern, $gravatar, $title, $description);

        if ($echo)
		echo $output;
	else
		return $output;
    }

    function rudr_filter_by_the_author()
    {
        if(is_singular('book_reviews') || is_singular('syllabi') || is_singular('website_on_religion') || is_singular('scholarship') || is_singular('grants') || is_archive()) {
            return;
        }
	$params = array(
		'name' => 'author', // this is the "name" attribute for filter <select>
		'show_option_all' => 'All authors' // label for all authors (display posts without filter)
	);

        if (isset($_GET['user']))
		$params['selected'] = $_GET['user']; // choose selected user by $_GET variable

        wp_dropdown_users($params); // print the ready author list
    }

    add_action('restrict_manage_posts', 'rudr_filter_by_the_author');

//END DISABLE AUTHOR BOX

function add_select2_script(){
	wp_enqueue_script('select2', get_stylesheet_directory_uri() . '/js/select2.full.min.js', array('jquery'), '1.0', 'all'); // Enqueue it!
}
add_action('wp_enqueue_scripts', 'add_select2_script');

function add_select2_style(){
	wp_register_style('select2style', get_stylesheet_directory_uri() . '/css/select2.min.css', array(), '1.0', 'all');
	wp_enqueue_style('select2style'); // Enqueue it!
}
add_action('wp_enqueue_scripts', 'add_select2_style'); // Add Theme Stylesheet

function collapse_menu_js()
{
    wp_enqueue_script('collapse_menu', get_stylesheet_directory_uri() . '/js/menu.js', array('select2'), '1.0', true);
}

add_filter('wp_enqueue_scripts', 'collapse_menu_js');

function social_icons()
{
    wp_register_style('simple_social_icons', get_stylesheet_directory_uri() . '/css/simple-social-icons.css', false, '1.0.0', 'all');
    wp_enqueue_style('simple_social_icons');
}

add_filter('wp_enqueue_scripts', 'social_icons');

add_action('wp_enqueue_scripts', 'sk_disable_simple_social_icons_styles', 11);

function sk_disable_simple_social_icons_styles()
{
    if (class_exists('Simple_Social_Icons_Widget')) {
        /** Dequeue icon styles */
        wp_dequeue_style('simple-social-icons-font');
    }
}

//Front Slider Tweaks
function front_slider_tweak()
{
    if (is_front_page() || is_page('Home (Copy)')) {
        wp_enqueue_script('front_slider-script', get_stylesheet_directory_uri() . '/js/front-slider.js', array(), '1.0', true);
    }
}
add_action('wp_enqueue_scripts', 'front_slider_tweak');
//Remove Header Banner in the front page
function remove_banner()
{
    if (is_front_page() || is_page('Home (Copy)')) {
        wp_enqueue_script('front_banner-script', get_stylesheet_directory_uri() . '/js/front_banner.js', array(), '1.0', true);
    }
}
add_action('wp_enqueue_scripts', 'remove_banner');
// make home copy page the same as home
function wpsites_remove_genesis_breadcrumbs() {
    if ( is_page('Home (Copy)') ){
        remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
    }
}
add_action( 'genesis_before', 'wpsites_remove_genesis_breadcrumbs' );


//After Load the theme and all Core Function from genesis we can remove some actions
add_action('after_setup_theme', function () {
    global $load_more_data;

    $load_more_data=false;

    //ADD VIDEOS SIDEBAR
    genesis_register_sidebar(array(
        'id' => 'videos-sidebar',
        'name' => __('Videos Sidebar', 'GenStarter'),
        'description' => __('This is the Video Sidebar', 'GenStarter'),
    ));
    //ADD BOOK REVIEWS SIDEBAR
    genesis_register_sidebar(array(
        'id' => 'book_reviews-sidebar',
        'name' => __('Book Reviews Sidebar', 'GenStarter'),
        'description' => __('This is the Book Reviews Sidebar', 'GenStarter'),
    ));
    //ADD SYLLABI SIDEBAR
    genesis_register_sidebar(array(
        'id' => 'syllabi-sidebar',
        'name' => __('Syllabi Sidebar', 'GenStarter'),
        'description' => __('This is the Syllabi Sidebar', 'GenStarter'),
    ));
    //ADD WoR SIDEBAR
    genesis_register_sidebar(array(
        'id' => 'website_on_religion-sidebar',
        'name' => __('Website on Religion Sidebar', 'GenStarter'),
        'description' => __('This is the Website on Religion Sidebar', 'GenStarter'),
    ));
    //ADD WoR SIDEBAR
    genesis_register_sidebar(array(
        'id' => 'grants-sidebar',
        'name' => __('Grants Sidebar', 'GenStarter'),
        'description' => __('This is the Grants Sidebar', 'GenStarter'),
    ));
    //ADD scholarship SIDEBAR
    genesis_register_sidebar(array(
        'id' => 'scholarship-sidebar',
        'name' => __('Scholarship Sidebar', 'GenStarter'),
        'description' => __('This is the Scholarship on Teaching Sidebar', 'GenStarter'),
    ));
});

add_action('get_header', 'cd_change_genesis_sidebar');
function cd_change_genesis_sidebar()
{
    global $post_type;

    if ( is_singular( 'post' ) || is_category() || is_author() || is_tag() ) {
        remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
        add_action('genesis_sidebar', 'cd_do_sidebar_blogs'); //add an action hook to call the function for my custom sidebar
    }

    //Check if we're on a single post for my CPT called "video"
    if (is_singular('video') || is_page('videos')) {
        remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
        add_action('genesis_sidebar', 'do_videos_sidebar'); //add an action hook to call the function for my custom sidebar
    }


    //Check if we're on a single post for my CPT called "book_reviews"
    if (is_singular('book_reviews') || is_page('book-reviews') || $post_type=='book_reviews') {
        remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
        add_action('genesis_sidebar', 'do_book_reviews_sidebar'); //add an action hook to call the function for my custom sidebar
    }
    //Check if we're on a single post for my CPT called "syllaby"
    if (is_singular('syllabi') || is_page('syllabi') || $post_type=='syllabi') {
        remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
        add_action('genesis_sidebar', 'do_syllabi_sidebar'); //add an action hook to call the function for my custom sidebar
    }
     //Check if we're on a single post for my CPT called "scholarship"
    if (is_singular('scholarship') || is_page('scholarship') || $post_type=='scholarship') {
        remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
        add_action('genesis_sidebar', 'do_scholarship_sidebar'); //add an action hook to call the function for my custom sidebar
    }

    //Check if we're on a single post for my CPT called "website_on_religion"
   if (is_singular('website_on_religion') || is_page('website_on_religion') || $post_type=='website_on_religion') {
       remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
       add_action('genesis_sidebar', 'do_website_on_religion_sidebar'); //add an action hook to call the function for my custom sidebar
   }

   //Check if we're on a single post for my CPT called "grants"
  if (is_singular('grants') ||  $post_type=='grants') {
      remove_action('genesis_sidebar', 'genesis_do_sidebar'); //remove the default genesis sidebar
      add_action('genesis_sidebar', 'do_grants_sidebar'); //add an action hook to call the function for my custom sidebar
  }
}
//FUNCTION TO SHOW VIDEOS SIDEBAR

//Function to output my custom sidebar
function cd_do_sidebar_blogs()
{
    dynamic_sidebar('sidebar');
}
function do_videos_sidebar()
{
    dynamic_sidebar('videos-sidebar');
}
//FUNCTION TO SHOW BOOK REVIEWS SIDEBAR
function do_book_reviews_sidebar()
{
    dynamic_sidebar('book_reviews-sidebar');
}
//FUNCTION TO SHOW syllabi SIDEBAR
function do_syllabi_sidebar()
{
    dynamic_sidebar('syllabi-sidebar');
}
//FUNCTION TO SHOW website_on_religion SIDEBAR
function do_website_on_religion_sidebar()
{
    dynamic_sidebar('website_on_religion-sidebar');
}
//FUNCTION TO SHOW website_on_religion SIDEBAR
function do_grants_sidebar()
{
    dynamic_sidebar('grants-sidebar');
}
//FUNCTION TO SHOW scholarship SIDEBAR
function do_scholarship_sidebar()
{
    dynamic_sidebar('scholarship-sidebar');
}
//UPDATE GENESIS POSTS INFO (TEXT BELOW THE TITLE (BOOK_REVIEWS AND OTHERS))
add_filter('genesis_post_info', 'sp_post_info_filter', 20);
function sp_post_info_filter($post_info)
{
    global $post_type;
    if ($post_type=='book_reviews' || $post_type=='video' || $post_type=='syllabi' || $post_type=='website_on_religion'
    || $post_type=='scholarship' || $post_type=='grants' || is_archive()) {
        remove_action('genesis_entry_header', 'genesis_post_info', 12);
        /*$post_info = '[post_date] [post_comments] [post_edit]';
        return $post_info;*/
    }

}

function be_add_blog_crumb( $crumb, $args ) {

    if ( is_singular( 'post' ) || is_category())
        return '<a href="' . get_permalink( get_page_by_title( 'Resources' ) ) . '">' . "Resources" .'</a>' . $args['sep'] . '<a href="' . get_permalink( get_page_by_path( 'resources/blog' )  ) . '">' . 'Blog' .'</a> ' . $args['sep'] . ' ' . $crumb;
    else
    return $crumb;
}
add_filter( 'genesis_single_crumb', 'be_add_blog_crumb', 10, 2 );
add_filter( 'genesis_archive_crumb', 'be_add_blog_crumb', 10, 2 );
add_filter( 'genesis_page_crumb', 'be_add_blog_crumb', 10, 2 );


//For category book reviews display Resources page in breadcrumb
function be_add_resources_crumb( $crumb, $args ) {
    if ( is_singular( 'book_reviews' ) || is_post_type_archive( 'book_reviews' ))
        return '<a href="' . get_permalink( get_page_by_title( 'Resources' ) ) . '">' . "Resources" .'</a> ' . $args['sep'] . ' ' . $crumb;
    elseif(is_tax('website-topic')){
        $crumb_array=explode('>',$crumb);
        $crrent_page = array_pop($crumb_array);
        return '<a href="' . get_permalink( get_page_by_title( 'Websites on Religion' ) ) . '">' . "Websites on Religion" .'</a> '. $args['sep'] .  $crrent_page;
    }
    elseif(is_tax('website-type')){
        $crumb_array=explode('>',$crumb);
        $crrent_page = array_pop($crumb_array);
        return '<a href="' . get_permalink( get_page_by_title( 'Websites on Religion' ) ) . '">' . "Websites on Religion" .'</a> '. $args['sep'] .  $crrent_page;
    }
    elseif(is_tax('syllabi-topic')){
        $crumb_array=explode('>',$crumb);
        $crrent_page = array_pop($crumb_array);
        return '<a href="' . get_permalink( get_page_by_title( 'Syllabi' ) ) . '">' . "Syllabi" .'</a> '. $args['sep'] .  $crrent_page;
    }

    else
        return $crumb;
}
add_filter( 'genesis_single_crumb', 'be_add_resources_crumb', 10, 2 );
add_filter( 'genesis_archive_crumb', 'be_add_resources_crumb', 10, 2 );

//To add [content type] in search results
function be_add_cpt_crumb( $crumb, $args ) {
    $single_results_page=is_page_template('template-section-results-page.php');
    if ( $single_results_page ) {
        // get post types active for serach
        if(!empty($_GET['pt'])){
            $post_types[$_GET['pt']] = $_GET['pt'];
        }
        elseif(!empty($_POST['pt']))
        {
            $post_types[$_POST['pt']] = $_POST['pt'];
        }
        else{
          if (!empty($_GET['st'])){
              $st = $_GET['st'];
          }
          elseif(!empty($_POST['st'])){
              $st = $_POST['st'];
          }
          else{
            $home_url = get_home_url() ;
              wp_redirect( $home_url );
          }

          $home_url = get_home_url() .'/?s='.$st;
          wp_redirect( $home_url );

        }
        switch(key($post_types)){
            case 'page':
                return '<a href="' . get_permalink( get_page_by_path( '/programs' )  ) . '">' . 'Wabash Programs' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'grants':
                return '<a href="' . get_permalink( get_page_by_path( '/wabash-grants' )  ) . '">' . 'Awarded Grants' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'post':

                return '<a href="' . get_permalink( get_page_by_path( '/blog' )  ) . '">' . 'Blog' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'scholarship':

                return '<a href="' . get_permalink( get_page_by_path( '/resources/scholarship-on-teaching' )  ) . '">' . 'Scholarship on Teaching' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'website_on_religion':

                return '<a href="' . get_permalink( get_page_by_path( '/resources/website-on-religion' )  ) . '">' . 'Websites on Religion' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'syllabi':

                return '<a href="' . get_permalink( get_page_by_path( '/resources/syllabi' )  ) . '">' . 'Syllabi' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'book_reviews':

                return '<a href="' . get_permalink( get_page_by_path( '/resources/book-reviews/' )  ) . '">' . 'Book Reviews' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;
            case 'video':

                return '<a href="' . get_permalink( get_page_by_path( '/resources/videos/' )  ) . '">' . 'Video' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;

            case 'ttr':

                return '<a href="' . get_permalink( get_page_by_path( '/resources/ttr/' )  ) . '">' . 'Teaching Theology and Religion' .'</a> ' . $args['sep'] . ' ' . 'Search Results';
                break;

        }

    }else{
        return $crumb;
    }

};

add_filter( 'genesis_page_crumb', 'be_add_cpt_crumb', 10, 2 );

function be_add_selected_resources_crumb( $crumb, $args ) {
    $selected_resources_page=is_page_template('mixed-results-page.php');
    if ( $selected_resources_page ) {
        return '<a href="' . get_permalink( get_page_by_title( 'Resources' ) ) . '">' . "Resources" .'</a> ' . $args['sep'] . ' ' . $crumb;

    }else{
        return $crumb;
    }

};

add_filter( 'genesis_page_crumb', 'be_add_selected_resources_crumb', 10, 2 );

//Change book reviews URL
add_action('registered_post_type', 'change_slug_bookreviews', 10, 2);
function change_slug_bookreviews($post_type, $args) {
    global $wp_rewrite;
    if ($post_type == 'book_reviews') {

        $args->rewrite['slug'] = 'resources/book_reviews'; //write your new slug here

        if ( $args->has_archive ) {
            $archive_slug = $args->has_archive === true ? $args->rewrite['slug'] : $args->has_archive;
            if ( $args->rewrite['with_front'] )
                $archive_slug = substr( $wp_rewrite->front, 1 ) . $archive_slug;
            else
                $archive_slug = $wp_rewrite->root . $archive_slug;

            add_rewrite_rule( "{$archive_slug}/?$", "index.php?post_type=$post_type", 'top' );
            if ( $args->rewrite['feeds'] && $wp_rewrite->feeds ) {
                $feeds = '(' . trim( implode( '|', $wp_rewrite->feeds ) ) . ')';
                add_rewrite_rule( "{$archive_slug}/feed/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
                add_rewrite_rule( "{$archive_slug}/$feeds/?$", "index.php?post_type=$post_type" . '&feed=$matches[1]', 'top' );
            }
            if ( $args->rewrite['pages'] )
                add_rewrite_rule( "{$archive_slug}/{$wp_rewrite->pagination_base}/([0-9]{1,})/?$", "index.php?post_type=$post_type" . '&paged=$matches[1]', 'top' );
        }

        $permastruct_args = $args->rewrite;
        $permastruct_args['feed'] = $permastruct_args['feeds'];
        add_permastruct( $post_type, "{$args->rewrite['slug']}/%$post_type%", $permastruct_args );
    }
}
//Add Jquery Cookie Script (Only in Single Results Page)
function jquery_cookie_script()
{
    if (is_page('single-results')) {
        wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
    }
}
add_action('wp_enqueue_scripts', 'jquery_cookie_script');

//Add Scripts (Only in Single Results Page,to make right hand sidebar floating)
function single_results_right_sidebar_scripts()
{
    if (is_page('single-results') || is_page('Selected Resources')) {
        wp_register_style('single_results_right_sidebar_css', get_stylesheet_directory_uri() . '/css/single_result_filter_panel.css', false, '1.0.0', 'all');
        wp_enqueue_style('single_results_right_sidebar_css');
        wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
        $home_url = array(
            'home' => home_url()
        );
        wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );
    }
}
add_filter('wp_enqueue_scripts', 'single_results_right_sidebar_scripts');
//Remove Comments from Details Pages
function pw_remove_genesis_comments() {
    global $post_type;
    //Video Detail Page
    if($post_type=='video') {
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
    }
    elseif($post_type=='book_reviews') {
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
    }
    elseif($post_type=='syllabi') {
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
    }
    elseif($post_type=='website_on_religion') {
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
    }
    elseif($post_type=='scholarship') {
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
    }
    elseif($post_type=='grants') {
        remove_action('genesis_after_entry', 'genesis_get_comments_template');
    }
}
add_action( 'wp_enqueue_scripts', 'pw_remove_genesis_comments' );



//Script to be used in the footer
function footer_scripts() {
    wp_register_script('footer_contact_script', get_stylesheet_directory_uri() . '/js/footer_script.js', array(), '1.0', true);
    $subscription_success = get_field('subscription_success_message', 'option');

    $contact_success = get_field('contact_us_success_message', 'option');

    $subscriber_message = array(
        'msg' => $subscription_success
    );
    $contact_message = array(
        'msg' => $contact_success
    );
    wp_localize_script( 'footer_contact_script', 'SUBSCRIBER', $subscriber_message );
    wp_localize_script( 'footer_contact_script', 'CONTACT', $contact_message );
    wp_enqueue_script( 'footer_contact_script' );
}
add_action( 'wp_enqueue_scripts', 'footer_scripts' );


//To use the ajaxurl var in the child theme
add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {
    echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

// allow to send html mails
add_filter( 'wp_mail_content_type', 'set_wabash_email_content_type' );
function set_wabash_email_content_type( $content_type ) {
    return 'text/html';
}

function contact_form() {
    //code
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $final_message = "From: ".$email."<br>";
    $final_message.= "Name: ".$name."<br>";
    $final_message.= "Message: ".$message;
    $contact_email = get_field('contact_us_email_message', 'option');
    $group_emails = array(
        'wrens@wabash.edu',
        'pearsont@wabash.edu',
        'lnoguera@soliantconsulting.com'
    );
    if(wp_mail( $group_emails, "Wabash Contact Us", $final_message)){
        wp_mail($email,"Wabash Center Contact","Hello ".$name."<br>".$contact_email);
        die('1');
    }else{
        die('2');
    }

}
add_action('wp_ajax_contact_form', 'contact_form');
add_action('wp_ajax_nopriv_contact_form', 'contact_form');

function subscriber_form() {
    //code
    $name = $_POST['name_subscriber'];
    $email = $_POST['email_subscriber'];
    $message = "New subscriber: "  .$name."<br>Email: ".$email;
    $subscription_email = get_field('subscription_email_message', 'option');
    $group_emails = array(
        'wrens@wabash.edu',
        'pearsont@wabash.edu'.
        'lnoguera@soliantconsulting.com'
    );
    if(wp_mail( $group_emails, "New Subscriber", $message )){
        wp_mail($email,"Wabash Center Subscription","Hello ".$name." <br>".$subscription_email);
        die('1');
    }else{
        die('2');
    }

}
add_action('wp_ajax_subscriber_form', 'subscriber_form');
add_action('wp_ajax_nopriv_subscriber_form', 'subscriber_form');

add_action( 'init', 'my_custom_init' );
function my_custom_init() {
	remove_post_type_support( 'grants', 'comments' );
    remove_post_type_support( 'scholarship', 'comments' );



}

function limit_text($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
}

function display_results_panel( $post_types_names, $total, $content, $taxonomy='', $term='',$subtypes_titles='', $sort_panel_html=''){
    global $post_types;

    $bsclass = 'col-md-6';
    if (key($post_types) == 'book_reviews' || key($post_types) == 'post') {
        $bsclass = 'col-md-9';
    }

    $title = $post_types_names . $subtypes_titles.'  - ' .((!empty($taxonomy) && !empty($term))?$taxonomy.': '.$term.' - ':''). $total .' results';

    if ($taxonomy == 'selected_resources') {
        $title = $total . ' ' . $post_types_names . $subtypes_titles.' ' .((!empty($taxonomy) && !empty($term))?$taxonomy.': '.$term.' - ':'');
    }

	$html = $sort_panel_html.'
        <div class="'.$bsclass.' col-sm-9 col-xs-12 single_results_main_container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="title-tiles">
                        <h2> ' . $title . ' </h2>
                        <span style="width:100%;display: inline-block;text-align: right;">Select an item by clicking its checkbox</span>
                    </div>
                    '.$content.'
                </div>
            </div>
        </div>
    ';

    return $html;
}


function display_excerpt_program_object($program){

    $html = '<p><a href="'.get_permalink($program->id).'">' .$program->title.'</a><br/>';
    $html .=limit_text(wp_strip_all_tags( do_shortcode($program->content) ), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_program_object($item){

    global $load_more_data;

    $single_results_page=is_page_template('template-section-results-page.php') || is_page_template('template-selected-resources-page.php') || $load_more_data;
    $detail_page=is_singular() && !$single_results_page;

    $content= limit_text( wp_strip_all_tags( do_shortcode($item->content) ), 50);


$html = '
<div class="panel card program-card">
   <div class="panel-body">
      <div class="row is-flex">
          <div class="card-info col-xs-11 col-sm-11 col-md-10">
             <h3 class="card-title">
                <a class="" rel="bookmark" href="'.get_permalink($item->id).'">'.$item->title.'</a>
             </h3>
          </div>
          <div class="card-select col-xs-1 col-sm-1 col-md-1 col-md-offset-1">';


                $html .= '<div class="card-checkbox">
                            <input value="'.$item->id.'" id="resource_'.$item->id.'" class="case" type="checkbox">
                        </div>';


            $html .= ' </div>

      </div>
      <div class="row">
         <div class="card-content-container col-xs-12">
            <div class="card-content">
            <p>' . $content . '</p>';


          if (!$detail_page) {
              $html .= '<div id="' . $item->id . '" class="more-link-single-results_blogs1">
                                              <a title="Read More" target="_blank" href="' .get_permalink($item->id). '">Details
                                              </a>
                                              <i class="fa fa-caret-right" aria-hidden="true"></i>
                                          </div>';
          }
           $html .= '

            </div>
         </div>
      </div>
   </div>
</div>';

    return $html;

}

function display_page_sort_panel($sort_term){
  $html='';
  return $html;
}

function get_program_object($id){
    global $wpdb;

    $query = $wpdb->prepare('SELECT * FROM wp_v_programs WHERE id = %s LIMIT 1', $id);
    $program = $wpdb->get_row($query);

    return $program;
}

function search_programs($search_string, $limit='', $offset=''){
	global $wpdb;

    $limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
    }

    $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

    $search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);

    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }
	// where do you want to search
	$fields = array('title', 'content');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_programs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_programs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);


	$sql="SELECT wp_v_programs.*,
	if(
		wp_v_programs.title = '$search_string',  100,
	         IF(
	             wp_v_programs.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_programs.content =  '$search_string',  40,
	        IF(
	             wp_v_programs.content LIKE '%$search_string%', 30,0

	          )


	) AS weight
	FROM wp_v_programs
	HAVING weight > 0
	ORDER BY weight DESC".$limit_sql.$offset_sql;


    //var_dump($sql);

	$programs = $wpdb->get_results($sql);
	//var_dump($syllabi);
	//
	return $programs;


}

function search_programs_total($search_string){
	global $wpdb;


    $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	// where do you want to search
	$fields = array('title', 'content');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_programs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_programs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);


	$sql="SELECT wp_v_programs.id,
	if(
		wp_v_programs.title = '$search_string',  100,
	         IF(
	             wp_v_programs.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_programs.content =  '$search_string',  40,
	        IF(
	             wp_v_programs.content LIKE '%$search_string%', 30,0

	          )


	) AS weight
	FROM wp_v_programs
	HAVING weight > 0";


    //var_dump($sql);

	$programs = $wpdb->get_results($sql);
	//var_dump($syllabi);
	//
	return COUNT($programs);


}

function display_excerpt_blog_object($blog){

    $html = '<p><a href="'.get_permalink($blog->id).'">' .$blog->title.'</a><br/>';
    $html .=limit_text(wp_strip_all_tags( do_shortcode($blog->content) ), 50);

	$html .= '</p>';

	return $html;
}

function display_tile_blog_object($item){

    global $load_more_data;

    $single_results_page=is_page_template('template-section-results-page.php') || is_page_template('template-selected-resources-page.php') || $load_more_data;
    $mixed_results_page=is_page_template('mixed-results-page.php');
    $detail_page=is_singular() && !$single_results_page && !$mixed_results_page;

    $feat_image = has_post_thumbnail($item->id);
    if($feat_image) {

        $img = get_the_post_thumbnail( $item->id, 'thumbnail', array( 'class' => 'img-responsive' ) );
    }
    else {
        $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/blog-icon.png" alt="" height="150" width="150">';
    }
    if (empty($img)) { // some times, the post says that have a feature image, but return an empty string
         $img='<img class="img-responsive" src="'.get_stylesheet_directory_uri().'/images/blog-icon.png" alt="" height="150" width="150">';
    }

    $date=date("d M Y",strtotime($item->date));


    $author_url = get_author_posts_url($item->author_id);

    // get categories
    $terms = array_map('trim', explode('|', $item->category));

    $categories = array();
    foreach($terms as $category){
        if(!empty($category)) {
            $term = get_term_by('name', $category, 'category');
            $categories[]='<a href="'.get_category_link( $term->term_id ).'" >'.$term->name.'</a>';
        }
    }

    $categories=implode(' | ',$categories);

    // get tags
    $terms = array_map('trim', explode('|', $item->tags));

    $tags = array();
    foreach($terms as $tag){
        if(!empty($tag)) {
            $term = get_term_by('name', $tag, 'post_tag');
            $tags[]='<a href="'.get_category_link( $term->term_id ).'">'.$term->name.'</a>';
        }
    }

    $tags=implode('&nbsp;&nbsp; | &nbsp;&nbsp;',$tags);

    $content = '';
    if ($detail_page) {
        $content = do_shortcode($item->content);
    }else{
        $content= limit_text(wp_strip_all_tags( do_shortcode($item->content) ), 50);
    }

    if ($detail_page) {
        $title = $item->title;
    }else{
        $title = '<a class="" rel="bookmark" href="'.get_permalink($item->id).'">'.$item->title.'</a>';
    }

$html = '
<div class="panel card post-card">
   <div class="panel-body">
      <div class="row is-flex">
         <div class="card-image col-xs-4 col-sm-4 col-md-3">
            '.$img.'
         </div>
         <div class="card-select col-xs-2 col-xs-offset-6 col-sm-2 col-sm-offset-6 col-md-1 col-md-offset-0 col-md-push-8">';


            $html .= '<div class="card-checkbox">
                        <input value="'.$item->id.'" id="resource_'.$item->id.'" class="case" type="checkbox">
                    </div>';


        $html .= ' </div>
         <div class="card-info col-xs-12 col-sm-12 col-md-8 col-md-pull-1">
            <h3 class="card-title">
              ' . $title . '
            </h3>
            <p class="card-meta">';
             $html .= 'Blog<br>
            <span itemtype="http://schema.org/Person" itemscope="" itemprop="author" class="entry-author"><a  rel="author" itemprop="url" class="entry-author-link" href="' . $author_url . '"><span itemprop="name" class="entry-author-name">' . $item->author  . '</span></a></span><br>
            <span>' . $date . '</span><br>';

            if(!empty($categories) ) {
               $html.= '<span>'.$categories.'</span><br>';
             }
             if(!empty($tags) ) {
               $html.= '<span>Tags: '.$tags.'</span><br>';
             }
            $html .='</p>
         </div>
      </div>
      <div class="row">
         <div class="card-content-container col-xs-12">
            <div class="card-content">
            <p>' . $content . '</p>';


          if (!$detail_page) {
              $html .= '<div id="' . $item->id . '" class="more-link-single-results_blogs1">
                                              <a title="Read More" href="' .get_permalink($item->id). '">Details
                                              </a>
                                              <i class="fa fa-caret-right" aria-hidden="true"></i>
                                          </div>';
          }
           $html .= '

            </div>
         </div>
      </div>
   </div>
</div>';

    return $html;

}

function display_post_sort_panel($sort_term){
	global $search_string, $post_types;

	$html = '
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="post" action="">
    <input type="hidden" name="st" id="st" value="'.$search_string.'" />
    <input type="hidden" name="pt" id="pt" value="'.key($post_types).'" />
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <span class="panel-title">Sort by:</span>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" checked="checked" value="weight" '.((empty($sort_term) || $sort_term=='weight')? 'checked="checked"' : '').'
                                   id="relevance_radio">
                            <label for="relevance_radio">Relevance</label>
                        </div>
                    </div>                                      
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="author" id="author_radio" '.((!empty($sort_term) && $sort_term == 'author') ? 'checked="checked" ' : '') . '>
                            <label for="author_radio">Author</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="date" '.(($sort_term=='date')? 'checked="checked"' : '').'
                                   id="year_radio">
                            <label for="year_radio">Date</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="display: none;">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div>
                    <span class="panel-title">Filter by:</span>
                    <button id="clear_filters2" title="Clear Filters" type="button">
                        <span class="hidden">Clear checkboxes</span>Clear
                    </button>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Reviewed" class="checkbox">
                            <input class="styled" type="checkbox" name="reviewed" ' . ((!empty($_POST['reviewed']) || !empty($_GET['reviewed'])) ? 'checked="checked" ' : '') . '
                                   id="reviewed">
                            <label for="reviewed">Reviewed</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div title="Book" class="checkbox">
                            <input class="styled" type="checkbox" name="book" ' . ((!empty($_POST['book']) || !empty($_GET['book'])) ? 'checked="checked" ' : '') . '
                                    id="book">
                            <label for="book">Book</label>
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</form>
    ';
	return $html;
}

function get_post_object($id){
    global $wpdb;

    $query = $wpdb->prepare('SELECT * FROM wp_v_blogs WHERE id = %s LIMIT 1', $id);
    $blog_post = $wpdb->get_row($query);

    return $blog_post;
}

function search_blogs($search_string, $sort_by = 'weight', $limit='', $offset=''){
	global $wpdb;

    $limit_sql='';
    $offset_sql='';
    if(!empty($limit))
    {
        $limit_sql= ' LIMIT '.$limit;
    }
    if(!empty($offset))
    {
        $offset_sql= ' OFFSET '.$offset;
    }

    if($sort_by!= 'weight' && $sort_by!= 'date'){
       $sort_direction='ASC';
    }
    else{
       $sort_direction='DESC';
    }

    $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

    $search_string = esc_sql($search_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return $terms_array;
    }
	// where do you want to search
	$fields = array('title', 'content', 'author', 'category', 'tags');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_blogs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_blogs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);


	$sql="SELECT wp_v_blogs.*,
	if(
		wp_v_blogs.title = '$search_string',  100,
	         IF(
	             wp_v_blogs.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_blogs.content =  '$search_string',  40,
	        IF(
	             wp_v_blogs.content LIKE '%$search_string%', 30,0

	          )


	)
    +
    IF(
        wp_v_blogs.author =  '$search_string',  20,
            IF(
                 wp_v_blogs.author LIKE '%$search_string%', 10,

                     if( {$terms_query['author']}, 5, 0)

              )


    )
    +
	IF(
	    wp_v_blogs.category =  '$search_string',  4, 0

	)
    +
    IF(
        wp_v_blogs.tags =  '$search_string',  3,
            IF(
                 wp_v_blogs.tags LIKE '%$search_string%', 2, 0
             )


    ) AS weight
	FROM wp_v_blogs
    WHERE wp_v_blogs.post_status = 'publish'
	HAVING weight > 0 ";

    if ($sort_by == 'author') {
		$sql .= "ORDER BY substring_index($sort_by, ' ', -1) $sort_direction".$limit_sql.$offset_sql;
    } else {
		$sql .= "ORDER BY $sort_by $sort_direction".$limit_sql.$offset_sql;
    }



    //var_dump($sql);

	$blogs = $wpdb->get_results($sql);
	//var_dump($syllabi);
	//
	return $blogs;


}

function generateMoreLinksSearch($search_string,$post_type=null,$subTypes=array())
{


    //More link to Single Results

    $finalUrlSubTypes = '';
    $subtypesUrl = array();
    if (!empty($subTypes)) {
        foreach ($subTypes as $subType) {
            $subtypesUrl[] = array_search($subType, $subTypes) . '=on';
        }
        $finalUrlSubTypes = '&' . implode('&', $subtypesUrl);
    }
    $link = 'st=' . $search_string . '&pt=' . $post_type . $finalUrlSubTypes;


    return $link;
}

function search_blogs_total($search_string){
	global $wpdb;



    $clean_string = preg_replace('/[[:punct:]]/', ' ', $search_string);
    $clean_string = preg_replace('/\s+/', ' ', $clean_string);

	$terms_array = explode(' ', $clean_string);

	$stop_words = json_decode(file_get_contents(get_stylesheet_directory().'/lib/stop_words.json'), true);


    $terms_array = array_diff($terms_array, $stop_words);
    if (empty($terms_array)){
        return 0;
    }
	// where do you want to search
	$fields = array('title', 'content', 'author', 'category', 'tags');

	$terms_query=array();

	foreach($fields as $field){
		foreach($terms_array as $term){
			if(!empty($terms_query[$field])){
				$terms_query[$field].=" OR wp_v_blogs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
			else{
				$terms_query[$field] ="wp_v_blogs.$field RLIKE '[[:<:]]{$term}[[:>:]]'";
			}
		}
	}

	//var_dump($terms_query);


	$sql="SELECT wp_v_blogs.id,
	if(
		wp_v_blogs.title = '$search_string',  100,
	         IF(
	             wp_v_blogs.title LIKE '%$search_string%', 50,

	                 if( {$terms_query['title']}, 15, 0)

	          )
	)
	+
	IF(
	    wp_v_blogs.content =  '$search_string',  40,
	        IF(
	             wp_v_blogs.content LIKE '%$search_string%', 30,0

	          )


	)
    +
    IF(
        wp_v_blogs.author =  '$search_string',  20,
            IF(
                 wp_v_blogs.author LIKE '%$search_string%', 10,

                     if( {$terms_query['author']}, 5, 0)

              )


    )
    +
	IF(
	    wp_v_blogs.category =  '$search_string',  4, 0

	)
    +
    IF(
        wp_v_blogs.tags =  '$search_string',  3,
            IF(
                 wp_v_blogs.tags LIKE '%$search_string%', 2, 0
             )


    ) AS weight
	FROM wp_v_blogs
    WHERE wp_v_blogs.post_status = 'publish'
	HAVING weight > 0";


    //var_dump($sql);

	$blogs = $wpdb->get_results($sql);
	//var_dump($syllabi);
	//
	return COUNT($blogs);


}



// change the excerpt on the events list view
function custom_excerpt_trive_event_length( $length ) {
     global $post;

    if ($post->post_type == 'tribe_events') {
        return 100000;
    }

    return $length;
}
add_filter( 'excerpt_length', 'custom_excerpt_trive_event_length', 999,1 );
add_filter( 'tribe_events_excerpt_allow_shortcode', '__return_true' );
add_filter( 'tribe_events_excerpt_shortcode_removal', '__return_false' );

if( function_exists('acf_add_options_page') ) {


    acf_add_options_sub_page(array(
        'page_title'  => 'Mails messages',
        'menu_title' => 'Mails',
        'menu_slug'  => 'mail-settings',
        'capability' => 'edit_pages',
        'parent_slug' => '',
        'position'  => false,
        'redirect' => false,
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Banner pictures',
        'menu_title' => 'Banners',
        'menu_slug'  => 'banner-settings',
        'capability' => 'edit_pages',
        'parent_slug' => '',
        'position'  => false,
        'redirect' => false,
    ));

}

function enqueue_books_reviews_landing() {

    if (is_tax() || is_page('Book Reviews') || is_page('Write a Book Review')){
        wp_register_script('singleResults', get_stylesheet_directory_uri().'/js/search/singleResults.js', array('jquery'), '1', true);
        wp_enqueue_script('singleResults');
        wp_register_style('customsearchcss', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');
        wp_enqueue_style('customsearchcss');
    }
}
add_filter('wp_enqueue_scripts', 'enqueue_books_reviews_landing');

function be_add_book_reviews_landing_crumb( $crumb, $args ) {
    $pattern = "/resources\/book_reviews\//";
    $replacement = 'resources/book-reviews/';
    $final_crumb = preg_replace($pattern,$replacement,$crumb);
    return  $final_crumb;
}

add_filter( 'genesis_single_crumb', 'be_add_book_reviews_landing_crumb', 10, 2 );

add_shortcode('wabash-sitemap', 'wabash_sitemap');

function wabash_sitemap()
{


    wp_register_style('sitemap', get_stylesheet_directory_uri().'/css/sitemap.css', array(), '1', 'all');
    wp_enqueue_style('sitemap');

    wp_register_script('sitemap', get_stylesheet_directory_uri().'/js/sitemap.js', array('jquery'), '1', true);
    wp_enqueue_script('sitemap');

    global $wpdb;


    $parent_pages = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type='page' AND post_parent=0 AND post_status='publish' AND post_name IN ('home','about','programs','wabash-grants','resources') ORDER BY menu_order");

?>
<div class='sitemap-section'>
<h3 class='sitemap-section-label'>Pages</h3>
<span>+</span>
<div class="panel open">
<ul>
<?php
    foreach ($parent_pages as $page) {
        echo '<li class="sitemap"><a href="'.get_the_permalink($page->ID).'">'.get_the_title($page->ID).'</a>';
        $first_level_pages = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type='page' AND post_parent=$page->ID AND post_status='publish'");
        if (!empty($first_level_pages)) {
            echo '<ul>';
            foreach ($first_level_pages as $child_page) {
                echo '<li class="sitemap"><a href="'.get_the_permalink($child_page->ID).'">'.get_the_title($child_page->ID).'</a></li>';
            }
            echo '</ul></li>';
        } else {
            echo '</li>';
        }
    }
?>
</ul>
</div>
</div>
<?php

    $cpts=[
'grants',
'syllabi',
'website_on_religion',
'scholarship',
'book_reviews',
'video'
];

    $cpt_names=[
'grants'=>'Grants',
'syllabi'=>'Syllabi',
'website_on_religion'=>'Websites on Religion',
'scholarship'=>'Scholarship on Teaching',
'book_reviews'=>'Book Reviews',
'video'=>'Videos and Audios'
];
    foreach ($cpts as $cpt) {
?>
<div class='sitemap-section'>
<h3 class='sitemap-section-label'><?php echo $cpt_names[$cpt] ?><small> last 25 published</small></h3>
<span>+</span>
<div class="panel">
<?php
    if($cpt=='book_reviews'){
        $posts = $wpdb->get_results("SELECT wp_posts.* FROM wp_posts, wp_books_reviews WHERE post_type='$cpt' AND post_status='publish' AND wp_posts.ID=wp_books_reviews.post_id AND wp_books_reviews.reviewed = 1 ORDER BY post_date DESC LIMIT 25");

    }
    else{
        $posts = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type='$cpt' AND post_status='publish' ORDER BY post_date DESC LIMIT 25");

}
        echo '<ul>';
        foreach ($posts  as $post) {
            echo    '<li class="sitemap">'.get_the_time('m/d/y', $post->ID).': <a href="'.get_the_permalink($post->ID).'">'.get_the_title($post->ID).'</a></li>';
        }
        echo '</ul>';
        echo '</div>';
        echo '</div>';
    }
?>

<div class='sitemap-section'>
<h3 class='sitemap-section-label'>Blog</h3>
    <span>+</span>
    <div class="panel">
    <?php

    $cats = get_categories('exclude=1'); //***Exclude categories by ID, separated by comma if you like.

    foreach ($cats as $cat) {
        echo '<h4>'.$cat->cat_name.'</h4>'."\n";
        echo '<ul class="cat-posts">'."\n";


      query_posts('posts_per_page=-1&cat='.$cat->cat_ID); //-1 shows all posts per category. 1 to show most recent post.


      while (have_posts()): the_post();

         $category = get_the_category();
         //Display a post once, even if it is in multiple categories/subcategories. Lists the post in the first Category displayed.
         if ($category[0]->cat_ID == $cat->cat_ID) {
             ?>
            <li><?php the_time('m/d/y')?>: <a href="<?php the_permalink() ?>"  title="<?php the_title(); ?>">
            <?php the_title(); ?></a> (<?php comments_number('0', '1', '%'); ?>)</li>
       <?php
         } //endif
        endwhile; //endwhile
        wp_reset_query();
       ?>
      </ul>

<?php

    }
?>
</div>
</div>
<div class='sitemap-section'>
<h3 class='sitemap-section-label'>Blog Archives</h3>
<span>+</span>
<div class="panel">
        <ul>
            <?php wp_get_archives('type=monthly&show_post_count=true'); ?>
    </ul>
</div>
</div>
<div class='sitemap-section'>
<h3 class='sitemap-section-label'>Blog Authors</h3>
<span>+</span>
<div class="panel">
<ul>
<?php
wp_list_authors('exclude_admin=1&optioncount=1&show_fullname=1&hide_empty=1');
?>
</ul>
</div>
</div>
<?php
}

add_action( 'genesis_404_after_content', 'add_wabash_sitemap' );



function add_wabash_sitemap(){
    wabash_sitemap();
}

function add_meta_tags() {
    global $post;
    if ( is_page() ) {
        $permalink=get_permalink($post->ID);

        if(preg_match('#/tutorials/#',$permalink,$match)){
            echo '<meta name="robots" content="noindex,nofollow" />' . "\n";
        }
        if(preg_match('#/junk-folder/#',$permalink,$match)){
            echo '<meta name="robots" content="noindex,nofollow" />' . "\n";
        }
    }
}
add_action( 'wp_head', 'add_meta_tags' , 2 );


function comment_send_to_this_person_only($emails, $comment_id){
    // only send notification to the admin:
    return array( 'wrens@wabash.edu' );

}

 /**
  * Disable comment notifications emails except for admins.
 */

add_filter( 'comment_notification_recipients', 'comment_send_to_this_person_only', PHP_INT_MAX,2 );
add_filter( 'comment_moderation_recipients', 'comment_send_to_this_person_only', PHP_INT_MAX,2 );

function wabash_dont_sent_nofication_to_author($maybe_notify, $comment_ID){
    // disable notifications to blog authors
    return false;

}
add_filter( 'notify_post_author', 'wabash_dont_sent_nofication_to_author', PHP_INT_MAX,2 );


function display_custom_panel(){
	global $search_string;

    $original_url = home_url().'/selected-resources/?post_ids=';
	$id = rand(1,999999); //random id number to call load more data and avoid concurrency problems

	$sort_panel = '
<div class="col-md-4 col-sm-4 col-xs-12 single-results-filter">
	<div id="data_container" class="panel-body-single-results results">
		<div class="single_results_right_sidebar single_results_show_filter"
			 style="position: relative; height: auto;">
			<div class="row">				
				<input class="firstid" type="hidden" data-firstid = "'.$id.'" id="first-'.$id.'" value="5" >
                <input type="hidden" id="limit" value="5" >
			</div>
			<input type="hidden" value="'.$search_string.'" id="st" name="st" >
      		<input type="hidden" value="'.$_GET['pt'].'" id="pt" name="pt" >
			<div class="panel panel-default action-panel-search">
				<div class="panel-heading">
					<div>
						<span class="panel-title">Actions for selected results:</span>
					</div>
				</div>
				<div class="panel-body">
					<input type="hidden" id="original_search_url" value="'.$original_url.'">
            		<textarea rows="2" class="share_url_container hide" id="share_url">'.$original_url.'</textarea>
					<div class="share_results">
						<span title="Selected items">
							<span><span id="total_selected_items"></span> Selected Items</span>
						</span>
					</div>
					<div class="selected-buttons">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button title="Go to selected items" id="btn_selected_items" class="disabled"
										onclick="go_to_mixed_search()">
									Go to
								</button>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button id="copy_to_clipboard" class="disabled">
									Copy URL
								</button>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button title="Clear list" class="clear_share_list">Clear list</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="button-left-panel-wrap single-page-panel">
		<a class="button-left-panel" href="#"></a>
	</div>
</div>
    ';

	return $sort_panel;
}

/* New excerpt length of 30 words*/
function my_excerpt_length($length) {
return 30;
}
add_filter('excerpt_length', 'my_excerpt_length');

/* change text tab to html tab */
function change_editor_name_to_html( $translated, $text, $context, $domain )
{
    if ( 'default' !== $domain )
        return $translated;

    if ( 'Text' !== $text )
        return $translated;

    if ( 'Name for the Text editor tab (formerly HTML)' !== $context )
        return $translated;

    return 'HTML';
}
add_filter( 'gettext_with_context', 'change_editor_name_to_html', 10, 4 );


function custom_password_reset_message($message, $key, $user_login, $user_data )    {

	if ( is_multisite() ) {
		$site_name = get_network()->site_name;
	} else {
		/*
		 * The blogname option is escaped with esc_html on the way into the database
		 * in sanitize_option we want to reverse this for the plain text arena of emails.
		 */
		$site_name = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
	}

	$message = __( 'Someone has requested a password reset for the following account:' ) . "\r\n\r\n";
	/* translators: %s: site name */
	$message .= sprintf( __( 'Site Name: %s'), $site_name ) . "\r\n\r\n";
	/* translators: %s: user login */
	$message .= sprintf( __( 'Username: %s'), $user_login ) . "\r\n\r\n";
	$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.' ) . "\r\n\r\n";
	$message .= __( 'To reset your password, visit the following address:' ) . "\r\n\r\n";
	$message .= ' ' . network_site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . " \r\n";

	return $message;
}
add_filter("retrieve_password_message", "custom_password_reset_message", 99, 4);