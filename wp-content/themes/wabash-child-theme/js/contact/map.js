var map;
jQuery(document).ready(function(){

    map = new GMaps({
        div: '#map',
        lat: 40.036585,
        lng: -86.904283,
        scrollwheel: false,
    });
    map.addMarker({
        lat: 40.036585,
        lng: -86.904283,
        title: 'Address',
        infoWindow: {
            content: '<h5 class="title">Wabash Center</h5><p><span class="region">301 W. Wabash Ave.</span><br><span class="postal-code">47933 Crawfordsville, IN</span></p>'
        }

    });

});
