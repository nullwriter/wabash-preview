jQuery(document).ready(function($) {
  $('#accordion').on('hidden.bs.collapse', function(e) {
    // Get clicked element that initiated the collapse...
    var clicked = $(document).find("[href='#" + $(e.target).attr('id') + "']")

    clicked.prev().removeClass('fa-caret-down').addClass('fa-caret-right');

  })
  $('#accordion').on('shown.bs.collapse', function(e) {
    // Get clicked element that initiated the collapse...
    var clicked = $(document).find("[href='#" + $(e.target).attr('id') + "']")

    clicked.prev().removeClass('fa-caret-right').addClass('fa-caret-down');
  })
});
