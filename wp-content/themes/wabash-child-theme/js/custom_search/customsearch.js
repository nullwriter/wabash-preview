jQuery(document).ready(function($) {

$('select').select2();

// $(".btn-group > button").click(function () {
//     $(this).toggleClass("active");
// });

$('#select_all').change(function() {
    var checkboxes = $('.case:checkbox');
    if($(this).is(':checked')) {
        checkboxes.prop('checked', true);
    } else {
        checkboxes.prop('checked', false);
    }
});

$('#clear_all').click(function() {
  $('#select_all').prop('checked', false);
    var checkboxes = $('.case:checkbox');

        checkboxes.prop('checked', false);

});

$( "input[name='sort_term']" ).change(function() {
  //alert( "Handler for .change() called." + $( this ).val() );
  $('#sort_term').text($( this ).val());
});

    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".case").click(function(){

        if($(".case").length == $(".case:checked").length) {
            $("#select_all").prop('checked', true);
        } else {
            $("#select_all").prop('checked', false);
        }

    });

    // Minimize panel
    $('.panel-minimize').click(function() {
      var parent = $(this).closest('.panel');

      parent.find('.panel-body').slideToggle(function() {
        var panelHeading = parent.find('.panel-heading');

        if(panelHeading.hasClass('min')) {
          panelHeading.removeClass('min');
           parent.find('i').removeClass('fa-caret-up').addClass('fa-caret-down');
        } else {
          panelHeading.addClass('min');
          parent.find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
        }

      });

    });


});

function go_to_general_search()
{
    window.location.href='';
    e.preventDefault();
    alert(123);
}
