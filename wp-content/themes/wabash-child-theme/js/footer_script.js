/**
 * Script to be use in footer.
 */


(function($) {
    // handle the submit form event if we replace it on the dom
    $(document).on('submit', '#contactForm', function (event) {
        event.preventDefault();
        var name = document.getElementById("name").value;
        var email = document.getElementById("email").value;
        var message = document.getElementById("message").value;
            $.ajax({
                url: ajaxurl,
                type: 'post',
                data : {
                    action : 'contact_form',
                    name : name,
                    email : email,
                    message : message
                },
                beforeSend: function() {
                    jQuery('.footer-contact-right').html("<h3>Sending ...</h3>");
                },
                success: function (result) {
                    console.log(result);
                    $('#name').val('');
                    $('#email').val('');
                    $('#message').val('');
                    if(result=='1')
                        jQuery('.footer-contact-right').html("<h3>"+CONTACT.msg+"</h3>");
                    else
                        jQuery('.footer-contact-right').html("<h3>Error sending the email, please try later...</h3>");


                },
                error: function(errorThrown){
                    $('#name').val('');
                    $('#email').val('');
                    $('#message').val('');
                    console.log(errorThrown);
                },
                complete: function() {
                    setTimeout(function(){
                        jQuery('.footer-contact-right').html('<h4>Contact Us Directly</h4><div class="form"><form id="contactForm" method="post"><div class="form-group"><input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name."><p class="help-block text-danger"></p></div><div class="form-group"><input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address."><p class="help-block text-danger"></p></div><div class="form-group"><textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea><p class="help-block text-danger"></p></div><button id="submit" type="submit" class="btn btn-default pull-right">Send Message</button></form></div>');
                    },4000);

                }
            });

        //alert( 'Clicked Link: '+ ajaxurl.url );
    });

    // handle the submit form event if we replace it on the dom
     $(document).on('submit', '#subscriber-form', function (event) {

        event.preventDefault();
        var name = document.getElementById("subscribername").value;
        var email = document.getElementById("subscriberemail").value;

        $.ajax({
            url: ajaxurl,
            type: 'post',
            data : {
                action : 'subscriber_form',
                name_subscriber : name,
                email_subscriber : email
            },
            beforeSend: function() {
                jQuery('#inline_form').html("<h3 style='color:#fff'>Sending ...</h3>");
            },
            success: function (result) {
                console.log(result);
                $('#subscribername').val('');
                $('#subscriberemail').val('');
                if(result=='1')
                    jQuery('#inline_form').html("<h3 style='color:#fff'>"+SUBSCRIBER.msg+"</h3>");
                else
                    jQuery('#inline_form').html("<h3>Error sending the email, please try later...</h3>");
            },
            error: function(errorThrown){
                $('#subscribername').val('');
                $('#subscriberemail').val('');
                console.log(errorThrown);
            },
            complete: function() {
                setTimeout(function(){
                    jQuery('#inline_form').html('<p>Subscribe to our "Teaching Links" e-Newsletter</p><form id="subscriber-form" class="form-inline"><div class="form-group"><label class="sr-only" for="exampleInputEmail3">Email address</label><input type="text" class="form-control" placeholder="Your Name *" id="subscribername" required data-validation-required-message="Please enter your name."><p class="help-block text-danger"></p></div><div class="form-group"><label class="sr-only" for="exampleInputPassword3">Password</label><input type="email" class="form-control" placeholder="Your Email *" id="subscriberemail" required data-validation-required-message="Please enter your email address."><p class="help-block text-danger"></p></div><div class="form-group"><button type="submit" class="btn btn-xs fa fa-edit"></button> </div><div id="success2"></div></form>');
                },4000);

            }
        });
        //alert( 'Clicked Link: '+ ajaxurl.url );
    });

})(jQuery);
