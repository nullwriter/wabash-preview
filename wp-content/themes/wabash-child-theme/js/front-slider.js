jQuery(document).ready(function() {
    //Headline Clickable
    jQuery('.caption-wrap').css('cursor','pointer');
    jQuery(document).on("click",".caption-wrap",function() {
        //Cursor pointer
        var href=jQuery(this).prev('a').attr('href');
        var target=jQuery(this).prev('a').attr('target');
        window.open(href,target);
    });
    //Put Events Calendar Overlapping Slider
    //jQuery(".tribe-events-list-widget").parent().addClass('front_page_event_calendar');
    jQuery(".tribe-events-list-widget").parent().find('ul.ecs-event-list').addClass('front_page_event_calendar_container');

});

jQuery(window).load(function(){
    //Add arrows to Slider (next and prev)
    jQuery(".flex-direction-nav").attr('style','display:block !important');
    jQuery(".flex-direction-nav").find("a").text('');
    jQuery(".flex-direction-nav").find("a").attr('style','text-indent:0 !important');
    /* //Prev Arrow
    var prevArrow='<i class="fa fa-chevron-left arrow_custom_prev" aria-hidden="true"></i>';
    jQuery(".flex-direction-nav").find("li:first-child").find("a").html(prevArrow);
    //Next Arrow
    var nextArrow='<i class="fa fa-chevron-right arrow_custom_next" aria-hidden="true"></i>';
    jQuery(".flex-direction-nav").children("li").eq(1).append(nextArrow);*/
});