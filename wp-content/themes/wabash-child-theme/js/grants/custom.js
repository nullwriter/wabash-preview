jQuery(document).ready(function( $ ) {

      //jQuery("#title").prop('disabled', true);
      $('#title').prop('readonly', true);

      $( "#post" ).submit(function( event ) {
        jQuery("#myanchor")[0].click();
        event.preventDefault();
      });
});

jQuery(document).ready(function ($) {
        $(document).on("click", '.more-link-single-results', function () {
            var contentId = $(this).attr('id');
            if ($(this).find('a').html() == 'More') {
                $(this).find('a').html('Less');
                $('#morePoints-' + contentId).hide();
                $('#more_content-' + contentId).show();
                $(this).find('i').removeClass('fa-caret-down');
                $(this).find('i').addClass('fa-caret-up');
            }
            else {
                $(this).find('a').html('More');
                $('#morePoints-' + contentId).show();
                $('#more_content-' + contentId).hide();
                $(this).find('i').removeClass('fa-caret-up');
                $(this).find('i').addClass('fa-caret-down');
            }
        });

        $('.img_single_results').find('img').addClass('img-single-results');
});
