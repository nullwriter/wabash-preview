jQuery( document ).ready(function() {
    jQuery('.tree-toggle').click(function () {
        jQuery(this).parent().children('ul.tree').toggle(200);
    });

    var viewportWidth = jQuery(window).width();
    if (viewportWidth < 988) {
        jQuery(".link_parent").css('height','');
        jQuery(".link_parent").parent().parent().map(function(){
            jQuery(this).find("a").first().attr("href",'javascript:void(0)');
            jQuery(this).children().children().children().first().html(jQuery(this).find("a").html());
            jQuery(this).children().children().children().first().attr("href",jQuery(this).find("a").attr('id'));
            if(jQuery(this).find("a").attr('id')=='#' || jQuery(this).find("a").attr('id')=='')
                jQuery(this).children().children().children().first().hide();

        }).get();
    }
    else
	{
        jQuery(".link_parent").css('height','0px');
	}

    jQuery(window).resize(function () {
        var viewportWidth = jQuery(window).width();
        if (viewportWidth > 988) {
            jQuery( ".hide-small" ).each(function( index )
            {
                jQuery(this).show();
            });
            jQuery(".link_parent").css('height','0px')
            jQuery(".link_parent").parent().parent().map(function(){
                //console.log( jQuery(this).find("a").attr('id')+'\r\n\r\n');
                jQuery(this).find("a").first().attr("href",jQuery(this).find("a").attr('id'));
                //jQuery(this).find("a").attr("href",jQuery(this).find("a").attr('id'));
                jQuery(this).children().children().children().first().html('');
            }).get();
        }
        else
        {
            jQuery(".link_parent").css('height','')
            jQuery(".link_parent").parent().parent().map(function(){
                jQuery(this).find("a").first().attr("href",'javascript:void(0)');
                jQuery(this).children().children().children().first().html(jQuery(this).find("a").html());
                jQuery(this).children().children().children().first().attr("href",jQuery(this).find("a").attr('id'));
                if(jQuery(this).find("a").attr('id')=='#' || jQuery(this).find("a").attr('id')=='')
                    jQuery(this).children().children().children().first().hide();

            }).get();
        }
    });

    jQuery("select#search_content_type").select2({
        minimumResultsForSearch: -1
    });

    // Select content type if available in URL
    var prevContentType = jQuery("#prev_content_type").val();
    jQuery("select#search_content_type option[value='" + prevContentType + "']").prop("selected", true).trigger('change');
    updateSearchForm();

    // Add search string to input
    var searchString = jQuery("#search_string").val();
    jQuery(".general-search-input").val(searchString);

    jQuery("select#search_content_type").on("change", function(){
        updateSearchForm();
    });

    function updateSearchForm() {
        var selectItem = jQuery("select#search_content_type");
        var contentType = selectItem.val();
        var searchInput = jQuery(".general-search-input");
        var searchForm = jQuery("#search_form");
        var homeUrl = jQuery("#home_url").val();

        if (contentType !== "") {
            selectItem.attr("name", "pt");
            searchInput.attr("name", "st");
            searchForm.attr("action", homeUrl+"/single-results");
        } else {
            searchInput.attr("name", "s");
            searchForm.attr("action", homeUrl);
            selectItem.attr("name", "");
        }
    }
});