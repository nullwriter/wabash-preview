is_loading = false;
more_data = true;
(function($, window, undefined) {
    var InfiniteScroll = function() {
        this.initialize = function() {
            this.setupEvents();
        };

        this.setupEvents = function() {
            $(window).on(
                'scroll',
                this.handleScroll.bind(this)
            );
        };

        this.handleScroll = function() {
            var scrollTop = $(document).scrollTop();
            var windowHeight = $(window).height();
            var height = $(document).height() - windowHeight;
            var scrollPercentage = (scrollTop / height);

            // if the scroll is more than 90% from the top, load more content.
            if(scrollPercentage > 0.9) {
                this.doSomething();
            }
        }

        this.doSomething = function() {
            // Do something.
            // Load data or whatever.
            load_more_data();

        }

        this.initialize();
    }

    function load_more_data(){
      first = $('#first').val();
		  limit = $('#limit').val();
      st = $('#st').val();
		  pt = $('#pt').val();


      if(!is_loading && more_data){
        console.log('calling ajax more data');
			is_loading = true;
			$('#loader').show();
			$.ajax({
				url : '../books_load_more_data.php',
				dataType: 'json',
				method: 'post',
				data: {
				   start : first,
				   limit : limit,
           st: st,
           pt: pt
				},
				success: function( data ) {

					$('#loader').hide();
					if(data.count > 0 ){
						first = parseInt($('#first').val());
						limit = parseInt($('#limit').val());
						$('#first').val( first+limit );
            console.log('feeding more data');
            	$('#data_container').append( data.html );


					}else{
						console.log('No more data to show');
						more_data = false;
					}
          is_loading = false;
				},
				error: function( data ){
					is_loading = false;
					$('#loader').hide();
					more_data = false;
					console.log('Something went wrong, Please contact admin');
				}
			});
		}
    }

    $(document).ready(
        function() {
            // Initialize scroll
            new InfiniteScroll();
        }
    );
})(jQuery, window);
