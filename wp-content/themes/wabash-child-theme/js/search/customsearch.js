jQuery(document).ready(function($) {

     jQuery('.grid').masonry({
        // options...
        itemSelector: '.grid-item',
        // use element for option
        /*columnWidth: '.grid-sizer',*/
        percentPosition: true

    });
    jQuery('a.btn_mobile').click(function() {
        jQuery(this).toggleClass("active");
        var images_url=url_object.templateUrl+'/images/cpt_small_icons/';
        //Change Image on click
        //console.log(jQuery(this).attr('name'));
        if(jQuery(this).hasClass('active')) {
            jQuery(this).find('img').attr('src',images_url+jQuery(this).attr('name')+'_icon.png');
        }
        else
        {
            jQuery(this).find('img').attr('src',images_url+jQuery(this).attr('name')+'_icon_white.png');
        }
        //Check the Checkbox to filter
        if(jQuery('#'+jQuery(this).attr('name')).prop( "checked")==false)
        {
            jQuery('#'+jQuery(this).attr('name')).prop('indeterminate',false);
            //console.log(jQuery('#'+jQuery(this).attr('name')).prop('indeterminate'));
            jQuery('#'+jQuery(this).attr('name')).prop("checked",true);
            var checked =  jQuery('#'+jQuery(this).attr('name')).prop("checked"),container =  jQuery('#'+jQuery(this).attr('name')).parent(),siblings = container.siblings();
            container.find('input[type="checkbox"]').prop({
                indeterminate: false,
                checked: checked
            });
            checkSiblings(container,checked);
        }
        else
        {
            jQuery('#'+jQuery(this).attr('name')).prop('indeterminate',false);
            //console.log(jQuery('#'+jQuery(this).attr('name')).prop('indeterminate'));
            var checked =  false,container =  jQuery('#'+jQuery(this).attr('name')).parent(),siblings = container.siblings();
            container.find('input[type="checkbox"]').prop({
                indeterminate: false,
                checked: checked
            });
            checkSiblings(container,checked);
            jQuery('#'+jQuery(this).attr('name')).prop("checked",false)
        }
        jQuery('#filter_form').submit();
    });

});
if(jQuery( window ).width()>=991)
    jQuery('.general_search_apply_filter_buttons').removeClass('text-center');
else
    jQuery('.general_search_apply_filter_buttons').addClass('text-center');
jQuery(document).ready(function($) {

    // Select and loop the container element of the elements you want to equalise
    /*jQuery('.general_search_tiles').each(function(){

        // Cache the highest
        var highestBox = 0;

        // Select and loop the elements you want to equalise
        jQuery(".panel-default-general-search", this).each(function(){

            // If this box is higher than the cached highest then store it
            if(jQuery(this).height() > highestBox) {
                highestBox = jQuery(this).height();
            }
        });

        // Set the height of all those children to whichever was highest
        jQuery(".panel-default-general-search",this).height(highestBox);
    });*/

    $(function(){
        $('.filters').find('.tree-toggle').parent().children('ul.tree').toggle(200);
    })
    /*Filter Floating*/
    //Button Container
    // var button_container=jQuery('#filter_button_container');
    // //Buttom to open and close panel
    // var button=jQuery('.general_search_button_panel');
    // //Buttom to open and close panel (Clicked)
    // var buttonClicked=jQuery('.single_results_button_panel_clicked');
    // //Filter Panel
    // var filter_panel=jQuery(".general_search_filter");
    // var y = jQuery(this).scrollTop();
    // /*Show panel if scroll over 400px*/
    // var width = jQuery('.general_search_filter').parent().width();
    // var container=jQuery(".custom_search");
    // jQuery( window ).resize(function() {
    //     var width = container.width();
    //     filter_panel.css('width',width);
    //     button_container.css('width',width);
    //     if(jQuery( window ).width()>992)
    //         jQuery('.general_search_apply_filter_buttons').removeClass('text-center');
    //     else
    //         jQuery('.general_search_apply_filter_buttons').addClass('text-center');
    // });
    // //console.log(width);
    // if(y<400)
    // {
    //     /*button.css('top','85%');
    //     filter_panel.css('top','65%');*/
    //     button_container.css('position','relative');
    //     filter_panel.css('position','relative');
    // }
    // else
    // {
    //     if(button.attr('class') == "general_search_button_panel") {
    //         button_container.css('top','350px');
    // }
    // else
    // {
    //         button_container.css('top','0');
    //     }
    //     button_container.css('position','fixed');
    //     filter_panel.css('position','fixed');
    //     filter_panel.css('top','0');
    //     filter_panel.css('width',width);
    //     var width = container.width();
    //     button_container.css('width',width);
    // }
    // jQuery(document).scroll(function() {
    //     var y = jQuery(this).scrollTop();
    //     /*Show panel if scroll over 200px*/
    //     if (y < 400) {
    //
    //         button_container.css('top','0');
    //         button_container.css('position','relative');
    //         filter_panel.css('position','relative');
    //     } else {
    //         if(button.attr('class') == "general_search_button_panel") {
    //             button_container.css('top','350px');
    //         }
    //         else
    //         {
    //             button_container.css('top','0');
    //         }
    //         button_container.css('position','fixed');
    //         filter_panel.css('position','fixed');
    //         filter_panel.css('top','0');
    //         var width = container.width();
    //         // filter_panel.css('width',width);
    //         // button_container.css('width',width);
    //     }
    // });
    jQuery(document).on("click", ".general_search_button_panel", function()
    {
        if(button.attr('class') == "general_search_button_panel")
        {
            button.addClass("general_search_button_panel_clicked");
            button.html( "Open Filter" );
            filter_panel.removeClass('general_search_show_filter');
            filter_panel.addClass('general_search_hide_filter');
            button_container.css('top','0');
        }
        else
        {
            button.removeClass("general_search_button_panel_clicked");
            button.addClass("general_search_button_panel");
            button.html( "Close Filter" );
            filter_panel.removeClass('general_search_hide_filter');
            filter_panel.addClass('general_search_show_filter');
            if(jQuery('.general_search_filter').css('position')=='fixed')
                button_container.css('top','350px');
        }

    });
    //End Filter Floating

/*$('input[type=checkbox]').each(function ()
{
	var checked = $(this).prop("checked"),container = $(this).parent(),siblings = container.siblings();
	checkSiblings(container,checked=null);
});*/
$("#open_filter").click(function(){
  icon=$(this).find('i');
  status='';
  if(icon.hasClass( "fa-angle-double-right" )){
    $("#column_filter").show();
    $("#first_results_column").removeClass('col-sm-6').addClass('col-sm-4');
    $("#second_results_column").removeClass('col-sm-6').addClass('col-sm-4');
   icon.removeClass( "fa-angle-double-right" ).addClass("fa-angle-double-left");
   status='open';
  }
  else{
    $("#column_filter").hide();
    $("#first_results_column").removeClass('col-sm-4').addClass('col-sm-6');
    $("#second_results_column").removeClass('col-sm-4').addClass('col-sm-6');
    icon.removeClass( "fa-angle-double-left" ).addClass("fa-angle-double-right");
    status='close';
  }

  // Send Ajax request to backend.php, with src set as "img" in the POST data
    $.post("filter_panel_status.php", {"status": status});
});



$('#select_all').change(function() {
    var checkboxes = $('.case:checkbox');
    if($(this).is(':checked')) {
        checkboxes.prop('checked', true);
    } else {
        checkboxes.prop('checked', false);
    }
});

$('#general_search_clear_all').click(function() {
    $('input:checkbox').removeAttr('checked');
    $('.btn_mobile').each(function(i, obj) {
        $(this).removeClass("active");
    });
    $('#filter_form').submit();
});

$( "input[name='sort_term']" ).change(function() {
  //alert( "Handler for .change() called." + $( this ).val() );
  //$('#sort_term').text($( this ).val());
});

    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".case").click(function(){

        if($(".case").length == $(".case:checked").length) {
            $("#select_all").prop('checked', true);
        } else {
            $("#select_all").prop('checked', false);
        }

    });

    // Minimize panel
    $('.panel-minimize').click(function() {
      var parent = $(this).closest('.panel');

      parent.find('.panel-body').slideToggle(function() {
        var panelHeading = parent.find('.panel-heading');

        if(panelHeading.hasClass('min')) {
          panelHeading.removeClass('min');
           parent.find('.panel-minimize i').removeClass('fa-caret-up').addClass('fa-caret-down');
        } else {
          panelHeading.addClass('min');
          parent.find('.panel-minimize i').addClass('fa-caret-up').removeClass('fa-caret-down');
        }

      });

    });

    $('input[type="checkbox"]').change(function(e) 
	{
		var checked = $(this).prop("checked"),container = $(this).parent(),siblings = container.siblings();
		container.find('input[type="checkbox"]').prop({
		indeterminate: false,
		checked: checked
		});
		checkSiblings(container,checked);
	});

    // $(document).on('click', '[data-click=panel-collapse]', function (e) {
    //     e.preventDefault(),
    //     $(this).closest('.panel').find('.panel-body').slideToggle();
    //
    // })
	
	//More Content
	$(".more_content_link").click(function(e)
	{
		var contentId=$(this).attr('id').split('-');
		if($(this).html()=='More...')
		{
			$(this).html('Less...');
			$('#more_content-'+contentId[1]).show();
		}
		else
		{
			$(this).html('More...');
			$('#more_content-'+contentId[1]).hide();
		}
	});
	$(".more_content_link_BA").click(function(e)
	{
		var contentId=$(this).attr('id').split('-');
		if($(this).html()=='More...')
		{
			$(this).html('Less...');
			$('#more_content-'+contentId[1]).show();
			$('#additional_info-'+contentId[1]).hide();
		}
		else
		{
			$(this).html('More...');
			$('#more_content-'+contentId[1]).hide();
			$('#additional_info-'+contentId[1]).show();
		}
	});
	
	function checkSiblings(el,checked) {
    var parent = el.parent().parent(),
        all = true;
//console.log(parent);
    el.siblings().each(function() {
        return all = (jQuery(this).children('input[type="checkbox"]').prop("checked") === checked);
    });

    if (all && checked) {
        parent.children('input[type="checkbox"]').prop({
            indeterminate: false,
            checked: checked
        });
        checkSiblings(parent);
    } else if (all && !checked) {
        parent.children('input[type="checkbox"]').prop("checked", checked);
        parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
        checkSiblings(parent);
    }
    else {
        el.parents("li").children('input[type="checkbox"]').prop({
            indeterminate: true,
            checked: false
        });
    }
}
});
