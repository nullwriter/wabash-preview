is_loading = false;
more_data = true;
(function ($, window, undefined) {
    var InfiniteScroll = function () {
        this.initialize = function () {
            this.setupEvents();
        };

        this.setupEvents = function () {
            $(window).on(
                'scroll',
                this.handleScroll.bind(this)
            );
        };

        this.handleScroll = function () {
            var scrollTop = $(document).scrollTop();
            var windowHeight = $(window).height();
            var height = $(document).height() - windowHeight;
            var scrollPercentage = (scrollTop / height);

            // if the scroll is more than 90% from the top, load more content.
            if (scrollPercentage > 0.9) {
                this.doSomething();
            }
        }

        this.doSomething = function () {
            // Do something.
            // Load data or whatever.
            load_more_data();

        }

        this.initialize();
    }

    function load_more_data() {
       var firstid = $('.single_results_right_sidebar').find('.firstid').attr('data-firstid');
      first = $('#first-'+firstid).val();
		  limit = $('#limit').val();
      st = $('#st').val();
		  pt = $('#pt').val();
        var data = '{';
        data += '"start":"' + first + '"';
        data += ',"limit":"' + limit + '"';
        data += ',"st":"' + st + '"';
        data += ',"pt":"' + pt + '"';
        //GET THE SORT TERM SELECTED
        var sort_term = $('input:radio[name=sort_term]:checked').val();
        if (typeof sort_term === "undefined") {
            sort_term='weight';
        }
        data += ',"sort_term":"' + sort_term + '"';
        if (pt == 'scholarship' || pt == 'website_on_religion' || pt == 'ttr' || pt == 'book_reviews' || pt == 'grants' || pt == 'syllabi') {
            var SubTypesString = '';
            var ST_separator = ',';
            var semicolumn = ':';
            var subTypesString = [];
            $(".checkbox input:checkbox:checked").each(function () {
                data += ST_separator + '"' + $(this).attr('name') + '"' + semicolumn + '"on"';
          });

      }
        data += '}';

        if (!is_loading && more_data) {

            var data_container = $('.single_results_main_container').children('div.row').children('div');
            console.log('calling ajax more data');

			is_loading = true;
			$('#loader').show();
			$.ajax({
                url: '../load_more_data.php',
                type: "get",
				dataType: 'json',
				method: 'post',
				data: $.parseJSON(data),
                success: function (data) {

					$('#loader').hide();
                    if (data.count > 0) {
						first = parseInt($('#first-'+firstid).val());
						limit = parseInt($('#limit').val());
                        $('#first-'+firstid).val(first + limit);
            console.log('feeding more data');
                        data_container.append(data.html);
                        //If find checkboxes checked into Cookies put input checked
                        $('.case').each(function(obj) {
                            if(jQuery.inArray($(this).val(),Cookies.getJSON('post_ids'))>-1)
                            {
                                $(this).prop('checked', true);
                            }
                        });

                    } else {
						console.log('No more data to show');
						more_data = false;
					}
          is_loading = false;
				},
                error: function (data) {
					is_loading = false;
					$('#loader').hide();
					more_data = false;
					console.log('Something went wrong, Please contact admin');
				}
			});
		}
    }

    $(document).ready(
        function () {
            // Initialize scroll
            new InfiniteScroll();
        }
    );
})(jQuery, window);
