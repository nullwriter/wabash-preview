jQuery(document).ready(function($)
	{
        if(typeof Cookies.getJSON('post_ids')!=='undefined') {
            jQuery('#share_url').val(jQuery('#original_search_url').val()+Cookies.getJSON('post_ids').join());
        }
		//If find checkboxes checked into Cookies put input checked
        $('.case').each(function(obj) {
            //console.log(Cookies.getJSON('post_ids'));
            var PtIdsArray = Cookies.getJSON('post_ids');
            if(jQuery.inArray($(this).val(),Cookies.getJSON('post_ids'))>-1)
			{
                $(this).prop('checked', true);
			}
            if(PtIdsArray != null) {
                if (PtIdsArray.length > 0) {
                    jQuery('#btn_selected_items').attr('disabled', false);
                    jQuery('#copy_to_clipboard').attr('disabled', false);
                    jQuery('.clear_share_list').attr('disabled', false);
                }
                else {
                    jQuery('#btn_selected_items').attr('disabled', true);
                    jQuery('#copy_to_clipboard').attr('disabled', true);
                    jQuery('.clear_share_list').attr('disabled', true);
                    changeSelectItemsButtonToolTip(jQuery('#btn_selected_items'), jQuery('#copy_to_clipboard'), jQuery('.clear_share_list'), true);
                }
                jQuery('#total_selected_items').html(PtIdsArray.length);
            } else {
                jQuery('#btn_selected_items').attr('disabled', true);
                jQuery('#copy_to_clipboard').attr('disabled', true);
                jQuery('.clear_share_list').attr('disabled', true);
                jQuery('#total_selected_items').html('0');
                changeSelectItemsButtonToolTip(jQuery('#btn_selected_items'), jQuery('#copy_to_clipboard'), jQuery('.clear_share_list'), true);
            }
        });

		$(document).on("click", ".case", function()
		{
            //IF EXIST COOKIE
            if (typeof Cookies.getJSON('post_ids') !== 'undefined') {
                var PtIdsArray = Cookies.getJSON('post_ids');

            }
            //IF NOT EXIST MAKE A NEW ARRAY TO FILL IDS
            else {
                var PtIdsArray = [];
            }

            if($(this).prop('checked')) {
                $('.case:checked').each(function (obj) {
                    //IF NO FOUND IDS INTO ARRAY
                    if ((jQuery.inArray($(this).val(), Cookies.getJSON('post_ids')) == -1)) {
                        PtIdsArray.push($(this).val());
                    }
                });
            }
            else
            {
                var index = $.inArray($(this).val(), PtIdsArray);
                if (index > -1) {
                    PtIdsArray.splice(index, 1)
                }
            }
            Cookies.set('post_ids',JSON.stringify(PtIdsArray));
            jQuery('#share_url').val(jQuery('#original_search_url').val()+Cookies.getJSON('post_ids').join());
            //console.log(Cookies.getJSON('post_ids').join());
            if(PtIdsArray.length>0)
            {
                jQuery('#btn_selected_items').attr('disabled', false);
                jQuery('#copy_to_clipboard').attr('disabled', false);
                jQuery('.clear_share_list').attr('disabled', false);changeSelectItemsButtonToolTip(jQuery('#btn_selected_items'), jQuery('#copy_to_clipboard'), jQuery('.clear_share_list'), false);
            }
            else
            {
                jQuery('#btn_selected_items').attr('disabled', true);
                jQuery('#copy_to_clipboard').attr('disabled', true);
                jQuery('.clear_share_list').attr('disabled', true);
                changeSelectItemsButtonToolTip(jQuery('#btn_selected_items'), jQuery('#copy_to_clipboard'), jQuery('.clear_share_list'), true);
            }
            //Update Total
            jQuery('#total_selected_items').html(PtIdsArray.length);

		});

		//void keypress
		$(document).on("keydown", "#share_url", function(event)
		{
			event.preventDefault();
			return false;
		});
		/*$(document).on("click", ".share_results", function()
		{
			if($('#share_url').css('display')=='none')
			{
				$('#share_url').show();
                $('.clear_share_list').show();
            }
			else
			{
				$('#share_url').hide();
                $('.clear_share_list').hide();
			}
		});*/
		//Clear Cookies
        $(document).on("click", ".clear_share_list", function()
        {
            Cookies.remove('post_ids');
            jQuery('#share_url').val(jQuery('#original_search_url').val());
            $('.case').each(function(obj) {
                $(this).prop('checked', false);
            });
            jQuery('#btn_selected_items').attr('disabled', true);
            jQuery('#copy_to_clipboard').attr('disabled', true);
            jQuery('.clear_share_list').attr('disabled', true);
            jQuery('#total_selected_items').html('0');
            changeSelectItemsButtonToolTip(jQuery('#btn_selected_items'), jQuery('#copy_to_clipboard'), jQuery('.clear_share_list'), true);
        });
        //Copy to clipboard instance
        new Clipboard('#copy_to_clipboard', {
            text: function() {
                return jQuery('.share_url_container').val();
            }
        });

    });

var disabledTooltip = 'Select an item by clicking its checkbox';
var btnViewSelectedToolTip = 'Go to selected items';
var btnShareLinkToolTip = 'Copy permalink of selected items to your computer’s clipboard';
var btnClearListToolTip = 'Clear list';

function changeSelectItemsButtonToolTip(btnView, btnShare, btnClear, disabled) {
    if (disabled) {
        btnView.attr('title', disabledTooltip);
        btnShare.attr('title', disabledTooltip);
        btnClear.attr('title', disabledTooltip);
    } else {
        btnView.attr('title', btnViewSelectedToolTip);
        btnShare.attr('title', btnShareLinkToolTip);
        btnClear.attr('title', btnClearListToolTip);
    }
}

//Go to url (Mixed Search)
function go_to_mixed_search()
{
    window.open(decodeURI(jQuery('.share_url_container').val()));
}
