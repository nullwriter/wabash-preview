jQuery( document ).ready(function()
{
    //button toggle
    jQuery(document).on("click", ".button-left-panel", function(event)
    {
        event.preventDefault();
        var panel_main_container=jQuery('.single_results_main_container');
        var filter_container=jQuery('.panel-body-single-results');
        var post_type = jQuery('#pt').val() || "";
        var pageType = jQuery('#pagetype').val() || "normal";


        filter_container.toggleClass('hidden');
        jQuery(this).parent().toggleClass('open');
        jQuery(this).parent().parent().toggleClass('filter-closed');

        if (post_type === 'book_reviews' || post_type === 'post') {
            if(panel_main_container.hasClass('col-md-9'))
            {
                // 1 column
                panel_main_container.removeClass('col-md-9');
                panel_main_container.removeClass('col-sm-9');
                panel_main_container.addClass('col-md-12');
                panel_main_container.addClass('col-sm-12');
            }
            else
            {
                // 2 columns
                panel_main_container.removeClass('col-md-12');
                panel_main_container.removeClass('col-sm-12');
                panel_main_container.addClass('col-md-9');
                panel_main_container.addClass('col-sm-9');
            }

        } else if(pageType === 'single') {
            //single pages panel

            filter_container = jQuery('.panel-body-single-results').parent();

            if(panel_main_container.hasClass('col-md-8'))
            {
                // 1 column
                panel_main_container.removeClass('col-md-8');
                panel_main_container.removeClass('col-sm-8');
                panel_main_container.addClass('col-md-11');
                panel_main_container.addClass('col-sm-11');
                filter_container.removeClass('col-md-4');
                filter_container.removeClass('col-sm-4');
                filter_container.addClass('col-sm-1');
                filter_container.addClass('col-md-1');
            }
            else
            {
                // 2 columns
                panel_main_container.removeClass('col-md-11');
                panel_main_container.removeClass('col-sm-11');
                panel_main_container.addClass('col-md-8');
                panel_main_container.addClass('col-sm-8');
                filter_container.removeClass('col-md-1');
                filter_container.removeClass('col-sm-1');
                filter_container.addClass('col-sm-4');
                filter_container.addClass('col-md-4');
            }

        } else {
            if(panel_main_container.hasClass('col-md-6'))
            {
                // 2 columns
                panel_main_container.removeClass('col-md-6');
                panel_main_container.addClass('col-md-9');
            }
            else
            {
                // 3 columns
                panel_main_container.addClass('col-md-6');
            }

            if(panel_main_container.hasClass('col-sm-9'))
            {
                panel_main_container.removeClass('col-sm-9');
                panel_main_container.addClass('col-sm-12');
            }
            else {
                panel_main_container.addClass('col-sm-9');
            }
        }
    });

    // Disable the radio button when all are checked in Book Reviews Filter panel
    if ( jQuery('#reviewed_checkbox').prop("checked") === true && jQuery('#book_checkbox').prop("checked") === true ) {
        jQuery('#date_reviewed_radio').prop('disabled', true);
        jQuery('#year_radio').prop('disabled', true);
    }else {
        jQuery('#date_reviewed_radio').prop('disabled', false);
        jQuery('#year_radio').prop('disabled', false);
    }
    //Clear Filter
    jQuery(document).on("click", "#clear_filters2", function(event)
    {
         event.preventDefault();
        jQuery('.checkbox input:checkbox').each(function(obj) {
            jQuery(this).prop('checked', false);
        });
        // clean_uri();
        jQuery('.form_single_results_sort_filter_by').submit();
    });


    //SORT BUTTONS
    jQuery('.radio input:radio').click(function(){
        jQuery('.form_single_results_sort_filter_by,.form_single_results_sort_by').submit();
        });

    jQuery('a.btn_mobile_single_search.single_sort').click(function() {
        jQuery('a.btn_mobile_single_search.single_sort').each(function(i, obj) {
            jQuery(this).removeClass('active');
        });
        jQuery(this).toggleClass("active");
        //Check the Checkbox to filter
        if (jQuery('#' + jQuery(this).attr('name')+'_radio').prop("checked") === false) {
            jQuery('#' + jQuery(this).attr('name')+'_radio').prop("checked", true);
        }
        else {
            jQuery('#' + jQuery(this).attr('name')+'_radio').prop("checked", false);
        }
        jQuery('.form_single_results_sort_filter_by,.form_single_results_sort_by').submit();
    });

    //FILTER BUTTONS
    jQuery('.checkbox input:checkbox').click(function(){
        // clean_uri();
        jQuery('.form_single_results_sort_filter_by').submit();
    });

    jQuery('a.btn_mobile_single_search.single_filter').click(function() {

        //Check the Checkbox to filter
        if (jQuery('#' + jQuery(this).attr('name')+'_checkbox').prop("checked") === false) {
            jQuery('#' + jQuery(this).attr('name')+'_checkbox').prop("checked", true);
        }
        else {
            jQuery('#' + jQuery(this).attr('name')+'_checkbox').prop("checked", false);
        }
        // clean_uri();
        jQuery('.form_single_results_sort_filter_by').submit();
    });


    //Button Container
    var button_container=jQuery('#filter_button_container');
    //Buttom to open and close panel
    var button=jQuery('.single_results_button_panel');
    //Filter Panel
    var filter_panel=jQuery(".single_results_right_sidebar");
    //Container to resize after the button is clicked
    var container=jQuery(".single_results_main_container");
    if(container.length===0){
        container=jQuery("#genesis-content");
    }
    // var width = container.width();
    // var filter_panel_height=filter_panel.outerHeight();
    // filter_panel.css('width',width);
    // jQuery( window ).resize(function() {
    //     width = container.width();
    //     filter_panel.css('width',width);
    //     button_container.css('width',width);
    //     filter_panel_height=filter_panel.outerHeight();
    //     button_container.css('top',filter_panel_height);
    // });
    // var y = jQuery(this).scrollTop();
    // if(y<400)
    // {
    //     button_container.css('position','relative');
    //     filter_panel.css('position','relative');
    // }
    // else
    // {
    //     button_container.css('top',filter_panel_height);
    //     button_container.css('position','fixed');
    //     filter_panel.css('position','fixed');
    //     width = container.width();
    //     button_container.css('width',width);
    // }
    // //Show Filter button after scroll down x px to avoid solaping the top banner
    // jQuery(document).scroll(function() {
    //     var y = jQuery(this).scrollTop();
    //     if(y<400)
    //     {
    //         button_container.css('top',0);
    //         button_container.css('position','relative');
    //         filter_panel.css('position','relative');
    //     }
    //     else
    //     {
    //         if(button.attr('class') == "single_results_button_panel") {
    //
    //             button_container.css('top',filter_panel_height);
    //             filter_panel_height=filter_panel.outerHeight();
    //         }
    //         else
    //         {
    //             button_container.css('top','0');
    //             filter_panel_height=filter_panel.height();
    //         }
    //         button_container.css('top',filter_panel_height);
    //         button_container.css('position','fixed');
    //         filter_panel.css('position','fixed');
    //         var width = container.width();
    //         filter_panel.css('width',width);
    //         button_container.css('width',width);
    //     }
    // });
    jQuery(document).on("click", ".single_results_button_panel", function()
    {
        if(button.attr('class') == "single_results_button_panel")
        {
            filter_panel.outerHeight(0);
            filter_panel.removeClass('single_results_show_filter');
            filter_panel.addClass('single_results_hide_filter');
            button.addClass("single_results_button_panel_clicked");
            button.html( "Open Filter Panel" );
            button_container.css('top','0');
        }
        else
        {
            filter_panel.css('height','auto');
            button.removeClass("single_results_button_panel_clicked");
            button.addClass("single_results_button_panel");
            button.html( "Close Filter Panel" );
            filter_panel.removeClass('single_results_hide_filter');
            filter_panel.addClass('single_results_show_filter');
            if(jQuery('.single_results_right_sidebar').css('position')=='fixed')
                button_container.css('top',filter_panel_height);
        }
    });
});
function go_to_general_search()
{
    var search_term = jQuery('#s').val() ? jQuery('#s').val() : jQuery('#st').val()? jQuery('#st').val() :'';

    window.location.href=HOME.home+'/?s='+search_term;
}
function clean_uri(){
    var uri = window.location.toString();
    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        window.history.replaceState({}, document.title, clean_uri);
    }
}
