jQuery(document).ready(function ($) {

 $('.sitemap-section-label').click(function () {

  var panel = $(this).next().next();

  if (panel.is(':hidden')) {
   panel.slideDown('200');
   $(this).next().html('-');
  } else {
   panel.slideUp('200');
   $(this).next().html('+');
  }

 });

});
