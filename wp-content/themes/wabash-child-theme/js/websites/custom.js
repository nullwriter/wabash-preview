jQuery(document).ready(function(){
                    // Init dataTable
                    jQuery('#websites_table').dataTable({
                        "aaSorting":[],
                        "bSortClasses":false,
                        "asStripeClasses":['even','odd'],
                        "bSort":true,
                        aoColumnDefs: [
                            {
                                bSortable: false,
                                aTargets: [ 3 ]
                            }
                        ]
                    });
               });
