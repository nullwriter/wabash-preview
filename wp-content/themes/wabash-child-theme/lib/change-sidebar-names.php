<?php


add_action( 'widgets_init', 'ddw_change_sidebar_names', 20 );
/**
 * Genesis Framework: Change Sidebar Names
 *
 */
function ddw_change_sidebar_names() {
	global $wp_registered_sidebars;
	// Change Primary Sidebar name
	$wp_registered_sidebars['sidebar']['name'] = __( 'Blog Sidebar', 'textdomain' );
	$wp_registered_sidebars['sidebar']['description'] = __( 'This is the sidebar that will be displayed on all your blog posts', 'textdomain' );

	// Change Secondary Sidebar name
	// $wp_registered_sidebars['sidebar-alt']['name'] = __( 'News Sidebar', 'textdomain' );
	// $wp_registered_sidebars['sidebar-alt']['description'] = __( 'This is the sidebar that will be displayed on all your news posts', 'textdomain' );

}
