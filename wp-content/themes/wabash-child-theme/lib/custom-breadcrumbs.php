<?php

add_filter('genesis_breadcrumb_args', 'remove_breadcrumbs_yourarehere_text');
/**
* @author Brad Dalton - WP Sites
* @example http://wpsites.net/web-design/change-breadcrumbs-in-genesis/
*/
function remove_breadcrumbs_yourarehere_text( $args )
{
    $args['labels']['prefix'] = '';
    return $args;
}

add_filter('genesis_breadcrumb_args', 'modify_separator_breadcrumbs');
/**
* @author Brad Dalton - WP Sites
* @example http://wpsites.net/web-design/change-breadcrumbs-in-genesis/
*/
function modify_separator_breadcrumbs($args)
{
    $args['sep'] = ' > ';
    return $args;
}
