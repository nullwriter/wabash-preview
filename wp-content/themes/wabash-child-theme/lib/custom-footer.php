<?php

remove_action( 'genesis_footer', 'genesis_do_footer' );

add_action( 'genesis_footer', 'wabash_custom_footer' );
function wabash_custom_footer() {
  //echo '<p>&copy; Copyright 2016 | Anything Else using simple HTML or PHP</p>';
  // $recent = new WP_Query("pagename=custom-sidebar-".$post_type."");
  //  while($recent->have_posts()) : $recent->the_post();
  //         //the_title();
  //         the_content();
  //          endwhile;

  $page = get_page_by_title( 'Custom Footer' );
  echo apply_filters( 'the_content', $page->post_content );
}
