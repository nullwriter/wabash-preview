<?php

//function customize_site_title()
//{
//    $custom = '	<ul class="clearfix">
//                         <li><a href="#"><span class="fa fa-phone"></span> (765) 361-6047</a></li>
//                            <li><a href="mailto:wabashcenter@wabash.edu"><span class="fa fa-at"></span> wabashcenter@wabash.edu</a></li>
//
//                        </ul>';
//    echo $custom;
//}
//add_action('genesis_site_title', 'customize_site_title');
//
//// Adds a widget area.
//genesis_register_sidebar(
//    array(
//    'id'                => 'extra-header-widget-area',
//    'name'            => __('Extra Header Widget Area', 'wabash'),
//    'description'    => __('header below nav', 'wabash'),
//      )
//);
//
//// Adds a widget area.
//genesis_register_sidebar(
//    array(
//    'id'                => 'header-image-widget-area',
//    'name'            => __('Header Image Widget Area', 'wabash'),
//    'description'    => __('header image before content', 'wabash'),
//    'before_widget' => '<div class="widget-area container">',
//      'after_widget'  => '</div>',
//      )
//);
//
//remove_action('genesis_site_title', 'genesis_seo_site_title');
//remove_action('genesis_site_description', 'genesis_seo_site_description');
remove_action( 'genesis_header', 'genesis_do_header');

//* Reposition the primary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_nav');



//add in the new header markup - prefix the function name - here wabash_ is used
add_action( 'genesis_header', 'wabash_genesis_do_header' );

function wabash_genesis_do_header() {
    $page = get_page_by_title( 'Custom Top Header' );
    echo apply_filters( 'the_content', $page->post_content );
}

add_action( 'genesis_header', 'genesis_do_nav' );

add_action( 'genesis_header', 'wabash_genesis_do_header_after_nav' );
function wabash_genesis_do_header_after_nav() {
    $page = get_page_by_title( 'Custom Bottom Header' );

    $contentType = "";
    if (isset($_GET["pt"]) && !empty($_GET["pt"])) {
		$contentType = $_GET["pt"];
	}

	$searchString = "";
	if (isset($_GET["s"]) && !empty($_GET["s"])) {
		$searchString = $_GET["s"];
	}
	if (isset($_GET["st"]) && !empty($_GET["st"])) {
		$searchString = $_GET["st"];
	}

    $contentTypeInput = "<input type='hidden' id='prev_content_type' value='".$contentType."' />";
    $homeUrlInput = "<input type='hidden' id='home_url' value='".home_url()."' />";
    $searchStringInput = "<input type='hidden' id='search_string' value='".$searchString."' />";
    echo apply_filters( 'the_content', ($page->post_content . $contentTypeInput . $homeUrlInput . $searchStringInput) );
}