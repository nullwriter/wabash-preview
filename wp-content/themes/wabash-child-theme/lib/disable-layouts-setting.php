<?php

//* Set full-width content as the default layout

// disable layouts from theme
remove_theme_support('genesis-inpost-layouts', 15);
unregister_sidebar( 'sidebar-alt' );
/** Remove unused theme settings */
add_action('genesis_theme_settings_metaboxes', 'child_remove_metaboxes');
function child_remove_metaboxes($_genesis_theme_settings_pagehook)
{
    remove_meta_box('genesis-theme-settings-layout', $_genesis_theme_settings_pagehook, 'main');
}
/** Force Full Width Layout On All Posts In A Category (Uses Category I.D*/
add_filter('genesis_pre_get_option_site_layout', 'full_width_layout_category_posts');
/**
 * @author Brad Dalton
 * @link http://wpsites.net/web-design/change-layout-genesis/
 */
function full_width_layout_category_posts($opt)
{
    if ( is_search()  ) {
          // var_dump( get_post_type());
      $opt = 'full-width-content';
      return $opt;

    }  else if ( is_single() || is_singular( 'post' ) || is_category() || is_author() || is_tag() || ('book_reviews' == get_post_type()) || 'syllabi' == get_post_type() || ('website_on_religion' == get_post_type())) {
      // var_dump(is_single());
      // var_dump(is_singular( 'post' ));
      // var_dump(is_category());
      // var_dump(is_author());
      // var_dump(is_tag());
      // var_dump( ('book_reviews' == get_post_type()));
      // var_dump('syllabi' == get_post_type());
      //   var_dump( get_post_type());
      //
      //
      //
      //   echo "Aqui segundo";exit();
        $opt = 'content-sidebar';
        return $opt;
    }
else if (is_page('book-reviews') || is_page('videos') || is_page('syllabi') ) {
        // echo "Aqui tercero";exit();
        $opt = 'content-sidebar';
        return $opt;
    }
else {
    // echo "Aqui cuarto";
    // exit();
      $opt = 'full-width-content';
      return $opt;
    }
}

//Read More Button For Excerpt
function themeprefix_excerpt_read_more_link($output)
{
	global $post;
    return $output . ' <a class="btn btn-success more-link" href="' . get_permalink($post->ID) . '" title="Read More">Read More</a>';
}

add_filter('the_excerpt', 'themeprefix_excerpt_read_more_link');

/** Customize the post info function. */
add_filter('genesis_post_info', 'wpse_108715_post_info_filter', 30);

/** Customize the post meta function. */
add_filter('genesis_post_meta', 'wpse_108715_post_meta_filter', 30);


/**
 * Change the default post information line.
 */
function wpse_108715_post_info_filter($post_info)
{
  $num_comments = get_comments_number(); // get_comments_number returns only a numeric value

    if (comments_open()) {
        if ($num_comments == 0) {
      $comments = __('No Comments');
        } elseif ($num_comments > 1) {
      $comments = $num_comments . __(' Comments');
    } else {
      $comments = __('1 Comment');
    }
        $write_comments = '<a href="' . get_comments_link() . '">' . $comments . '</a>';
  } else {
    $write_comments =  __('Comments are off for this post.');
  }
  $category = get_the_category();
    if (!empty($category[0])) {
  $firstCategory = $category[0]->cat_name;
  //$post_date=the_date();

        $post_info = '<i class="fa fa-calendar-o"></i> ' . get_the_date('d M Y') .'<i class="fa fa-eye" style="margin-right:4px;"></i>'.subh_get_post_view( get_the_ID() ) .' <i class="fa fa-comment"></i> ' . $num_comments . ' comments <i class="fa fa-tag"></i> ' . '<a href="' . esc_url(get_category_link($category[0]->term_id)) . '" title="' . esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $category[0]->name)) . '">' . esc_html($category[0]->name) . '</a>';
   }
  return $post_info;
}

/**
 * Change the default post meta line.
 */
function wpse_108715_post_meta_filter($post_meta)
{
    $post_meta = '';
    return $post_meta;
}

//BY AUTHOR TEXT
function after_title_text()
{
    $post_type = get_post_type();
    if (!is_page() && $post_type!='book_reviews' && $post_type!='video' && $post_type!='syllabi' && $post_type!='website_on_religion' && $post_type!='scholarship' && $post_type!='grants' && !is_archive()) {
        $temp = '<span id="author_info">By [post_authors_post_link]</span>';
        $temp = do_shortcode($temp);
  echo $temp;
  }
}
add_action('genesis_entry_header', 'after_title_text', 11);
//* Display author box on single posts
add_filter('get_the_author_genesis_author_box_single', '__return_true');
