<?php

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

add_shortcode('url', 'home_url');
