<?php
//* Load Google Fonts
add_action( 'wp_enqueue_scripts', 'enqueue_google_fonts' );
function enqueue_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans:400,600,700|Roboto:100,300,400,500,700,900|PT+Sans|Roboto+Condensed', array(), CHILD_THEME_VERSION );

}
