<?php

// // replace style.css - Theme Information (no css)
// // with css/style.min.css -  Compressed CSS for Theme
// //remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
// add_action( 'wp_enqueue_scripts', 'bsg_enqueue_css_js' );
//
// function bsg_enqueue_css_js() {
//     $version = wp_get_theme()->Version;
//
//     // wp_enqueue_style( $handle, $src, $deps, $ver, $media );
//     wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css', array(), $version );
//
//     // wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
//     // NOTE: this script is loading in the footer
//     wp_enqueue_script( 'bootstrap_js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'), $version, true );
//
//
// }


// THIS Way you alter the order of your css child theme, we want the child theme css added last in the queue
/**
 * Remove Genesis child theme style sheet
 * @uses  genesis_meta  <genesis/lib/css/load-styles.php>
*/
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
/**
 * Enqueue Genesis child theme style sheet at higher priority
 * @uses wp_enqueue_scripts <http://codex.wordpress.org/Function_Reference/wp_enqueue_style>
 */
add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', 15 );


function custom_post_status(){
	register_post_status( 'notforweb', array(
		'label'                     => _x( 'Notforweb', 'post' ),
		'public'                    => false,
		'exclude_from_search'       => true,
		'show_in_admin_all_list'    => false,
		'show_in_admin_status_list' => false,
		'label_count'               => _n_noop( 'Notforweb <span class="count">(%s)</span>', 'Notforweb <span class="count">(%s)</span>' ),
	) );
}
add_action( 'init', 'custom_post_status' );
