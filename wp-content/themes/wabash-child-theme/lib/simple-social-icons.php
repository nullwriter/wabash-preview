<?php

add_filter( 'simple_social_default_css', 'rgc_simple_social_icons_css' );
/**
 * Replace Simple Social Icons' stylesheet
 */
function rgc_simple_social_icons_css( $css_file ) {
	$css_file = get_stylesheet_directory_uri() . '/css/simple-social-icons.css';
	// $css_file = ''; // alternate method: if you want to just add your styling to your theme styesheet
	return $css_file;
}
add_filter( 'simple_social_default_glyphs', 'rgc_simple_social_icons_glyphs' );
/**
 * Replace Simple Social Icons's glyphs with FontAwesome
 */
function rgc_simple_social_icons_glyphs( $glyphs ) {
	$glyphs = array(
		'bloglovin'   => '<span class="fa fa-heart"></span><span class="screen-reader-text">Bloglovin</span>',
		'dribbble'    => '<span class="fa fa-dribbble"></span><span class="screen-reader-text">Dribbble</span>',
		'email'       => '<span class="fa fa-envelope"></span><span class="screen-reader-text">Email</span>',
		'facebook'    => '<span class="fa fa-facebook"></span><span class="screen-reader-text">Facebook</span>',
		'flickr'      => '<span class="fa fa-flickr"></span><span class="screen-reader-text">Flickr</span>',
		'github'      => '<span class="fa fa-github"></span><span class="screen-reader-text">Github</span>',
		'gplus'       => '<span class="fa fa-google-plus"></span><span class="screen-reader-text">Google+</span>',
		'instagram'   => '<span class="fa fa-instagram"></span><span class="screen-reader-text">Instagram</span>',
		'linkedin'    => '<span class="fa fa-linkedin"></span><span class="screen-reader-text">Linkedin</span>',
		'pinterest'   => '<span class="fa fa-pinterest"></span><span class="screen-reader-text">Pinterest</span>',
		'rss'         => '<span class="fa fa-rss"></span><span class="screen-reader-text">RSS</span>',
		'stumbleupon' => '<span class="fa fa-stumbleupon"></span><span class="screen-reader-text">Stumbleupon</span>',
		'tumblr'      => '<span class="fa fa-tumblr"></span><span class="screen-reader-text">Tumblr</span>',
		'twitter'     => '<span class="fa fa-twitter"></span><span class="screen-reader-text">Twitter</span>',
		'vimeo'       => '<span class="fa fa-vimeo-square"></span><span class="screen-reader-text">Vimeo</span>',
		'youtube'     => '<span class="fa fa-youtube-play"></span><span class="screen-reader-text">Youtube</span>',
	);

	return $glyphs;
}
