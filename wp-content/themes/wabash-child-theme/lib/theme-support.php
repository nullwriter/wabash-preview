<?php
// Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );


//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption'  ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array(
  '404-page',
  'drop-down-menu',
  'headings',
  /*'rems',*/
  'search-form',
  'skip-links',
) );


// Remove structural Wraps
remove_theme_support( 'genesis-structural-wraps' );
