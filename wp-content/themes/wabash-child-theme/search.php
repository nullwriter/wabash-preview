<?php
/**
 * custom search template
 */

// increase time limit
// set_time_limit(0);
// ini_set('xdebug.var_display_max_depth', -1);
// ini_set('xdebug.var_display_max_children', -1);
// ini_set('xdebug.var_display_max_data', -1);
// ini_set('memory_limit', '512M');

function mythemename_all_scriptsandstyles() {
    //Load JS and CSS files in here
    wp_register_script('mosaic-script', get_stylesheet_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '1', true);
    wp_register_script('customsearch', get_stylesheet_directory_uri() . '/js/search/customsearch.js', array('jquery'), '1', true);
    wp_register_style('awesomecheckbox', get_stylesheet_directory_uri() . '/css/awesome-bootstrap-checkbox.css', array(), '1', 'all');
    wp_register_style('customsearchcss', get_stylesheet_directory_uri() . '/css/custom_search.css', array(), '1', 'all');

    wp_enqueue_script('mosaic-script');
    wp_enqueue_script('customsearch');
    wp_enqueue_style('awesomecheckbox');
    wp_enqueue_style('customsearchcss');

    $translation_array = array( 'templateUrl' => get_stylesheet_directory_uri() );
    wp_localize_script( 'customsearch', 'url_object', $translation_array );

}

add_action('wp_enqueue_scripts', 'mythemename_all_scriptsandstyles');


remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'sk_do_search_loop');
/**
 * Outputs a custom loop
 *
 * @global mixed $paged current page number if paginated
 * @return void
 */

$search_string='';
$post_types=array();
$scholar_sub_types=array();
$wor_sub_types=array();

process_request();

function display_search_results_panel($post_type, $post_types_names, $total, $content, $subtypes_titles=''){
    global $search_string, $scholar_sub_types, $wor_sub_types, $post_types;
    $subTypes=array();
    if(!empty($scholar_sub_types) && $post_type=='scholarship' )
    {
        $subTypes=$scholar_sub_types;
    }
    if(!empty($wor_sub_types) && $post_type=='website_on_religion' )
    {
        $subTypes=$wor_sub_types;
    }
    $html = '<div class="col-sm-6 grid-item">
             <div class="panel panel-default-general-search">
                 <div class="panel-heading">';
    if ($total>0) {
        $html .= '<a class="title_tile" href="single-results/?' . generateMoreLinksSearch($search_string, $post_type, $subTypes) . '">
                    <h4 class="panel-title"> (' . $total . ') results for ' . $post_types_names. $subtypes_titles . (($total > 5) ? ' <span class="more_tiles0"><strong>More ...</strong></span>' : '') . '</h4>
                    </a>';
    }else{
        $html .= '<h4 class="panel-title"> (' . $total . ') results for ' . $post_types_names. $subtypes_titles. (($total > 5) ? ' <span class="more_tiles0"><strong>More ...</strong></span>' : '') . '</h4>';
    }
    $html .= '</div>
                <div class="panel-body results">
                    ' . $content . '
                </div>
            </div>
        </div>';
    return $html;
}

function process_request(){
    global $wp_query, $search_string, $scholar_sub_types, $wor_sub_types, $post_types;

    // get search expression
    $search_string = $wp_query->query_vars['s'];

    //copy over to the input search again


    // get post types active for search via $_GET
    if(!empty($_GET['post-type'])){
        foreach ($_GET['post-type'] as $cpt) {
            $post_types[$cpt] = true;
        }
    }
    // get post types active for search via
    if(!empty($_POST['post-type'])){
        foreach ($_POST['post-type'] as $cpt) {
            $post_types[$cpt] = true;
        }
    }

    //SCHOLARSHIP SUBTYPES
    if ((!empty($_GET['st-web']) && $_GET['st-web'] == 'on' ) || ( !empty($_POST['st-web']) && $_POST['st-web'] == 'on')) {
        $scholar_sub_types['st-web'] = 'Web';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-journal']) && $_GET['st-journal'] == 'on' ) || (!empty($_POST['st-journal']) && $_POST['st-journal'] == 'on')) {
        $scholar_sub_types['st-journal'] = 'Journal Issue';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-book']) && $_GET['st-book'] == 'on')  || (!empty($_POST['st-book']) && $_POST['st-book'] == 'on')) {
        $scholar_sub_types['st-book'] = 'Book';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-article']) && $_GET['st-article'] == 'on' ) || (!empty($_POST['st-article']) &&  $_POST['st-article'] == 'on')) {
        $scholar_sub_types['st-article'] = 'Article';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-ttr']) && $_GET['st-ttr'] == 'on') || (!empty($_POST['st-ttr']) && $_POST['st-ttr'] == 'on')) {
        $scholar_sub_types['st-ttr'] = 'TTR';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-tactic']) && $_GET['st-tactic'] == 'on') || (!empty($_POST['st-tactic']) && $_POST['st-tactic'] == 'on')){
        $scholar_sub_types['st-tactic'] = 'Tactic';
        $post_types['scholarship'] = true;
    }
    //WEBSITE SUBTYPES
    if ((!empty($_GET['wt-e-text']) && $_GET['wt-e-text'] == 'on' ) || (!empty($_POST['wt-e-text']) && $_POST['wt-e-text'] == 'on')) {
        $wor_sub_types['wt-e-text'] = 'e-text';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-e-journal']) && $_GET['wt-e-journal'] == 'on') || (!empty($_POST['wt-e-journal']) && $_POST['wt-e-journal'] == 'on')) {
        $wor_sub_types['wt-e-journal'] = 'e-journal';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-image']) && $_GET['wt-image'] == 'on') || (!empty($_POST['wt-image']) && $_POST['wt-image'] == 'on' )) {
        $wor_sub_types['wt-image'] = 'image';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-biblio']) && $_GET['wt-biblio'] == 'on') || (!empty($_POST['wt-biblio']) && $_POST['wt-biblio'] == 'on')) {
        $wor_sub_types['wt-biblio'] = 'biblio';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-web']) && !empty($_GET['wt-web'] == 'on')) || (!empty($_POST['wt-web']) && !empty($_POST['wt-web'] == 'on'))) {
        $wor_sub_types['wt-web'] = 'web';
        $post_types['website_on_religion'] = true;
    }



    if(empty($post_types)){
        $post_types['page'] = 'Programs';
        $post_types['ttr'] = 'Teaching Theology and Religion (Wabash Journal)';
        $post_types['grants'] = 'Awarded Grants';
        $post_types['scholarship'] = 'Scholarchip on Teaching';
        $post_types['post'] = 'Blog';
        $post_types['website_on_religion'] = 'Website on Religion';
        $post_types['book_reviews'] = 'Book Reviews';
        $post_types['syllabi'] = 'Syllabi';
        $post_types['video'] = 'Video';
    }

    // var_dump($search_string);
    // var_dump($post_types);
    // var_dump($scholar_sub_types);
    // var_dump($wor_sub_types);

}

//print_r($post_types);
function sk_do_search_loop() {
    //SCHOLARSHIP SUB TYPE
    global $post_types,$search_string,$wor_sub_types,$scholar_sub_types;

    $limit_items_per_panel=5;
    $general_banner = get_field('general_search_banner', 'option');
    if(!empty($general_banner)){
       echo '<style>
            .inner_banner {
                background: rgba(0, 0, 0, 0) url('.$general_banner.') no-repeat scroll center center / 100% auto;
            }
        </style>';
     }
    //SQL FOR EACH POST TYPE
    echo '<div class="custom_search">
            <header class="entry-header">
                <h1 class="entry-title" itemprop="headline">Search Results</h1>
            </header>
            <div class="row grid">';

    foreach ($post_types as $post_type => $value) {
        switch($post_type){
            case 'page':

                $programs=search_programs($search_string);
                $html='';

                $index=0;
                if(!empty($programs)) {
                    foreach ($programs as $program) {
                        $html .= display_excerpt_program_object($program);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                echo display_search_results_panel('page', 'Wabash Programs', COUNT($programs), $html);
                break;
            case 'ttr':

                $ttrs=search_scholarship($search_string,$scholar_sub_types, 'ttr');
                $html='';

                $index=0;
                if (!empty($ttrs)) {
                    foreach ($ttrs as $ttr) {
                        $html .= display_excerpt_scholarship_object($ttr);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                $st_subtypes_titles='';
                if (count($scholar_sub_types) > 0) {
                    $st_subtypes_titles = ' (' . implode(', ', $scholar_sub_types) . ')';
                }

                echo display_search_results_panel('ttr', 'Teaching Theology and Religion (Wabash Journal)', COUNT($ttrs), $html, $st_subtypes_titles);
                break;
            case 'grants':

                $grants=search_grant($search_string);
                $html='';

                $index=0;
                if (!empty($grants)) {
                    foreach ($grants as $grant) {
                        $html .= display_excerpt_grant_object($grant);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                echo display_search_results_panel('grants', 'Awarded Grants', COUNT($grants), $html);
                break;
            case 'post':

                $blogs=search_blogs($search_string);
                $html='';

                $index=0;
                if (!empty($blogs)) {
                    foreach ($blogs as $blog) {
                        $html .= display_excerpt_blog_object($blog);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                echo display_search_results_panel('post', 'Wabash Blog', COUNT($blogs), $html);
                break;
            case 'scholarship':
                $scholar_sub_types=array();
                process_request();

                $scholarships = search_scholarship($search_string,$scholar_sub_types);
                $html = '';

                $index = 0;
                if (!empty($scholarships)) {
                    foreach ($scholarships as $scholar) {
                        $html .= display_excerpt_scholarship_object($scholar);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                $st_subtypes_titles='';
                if (count($scholar_sub_types) > 0) {
                    $st_subtypes_titles = ' (' . implode(', ', $scholar_sub_types) . ')';
                }

                echo display_search_results_panel('scholarship', 'Scholarship On Teaching', COUNT($scholarships), $html, $st_subtypes_titles);
                break;
            case 'website_on_religion':

//                $wors=search_website_on_religion($search_string,$wor_sub_types);
//                $html='';
//
//                $index=0;
//                if (!empty($wors)) {
//                    foreach ($wors as $wor) {
//                        $html .= display_excerpt_wor_object($wor);
//                        if (++$index >= $limit_items_per_panel) {
//                        break;
//                    }
//                }
//                }
//
//                $st_wor_sub_types='';
//                if (count($wor_sub_types) > 0) {
//                    $st_wor_sub_types = ' (' . implode(', ', $wor_sub_types) . ')';
//                }
//
//                echo display_search_results_panel('website_on_religion', 'Website On Religion', COUNT($wors), $html, $st_wor_sub_types);
                break;
            case 'syllabi':

                $syllabi=search_syllabi($search_string);
                $html='';

                $index=0;
                if (!empty($syllabi)) {
                    foreach ($syllabi as $syllabus) {
                        $html .= display_excerpt_syllabi_object($syllabus);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                echo display_search_results_panel('syllabi', 'Syllabi', COUNT($syllabi), $html);
                break;
            case 'book_reviews':

                $book_reviews=search_book_reviews($search_string);
                $html='';

                $index=0;
                if (!empty($book_reviews)) {
                    foreach ($book_reviews as $book) {
                        $html .= display_excerpt_book_reviews_object($book);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                echo display_search_results_panel('book_reviews', 'Book Reviews', COUNT($book_reviews), $html);
                break;
            case 'video':

                $videos=search_videos($search_string);
                $html='';

                $index=0;
                if (!empty($videos)) {

                    foreach ($videos as $video) {
                        $html .= display_excerpt_video_object($video);
                        if (++$index >= $limit_items_per_panel) {
                        break;
                    }
                }
                }

                echo display_search_results_panel('video', 'Videos', COUNT($videos), $html);
                break;



        }
    }
    echo '</div>';

    echo '</div>';
}


genesis();
