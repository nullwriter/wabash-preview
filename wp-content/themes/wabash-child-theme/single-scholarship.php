<?php
/*
Template Name: Single Sillaby
*/

wp_register_style('scholarship-style', get_stylesheet_directory_uri() . '/css/scholarship.css', array(), '1', 'all');
wp_enqueue_style('scholarship-style');
wp_register_style('customsearchcss', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');
wp_enqueue_style('customsearchcss');


wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
wp_enqueue_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
wp_enqueue_script('clipboard', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);
$home_url = array(
    'home' => home_url()
);
wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );
//* Remove the post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 1 );
//* Remove the author box on single posts
remove_action( 'genesis_after_entry', 'genesis_do_author_box_single', 8 );
//* Remove the post meta function
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
//Remove Title
remove_action( 'genesis_entry_header', 'genesis_do_post_title');

add_action( 'genesis_entry_content', 'sk_display_custom_fields' );


function sk_display_custom_fields() {
    global $post;

    echo '<input type="hidden" id="pagetype" value="single" />';
    echo display_custom_panel();

    $scholar=get_scholarship_object($post->ID);

	$html = '<div class="col-md-8 col-sm-8 col-xs-12 single_results_main_container" style="padding-left: 3rem;">
				<div class="row">';
    $html .= display_tile_scholarship_object($scholar);
	$html .= '	</div>
			</div>';
    echo $html;
}
genesis();
