<?php

/*
Template Name: single-staff-page
*/

error_reporting(E_ALL ^ E_DEPRECATED);


global $wpdb; // this is how you get access to the database

$uri = $_SERVER['REQUEST_URI'];
$pieces = explode("?", $uri);
$staff_id = intval( $pieces[1] );
$table_name = $wpdb->prefix . "wabash_staff";
$sql = "SELECT * FROM ".$table_name." WHERE id=".$staff_id;

$staff_data = $wpdb->get_results($sql);

$staff_member=$staff_data[0];

//var_dump($staff_member );
function mythemename_all_scriptsandstyles()
{
    //Load JS and CSS files in here
    wp_register_script('custom_single_staff_js', get_stylesheet_directory_uri().'/js/staff/single-page.js', array('jquery'), '1', true);

    wp_enqueue_script('custom_single_staff_js');

}

add_action('wp_enqueue_scripts', 'mythemename_all_scriptsandstyles');

// Add our custom loop
add_action('genesis_loop', 'cd_goh_loop');
function cd_goh_loop()
{
 global $staff_member;
 $upload_dir = wp_upload_dir();
 $target_dir = $upload_dir['baseurl'].'/';

 echo '


   <div class="row">
      <div class="col-md-9">
       <span id="staff-firstname" style="display:none">'.$staff_member->firstname.'</span>
       <span id="staff-lastname" style="display:none">'.$staff_member->lastname.'</span>
       <h2>'.$staff_member->position.'</h2>
        <div>

          <img class="main-staff-image img-responsive" src="'.$target_dir.$staff_member->main_photo.'" alt="image" />
        '.$staff_member->description.'

        </div>
        <div style="margin-top: 40px;">
          <a href="'.$target_dir.$staff_member->resume.'" class="btn btn-success btn-lg" style="padding: 6px 16px;"><i style="margin-right: 10px; font-size: 20px;" class="fa fa-file-pdf-o"></i> Download CV </a>
        </div>
      </div>
      <div class="col-md-3">

     <div class="panel panel-default">

       <div class="panel-heading">
         <strong>Our Staff</strong>
       </div>

             <div class="panel-body" id="accordion">
             <div class=staff>
               <div>
                 <i class="fa fa-caret-down"></i>
                 <a aria-controls="collapseOne" data-parent="#accordion" data-toggle="collapse" href="#collapseOne">
                   Nadine Pence
                 </a>
               </div>
               <div class="collapse in" id="collapseOne">

                 <p>
                   <strong>Director</strong><br/>
                   765-361-6047<br/>
                   pencen@wabash.edu
                 </p>

               </div>
             </div>
             <div class=staff>
               <div>
                 <i class="fa fa-caret-right"></i>
                 <a aria-controls="collapseTwo" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo">
                   Paul Myhre
                 </a>
               </div>
               <div class="collapse" id="collapseTwo">

                 <p>
                   <strong>Associate Director</strong><br/>
                   765-361-6436<br/>
                   myhrep@wabash.edu
                 </p>

               </div>
             </div>
             <div class=staff>
               <div>
                 <i class="fa fa-caret-right"></i>
                 <a aria-controls="collapseThree" data-parent="#accordion" data-toggle="collapse" href="#collapseThree">
                   Thomas Pearson
                 </a>
               </div>
               <div class="collapse" id="collapseThree">

                 <p>
                   <strong>Associate Director</strong><br/>
                   765-361-6436<br/>
                   myhrep@wabash.edu
                 </p>

               </div>
             </div>
             <div class=staff>
               <div>
                 <i class="fa fa-caret-right"></i>
                 <a aria-controls="collapseFour" data-parent="#accordion" data-toggle="collapse" href="#collapseFour">
                   Rita Arthur
                 </a>
               </div>
               <div class="collapse" id="collapseFour">

                 <p>
                   <strong>Coordinator of Grants</strong><br/>
                   765-361-6435<br/>
                   arthurr@wabash.edu
                 </p>

               </div>
             </div>
             <div class=staff>
               <div>
                 <i class="fa fa-caret-right"></i>
                 <a aria-controls="collapseFive" data-parent="#accordion" data-toggle="collapse" href="#collapseFive">
                   Sherry &#8220;She&#8221; Wren
                 </a>
               </div>
               <div class="collapse" id="collapseFive">

                 <p>
                   <strong>Communications Coordinator</strong><br/>
                   765-361-6477<br/>
                   wrens@wabash.edu
                 </p>

               </div>
             </div>
             <div class=staff>
               <div>
                 <i class="fa fa-caret-right"></i>
                 <a aria-controls="collapseSix" data-parent="#accordion" data-toggle="collapse" href="#collapseSix">
                   Trish Overpeck
                 </a>
               </div>
               <div class="collapse" id="collapseSix">

                 <p>
                   <strong>Meeting Coordinator</strong><br/>
                   765-361-6047<br/>
                   overpecp@wabash.edu
                 </p>

               </div>
             </div>
             <div class=staff>
               <div>
                 <i class="fa fa-caret-right"></i>
                 <a aria-controls="collapseSeven" data-parent="#accordion" data-toggle="collapse" href="#collapseSeven">
                   Beth Reffett
                 </a>
               </div>
               <div class="collapse" id="collapseSeven">

                 <p>
                   <strong>Administrative Assistant to the Assoc. Directors</strong><br/>
                   765-361-6493<br/>
                   reffettb@wabash.edu
                 </p>

               </div>
             </div>
           </div>
     </div>
     </div>

   </div>



';


    wp_reset_postdata();
}
genesis();
