<?php

/*
Template Name: contact
*/

error_reporting(E_ALL ^ E_DEPRECATED);

// FORCE FULL WIDTH LAYOUT
//add_filter ( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

function mythemename_all_scriptsandstyles()
{
    //Load JS and CSS files in here
    //wp_register_script('custom_blog_js', get_stylesheet_directory_uri().'/js/blog/page.js', array('jquery'), '1', true);
    wp_register_style('custom_blog_css', get_stylesheet_directory_uri().'/css/single_post.css', array(), '1', 'all');

    //wp_enqueue_script('custom_blog_js');
    wp_enqueue_style('custom_blog_css');
}

add_action('wp_enqueue_scripts', 'mythemename_all_scriptsandstyles');

// /** Customize the post info function. */
// add_filter( 'genesis_post_info', 'wpse_108715_post_info_filter' ,30);
//
// /** Customize the post meta function. */
// add_filter( 'genesis_post_meta', 'wpse_108715_post_meta_filter' ,30);
// 
//
//
//
//
// /**
//  * Change the default post information line.
//  */
// function wpse_108715_post_info_filter( $post_info ) {
//   $num_comments = get_comments_number(); // get_comments_number returns only a numeric value
//
//   if ( comments_open() ) {
//     if ( $num_comments == 0 ) {
//       $comments = __('No Comments');
//     } elseif ( $num_comments > 1 ) {
//       $comments = $num_comments . __(' Comments');
//     } else {
//       $comments = __('1 Comment');
//     }
//     $write_comments = '<a href="' . get_comments_link() .'">'. $comments.'</a>';
//   } else {
//     $write_comments =  __('Comments are off for this post.');
//   }
//   $category = get_the_category();
//   $firstCategory = $category[0]->cat_name;
//   //$post_date=the_date();
//   $post_info = '<i class="fa fa-calendar-o"></i> '.get_the_date('d M Y') .' <i class="fa fa-comment"></i> '.$num_comments.' comments <i class="fa fa-tag"></i> '.'<a href="' . esc_url( get_category_link( $category[0]->term_id ) ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category[0]->name ) ) . '">' . esc_html( $category[0]->name ) . '</a>';
//     return $post_info;
// }
//
// /**
//  * Change the default post meta line.
//  */
// function wpse_108715_post_meta_filter( $post_meta ) {
//     $post_meta = '';
//     return $post_meta;
// }

genesis();
