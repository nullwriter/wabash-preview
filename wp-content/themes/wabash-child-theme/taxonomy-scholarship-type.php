<?php
/**
 * Template Name: websites-on-religion Archives
 * Description: Used as a page template to show page contents, followed by a loop through a CPT archive
 */

 function scholarship_topic_scripts_and_styles()
 {
     //Load JS and CSS files in here

    wp_register_style('custom-search', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');
    wp_enqueue_style('custom-search');
    wp_register_style('panel', get_stylesheet_directory_uri().'/css/panel.css', array(), '1', 'all');
    wp_enqueue_style('panel');


    wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
    wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
    wp_enqueue_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
    wp_enqueue_script('clipboard', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);
     $home_url = array(
         'home' => home_url()
     );
     wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );
 }

 add_action('wp_enqueue_scripts', 'scholarship_topic_scripts_and_styles');





remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'custom_scholarship_topic_grid_loop' ); // Add custom loop


function custom_scholarship_topic_grid_loop() {
  global $wpdb;


  //echo "here<br/>";
  $terms_data = get_queried_object();
  //var_dump($terms_data);
  $name = htmlspecialchars_decode($terms_data->name);

  $query = $wpdb->prepare('SELECT * FROM wp_scholarship_of_teaching WHERE Web_flag = 1 AND web_type = "%s" LIMIT 500', $name);
	$scholars = $wpdb->get_results($query);


// var_dump(COUNT($scholars));
//
// exit();

  $html = '';
  $sort_panel_html = display_custom_panel();

  foreach($scholars as $scholar){
      //var_dump($scholar);

    $html.=display_tile_scholarship_object($scholar);
    //exit();
  }
  echo display_results_panel('Scholarship On Teaching', COUNT($scholars), $html, 'Type', $name, '', $sort_panel_html);

  // display sidebar
    $html = '<aside class="sidebar sidebar-primary widget-area col-sm-3">';
    $page = get_page_by_title( "custom-sidebar-scholarship");
    $html .=apply_filters( 'the_content', $page->post_content );
    $html .='</aside>';


    $html .='</div>';
    $html .='</div>';

    echo $html;

}


genesis();
