<?php
/**
 * Template Name: Wor Archives Types
 * Description: Used as a page template to show page contents, followed by a loop through a CPT archive
 */

function wor_type_scripts_and_styles()
{
    //Load JS and CSS files in here

    wp_register_style('custom-search', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');
    wp_enqueue_style('custom-search');
    wp_register_style('panel', get_stylesheet_directory_uri().'/css/panel.css', array(), '1', 'all');
    wp_enqueue_style('panel');


    wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
    wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
    wp_enqueue_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
    wp_enqueue_script('clipboard', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);
    $home_url = array(
        'home' => home_url()
    );
    wp_localize_script( 'single_results_right_sidebar_script', 'HOME', $home_url );
}

add_action('wp_enqueue_scripts', 'wor_type_scripts_and_styles');

add_filter ( 'genesis_pre_get_option_site_layout', 'full_width_layout_single_wor' );
function full_width_layout_single_wor($opt) // for some reason if i dont put this, i get 2 sidebars
{
    $opt = 'full-width-content';
    return $opt;
}


remove_action ('genesis_loop', 'genesis_do_loop'); // Remove the standard loop
add_action( 'genesis_loop', 'custom_wor_type_grid_loop' ); // Add custom loop


function custom_wor_type_grid_loop() {
    global $wpdb;

    $terms_data = get_queried_object();
    $name = htmlspecialchars_decode($terms_data->name);
    
    $query = $wpdb->prepare('SELECT * FROM wp_v_website_on_religion where type LIKE "%s"','%'.$name.'%');
    $wors = $wpdb->get_results($query);

	$html = '';
	$sort_panel_html = display_custom_panel();

    foreach ($wors as $wor) {
        $html.= display_tile_wor_object($wor);
    }
    echo display_results_panel('Website On Religion', COUNT($wors), $html, 'Type', $name, '', $sort_panel_html);

    // display sidebar
    $html = '<aside class="sidebar sidebar-primary widget-area col-sm-3">';
    $page = get_page_by_title( "custom-sidebar-website_on_religion");
    $html .=apply_filters( 'the_content', $page->post_content );
    $html .='</aside>';


    $html .='</div>';
    $html .='</div>';

    echo $html;

}
genesis();
