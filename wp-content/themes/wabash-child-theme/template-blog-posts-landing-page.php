<?php

/*
Template Name: blog-posts-landing-page
*/
wp_register_style('blog_style', get_stylesheet_directory_uri() . '/css/custom_blog.css', array(), '1', 'all');
wp_enqueue_style('blog_style');
wp_register_script('singleResults', get_stylesheet_directory_uri().'/js/search/singleResults.js', array('jquery'), '1', true);
wp_enqueue_script('singleResults');
error_reporting(E_ALL ^ E_DEPRECATED);

// FORCE FULL WIDTH LAYOUT
add_filter ( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );





// Add our custom loop
add_action('genesis_loop', 'cd_goh_loop');
function cd_goh_loop()
{
  global $paged; // current paginated page
      global $query_args; // grab the current wp_query() args
      $args = array(
          'category__not_in' => array(8), /* shows all posts and child posts from category id */
          'paged'            => $paged, // respect pagination
      );

    $loop = new WP_Query( $args );
    if( $loop->have_posts() ) {
        // loop through posts
        while( $loop->have_posts() ): $loop->the_post();
            $html = '';
            $clean = preg_replace('/\s+/', ' ', wp_strip_all_tags(get_the_content()));
            /*var_dump($data);
            exit;*/
            $extra_clean = preg_replace('/[^:\/A-Za-z0-9\- ]/', '', $clean);
            $extra_clean = preg_replace('/\s+/', ' ', $extra_clean);
            $contentExplode = explode(' ', $extra_clean);
            $content1Array = array_slice($contentExplode, 0, 49);
            $morePoints='';
            $totalWords=count($contentExplode);
            $content2Array = array_slice($contentExplode, 50, $totalWords);
            $moreDisplayClass='hiddeMoreButton';
            if(count($contentExplode)>50)
            {
                $morePoints='...';
                $moreDisplayClass='displayMoreButton';
            }
            $tag = wp_get_post_tags(get_the_ID());
            $tagNames=array();
            foreach($tag as $tagsValues)
            {
                $tagNames[]='<a style="color:rgb(166, 25, 46);" target=_blank href="'.get_tag_link($tagsValues->term_id).'">'.$tagsValues->name.'</a>';
            }
            if(!empty($tagNames))
                $tags=implode(' | ',$tagNames);
            else
                $tags='';
            $content1=implode(' ', $content1Array);
            $content2=implode(' ', $content2Array);
            $date=date("d M Y",strtotime(get_the_date()));
            //GET COVER IMAGE
            $img = get_the_post_thumbnail( get_the_ID(), 'thumbnail' );

            if(empty($img)) {

                $img='<img src="'.get_stylesheet_directory_uri().'/images/blog-icon.png" alt="" height="150" width="150">';
            }

            $category=get_the_category(get_the_ID());
            $author = get_the_author();
            $author_url = get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) );
            /*$year = $data->post_date;
            $content1Array = array_slice($data->post_content, 0, 49);
            $morePoints='';
            $totalWords=count($data->post_content);
            $content2Array = array_slice($data->post_content, 50, $totalWords);
            $moreDisplayClass='hiddeMoreButton';*/
            /*		$html='
                    <article class="post-'.$data->ID.' post status-publish page entry category-'.$data->cat_slug.'">
                            <div class="checkbox-share">
                                <input type="checkbox" value="'.$data->ID.'" id="resource_'.$data->ID.'" class="case">
                            </div>
                            <div class="single_results-title-content">
                                <header class="search-page-header">
                                <h3 itemprop="headline"><a class="link_title_tile" rel="bookmark" href="'.$data->guid.'">'.$data->post_title.'</a></h3>
                                <span><span itemtype="http://schema.org/Person" itemscope="" itemprop="author" class="entry-author"><a rel="author" itemprop="url" class="entry-author-link" href="'.$data->author_posts_url.'"><span itemprop="name" class="entry-author-name">'.$data->author_nickname.'</span></a></span></span>
                                <p>'.$date.' </p>
                                <p>'.$data->comment_count.' comments </p>
                                <p>'.$category[0]->name.'</p>
                            </div>
                            <div class="img_single_results">'.$image.'</div>
                            </header>
                            <div style="clear:both"></div>
                            <div itemprop="text" class="clearfix single-results-info">
                              <p>'.$abstract.'</p>
                                <a title="Read More" href="'.$data->guid.'" class="more-link-single-results">Details <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>
                            <div style="clear:both"></div>
                         <footer class="tags-footer"><strong>Tags:</strong> '.$tags.'</footer>
                        </article>';*/
            if( !empty(get_the_content()) ) {
                $html = '<div class="panel panel-default">
							<div class="panel-body results">
								<div class="single_results-title-content">
									<div class="row">
										<div class="blog-cover-image col-lg-2 col-md-2 col-sm-2 col-xs-2" style="padding:15px;float: left;margin-right: 30px;">
											' . $img . '
										</div>
										<div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
											<h3 style="margin-top:12px;">
												<label for="resource_title">
													<a class="link_title_tile" target="_blank" rel="bookmark" href="'.get_the_guid().'">'.get_the_title().'</a>
												</label>
											</h3>
											<p style="margin-top:-9px;">Blog</p>
											<p class="book_author"><span><span itemtype="http://schema.org/Person" itemscope="" itemprop="author" class="entry-author"><a target=_blank rel="author" itemprop="url" class="entry-author-link" href="'.$author_url.'"><span itemprop="name" class="entry-author-name">'.$author.'</span></a></span></span></p>
											<p>'.$date.' </p>
											<p style="color:rgb(166, 25, 46);"><a target=_blank href ="'.get_category_link($category[0]->term_id).'">'.$category[0]->name.'</a></p>
											<p><strong>Tags:</strong> ' . $tags . '</p>';

                $html.= ' </div>
										<div style="float: right;"  class="col-lg-1 col-md-1 col-sm-1 col-xs-1">

										</div>
									</div>
								</div>
						<div style="clear:both"></div>
						<div class="single-results-info clearfix">
							<p>
							 ' . $content1 . ' <span id="morePoints-' . get_the_ID() . '">' . $morePoints . '</span></p>
							<div id="' . get_the_ID() . '" class="more-link-single-results_blogs1">
								<a title="Read More" target="_blank" href="'.get_the_guid().'">Details</a>
								<i class="fa fa-caret-right" aria-hidden="true"></i>
							</div>
						</div>
					</div>
				</div>';
            }
            echo $html;
        endwhile;
    }
    wp_reset_postdata();
  /* If you want to show posts from a category only and no subcategory posts, use 'category_name' => 'category-slug', instead of 'cat' => 8, for example, 'category_name' => 'articles', */
}
genesis();
