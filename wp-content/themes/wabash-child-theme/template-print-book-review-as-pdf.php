<?php
/*
Template Name: print-book-review-as-pdf
*/

//require_once __DIR__ . '/lib/vendor/autoload.php';


require_once(__DIR__ . '/../../../vendor/autoload.php');





process_single_results_request();

function process_single_results_request(){
    global $post_id,$post_types;
    if(!empty($_GET['id']))
    {
        $post_id = $_GET['id'];
    }
    else
    {
        $post_id = $_POST['id'];
    }

}

$book = get_book_reviews_object($post_id);

$html = '<html>
<head>
  <style>
    p {
        text-align: justify;
    }
    .link-pdf, .icon-pdf{
      display: none;
    }
  </style>
</head>
<body>';

$html .= display_tile_book_reviews_object($book);

$html.= '
</body>
</html>';
$pattern = '#(<a.*?>)Download as PDF(</a>)#';

$html = preg_replace($pattern, '', $html);
$pattern = '#(<a.*?>)More(</a>)#';
$html = preg_replace($pattern, '', $html);

$mpdf = new mPDF();
//$mpdf->showImageErrors = true;
$mpdf->SetHTMLHeader('<img style = "margin-left:25px;" src="'.get_stylesheet_directory_uri().'/images/book_review_header.png">');
$mpdf->SetHTMLFooter('<p style="text-align: center;font-size: 8px">Published by the Wabash Center for Teaching and Learning in Theology and Religion</p>
      <p style="text-align: center;font-size: 8px">301 West Wabash Ave, Crawfordsville, IN 47933<br>
      (765) 361-6047 (800) 655-7177 fax (765) 361-6051</p>
      <p style="text-align: center;font-size: 8px">Fully Funded by Lilly Endowment Inc. and Located at Wabash College</p> ');
$mpdf->AddPage('P', // L - landscape, P - portrait
    '', '', '', '',
    20, // margin_left
    20, // margin right
    50, // margin top
    50, // margin bottom
    5, // margin header
    10);// margin footer
$mpdf->WriteHTML($html);

$mpdf->Output($book->title .'.pdf', 'I');
