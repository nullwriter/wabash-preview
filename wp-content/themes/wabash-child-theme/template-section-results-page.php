<?php
/*
Template Name: section-results-page
*/

// set_time_limit(0);
// ini_set('xdebug.var_display_max_depth', -1);
// ini_set('xdebug.var_display_max_children', -1);
// ini_set('xdebug.var_display_max_data', -1);
// ini_set('memory_limit', '512M');

function single_results_all_scriptsandstyles(){
    //Load JS and CSS files in here

    wp_register_script('singleResults', get_stylesheet_directory_uri().'/js/search/singleResults.js', array('jquery'), '1', true);
    wp_register_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
    wp_register_script('infinitescroll', get_stylesheet_directory_uri().'/js/search/infinite_scroll.js', array('jquery'), '1', true);
    wp_register_script('videos_script', get_stylesheet_directory_uri().'/js/videos.js', array('jquery'), '1', true);
    wp_register_script('clipboard-script', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);
    wp_register_style('awesomecheckbox', get_stylesheet_directory_uri().'/css/awesome-bootstrap-checkbox.css', array(), '1', 'all');
    wp_register_style('customsearchcss', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');

    wp_enqueue_script('singleResults');
    wp_enqueue_script('infinitescroll');
    wp_enqueue_script('videos_script');
    wp_enqueue_script('clipboard-script');
    wp_enqueue_style('awesomecheckbox');
    wp_enqueue_style('customsearchcss');
    wp_enqueue_script('share');

    //Infinite Scroll
    $translation_array = array( 'template_url' => get_stylesheet_directory_uri() );
    wp_localize_script( 'infinitescroll', 'url_object', $translation_array );
}
add_action('wp_enqueue_scripts', 'single_results_all_scriptsandstyles');
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'sk_do_single_search_loop');


$search_string='';
$sort_term='weight';
$post_types=array();
$scholar_sub_types=array();
$wor_sub_types=array();
$grants_sub_types=array();
$syllabi_sub_types=array();
$url_pt='';
$empty_search = false;
process_single_results_request();



function process_single_results_request(){
    global $search_string, $scholar_sub_types, $wor_sub_types, $post_types, $sort_term, $empty_search, $grants_sub_types, $syllabi_sub_types;

    if(!empty($_POST['st'])){
        $search_string = $_POST['st'];
    }
    elseif(!empty($_GET['st'])){
        $search_string = $_GET['st'];
    }
    else{
        $search_string='';
        $empty_search = true;
    }
    // get post types active for search via $_POST
    if(!empty($_POST['pt'])){
        if(is_array($_POST['pt'])){
            foreach ($_POST['pt'] as $cpt) {
                $post_types[$cpt] = true;
            }
        }
        else{
            $post_types[$_POST['pt']] = true;
        }
    }
    // get post types active for search via $_GET
    elseif(!empty($_GET['pt'])){
        if(is_array($_GET['pt'])){
            foreach ($_GET['pt'] as $cpt) {
                $post_types[$cpt] = true;
            }
        }
        else{
            $post_types[$_GET['pt']] = true;
        }

    }
    else{

        $post_types['page'] = 'Programs';
        $post_types['ttr'] = 'Teaching Theology and Religion (Wabash Journal)';
        $post_types['grants'] = 'Awarded Grants';
        $post_types['scholarship'] = 'Scholarchip on Teaching';
        $post_types['post'] = 'Blog';
        $post_types['website_on_religion'] = 'Website on Religion';
        $post_types['book_reviews'] = 'Book Reviews';
        $post_types['syllabi'] = 'Syllabi';
        $post_types['video'] = 'Video';

    }

    //SCHOLARSHIP SUBTYPES
    if ((!empty($_GET['st-web']) && $_GET['st-web'] == 'on' ) || ( !empty($_POST['st-web']) && $_POST['st-web'] == 'on')) {
        $scholar_sub_types['st-web'] = 'Web';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-journal']) && $_GET['st-journal'] == 'on' ) || (!empty($_POST['st-journal']) && $_POST['st-journal'] == 'on')) {
        $scholar_sub_types['st-journal'] = 'Journal Issue';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-book']) && $_GET['st-book'] == 'on')  || (!empty($_POST['st-book']) && $_POST['st-book'] == 'on')) {
        $scholar_sub_types['st-book'] = 'Book';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-article']) && $_GET['st-article'] == 'on' ) || (!empty($_POST['st-article']) &&  $_POST['st-article'] == 'on')) {
        $scholar_sub_types['st-article'] = 'Article';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-ttr']) && $_GET['st-ttr'] == 'on') || (!empty($_POST['st-ttr']) && $_POST['st-ttr'] == 'on')) {
        $scholar_sub_types['st-ttr'] = 'TTR';
        $post_types['scholarship'] = true;
    }
    if ((!empty($_GET['st-tactic']) && $_GET['st-tactic'] == 'on') || (!empty($_POST['st-tactic']) && $_POST['st-tactic'] == 'on')){
        $scholar_sub_types['st-tactic'] = 'Tactic';
        $post_types['scholarship'] = true;
    }
    //WEBSITE SUBTYPES
    if ((!empty($_GET['wt-e-text']) && $_GET['wt-e-text'] == 'on' ) || (!empty($_POST['wt-e-text']) && $_POST['wt-e-text'] == 'on')) {
        $wor_sub_types['wt-e-text'] = 'e-text';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-e-journal']) && $_GET['wt-e-journal'] == 'on') || (!empty($_POST['wt-e-journal']) && $_POST['wt-e-journal'] == 'on')) {
        $wor_sub_types['wt-e-journal'] = 'e-journal';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-image']) && $_GET['wt-image'] == 'on') || (!empty($_POST['wt-image']) && $_POST['wt-image'] == 'on' )) {
        $wor_sub_types['wt-image'] = 'image';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-biblio']) && $_GET['wt-biblio'] == 'on') || (!empty($_POST['wt-biblio']) && $_POST['wt-biblio'] == 'on')) {
        $wor_sub_types['wt-biblio'] = 'biblio';
        $post_types['website_on_religion'] = true;
    }
    if ((!empty($_GET['wt-web']) && !empty($_GET['wt-web'] == 'on')) || (!empty($_POST['wt-web']) && !empty($_POST['wt-web'] == 'on'))) {
        $wor_sub_types['wt-web'] = 'web';
        $post_types['website_on_religion'] = true;
    }
	// GRANTS SUBTYPES
	if ((!empty($_GET['gs-college-uni']) && $_GET['gs-college-uni'] == 'on' ) || (!empty($_POST['gs-college-uni']) && $_POST['gs-college-uni'] == 'on')) {
		$grants_sub_types['gs-college-uni'] = 'Colleges/Universities';
	}
	if ((!empty($_GET['gs-theo-school']) && $_GET['gs-theo-school'] == 'on' ) || (!empty($_POST['gs-theo-school']) && $_POST['gs-theo-school'] == 'on')) {
		$grants_sub_types['gs-theo-school'] = 'Theological Schools';
	}
	if ((!empty($_GET['gs-agencies']) && $_GET['gs-agencies'] == 'on' ) || (!empty($_POST['gs-agencies']) && $_POST['gs-agencies'] == 'on')) {
		$grants_sub_types['gs-agencies'] = 'Agencies';
	}
	// SYLLABI SUBTYPES
	if ((!empty($_GET['sy-undergrad']) && $_GET['sy-undergrad'] == 'on' ) || (!empty($_POST['sy-undergrad']) && $_POST['sy-undergrad'] == 'on')) {
		$syllabi_sub_types['sy-undergrad'] = '%Undergraduate%';
	}
	if ((!empty($_GET['sy-grad']) && $_GET['sy-grad'] == 'on' ) || (!empty($_POST['sy-grad']) && $_POST['sy-grad'] == 'on')) {
		$syllabi_sub_types['sy-grad'] = '%Graduate%';
	}
	if ((!empty($_GET['sy-intro-course']) && $_GET['sy-intro-course'] == 'on' ) || (!empty($_POST['sy-intro-course']) && $_POST['sy-intro-course'] == 'on')) {
		$syllabi_sub_types['sy-intro-course'] = '%Introductory%';
	}
	if ((!empty($_GET['sy-online']) && $_GET['sy-online'] == 'on' ) || (!empty($_POST['sy-online']) && $_POST['sy-online'] == 'on')) {
		$syllabi_sub_types['sy-online'] = '%Online Only%';
	}
	if ((!empty($_GET['sy-online-f2f']) && $_GET['sy-online-f2f'] == 'on' ) || (!empty($_POST['sy-online-f2f']) && $_POST['sy-online-f2f'] == 'on')) {
		$syllabi_sub_types['sy-online-f2f'] = '%Online and F2F%';
	}

    // get sort term
    if (!empty($_POST['sort_term'])) {
        $sort_term = $_POST['sort_term'];
    }
    if (!empty($_GET['sort_term'])) {
        $sort_term = $_GET['sort_term'];
    }

    // var_dump($search_string);
    // var_dump($post_types);
    // var_dump($scholar_sub_types);
    // var_dump($wor_sub_types);
    // var_dump($sort_term);

}


//GENERAL SORT PANEL FOR ALL POST TYPE
function display_sort_panel(){
    global $search_string, $post_types, $sort_term;
    $sort_function='display_'.key($post_types).'_sort_panel';
	$original_url = home_url().'/selected-resources/?post_ids=';

    $id = rand(1,999999); //random id number to call load more data and avoid concurrency problems

    //$browse_url='<a href="../'.$url_pt.'">Browse '.$post_types[key($post_types)].'</a> |  ';

    $sort_panel = '
<div class="col-md-3 col-sm-3 col-xs-12">
	<div id="data_container" class="panel-body-single-results results">
		<div class="single_results_right_sidebar single_results_show_filter"
			 style="position: relative; height: auto;">
			<div class="row">
				'.$sort_function($sort_term).'
				<input class="firstid" type="hidden" data-firstid = "'.$id.'" id="first-'.$id.'" value="5" >
                <input type="hidden" id="limit" value="5" >
			</div>
			<div class="panel panel-default action-panel-search">
				<div class="panel-heading">
					<div>
						<span class="panel-title" style="font-size: 15px !important;">
							<span id="total_selected_items"></span> Selected Items						
						</span>
						<small style="display: inline-block;font-size: 65%;font-weight: initial;color: #777 !important;">
							Select an item by clicking its checkbox
						</small>
					</div>
				</div>
				<div class="panel-body">
					<input type="hidden" id="original_search_url" value="'.$original_url.'">
            		<textarea rows="2" class="share_url_container hide" id="share_url">'.$original_url.'</textarea>
					<div class="share_results">
						<span title="Selected items">
							<span style="font-size: 13px;">Actions for selected results:</span>
						</span>
					</div>
					<div class="selected-buttons">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button title="Go to selected items" id="btn_selected_items" class="disabled selected_button"
										onclick="go_to_mixed_search()">
									View selected items
								</button>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button id="copy_to_clipboard" class="disabled selected_button" title="Copy permalink of selected items to your computer’s clipboard.">									
									Copy link to selected items
								</button>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button title="Clear list" class="clear_share_list selected_button">Clear selected items</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="button-left-panel-wrap">
		<a class="button-left-panel" href="#"></a>
	</div>
</div>
    ';

    return $sort_panel;
}






//SINGLE CUSTOM DATA
//Add our custom loop
function sk_do_single_search_loop(){

  global $post_types,$search_string,$wor_sub_types,$scholar_sub_types,$sort_term,$url_pt,$wpdb,$empty_search,$grants_sub_types,$syllabi_sub_types;

  //var_dump($post_types);

  echo '<div class="container custom_search '.key($post_types).'_search">';
  echo '<div class="row">';
  echo '<!-- this is the paging loader, now is hidden, it will be shown when we scroll to bottom -->
		  <div id="loader"><img src="' . get_stylesheet_directory_uri() . '/images/ajax-loader.gif"></div>';

  $html = '';
  $sort_panel_html = display_sort_panel();
  switch(key($post_types)){
          case 'page':
              $cpt_banner = get_field('programs_banner', 'option');
              if(!empty($cpt_banner)){
                echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
            }
              $url_pt='programs';
              if (!$empty_search) {
                  $programs = search_programs($search_string, 5, 0);
                  $total = search_programs_total($search_string);
              }else{
                  $programs = array();
                  $total = 0;
              }
              foreach($programs as $program){
                  $html.= display_tile_program_object($program);
              }

              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
              echo display_results_panel('Wabash Programs', $total, $html, '', '', '', $sort_panel_html);
              break;
          case 'grants':
              $cpt_banner = get_field('grants_banner', 'option');
              if(!empty($cpt_banner)){
                echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
              }
              $url_pt='wabash-grants';
              if (!$empty_search) {
                  $grants = search_grant($search_string, $grants_sub_types, $sort_term, 5, 0);
              $total = search_grant_total($search_string, $grants_sub_types);
              }else{
                  $grants = array();
                  $total = 0;
              }
              foreach($grants as $grant){
                  $html.= display_tile_grant_object($grant);
              }

			  $st_subtypes_titles='';
			  if (count($grants_sub_types) > 0) {
				  $st_subtypes_titles = ' (' . implode(', ', $grants_sub_types) . ')';
			  }

              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
              echo display_results_panel('Awarded Grants', $total, $html, '', '', $st_subtypes_titles, $sort_panel_html);
              break;
          case 'post':
              $cpt_banner = get_field('blog_banner', 'option');
              if(!empty($cpt_banner)){
                echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
              }
              $url_pt='resources/blog';
              if (!$empty_search) {
                  $blogs = search_blogs($search_string, $sort_term, 5, 0);
                  $total = search_blogs_total($search_string);
              }else{
                  $blogs = array();
                  $total = 0;
              }
              foreach($blogs as $blog){
                  $html.= display_tile_blog_object($blog);
              }
              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
              echo display_results_panel('Wabash Blog', $total, $html, '', '', '', $sort_panel_html);
              break;
          case 'ttr':
              $cpt_banner = get_field('ttr_&_tactics_banner', 'option');
              if(!empty($cpt_banner)){
                    echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
                }
              $url_pt = 'resources/scholarship-on-teaching';

              if (!$empty_search) {
                  $ttrs = search_scholarship($search_string, $scholar_sub_types, 'ttr', $sort_term, 5, 0);
              $total = search_scholarship_total($search_string, $scholar_sub_types, 'ttr');
              }else{
                  $ttrs = array();
                  $total =0;
              }
              foreach($ttrs as $scholar){
                  $html.= display_tile_scholarship_object($scholar);
              }

              $st_subtypes_titles='';
              if (count($scholar_sub_types) > 0) {
                  $st_subtypes_titles = ' (' . implode(', ', $scholar_sub_types) . ')';
              }

              echo display_results_panel('Teaching Theology and Religion (Wabash Journal)', $total, $html, '', '', $st_subtypes_titles, $sort_panel_html);
              break;
          case 'scholarship':
              $cpt_banner = get_field('scholarship_banner', 'option');
              if(!empty($cpt_banner)){
                echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
                }
                 $url_pt = 'resources/scholarship-on-teaching';

              if (!$empty_search) {
                  $scholarships = search_scholarship($search_string, $scholar_sub_types, 'scholarship', $sort_term, 5, 0);
              $total = search_scholarship_total($search_string, $scholar_sub_types, 'scholarship');
              }else{
                  $scholarships = array();
                  $total =0;
              }
              foreach($scholarships as $scholar){
                  $html.= display_tile_scholarship_object($scholar);
              }
              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
            /*Show Sub Types into the Results Title*/
            //ST
            $st_subtypes_titles='';
            if (count($scholar_sub_types) > 0) {
                $st_subtypes_titles = ' (' . implode(', ', $scholar_sub_types) . ')';
            }
            echo display_results_panel('Scholarship On Teaching', $total, $html, '', '', $st_subtypes_titles, $sort_panel_html);
              break;
          case 'website_on_religion':
              $cpt_banner = get_field('website_on_religion_banner', 'option');
              if(!empty($cpt_banner)){
                echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
              }
              $url_pt='resources/websites-on-religion';

              //var_dump($wor_sub_types);
              //var_dump($sort_term);
              if (!$empty_search) {
                  $wors = search_website_on_religion($search_string, $wor_sub_types, $sort_term, 5, 0);
                  $total = search_website_on_religion_total($search_string, $wor_sub_types);
              }else{
                  $wors =  array();
                  $total =0;
              }
              foreach($wors as $wor){
                  $html.= display_tile_wor_object($wor);
              }

              $st_wor_sub_types='';
              if (count($wor_sub_types) > 0) {
                  $st_wor_sub_types = ' (' . implode(', ', $wor_sub_types) . ')';
              }

               echo display_results_panel('Website On Religion', $total, $html, '', '', $st_wor_sub_types, $sort_panel_html);

              break;
          case 'syllabi':
              $cpt_banner = get_field('syllabi_banner', 'option');
              if(!empty($cpt_banner)){
                    echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
              }
              $url_pt='resources/syllabi';
              if (!$empty_search) {
                  $syllabi = search_syllabi($search_string, $syllabi_sub_types, $sort_term, 5, 0);
				  $total = search_syllabi_total($search_string, $syllabi_sub_types);
              }else{
                  $syllabi = array();
                  $total =0;
              }
              foreach($syllabi as $syllabus){
                  $html.= display_tile_syllabi_object($syllabus);
              }
              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
              echo display_results_panel('Syllabi', $total, $html, '', '', '', $sort_panel_html);
              break;
          case 'book_reviews':
            $book_reviewed_filter = '';
            $book_filter = '';
            $book_sub_type = array();
              $cpt_banner = get_field('book_reviews_banner', 'option');
              if(!empty($cpt_banner)){
                    echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
                }
            if (isset($_POST['reviewed'])) {
                $book_reviewed_filter = 1;
                array_push($book_sub_type,"Book Reviewed");
            }
            /*If filter by book only*/
            if (isset($_POST['book'])) {
                $book_filter = 1;
                array_push($book_sub_type,"Book");
            }
            $url_pt = 'resources/book-reviews';
              if (!$empty_search) {
            $book_reviews = search_book_reviews($search_string, $sort_term, 5, 0, $book_reviewed_filter, $book_filter);
                  $total = search_book_reviews_total($search_string, $book_reviewed_filter, $book_filter);
              }else{
                  $book_reviews = array();
                  $total =0;
              }
            foreach ($book_reviews as $book) {
                $html .= display_tile_book_reviews_object($book);
              }
              $st_book_sub_types = '';
              if (count($book_sub_type) > 0) {
                  $st_book_sub_types = ' (' . implode(', ', $book_sub_type) . ')';
              }
              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
              echo display_results_panel('Book Reviews', $total, $html, '', '', $st_book_sub_types, $sort_panel_html);

              break;
          case 'video':
              $cpt_banner = get_field('videos_banner', 'option');
              if(!empty($cpt_banner)){
               echo '<style>
                        .inner_banner {
                            background: rgba(0, 0, 0, 0) url('.$cpt_banner.') no-repeat scroll center center / 100% auto;
                        }
                    </style>';
              }
              $url_pt='resources/videos';
              if (!$empty_search) {
                  $videos = search_videos($search_string, 5, 0);
                  $total = search_videos_total($search_string);
              }else{
                  $videos = array();
                  $total =0;
              }
              foreach($videos as $video){
                  $html.= display_tile_video_object($video);
              }
              //$total = $wpdb->get_var('SELECT FOUND_ROWS()');
              //var_dump($total);
              echo display_results_panel('Videos', $total, $html, '', '', '', $sort_panel_html);
              break;
      }


    // display sidebar
	$html = '';
	if (key($post_types) !== 'book_reviews' && key($post_types) !== 'post') {
		$html = '<aside class="sidebar sidebar-primary widget-area col-md-3 col-sm-12 col-xs-12">';
		if (key($post_types) == 'ttr') {
			$page = get_page_by_title( "custom-sidebar-scholarship");
		}else {
			$page = get_page_by_title("custom-sidebar-" . key($post_types) . "");
		}
		$html .=apply_filters( 'the_content', $page->post_content );
		$html .='</aside>';
	}

    $html .='</div>';
    $html .='</div>';



    echo $html;
}

//CALL TEMPLATE
genesis();
