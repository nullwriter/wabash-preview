<?php
/*
Template Name: selected-resources-page
*/

// set_time_limit(0);
// ini_set('xdebug.var_display_max_depth', -1);
// ini_set('xdebug.var_display_max_children', -1);
// ini_set('xdebug.var_display_max_data', -1);
// ini_set('memory_limit', '512M');

function single_results_all_scriptsandstyles(){
    //Load JS and CSS files in here

    wp_register_script('singleResults', get_stylesheet_directory_uri().'/js/search/singleResults.js', array('jquery'), '1', true);
    //wp_register_script('infinitescroll', get_stylesheet_directory_uri().'/js/search/infinite_scroll.js', array('jquery'), '1', true);
    wp_register_script('videos_script', get_stylesheet_directory_uri().'/js/videos.js', array('jquery'), '1', true);
    wp_register_style('awesomecheckbox', get_stylesheet_directory_uri().'/css/awesome-bootstrap-checkbox.css', array(), '1', 'all');
    wp_register_style('customsearchcss', get_stylesheet_directory_uri().'/css/custom_search.css', array(), '1', 'all');

    wp_enqueue_script('singleResults');
    //wp_enqueue_script('infinitescroll');
    wp_enqueue_script('videos_script');
    wp_enqueue_style('awesomecheckbox');
    wp_enqueue_style('customsearchcss');


    wp_enqueue_script('jquery_cookie_script', get_stylesheet_directory_uri() . '/js/js.cookie.js', array(), '1.0', true);
    wp_enqueue_script('single_results_right_sidebar_script', get_stylesheet_directory_uri() . '/js/single_result_filter_panel.js', array(), '1.0', true);
    wp_enqueue_script('share', get_stylesheet_directory_uri().'/js/search/share.js', array('jquery'), '1', true);
    wp_enqueue_script('clipboard', get_stylesheet_directory_uri().'/js/clipboard.min.js', array('jquery'), '1', true);


}
add_action('wp_enqueue_scripts', 'single_results_all_scriptsandstyles');
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'sk_do_single_search_loop');


$search_string='';
$sort_term='post_title';
$post_types=array();
$scholar_sub_types=array();
$wor_sub_types=array();
$url_pt='';
$post_ids='';

process_single_results_request();



function process_single_results_request(){
    global $search_string, $scholar_sub_types, $wor_sub_types, $post_types, $sort_term, $post_ids;

    if(!empty($_GET['post_ids'])){
        $post_ids=array_map('intval', explode(',',$_GET['post_ids']));
    }

	if (!empty($_GET['sort_term'])) {
		$sort_term = $_GET['sort_term'];
	}

//var_dump($post_ids);
}


//GENERAL SORT PANEL FOR ALL POST TYPE
function display_mixed_sort_panel()
{
    global $search_string, $post_types, $sort_term;

	$original_url = home_url().'/selected-resources/?post_ids=';

    $sort_panel = '
<form class="form_single_results_sort_filter_by form_single_results_sort_by" method="get" action="">
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-bottom: 0 !important;">
            <div class="panel-heading">
                <div>
                    <span class="panel-title">Sort by:</span>
                </div>
            </div>
            <div class="panel-body">
            	<input type="hidden" name="post_ids" value="'.$_GET['post_ids'].'">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="post_title" '.(($sort_term=='post_title')? 'checked="checked"' : '').'
                                   id="relevance_radio">
                            <label for="relevance_radio">Title</label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <input type="radio" name="sort_term" value="post_type" id="format_radio" '.(($sort_term=='post_type')? 'checked="checked"' : '').'>
                            <label for="format_radio">Type</label>
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</form>
    ';

    $sort_container = '<div class="col-md-3 col-sm-3 col-xs-12">
	<div id="data_container" class="panel-body-single-results results">
		<div class="single_results_right_sidebar single_results_show_filter"
			 style="position: relative; height: auto;">
			<div class="row">
				'.$sort_panel.'
			</div>
			<div class="panel panel-default action-panel-search">
				<div class="panel-heading">
					<div>
						<span class="panel-title" style="font-size: 15px !important;">
							<span id="total_selected_items"></span> Selected Items						
						</span>
						<small style="display: inline-block;font-size: 65%;font-weight: initial;color: #777 !important;">
							Select an item by clicking its checkbox
						</small>
					</div>
				</div>
				<div class="panel-body">
					<input type="hidden" id="original_search_url" value="'.$original_url.'">
            		<textarea rows="2" class="share_url_container hide" id="share_url">'.$original_url.'</textarea>
					<div class="share_results">
						<span title="Selected items">
							<span style="font-size: 13px;">Actions for selected results:</span>
						</span>
					</div>
					<div class="selected-buttons">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button title="Go to selected items" id="btn_selected_items" class="disabled selected_button"
										onclick="go_to_mixed_search()">
									View selected items
								</button>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button id="copy_to_clipboard" class="disabled selected_button" title="Copy permalink of selected items to your computer’s clipboard.">									
									Copy link to selected items
								</button>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button title="Clear list" class="clear_share_list selected_button">Clear selected items</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="button-left-panel-wrap">
		<a class="button-left-panel" href="#"></a>
	</div>
</div>';

    return $sort_container;
}



function deleteElement($element, &$array){
    $index = array_search($element, $array);
    if($index !== false){
        unset($array[$index]);
    }
}


//SINGLE CUSTOM DATA
//Add our custom loop
function sk_do_single_search_loop(){

  global $post_types,$search_string,$wor_sub_types,$scholar_sub_types,$sort_term,$url_pt,$wpdb,$post_ids;

  echo '<div class="container mixed-results">';
  echo '<div class="row">';
  echo '<!-- this is the paging loader, now is hidden, it will be shown when we scroll to bottom -->
		  <div id="loader"><img src="' . get_stylesheet_directory_uri() . '/images/ajax-loader.gif"></div>';

  $sort_panel_html = display_mixed_sort_panel();
  $html = '';

  $post_ids_sql = implode("','", $post_ids);
  $query = "SELECT id, post_type, post_title FROM wp_posts WHERE id IN ('".$post_ids_sql."') ORDER BY wp_posts.".$sort_term." ASC";

  $results = $wpdb->get_results($query);

  foreach($results as $post){

      switch($post->post_type){
              case 'page':

                  $url_pt='programs';
                  $item = get_program_object($post->id);
                  if(empty($item)){
                    deleteElement($post,$results);
                   }
                   else{
                  $html .= display_tile_program_object($item);
                   }

                  break;
              case 'grants':

                  $url_pt='wabash-grants';

                  $item = get_grant_object($post->id);

                 if(empty($item)){
                    deleteElement($post,$results);

                  }
                  else{
                    $html .= display_tile_grant_object($item);
                  }
                    //exit();
                  break;
              case 'post':

                  $url_pt='resources/blog';
                  $item = get_post_object($post->id);

                  if(empty($item)){
                     deleteElement($post,$results);

                   }
                   else{
                  $html .= display_tile_blog_object($item);
                   }

                  break;
              case 'scholarship':

                  $url_pt = 'resources/scholarship-on-teaching';
                  $item = get_scholarship_object($post->id);

                  if(empty($item)){
                    deleteElement($post,$results);

                   }
                   else{
                  $html .= display_tile_scholarship_object($item);
                   }

                  break;
              case 'website_on_religion':

                  $url_pt='resources/websites-on-religion';
                  $item = get_website_on_religion_object($post->id);

                  if(empty($item)){
                    deleteElement($post,$results);

                  }
                  else{
                    $html .= display_tile_wor_object($item);
                  }

                  break;
              case 'syllabi':

                  $url_pt='resources/syllabi';
                  $item = get_syllabi_object($post->id);

                  if(empty($item)){
                    deleteElement($post,$results);

                  }
                  else{
                  $html .= display_tile_syllabi_object($item);
                  }

                  break;
              case 'book_reviews':

                  $url_pt='resources/book-reviews';
                  $item = get_book_reviews_object($post->id);

                  if(empty($item)){
                    deleteElement($post,$results);

                  }
                  else{
                  $html .= display_tile_book_reviews_object($item);
                  }

                  break;
              case 'video':

                  $url_pt='resources/videos';
                  $item = get_video_object($post->id);

                  if(empty($item)){
                    deleteElement($post,$results);

                  }
                  else{
                  $html .= display_tile_video_object($item);
                  }

                  break;

          }

  }


    echo display_results_panel('Selected Items', COUNT($results), $html, 'selected_resources', '', '', $sort_panel_html);
    // display sidebar
    $html = '<aside class="sidebar sidebar-primary widget-area col-md-3 col-sm-12 col-xs-12">';
    $page = get_page_by_title( "custom-sidebar-mixed-results");
    $html .=apply_filters( 'the_content', $page->post_content );
    $html .='</aside>';


    $html .='</div>';
    $html .='</div>';



    echo $html;
}

//CALL TEMPLATE
genesis();
